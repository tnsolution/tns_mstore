﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(TNStores.Startup))]
namespace TNStores
{

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}