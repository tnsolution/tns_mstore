﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TNStores
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "dangnhap",
                url: "dang-nhap",
                defaults: new { controller = "Auth", action = "SignIn", id = UrlParameter.Optional }
            );

            #region [ADMIN]
            routes.MapRoute(
              name: "menu",
              url: "menu/{Module}",
              defaults: new { controller = "Admin", action = "ListRole", Module = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "khachhang",
               url: "a-khach-hang",
               defaults: new { controller = "Admin", action = "ListPartner", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "nguoidung",
               url: "nguoi-dung",
               defaults: new { controller = "Admin", action = "ListUser", id = UrlParameter.Optional }
            );
            #endregion

            #region [Desktop]

            #region config
            routes.MapRoute(
             name: "dloaiphithu",
             url: "d-loai-phi-thu",
             defaults: new { controller = "Config", action = "ListCategoryReciept", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "dloaiphichi",
             url: "d-loai-phi-chi",
             defaults: new { controller = "Config", action = "ListCategoryPayment", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "dChucVu",
                url: "chuc-vu",
                defaults: new { controller = "Config", action = "ListPosition", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "dPhongBan",
                url: "phong-ban",
                defaults: new { controller = "Config", action = "ListDepartment", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "dLoaiSpCap1",
                url: "loai-san-pham-cap-1",
                defaults: new { controller = "Config", action = "CategoryLevel1", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "dLoaiSpCap2",
                url: "loai-san-pham-cap-2",
                defaults: new { controller = "Config", action = "CategoryLevel2", id = UrlParameter.Optional }
            );
            routes.MapRoute(
            name: "dSanPham",
            url: "san-pham",
            defaults: new { controller = "Config", action = "ListProduct", id = UrlParameter.Optional }
         );
            routes.MapRoute(
           name: "dDonVi",
           url: "don-vi",
           defaults: new { controller = "Config", action = "ListProductUnit", id = UrlParameter.Optional }
        );
            #endregion

            #region nhan vien
            routes.MapRoute(
                name: "dNhanVien",
                url: "nhanh-vien",
                defaults: new { controller = "Desktop", action = "ListEmployee", id = UrlParameter.Optional }
            );
            #endregion

            #region hóa đơn
            routes.MapRoute(
                name: "hoadonvao",
                url: "hoa-don-vao",
                defaults: new { controller = "Financial", action = "ListInvoiceIN", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "hoadonra",
                url: "hoa-don-ra",
                defaults: new { controller = "Financial", action = "ListInvoiceOUT", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "hoadoncandoi",
                url: "can-doi-hoa-don",
                defaults: new { controller = "Financial", action = "InvoiceBalanceQuarter", id = UrlParameter.Optional }
            );

            #endregion

            #region thu chi
            routes.MapRoute(
                name: "dCanDoiTienMat",
                url: "can-doi-tien-mat",
                defaults: new { controller = "Financial", action = "CashBalance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "dPhieuThu",
                url: "phieu-thu",
                defaults: new { controller = "Financial", action = "ListReceipt", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "dPhieuChi",
                url: "phieu-chi",
                defaults: new { controller = "Financial", action = "ListPayment", id = UrlParameter.Optional }
           );
            #endregion



            routes.MapRoute(
                name: "dKhachhang",
                url: "d-khach-hang",
                defaults: new { controller = "Desktop", action = "ListCustomer", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "dPOlist",
                url: "d-danh-sach-nhap-hang",
                defaults: new { controller = "Desktop", action = "List_PurchaseOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                 name: "dPO",
                 url: "d-nhap-hang",
                 defaults: new { controller = "Desktop", action = "PurchaseOrderNew", id = UrlParameter.Optional }
            );

            #region [SALE]
            routes.MapRoute(
               name: "dSO",
               url: "d-SO",
               defaults: new { controller = "Desktop", action = "SaleOrderNew", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "dSOlist",
               url: "d-danh-sach-don-hang",
               defaults: new { controller = "Desktop", action = "List_SaleOrder", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //   name: "dSOReport",
            //   url: "d-bang-SO",
            //   defaults: new { controller = "Desktop", action = "Board_SaleOrder", id = UrlParameter.Optional }
            //);
            #endregion

            routes.MapRoute(
               name: "dTrangChu",
               url: "trang-chu",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            #endregion

            //-------------------------------------------------------------------------------------------------------------------------------------------

            #region [Di động]
            
            routes.MapRoute(
                name: "mChonSanPham",
                url: "m-chon-san-pham",
                defaults: new { controller = "Mobile", action = "Product", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "mBoard",
                url: "m-thong-tin",
                defaults: new { controller = "Mobile", action = "mDashboard", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "mTrangChu",
                url: "m-trang-chu",
                defaults: new { controller = "Mobile", action = "Index", id = UrlParameter.Optional }
            );

            #region [Cài đặt]
            routes.MapRoute(
               name: "mCaiDat",
               url: "m-cai-dat",
               defaults: new { controller = "Mobile", action = "Setting", id = UrlParameter.Optional }
           );
            #endregion


            #region [--Hóa đơn--]           

            routes.MapRoute(
                name: "mHoaDonVao",
                url: "m-hoa-don-vao",
                defaults: new { controller = "Mobile", action = "SearchInvoiceIN_Button", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "mHoaDonRa",
                url: "m-hoa-don-ra",
                defaults: new { controller = "Mobile", action = "SearchInvoiceOUT_Button", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "mHoaDonCB",
                url: "m-can-doi-hoa-don",
                defaults: new { controller = "Mobile", action = "InvoiceBalanceQuarter", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "mHoaDonCBNam",
                url: "m-can-doi-hoa-don-nam",
                defaults: new { controller = "Mobile", action = "InvoiceBalanceYear", id = UrlParameter.Optional }
           );
            #endregion

            #region [--Bán hàng--]           
            routes.MapRoute(
              name: "mBaoCaoTuan",
              url: "Bao-cao-tuan",
              defaults: new { controller = "Mobile", action = "rptWeek", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "mSanpham",
              url: "danh-sach-san-pham",
              defaults: new { controller = "Mobile", action = "Product", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "mGiohang",
              url: "gio-hang",
              defaults: new { controller = "Mobile", action = "Cart", id = UrlParameter.Optional }
            );
            #endregion

            #region [--Thu chi--]            

            routes.MapRoute(
            name: "mPhieuChi",
            url: "m-phieu-chi",
            defaults: new { controller = "Mobile", action = "SearchPayment_Button", id = UrlParameter.Optional }
           );

            routes.MapRoute(
           name: "mPhieuThu",
           url: "m-phieu-thu",
           defaults: new { controller = "Mobile", action = "SearchReceipt_Button", id = UrlParameter.Optional }
          );

            routes.MapRoute(
          name: "mCanDoiThuChi",
          url: "m-can-doi-thu-chi",
          defaults: new { controller = "Mobile", action = "Balance", id = UrlParameter.Optional }
         );
            #endregion
            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
