﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TNStores
{
    [HubName("DocumentHub")]
    public class DocumentHub : Hub
    {
        public static void BroadcastData()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<DocumentHub>();
            context.Clients.All.RefeshDocument();

            //name for javascript
            //DocumentHub
            //RefeshDocument 
        }
    }
}