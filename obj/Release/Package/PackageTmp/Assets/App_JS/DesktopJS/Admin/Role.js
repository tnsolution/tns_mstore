﻿function Edit(id) {
    $.ajax({
        url: URL_Edit,
        type: 'GET',
        data: {
            "RoleKey": id,
            "Module": $("#cbo_SearchModule").val()
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#modalEdit").empty();
        },
        success: function (r) {
            $("#modalEdit").html(r);
            Utils.OpenMagnific('#modalEdit');
        },
        error: function (err) {

        },
        complete: function () {
            $("#modalEdit .select2").select2({
                width: '100%',
                placeholder: '--Chọn--',
                tags: true,
                createTag: function (params) {
                    return {
                        id: params.term,
                        text: params.term,
                        newOption: true
                    }
                }
            });
            Utils.LoadOut();
        }
    });
}
function Delete(id) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: 'POST',
                        data: {
                            "RoleKey": id
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("table tr[ rolekey='" + id + "']").remove();
                            }
                            else {
                                Utils.OpenNotify('Lỗi !.', r.Message, 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify('Lỗi !.', err.responseText, 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

$(document).ready(function () {
    $("#cbo_SearchModule").change(function () {
        $("#form_Search").submit();
    });
});