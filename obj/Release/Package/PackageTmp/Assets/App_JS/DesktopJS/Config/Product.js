﻿$(document).ready(function () {
    $("#txt_ProductID").focus(function () { $(this).select(); });
    $("#txt_ProductID").blur(function () {
        if ($(this).val().length <= 0) {
            Utils.OpenNotify("Mã sản phẩm không được phép trống !.", "Thông báo", "warning");
        }
        CheckID();
    });

});

function Save() {
    var UnitKey = $("#cbo_Unit").val();
    if (UnitKey.length === 0) {
        Utils.OpenNotify("Bạn phải chọn đơn vị tính", "Thông báo", "warning");
        return false;
    }

    var obj = {
        ProductKey: $("#txt_ProductKey").val(),
        ProductID: $("#txt_ProductID").val(),
        ProductName: $("#txt_ProductName").val(),
        StandardCost: $("#txt_StandardCost").val(),
        SalePrice: $("#txt_SalePrice").val(),
        SafetyInStock: $("#txt_SafetyInStock").val(),
        CategoryKey: $("#cbo_Category").val(),
        CategoryName: $("#cbo_Category option:selected").text(),
        StandardUnitKey: $("#cbo_Unit").val(),
        StandardUnitName: $("#cbo_Unit option:selected").text(),
        QuantityFact: $("#txt_QuantityFact").val(),
    };

    var fileUpload = $("#FileAttach").get(0);
    var files = fileUpload.files;

    // Create  a FormData object
    var fileData = new FormData();

    // if there are multiple files , loop through each files
    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }

    fileData.append("ProductObj", JSON.stringify(obj));
    fileData.append("ProductKey", $("#txt_ProductKey").val());

    $.ajax({
        url: URL_Save,
        processData: false,
        contentType: false,
        type: "POST",
        data: fileData,
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}

function Detail(ProductKey) {
    Utils.OpenMagnific("#modalEdit");
    if (ProductKey !== "") {
        $.ajax({
            url: URL_Detail,
            type: "GET",
            data: {
                "ProductKey": ProductKey
            },
            beforeSend: function () {
                Utils.LoadIn();
            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $("#txt_ProductKey").val(obj.ProductKey);
                    $("#txt_ProductID").val(obj.ProductID);
                    $("#txt_ProductName").val(obj.ProductName);
                    $("#txt_StandardCost").val(obj.StandardCost);
                    $("#txt_SalePrice").val(obj.SalePrice);
                    $("#txt_SafetyInStock").val(obj.SafetyInStock);
                    $("#txt_QuantityFact").val(obj.QuantityFact);
                    $("#cbo_Category").val(obj.CategoryKey).trigger("change");
                    $("#cbo_Unit").val(obj.StandardUnitKey).trigger("change");

                    if (obj.ProductID.length > 0) {                        
                        ShowCode(obj.ProductID);
                    }
                    else {
                        $("#sectioncode").hide();
                        $("#printSection").empty();
                    }
                }
                else {
                    Utils.OpenNotify(r.Message, "Thông báo", "error");
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {
                Utils.LoadOut();
            }
        });

        $("#btn_QrCode").show();
    }
    else {
        Utils.ClearUI("#modalEdit");
        $("#FileAttach").val();
        $("#txt_CategoryKey").val(0);
        $("#txt_StandardCost").val(0);
        $("#txt_SalePrice").val(0);
        $("#txt_SafetyInStock").val(0);
        $("#txt_QuantityFact").val(0);
        $("#printSection").empty();
        $("#sectioncode").hide();
        $("#txt_ProductID").val(AutoID());
        $("#cbo_Unit").select2("val", "");
        $("#btn_QrCode").hide();
    }
}

function Delete(ProductKey) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Cảnh báo !.",
        content: "Bạn có chắc xóa thông tin này ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: "POST",
                        data: {
                            "ProductKey": ProductKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("tr[key=\"" + ProductKey + "\"]").remove();
                            }
                            else {
                                Utils.OpenNotify(r.Message, "Thông báo", "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}

function CheckID() {
    var ProductID = $("#txt_ProductID").val();
    var ProductKey = $("#txt_ProductKey").val();
    
    if (ProductKey.length <= 0 &&
        ProductID.length > 0) {
        $.ajax({
            url: URL_CheckID,
            type: "GET",
            data: {
                "ProductID": ProductID
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    Utils.OpenNotify("Mã sản phẩm này đã có rồi !.", "Thông báo", "warning");
                    $("#txt_ProductID").focus();
                    $("#printSection").empty();
                    $("#sectioncode").hide();
                }
                else {
                    ShowCode(ProductID);
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {

            }
        });
    }
}

function PrintCode() {
    var ID = $("#txt_ProductID").val();
    if (ID.length > 0) {
        Utils.PrintOnDesktop("sectioncode");
    }
    else {
        $("#btn_QrCode").hide();
        Utils.OpenNotify("Bạn phải nhập ID Code", "Thông báo", "error");
    }
}

function AutoID() {
    $.ajax({
        url: URL_AutoID,
        type: "GET",
        data: {
            
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#txt_ProductID").val(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}

function ShowCode(id) {
    $("#sectioncode").show();
    $("#printSection").empty(); 
    var qrcode = new QRCode(document.getElementById("printSection"), {
        text: id,
        width: 128,
        height: 128,
        colorDark: "#000000",
        colorLight: "#ffffff",
        correctLevel: QRCode.CorrectLevel.H
    });
    $("#printSection").find("img").css({ "margin": "auto" });
}