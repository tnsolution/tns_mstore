﻿$(document).ready(function () {

});
function Save(Amount) {
    var DateView = $("#txt_DateView").val();
    $.ajax({
        url: URL_Save,
        type: "POST",
        data: {
            "Amount": Amount,
            "DateView": DateView,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify('Đã lưu chuyển số tiền qua tháng sau !.', 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}