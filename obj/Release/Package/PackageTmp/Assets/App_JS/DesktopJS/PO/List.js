﻿function CancelOrder(Key) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Thông báo !.",
        content: "Bạn có chắc xóa ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_CancelOrder,
                        type: "POST",
                        data: {
                            "OrderKey": Key
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("tr[key=\"" + Key + "\"]").remove();
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}