﻿var width = $(window).width();
var height = $(window).height() - 350;
var method = "Pay";

const userAgent = navigator.userAgent.toLowerCase();
const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);

$(document).ready(function () {
    $(".content-body").css({ "padding": "10px" });

    $("#LeftPanel").css("height", height);
    $("#RightPanel").css("height", height + 130);

    $(".toggle-sidebar-left").click(function () {
        toggleSidebar("left");

        return false;
    });
    $(".toggle-sidebar-right").click(function () {
        toggleSidebar("right");

        return false;
    });

    $("#txt_VendorPhone").keypress(function (e) {
        if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
    });
    $("#txt_VendorPhone").on("blur", function () {
        GetCustomerByPhone();
    });

    $("#divProduct").on("click", "tr", function () {
        var ProductKey = $(this).attr("productkey");
        AddToPurchase(ProductKey);
    });

    $("#divCart").on("click", "tr:not([name=\"edit\"])", function () {
        $(this).next().slideToggle();
    });
    $("#divCart").on("click", "button[name=\"btn_Plus\"]", function () {
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        var $quantity = $data.find("input[name=\"txt_Quantity\"]");

        var quantity = parseFloat($quantity.val()) + 1;
        $quantity.val(quantity);
        CalPrice($data);
    });
    $("#divCart").on("click", "button[name=\"btn_Minus\"]", function () {
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        var $quantity = $data.find("input[name=\"txt_Quantity\"]");

        var quantity = parseFloat($quantity.val()) - 1;
        $quantity.val(quantity);
        CalPrice($data);
    });
    $("#divCart").on("keyup", "input[name=\"txt_PurchasePrice\"]", function () {
        var val = $(this).val();
        if (val <= 0) {
            return false;
        }
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        CalPrice($data);
    });
    $("#divCart").on("keyup", "input[name=\"txt_Quantity\"]", function () {
        var val = $(this).val();
        if (val <= 0) {
            return false;
        }
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        CalPrice($data);
    });

    if (isTablet || width <= 1366) {
        $("html").addClass("sidebar-left-collapsed");
    }

    $("#btn_MostBuy").click(function () { MostBuy(); });
    $("#btn_NewImport").click(function () { NewImport(); });
    $("#btn_Search").click(function () { SearchName(); });

    $(".nano").nanoScroller();
});

//an hien chi tiet
function toggleSidebar(side) {
    if (side !== "left" &&
        side !== "right") {
        return false;
    }
    var left = $("#sidebar-left"),
        right = $("#sidebar-right"),
        content = $("#content"),
        openSidebarsCount = 0,
        contentClass = "";

    // toggle sidebar
    if (side === "left") {
        left.toggleClass("collapsed");
    } else if (side === "right") {
        right.toggleClass("collapsed");
    }

    // determine number of open sidebars
    if (!left.hasClass("collapsed")) {
        openSidebarsCount += 1;
    }

    if (!right.hasClass("collapsed")) {
        openSidebarsCount += 1;
    }

    // determine appropriate content class
    if (openSidebarsCount === 0) {
        contentClass = "col-md-12";
    } else if (openSidebarsCount === 1) {
        contentClass = "col-md-9";
    } else {
        contentClass = "col-md-6";
    }

    // apply class to content
    content.removeClass("col-md-12 col-md-9 col-md-6")
        .addClass(contentClass);
}

function CalOrder() {
    var AmountOrder = $("#txt_AmountOrder").val();
    var PercentDiscount = $("#txt_PercentDiscount").val();
    var AmountDiscount = parseFloat(PercentDiscount) * parseFloat(AmountOrder) / 100;
    var MustPay = parseFloat(AmountOrder) - AmountDiscount;
    $("#txt_MustPay").val(MustPay);
    $("#txt_AmountDiscount").val(AmountDiscount);
}
function CalPrice($data) {
    var $quantity = $data.find("input[name=\"txt_Quantity\"]");
    var $purchaseprice = $data.find("input[name=\"txt_PurchasePrice\"]");
    var $amountprice = $data.find("input[name=\"txt_AmountPrice\"]");
    
    var p = parseFloat($purchaseprice.val());
    var q = parseFloat($quantity.val());
    var newprice = p * q;
    $amountprice.val(newprice);
}

function InitNumber() {
    $("span.number").number(true, 0, ",", ".");
    $("del.number").number(true, 0, ",", ".");
    $("input.number").number(true, 0, ",", ".");
}
function InitPreviewModal() {
    var FullName = $("#txt_VendorName").val();
    var Phone = $("#txt_VendorPhone").val();
    var Address = $("#txt_VendorAddress").val();

    $("#txt_Name").val(FullName);
    $("#txt_Phone").val(Phone);
    $("#txt_Address").val(Address);

    $("input[name=\"radioPayment\"]").click(function () {

        var rdo = $(this).val();

        if (rdo === "Pay") {
            method = "Pay";
            $("[name=\"Pay\"]").show();
        }
        else {
            method = "No";
            $("[name=\"Pay\"]").hide();
        }
    });    
}

//action on left
function Apply(key) {
    var $data = $("tr[for=" + key + "]");
    var $unit = $data.find("select[name=\"Unit\"] option:selected");
    var $quantity = $data.find("input[name=\"txt_Quantity\"]");
    var $purchaseprice = $data.find("input[name=\"txt_PurchasePrice\"]");
    var $saleprice = $data.find("input[name=\"txt_SalePrice\"]");
    var $amountprice = $data.find("input[name=\"txt_AmountPrice\"]");
    var $description = $data.find("textarea[name=\"txt_Description\"]");
    var Item = {
        ProductKey: key,
        PurchasePrice: $purchaseprice.val(),
        SalePrice: $saleprice.val(),
        AmountProductIncVAT: $amountprice.val(),
        QuantityFact: $quantity.val(),
        UnitKey: $unit.val(),
        UnitName: $unit.text(),
        Description: $description.val()
    };

    $.ajax({
        url: URL_EditCart,
        type: "POST",
        data: {
            "Item": JSON.stringify(Item)
        },
        beforeSend: function () {
            $("#divCart").empty();
        },
        success: function (r) {
            $("#divCart").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            InitNumber();
            SumCart();
        }
    });
}
function Remove(Key) {
    $.ajax({
        url: URL_Remove,
        type: "POST",
        data: {
            "ObjectKey": Key
        },
        beforeSend: function () {
            $("#divCart").empty();
        },
        success: function (r) {
            $("#divCart").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            InitNumber();
            SumCart();
        }
    });
}
function Close(key) {
    var $data = $("tr[for=" + key + "]");
    $data.hide();
}

//get khach hang
function GetCustomerByPhone() {
    var Phone = $.trim($("#txt_VendorPhone").val());
    if (Phone.length <= 9) {
        Utils.OpenNotify("Số điện thoại không đúng", "Thông báo", "warning");
        return;
    }

    $.ajax({
        url: URL_GetCustomerByPhone,
        type: "GET",
        data: {
            "Phone": Phone
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                var obj = JSON.parse(r.Data);
                if (obj.CustomerKey.length >= 36) {
                    $("#txt_VendorName").val(obj.FullName);
                    $("#txt_VendorKey").val(obj.CustomerKey);
                    $("#txt_VendorAddress").val(obj.Address);
                }
                else {
                    Utils.OpenNotify("Thông tin khách hàng sẽ được cập nhật !.", "Thông báo", "info");
                }
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}
//xem nhanh don hang
function Preview() {
    $.ajax({
        url: URL_CheckCart,
        type: "GET",
        data: {},
        beforeSend: function () {
            Utils.LoadIn();
            $("#modalPayment").empty();
        },
        success: function (r) {
            if (r.Success) {
                var url = r.Data;
                $.get(url, function (partial) {
                    $("#modalPayment").html(partial);
                }).done(function () {
                    Utils.LoadOut();
                    InitNumber();
                    InitPreviewModal();
                    Utils.OpenMagnific("#modalPayment");
                });               
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.LoadOut();
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}

//action on right
function AddToPurchase(ProductKey) {
    $.ajax({
        url: URL_AddToPurchase,
        type: "POST",
        data: {
            "ProductKey": ProductKey,
        },
        beforeSend: function () {
            $("#divCart").empty();
        },
        success: function (r) {
            $("#divCart").html(r);
        },
        error: function (err) {
            console.log(err.responseText);
        },
        complete: function () {
            InitNumber();
            SumCart();
        }
    });
}
//
function SumCart() {
    $.ajax({
        url: URL_Sum,
        type: "GET",
        data: {},
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                var obj = JSON.parse(r.Data);
                $("#txtMustPay").text(obj.AmountOrder);
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            InitNumber();
        }
    });
}
//
function NewOrder() {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Thông báo !.",
        content: "Đơn hàng  này chưa lưu, bạn có chắc lập đơn mới ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_NewOrder,
                        type: "GET",
                        data: {},
                        beforeSend: function () {
                            Utils.LoadIn();
                        },
                        success: function (r) {
                            location.reload(true);
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}
//
function Save() {
    var Info = {
        VendorKey: $("#txt_VendorKey").val(),
        VendorPhone: $("#txt_Phone").val(),
        VendorName: $("#txt_Name").val(),
        VendorAddress: $("#txt_Address").val(),
        AmountOrder: $("#txt_AmountOrder").val(),
        AmountOrderIncVAT: $("#txt_AmountOrder").val(),        
        OrderKey: $("#txt_OrderKey").val(),
        DocumentOrigin: $("#txt_DocumentOrigin").val()
    };
    $.ajax({
        url: URL_Save,
        type: 'POST',
        data: {
            "Info": JSON.stringify(Info),
            "Method": method
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                window.location = URL_PurchaseList;
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
            Utils.LoadOut();
        },
        complete: function () {
            //Utils.LoadOut();
            //Utils.OpenMagnific("#modalPrint");
        }
    });
}
//
function printDiv() {
    Utils.PrintOnDesktop("printSection");
}
//tim theo bên phải
function LoadProduct(Category) {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "CategoryKey": Category,
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}
//Bán chạy
function MostBuy() {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "Option": "TOP"
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}
//Mới nhập
function NewImport() {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "Option": "ORDER"
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}
//tim theo tên
function SearchName() {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "Name": $("#txt_ProductName").val()
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}