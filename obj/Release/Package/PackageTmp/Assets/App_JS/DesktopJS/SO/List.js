﻿$(document).ready(function () {

});
function ViewInvoice(Key) {

}
function CancelOrder(Key) {
    if (Key.length === 0) {
        Key = $("#txt_OrderKey").val();
    }
    if (Key.length <= 0) {
        Utils.OpenNotify("Lỗi mã đơn hàng xin liên hệ IT !.", "Thông báo", "error");
        return;
    }

    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Thông báo !.",
        content: "Bạn có chắc xóa, các thông tin thu tiền, hóa đơn sẽ thay đổi theo ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_CancelOrder,
                        type: "POST",
                        data: {
                            "OrderKey": Key
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("tr[key=\"" + Key + "\"]").remove();
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}
function PrintOrder(Key) {
    $.ajax({
        url: URL_PrintOrder,
        type: "GET",
        data: {
            "OrderKey": Key
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#printdata").empty();
        },
        success: function (r) {
            $("#printdata").html(r);
            $("#txt_OrderKey").val(Key);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.OpenMagnific("#modalPrint");
            Utils.LoadOut();
        }
    });
}
function printDiv() {
    Utils.PrintOnDesktop("printSection");
}

function OpenReceipt(Key) {
    $.ajax({
        url: URL_Receipt,
        type: "GET",
        data: {
            "OrderKey": Key
        },
        beforeSend: function () {
            $("#modalReceipt").empty();
            Utils.LoadIn();
        },
        success: function (r) {
            $("#modalReceipt").html(r);
            Utils.OpenMagnific("#modalReceipt");
            Utils.LoadOut();
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            $("span.number").number(true, 0, ",", ".");
            $("del.number").number(true, 0, ",", ".");
            $("input.number").number(true, 0, ",", ".");

            $("#txt_BuyerPay").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: URL_AutoComplete,
                        data: { term: request.term },
                        dataType: "json",
                        success: function (data) {
                            var obj = JSON.parse(data);
                            response(obj);
                        },
                        error: function (err) {
                            alert(err.responseText);
                        }
                    });
                },
                select: function (event, ui) {
                    event.preventDefault();
                    $("#txt_BuyerPay").val(ui.item.Value);
                    console.log("Selected: " + ui.item.Value + " aka " + ui.item.Text);
                    CalPay();
                }
            })
                .autocomplete("instance")
                ._renderItem = function (ul, item) {
                    return $("<li>")
                        .append("<div class=\"text-right\">" + item.Text + "</div>")
                        .appendTo(ul);
                };
        }
    });
}
function SaveReceipt() {
    $.ajax({
        url: URL_Collect,
        type: "POST",
        data: {
            "Amount": $("#txt_BuyerPay").val(),
            "OrderID": $("#txt_OrderID").val(),
            "Name": $("#txt_Name").val(),
            "Phone": $("#txt_Phone").val(),
            "Description": $("#txt_Description").val(),
            "CustomerKey": $("#txt_CustomerKey").val(),
            "OrderKey": $("#txt_OrderKey").val(),
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                location.reload(true);
            }
            else {
                Utils.LoadOut();
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.LoadOut();
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}

function CalPay() {
    var MustPay = parseFloat($("#txt_MustPay").val());
    var BuyerPay = parseFloat($("#txt_BuyerPay").val());
    if (BuyerPay > 0) {
        $("#txt_BuyerPay").val(BuyerPay);
    }
    else {
        $("#txt_BuyerPay").val(MustPay);
    }

    var Return = MustPay - BuyerPay;
    if (Return < 0) {
        $("#txt_Return").val(Return);
    } else {
        $("#txt_Return").val(0);
    }
}