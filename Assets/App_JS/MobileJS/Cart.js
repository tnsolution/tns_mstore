﻿var method = "Pay";
$(function () {
    $('#txt_BuyerPhone').keypress(function (e) {
        if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
    });

    $('tr:not([name="edit"])').click(function () {
        $(this).next().slideToggle();
    });

    $("button[name=\"btn_Plus\"]").click(function () {
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        var $quantity = $data.find("input[name=\"txt_Quantity\"]");

        var quantity = parseFloat($quantity.val()) + 1;
        $quantity.val(quantity);
        CalPrice($data);
    });
    $("button[name=\"btn_Minus\"]").click(function () {
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        var $quantity = $data.find("input[name=\"txt_Quantity\"]");

        var quantity = parseFloat($quantity.val()) - 1;
        $quantity.val(quantity);
        CalPrice($data);
    });
    $("input[name=\"txt_Percent\"]").on("keyup", function () {
        var val = $(this).val();
        if (val.length > 3) {
            return false;
        }
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        CalPrice($data);
    });
    $("#txt_BuyerPhone").on("blur", function () {
        GetCustomerByPhone();
    });
});
function CalPay() {
    var MustPay = parseFloat($("#txt_MustPay").val());
    var BuyerPay = parseFloat($("#txt_BuyerPay").val());
    if (BuyerPay > 0) {
        $("#txt_BuyerPay").val(BuyerPay);
    }
    else {
        $("#txt_BuyerPay").val(MustPay);
    }

    var Return = MustPay - BuyerPay;
    if (Return < 0) {
        $("#txt_Return").val(Return);
    } else {
        $("#txt_Return").val(0);
    }
}
function CalOrder() {
    var AmountOrder = $("#txt_AmountOrder").val();
    var PercentDiscount = $("#txt_PercentDiscount").val();
    var AmountDiscount = parseFloat(PercentDiscount) * parseFloat(AmountOrder) / 100;
    var MustPay = parseFloat(AmountOrder) - AmountDiscount;
    $("#txt_MustPay").val(MustPay);
    $("#txt_AmountDiscount").val(AmountDiscount);
}
function CalPrice($data) {
    var $quantity = $data.find("input[name=\"txt_Quantity\"]");
    var $saleprice = $data.find("input[name=\"txt_SalePrice\"]");
    var $amountprice = $data.find("input[name=\"txt_AmountPrice\"]");

    var p = parseFloat($saleprice.val());
    var q = parseFloat($quantity.val());
    var newprice = p * q;
    $amountprice.val(newprice);
}
function Close(key) {
    var $data = $("tr[for=" + key + "]");
    $data.hide();
}
function Apply(key) {
    var $data = $("tr[for=" + key + "]");
    var $quantity = $data.find("input[name=\"txt_Quantity\"]");
    var $price = $data.find("input[name=\"txt_SalePrice\"]");
    var $amountprice = $data.find("input[name=\"txt_AmountPrice\"]");
    var $description = $data.find("textarea[name=\"txt_Description\"]");
    var Item = {
        ObjectKey: key,
        ObjectPrice: $price.val(),
        Quantity: $quantity.val(),
        Amount: $amountprice.val(),
        Description: $description.val()
    };

    $.ajax({
        url: URL_Edit,
        type: 'POST',
        data: {
            "Item": JSON.stringify(Item)
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {

            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}
function Remove(Key) {
    $.ajax({
        url: URL_Remove,
        type: 'POST',
        data: {
            "ObjectKey": Key
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {

            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            //Utils.LoadOut();
        }
    });
}
function Save() {
    var Info = {
        BuyerKey: $("#txt_BuyerKey").val(),
        BuyerPhone: $("#txt_Phone").val(),
        BuyerName: $("#txt_Name").val(),
        BuyerAddress: $("#txt_Address").val(),
        AmountOrder: $("#txt_AmountOrder").val(),
        AmountReceived: $("#txt_BuyerPay").val(),
        AmountReturned: $("#txt_Return").val()
    };
    $.ajax({
        url: URL_Save,
        type: 'POST',
        data: {
            "Info": JSON.stringify(Info),
            "Method": method
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#printdata").empty();
        },
        success: function (r) {
            if (r.Success) {
                var url = URL_Print.replace('_id_', r.Data);
                
                $.get(url, function (partial) {
                    $("#printdata").html(partial);
                }).done(function () {
                    Utils.LoadOut();
                    Utils.OpenMagnific("#modalPrint");
                });
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}
function GetCustomerByPhone() {
    var Phone = $.trim($('#txt_BuyerPhone').val());
    if (Phone.length <= 9) {
        Utils.OpenNotify("Số điện thoại không đúng", "Thông báo", "warning");
        return false;
    }

    $.ajax({
        url: URL_GetCustomerByPhone,
        type: "GET",
        data: {
            "Phone": Phone
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                var obj = JSON.parse(r.Data);
                if (obj.CustomerKey.length >= 36) {
                    $("#txt_Name").val(obj.FullName);
                    $("#txt_Phone").val(obj.Phone);
                    $("#txt_Address").val(obj.Address);

                    $("#txt_BuyerName").val(obj.FullName);
                    $("#txt_BuyerKey").val(obj.CustomerKey);
                }
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}
//xem nhanh don hang
function Preview() {
    $.ajax({
        url: URL_CheckCart,
        type: "GET",
        data: {},
        beforeSend: function () {
            $("#modalPayment").empty();
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                var url = r.Data;
                $.get(url, function (partial) {
                    $("#modalPayment").html(partial);
                }).done(function () {
                    InitNumber();
                    InitPreviewModal();
                    Utils.OpenMagnific("#modalPayment");
                    Utils.LoadOut();
                });
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}

function InitNumber() {
    $("span.number").number(true, 0, ",", ".");
    $("del.number").number(true, 0, ",", ".");
    $("input.number").number(true, 0, ",", ".");
}
function InitPreviewModal() {
    $("#txt_Name").val($.trim($("#txt_BuyerName").val()));
    $("#txt_Phone").val($.trim($("#txt_BuyerPhone").val()));
    $("#txt_Address").val();
    $("input[name=\"radioPayment\"]").click(function () {

        var rdo = $(this).val();

        if (rdo === "Pay") {
            method = "Pay";
            $("[name=\"Pay\"]").show();
        }
        else {
            method = "No";
            $("[name=\"Pay\"]").hide();
        }

    });

    $("#txt_BuyerPay").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: URL_AutoComplete,
                data: { term: request.term },
                dataType: "json",
                success: function (data) {
                    var obj = JSON.parse(data);
                    response(obj);
                },
                error: function (err) {
                    alert(err.responseText);
                }
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_BuyerPay").val(ui.item.Value);
            console.log("Selected: " + ui.item.Value + " aka " + ui.item.Text);
            CalPay();
        }
    })
        .autocomplete("instance")
        ._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div class=\"text-right\">" + item.Text + "</div>")
                .appendTo(ul);
        };
}
function printDiv() {
    Utils.PrintOnDesktop("printdata");
}