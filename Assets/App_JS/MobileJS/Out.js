﻿var _CustomerKey = "";

function Return() {
    $("#return").show();
    $("#btn_Accept").show();
    $("#btn_Cancel").show();
    $("#info").hide();
    $("#btn_Return").hide();
    $("#btn_Save").hide();
}
function Accepted() {

    var fileUpload = $("#FileReplace").get(0);
    var files = fileUpload.files;

    // Create  a FormData object
    var fileData = new FormData();

    // if there are multiple files , loop through each files
    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }

    fileData.append("ID", $("#txt_ReplaceID").val());
    fileData.append("Description", $("#txt_ReplaceDescription").val());
    fileData.append("InvoiceKey", $("#txt_InvoiceKey").val());
    $.ajax({
        url: URL_InvoiceReturn,
        processData: false,
        contentType: false,
        type: "POST",
        data: fileData,
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}
$(document).ready(function () {
    $("#formSearchButton").attr("action", URL_SearchButton);
    $("#formSearchDate").attr("action", URL_SearchDate);

    $("#txt_PartnerTax").blur(function () {
        var Tax = $(this).val();
        if (Tax.length > 0) {
            $("#txt_PartnerTax").removeClass("errorInput");
            GetCustomerByTax(Tax);
        }
        else {
            $("#txt_PartnerTax").focus();
            $("#txt_PartnerTax").addClass("errorInput");
            Utils.OpenNotify("Bạn phải nhập MST đơn vị bán hàng !.", "Thông báo", "warning");
        }
    });
});
function Save() {

    var Tax = $("#txt_PartnerTax").val();
    if (Tax.length <= 0) {
        $("#txt_PartnerTax").focus();
        $("#txt_PartnerTax").addClass("errorInput");
        Utils.OpenNotify("Bạn phải nhập MST đơn vị bán hàng !.", "Thông báo", "warning");
        return;
    }
    else {
        $("#txt_PartnerTax").removeClass("errorInput");
    }
    var InvoiceID = $("#txt_InvoiceID").val();
    if (InvoiceID.length <= 0) {
        $("#txt_InvoiceID").focus();
        $("#txt_InvoiceID").addClass("errorInput");
        Utils.OpenNotify("Bạn phải nhập số hóa đơn !.", "Thông báo", "warning");
        return;
    }
    else {
        $("#txt_InvoiceID").removeClass("errorInput");
    }
    var InvoiceDate = $("#txt_InvoiceDate").val();
    if (InvoiceDate.length <= 0) {
        $("#txt_InvoiceDate").focus();
        $("#txt_InvoiceDate").addClass("errorInput");
        Utils.OpenNotify("Bạn phải nhập ngày hóa đơn !.", "Thông báo", "warning");
        return;
    }
    else {
        $("#txt_InvoiceDate").removeClass("errorInput");
    }
    var PartnerName = $("#txt_PartnerName").val();
    if (PartnerName.length <= 0) {
        $("#txt_PartnerName").focus();
        $("#txt_PartnerName").addClass("errorInput");
        Utils.OpenNotify("Bạn phải nhập tên đơn vị !.", "Thông báo", "warning");
        return;
    }
    else {
        $("#txt_PartnerName").removeClass("errorInput");
    }

    var obj = {
        InvoiceKey: $("#txt_InvoiceKey").val(),
        InvoiceID: $("#txt_InvoiceID").val(),
        InvoiceType: "IN",
        InvoiceDate: Utils.ConvertToYYYYMMDD($("#txt_InvoiceDate").val()),
        CategoryKey: $("#cbo_Category").val(),
        CategoryName: $("#cbo_Category option:selected").text(),
        PartnerKey: _CustomerKey,
        PartnerName: $("#txt_PartnerName").val(),
        PartnerTax: $("#txt_PartnerTax").val(),
        AmountNotV: $("#txt_AmountNotV").val(),
        AmountVat: $("#txt_AmountVat").val(),
        AmountTotal: $("#txt_AmountTotal").val(),
        Description: $("#txt_Description").val(),
    };

    var fileUpload = $("#FileAttach").get(0);
    var files = fileUpload.files;

    // Create  a FormData object
    var fileData = new FormData();

    // if there are multiple files , loop through each files
    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }

    fileData.append("InvoiceObj", JSON.stringify(obj));
    fileData.append("InvoiceKey", $("#txt_InvoiceKey").val());

    $.ajax({
        url: URL_Save,
        processData: false,
        contentType: false,
        type: "POST",
        data: fileData,
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            //Utils.LoadOut();
        }
    });
}
function Detail(InvoiceKey) {
    Utils.OpenMagnific("#modalEdit");
    if (InvoiceKey !== "") {
        $.ajax({
            url: URL_Detail,
            type: "GET",
            data: {
                "InvoiceKey": InvoiceKey
            },
            beforeSend: function () {
                Utils.LoadIn();
            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);

                    var InvoiceDate = Utils.ConvertToDDMMYYYY(obj.InvoiceDate);

                    $("#txt_InvoiceKey").val(obj.InvoiceKey);
                    $("#txt_InvoiceID").val(obj.InvoiceID);
                    $("#txt_InvoiceDate").val(InvoiceDate);
                    $("#cbo_InvoiceType").val(obj.InvoiceType).trigger("change");
                    $("#cbo_Category").val(obj.CategoryKey).trigger("change");
                    $("#txt_PartnerName").val(obj.PartnerName);
                    $("#txt_PartnerTax").val(obj.PartnerTax);
                    $("#txt_AmountNotV").val(obj.AmountNotV);
                    $("#txt_AmountVat").val(obj.AmountVat);
                    $("#txt_AmountTotal").val(obj.AmountTotal);
                    $("#txt_Description").val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, "Thông báo", "error");
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {
                Utils.LoadOut();
            }
        });
    }
    else {
        Utils.ClearUI("#modalEdit");
        $("#txt_AmountVat").val(0);
        $("#txt_AmountNotV").val(0);
        $("#txt_AmountTotal").val(0);

        _CustomerKey = "";
    }
}
function Delete(InvoiceKey) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Cảnh báo !.",
        content: "Bạn có chắc xóa thông tin này ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: "POST",
                        data: {
                            "InvoiceKey": InvoiceKey
                        },
                        beforeSend: function () {
                            Utils.LoadIn();
                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, "Thông báo", "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}
function TotalAmout() {
    var AmountNotV = $("#txt_AmountNotV").val();
    var AmountVat = $("#txt_AmountVat").val();
    var AmountTotal = 0;
    AmountTotal = parseFloat(AmountNotV) + parseFloat(AmountVat);
    $("#txt_AmountTotal").val(AmountTotal);
}
function OpenDate() {
    Utils.OpenMagnific("#modalDate");
}

function GetCustomerByTax(val) {
    $.ajax({
        url: URL_GetCustomerByTax,
        type: "GET",
        data: {
            "Tax": val
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                var obj = JSON.parse(r.Data);
                if (obj.CustomerKey.length >= 36) {
                    $("#txt_PartnerName").val(obj.FullName);
                    _CustomerKey = obj.CustomerKey;
                }
            }
            else {
                _CustomerKey = "NEW";
                Utils.OpenNotify("Phần mềm sẽ cập nhật MST, và tên khách hàng mới !.", "Thông báo", "warning");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}