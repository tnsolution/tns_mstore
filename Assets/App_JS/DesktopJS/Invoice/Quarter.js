﻿$(document).ready(function () {
    $("#cbo_Quarter").change(function () {
        var Quarter = $(this).val();
        var Year = $("#txt_Year").val();
        $.ajax({
            url: URL_AmountPrevious,
            type: "GET",
            data: {
                "Quarter": Quarter,
                "Year": Year,
            },
            beforeSend: function () {
                $("#txt_AmountPrevious").val(0);
            },
            success: function (r) {
                $("#txt_AmountPrevious").val(r);
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {

            }
        });
    });
});

function SaveQuarterInvoice() {
    var Quarter = $("#cbo_Quarter").val();
    var Year = $("#txt_Year").val();
    var Description = $("#txt_Description").val();

    $.ajax({
        url: URL_SaveQuarterInvoice,
        type: "POST",
        data: {
            "Quarter": Quarter,
            "Year": Year,
            "Description": Description
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Đã lưu thành công !.", "Thông báo", "success");
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}