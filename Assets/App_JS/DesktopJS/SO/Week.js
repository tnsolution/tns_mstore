﻿var tmp = [];

function View(v) {
    var url = "/doanh-so?Option=_id_&FromDate=_Fr_&ToDate=_To_";
    url = url.replace("_id_", v);
    url = url.replace("_Fr_", $("#txtStartDate").val());
    url = url.replace("_To_", $("#txtEndDate").val());
    window.location = url;
}
$(document).ready(function () {
    renderChart();
    $("#sidebar-left").click(function () {
        renderChart();
    });
    $("html").addClass("sidebar-left-collapsed");
    $("input[type='checkbox']").change(function () {

        $("input[type='checkbox']").each(function () {
            var checked = $(this).val();
            if ($(this).is(':checked')) {
                tmp.push(checked);
            }
            else {
                tmp.splice($.inArray(checked, tmp), 1);
            }
        });

        $("#txtOption").val(tmp);
        $("#formSubmit").submit();
    });

    //$("input[type='checkbox']").prop('checked', false);
    //var temp = chk.split(",");
    //for (var i = 0; i < temp.length; i++) {
    //    var s = temp[i];
    //    $("#" + s).prop('checked', true);
    //}
});


function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    };
    return x1 + x2;
}
function kFormatter(num) {
    if (num > 999999) {
        return Math.abs(num) > 999999 ? Math.sign(num) * ((Math.abs(num) / 1000000).toFixed(1)) + 'tr' : Math.sign(num) * Math.abs(num)
    }
    if (num > 999) {
        return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num) / 1000).toFixed(1)) + 'ng' : Math.sign(num) * Math.abs(num)
    }

    return num;
}