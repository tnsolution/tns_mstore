﻿var width = $(window).width();
var height = $(window).height() - 350;
var method = "Pay";

const userAgent = navigator.userAgent.toLowerCase();
const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);

$(document).ready(function () {
    $(".content-body").css({ "padding": "10px" });

    $("#LeftPanel").css("height", height);
    $("#RightPanel").css("height", height + 130);

    $(".toggle-sidebar-left").click(function () {
        toggleSidebar("left");

        return false;
    });
    $(".toggle-sidebar-right").click(function () {
        toggleSidebar("right");

        return false;
    });

    $("#txt_BuyerPhone").keypress(function (e) {
        if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
    });
    $("#txt_BuyerPhone").on("blur", function () {
        GetCustomerByPhone();
    });

    $("#divProduct").on("click", "tr", function () {
        var ProductKey = $(this).attr("productkey");
        AddToCart(ProductKey);
    });

    $("#divCart").on("click", "tr:not([name=\"edit\"])", function () {
        $(this).next().slideToggle();
    });
    $("#divCart").on("click", "button[name=\"btn_Plus\"]", function () {
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        var $quantity = $data.find("input[name=\"txt_Quantity\"]");

        var quantity = parseFloat($quantity.val()) + 1;
        $quantity.val(quantity);
        CalPrice($data);
    });
    $("#divCart").on("click", "button[name=\"btn_Minus\"]", function () {
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        var $quantity = $data.find("input[name=\"txt_Quantity\"]");

        var quantity = parseFloat($quantity.val()) - 1;
        if (quantity <= 0) {
            quantity = 0;
        }
        $quantity.val(quantity);
        CalPrice($data);
    });
    $("#divCart").on("keyup", "input[name=\"txt_Percent\"]", function () {
        var val = $(this).val();
        if (val.length > 3) {
            return false;
        }
        var key = $(this).attr("key");
        var $data = $(this).parents("tr[for=" + key + "]");
        CalPrice($data);
    });

    if (isTablet || width <= 1366) {
        $("html").addClass("sidebar-left-collapsed");
    }

    $("#btn_MostBuy").click(function () { MostBuy(); });
    $("#btn_NewImport").click(function () { NewImport(); });
    $("#btn_Search").click(function () { SearchName(); });

    $(".nano").nanoScroller();
});

//an hien chi tiet
function toggleSidebar(side) {
    if (side !== "left" &&
        side !== "right") {
        return false;
    }
    var left = $("#sidebar-left"),
        right = $("#sidebar-right"),
        content = $("#content"),
        openSidebarsCount = 0,
        contentClass = "";

    // toggle sidebar
    if (side === "left") {
        left.toggleClass("collapsed");
    } else if (side === "right") {
        right.toggleClass("collapsed");
    }

    // determine number of open sidebars
    if (!left.hasClass("collapsed")) {
        openSidebarsCount += 1;
    }

    if (!right.hasClass("collapsed")) {
        openSidebarsCount += 1;
    }

    // determine appropriate content class
    if (openSidebarsCount === 0) {
        contentClass = "col-md-12";
    } else if (openSidebarsCount === 1) {
        contentClass = "col-md-9";
    } else {
        contentClass = "col-md-6";
    }

    // apply class to content
    content.removeClass("col-md-12 col-md-9 col-md-6")
        .addClass(contentClass);
}

//tinh toan so luong don gia, giam gia
function CalPay() {
    var MustPay = parseFloat($("#txt_MustPay").val());
    var BuyerPay = parseFloat($("#txt_BuyerPay").val());
    if (BuyerPay > 0) {
        $("#txt_BuyerPay").val(BuyerPay);
    }
    else {
        $("#txt_BuyerPay").val(MustPay);
    }

    var Return = MustPay - BuyerPay;
    if (Return < 0) {
        $("#txt_Return").val(Return);
    } else {
        $("#txt_Return").val(0);
    }
}
function CalOrder() {
    var AmountOrder = $("#txt_AmountOrder").val();
    var PercentDiscount = $("#txt_PercentDiscount").val();
    var AmountDiscount = parseFloat(PercentDiscount) * parseFloat(AmountOrder) / 100;
    var MustPay = parseFloat(AmountOrder) - AmountDiscount;
    $("#txt_MustPay").val(MustPay);
    $("#txt_AmountDiscount").val(AmountDiscount);
}
function CalPrice($data) {
    var $quantity = $data.find("input[name=\"txt_Quantity\"]");
    var $saleprice = $data.find("input[name=\"txt_SalePrice\"]");
    var $amountprice = $data.find("input[name=\"txt_AmountPrice\"]");

    var p = parseFloat($saleprice.val());
    var q = parseFloat($quantity.val());
    var newprice = p * q;
    $amountprice.val(newprice);
}

function InitNumber() {
    $("span.number").number(true, 0, ",", ".");
    $("del.number").number(true, 0, ",", ".");
    $("input.number").number(true, 0, ",", ".");
}
function InitPreviewModal() {
    var FullName = $("#txt_BuyerName").val();
    var Phone = $("#txt_BuyerPhone").val();
    var Address = $("#txt_BuyerAddress").val();

    $("#txt_Name").val(FullName);
    $("#txt_Phone").val(Phone);
    $("#txt_Address").val(Address);

    $("input[name=\"radioPayment\"]").click(function () {

        var rdo = $(this).val();

        if (rdo === "Pay") {
            method = "Pay";
            $("[name=\"Pay\"]").show();
        }
        else {
            method = "No";
            $("[name=\"Pay\"]").hide();
        }

    });

    $("#txt_BuyerPay").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: URL_AutoComplete,
                data: { term: request.term },
                dataType: "json",
                success: function (data) {
                    var obj = JSON.parse(data);
                    response(obj);
                },
                error: function (err) {
                    alert(err.responseText);
                }
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_BuyerPay").val(ui.item.Value);
            console.log("Selected: " + ui.item.Value + " aka " + ui.item.Text);
            CalPay();
        }
    })
        .autocomplete("instance")
        ._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div class=\"text-right\">" + item.Text + "</div>")
                .appendTo(ul);
        };
}

//action on left
function Apply(key) {
    var $data = $("tr[for=" + key + "]");
    var $quantity = $data.find("input[name=\"txt_Quantity\"]");
    var $price = $data.find("input[name=\"txt_SalePrice\"]");
    var $amountprice = $data.find("input[name=\"txt_AmountPrice\"]");
    var $description = $data.find("textarea[name=\"txt_Description\"]");
    var Item = {
        ObjectKey: key,
        ObjectPrice: $price.val(),
        Quantity: $quantity.val(),
        Amount: $amountprice.val(),
        Description: $description.val()
    };

    $.ajax({
        url: URL_EditCart,
        type: "POST",
        data: {
            "Item": JSON.stringify(Item)
        },
        beforeSend: function () {
            $("#divCart").empty();
        },
        success: function (r) {
            $("#divCart").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            InitNumber();
            SumCart();
        }
    });
}
function Remove(Key) {
    $.ajax({
        url: URL_Remove,
        type: "POST",
        data: {
            "ObjectKey": Key
        },
        beforeSend: function () {
            $("#divCart").empty();
        },
        success: function (r) {
            $("#divCart").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            InitNumber();
            SumCart();
        }
    });
}
function Close(key) {
    var $data = $("tr[for=" + key + "]");
    $data.hide();
}

//get khach hang
function GetCustomerByPhone() {
    var Phone = $.trim($("#txt_BuyerPhone").val());
    if (Phone.length <= 9) {
        Utils.OpenNotify("Số điện thoại không đúng", "Thông báo", "warning");
        return;
    }

    $.ajax({
        url: URL_GetCustomerByPhone,
        type: "GET",
        data: {
            "Phone": Phone
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                var obj = JSON.parse(r.Data);
                if (obj.CustomerKey.length >= 36) {
                    $("#txt_BuyerName").val(obj.FullName);
                    $("#txt_BuyerKey").val(obj.CustomerKey);
                    $("#txt_BuyerAddress").val(obj.Address);
                }
                else {
                    Utils.OpenNotify("Thông tin khách hàng sẽ được cập nhật !.", "Thông báo", "info");
                }
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}
//xem nhanh don hang
function Preview() {
    $.ajax({
        url: URL_CheckCart,
        type: "GET",
        data: {},
        beforeSend: function () {
            $("#modalPayment").empty();
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                var url = r.Data;
                $.get(url, function (partial) {
                    $("#modalPayment").html(partial);
                })
                    .done(function () {
                    InitNumber();
                    InitPreviewModal();
                    Utils.OpenMagnific("#modalPayment");
                    Utils.LoadOut();
                });
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "waring");
                Utils.LoadOut();
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
            Utils.LoadOut();
        },
        complete: function () {

        }
    });
}

//action on right
function AddToCart(ProductKey) {
    $.ajax({
        url: URL_AddToCard,
        type: "POST",
        data: {
            "ProductKey": ProductKey,
        },
        beforeSend: function () {
            $("#divCart").empty();
            $("#LeftPanel").trigger('loading-overlay:show');
        },
        success: function (r) {
            $("#divCart").html(r);
            $("#LeftPanel").trigger('loading-overlay:hide');
        },
        error: function (err) {
            console.log(err.responseText);
        },
        complete: function () {
            InitNumber();
            SumCart();
        }
    });
}
//
function SumCart() {
    $.ajax({
        url: URL_Sum,
        type: "GET",
        data: {},
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                var obj = JSON.parse(r.Data);
                $("#txtMustPay").text(obj.AmountOrder);
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            InitNumber();
        }
    });
}
//
function NewOrder() {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Thông báo !.",
        content: "Đơn hàng  này chưa lưu, bạn có chắc lập đơn mới ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_NewOrder,
                        type: "GET",
                        data: {},
                        beforeSend: function () {
                            Utils.LoadIn();
                        },
                        success: function (r) {
                            location.reload(true);
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}
//
function Save() {
    var Info = {
        BuyerKey: $("#txt_BuyerKey").val(),
        BuyerPhone: $("#txt_Phone").val(),
        BuyerName: $("#txt_Name").val(),
        BuyerAddress: $("#txt_Address").val(),
        AmountOrder: $("#txt_AmountOrder").val(),
        AmountReceived: $("#txt_BuyerPay").val(),
        AmountReturned: $("#txt_Return").val(),
        PercentDiscount: $("#txt_PercentDiscount").val(),
        AmountDiscount: $("#txt_AmountDiscount").val(),
    };
    $.ajax({
        url: URL_Save,
        type: 'POST',
        data: {
            "Info": JSON.stringify(Info),
            "Method": method
        },
        beforeSend: function () {
            $("#printdata").empty();
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                var url = r.Data;
                $.get(url, function (partial) {
                    $("#printdata").html(partial);
                }).done(function () {
                    InitNumber();
                    InitPreviewModal();
                    Utils.OpenMagnific("#modalPrint");
                    Utils.LoadOut();
                });
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
        }
    });
}
//
function printDiv() {
    Utils.PrintOnDesktop("printSection");
}
//tim theo bên phải
function LoadProduct(Category) {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "CategoryKey": Category,
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}
//Bán chạy
function MostBuy() {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "Option": "TOP"
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}
//Mới nhập
function NewImport() {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "Option": "ORDER"
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}
//tim theo tên
function SearchName() {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        data: {
            "Name": $("#txt_ProductName").val()
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#divProduct").html(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            Utils.LoadOut();
            InitNumber();
            $("#sidebar-right").addClass("collapsed");
        }
    });
}

//
function CancelOrder(Key) {
    if (Key.length === 0) {
        Key = $("#txt_OrderKey").val();
    }
    if (Key.length <= 0) {
        Utils.OpenNotify("Lỗi mã đơn hàng xin liên hệ IT !.", "Thông báo", "error");
        return;
    }

    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Thông báo !.",
        content: "Bạn có chắc xóa, các thông tin thu tiền, hóa đơn sẽ thay đổi theo ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_CancelOrder,
                        type: "POST",
                        data: {
                            "OrderKey": Key
                        },
                        beforeSend: function () {
                            Utils.LoadIn();
                        },
                        success: function (r) {
                            location.reload(true);
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                            Utils.LoadOut();
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}