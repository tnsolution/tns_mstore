﻿$(document).ready(function () {
    $("#cbo_PartnerSearch").change(function () {
        var PartnerNumber = $(this).val();
        FilterUser(PartnerNumber);
    });
});

//khởi tạo giao diện checkbox
function InitCheckStyle() {
    $("[data-plugin-ios-switch]").each(function () {
        var $this = $(this);
        $this.themePluginIOS7Switch();
    });
}

//tim user theo số PartnerNumber
function FilterUser(PartnerNumber) {

    $.ajax({
        url: URL_SearchUser,
        contentType: "application/x-www-form-urlencoded;charset=ISO-8859-15",
        type: "GET",
        data: {
            "PartnerNumber": PartnerNumber
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            $("#tblUser").empty().append(r);
        },
        error: function (err) {
            console.log(err.responseText);
        },
        complete: function () {
            InitCheckStyle();
            Utils.LoadOut();
        }
    });
}

//lưu
function Save() {
    var PartnerNumber = $("#cbo_Partner").val();
    var UserKey = $("#txt_UserKey").val();
    var UserName = $("#txt_UserName").val();
    var Password = $("#txt_Password").val();
    var lbl_Password = $("#lbl_Password").val();
    var EmployeeKey = $("#cbo_Employee").val();
    var EmployeeName = $("#cbo_Employee option:selected").text();
    var Description = $("#txt_Description").val();
    var Activate = $("#cbo_Activate").val();
    var ExpireDate = $("#txt_ExpireDate").val();

    if (PartnerNumber.length <= 0) {
        Utils.OpenNotify("Bạn phải chọn đối tác, khách hàng", "Thông báo", "warning");
        return;
    }

    if (UserName.length <= 0) {
        Utils.OpenNotify("Bạn phải nhập tên đăng nhập", "Thông báo", "warning");
        return;
    }
    else {
        $.ajax({
            url: URL_CheckUser,
            type: "GET",
            data: {
                "UserName": UserName,
            },
            beforeSend: function () {
                Utils.LoadIn();
            },
            success: function (r) {
                if (!r.Success) {
                    Utils.OpenNotify(r.Message, "Thông báo", "error");
                    Utils.LoadOut();
                }
                else {
                    //
                    $.ajax({
                        url: URL_Save,
                        type: "POST",
                        data: {
                            "UserKey": UserKey,
                            "UserName": UserName,
                            "Password": Password,
                            "lbl_Password": lbl_Password,
                            "EmployeeKey": EmployeeKey,
                            "EmployeeName": EmployeeName,
                            "Description": Description,
                            "Activate": Activate,
                            "ExpireDate": ExpireDate,
                            "PartnerNumber": PartnerNumber
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                FilterUser(PartnerNumber);
                            }
                            else {
                                Utils.OpenNotify(r.Message, "Thông báo", "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                //
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {
                                
            }
        });
    }

    
}
//mở edit
function Detail(UserKey) {
    Utils.OpenMagnific("#modalEdit");
    if (UserKey.length >= 36) {
        $.ajax({
            url: URL_Detail,
            type: "GET",
            data: {
                "UserKey": UserKey
            },
            beforeSend: function () {
                Utils.LoadIn();
            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var Date = Utils.ConvertToDDMMYYYY(obj.ExpireDate);

                    $("#txt_UserKey").val(obj.UserKey);
                    $("#txt_UserName").val(obj.UserName);
                    $("#txt_Password").val(obj.Password);
                    $("#lbl_Password").val(obj.Password);
                    $("#cbo_Employee").val(obj.EmployeeKey);
                    if (obj.Activate === true)
                        $("#cbo_Activate").val(1).trigger("change");
                    else
                        $("#cbo_Activate").val(0).trigger("change");
                    $("#txt_ExpireDate").val(Date);
                    $("#txt_Description").val(obj.Description);

                }
                else {
                    Utils.OpenNotify(r.Message, "Thông báo", "error");
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {
                Utils.LoadOut();
            }
        });
    }
    else {
        Utils.ClearUI("#modalEdit");
    }
}
//xóa
function Delete(UserKey) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Cảnh báo !.",
        content: "Bạn có chắc xóa thông tin này ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: "POST",
                        data: {
                            "UserKey": UserKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("tr[id=\"" + UserKey + "\"]").remove();
                            }
                            else {
                                Utils.OpenNotify(r.Message, "Thông báo", "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}
//kich hoạt
function Activate(UserKey) {
    $.ajax({
        url: URL_Active,
        type: "POST",
        data: {
            "UserKey": UserKey,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Cập nhật tình trạng thành công", "Thông báo", "success");
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}
//reset mat khau
function Reset(UserKey) {
    $.ajax({
        url: URL_Reset,
        type: "POST",
        data: {
            "UserKey": UserKey,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify(r.Message, "Tạo mật khẩu mới thành công", "success");
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}

//event check
var roles = [];
function RoleEventCheckDesktop(obj, rolekey, userkey) {
    var chk = obj.find("input:checkbox");
    var read = chk[0].checked;
    var edit = chk[1].checked;
    var del = chk[2].checked;

    var newobj = {
        "RoleKey": rolekey,
        "UserKey": userkey,
        "RoleRead": read,
        "RoleEdit": edit,
        "RoleDel": del
    };

    roles = ObjCheck(roles, "RoleKey", newobj);
    $("#txt_AuthDesktop").val(JSON.stringify(roles));
}
function RoleEventCheckMobile(obj, rolekey, userkey) {
    var chk = obj.find("input:checkbox");
    var read = chk[0].checked;
    var edit = chk[1].checked;
    var del = chk[2].checked;

    var newobj = {
        "RoleKey": rolekey,
        "UserKey": userkey,
        "RoleRead": read,
        "RoleEdit": edit,
        "RoleDel": del
    };

    roles = ObjCheck(roles, "RoleKey", newobj);
    $("#txt_AuthMobile").val(JSON.stringify(roles));
}
//
function ObjCheck(array, name, newElement) {
    var found = false;
    for (var i = 0; i < array.length; i++) {
        var element = array[i];
        if (element === undefined) {
            array.push(newElement);
            return array;
        }
        if (element[name].toString() ===
            newElement[name].toString()) {
            found = true;
            array[i] = newElement;
        }
    }
    if (found === false) {
        array.push(newElement);
    }
    return array;
}

function LoadRoleDesktop(UserKey, Module) {
    $.ajax({
        url: URL_RoleDesktop,
        type: "GET",
        data: {
            "UserKey": UserKey,
            "Module": Module,
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#modelAuthDesktop").empty();
            $("#modelAuthMobile").empty();
        },
        success: function (r) {
            $("#modelAuthDesktop").html(r);
        },
        error: function (err) {
            Utils.OpenNotify("Lỗi !.", err.responseText, "error");
        },
        complete: function () {
            Utils.LoadOut();
            Utils.OpenMagnific("#modelAuthDesktop");
            $("#txt_User").val(UserKey);
            $("#txt_Module").val(Module);
            $("#txt_AuthDesktop").val("");
            $("#txt_AuthMobile").val("");
            roles = [];
        }
    });
}
function LoadRoleMobile(UserKey, Module) {
    $.ajax({
        url: URL_RoleMobile,
        type: "GET",
        data: {
            "UserKey": UserKey,
            "Module": Module,
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#modelAuthDesktop").empty();
            $("#modelAuthMobile").empty();
        },
        success: function (r) {
            $("#modelAuthMobile").html(r);
        },
        error: function (err) {
            Utils.OpenNotify("Lỗi !.", err.responseText, "error");
        },
        complete: function () {
            Utils.LoadOut();
            Utils.OpenMagnific("#modelAuthMobile");
            $("#txt_User").val(UserKey);
            $("#txt_Module").val(Module);
            $("#txt_AuthDesktop").val("");
            $("#txt_AuthMobile").val("");
            roles = [];
        }
    });
}

function SaveAuthRoleDesktop() {
    var data = $("#tbldata input:checkbox:checked").map(function () {
        return this.value;
    }).toArray();
    $("#txt_Data").val(JSON.stringify(data));

    $("#tblroleDesktop > tbody > tr").each(function () {
        var rolekey = $(this).attr("rolekey");
        var userkey = $(this).attr("userkey");
        RoleEventCheckDesktop($(this), rolekey, userkey);
    });

    var UserKey = $("#txt_User").val();
    var Role = $("#txt_AuthDesktop").val();
    var Module = $("#txt_Module").val();

    $.ajax({
        url: URL_RoleDesktopSave,
        type: "POST",
        data: {
            "UserKey": UserKey,
            "Role": Role,
            "Module": Module
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Thông báo", "Đã cập nhật thành công", "success");
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}
function SaveAuthRoleMobile() {
    var data = $("#tbldata input:checkbox:checked").map(function () {
        return this.value;
    }).toArray();
    $("#txt_Data").val(JSON.stringify(data));

    $("#tblroleMobile > tbody > tr").each(function () {
        var rolekey = $(this).attr("rolekey");
        var userkey = $(this).attr("userkey");
        RoleEventCheckMobile($(this), rolekey, userkey);
    });

    var UserKey = $("#txt_User").val();
    var Role = $("#txt_AuthMobile").val();
    var Module = $("#txt_Module").val();

    $.ajax({
        url: URL_RoleMobileSave,
        type: "POST",
        data: {
            "UserKey": UserKey,
            "Role": Role,
            "Module": Module
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Thông báo", "Đã cập nhật thành công", "success");
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}