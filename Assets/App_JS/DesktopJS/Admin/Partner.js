﻿//1 load html role desktop
function LoadViewRoleDesktop(PartnerNumber, Module) {
    $.ajax({
        url: URL_View,
        type: "GET",
        data: {
            "Type": "DESKTOP",
            "Module": Module,
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#modalRoleDesktop").empty();
        },
        success: function (r) {
            Utils.OpenMagnific("#modalRoleDesktop");
            $("#modalRoleDesktop").append(r);
        },
        error: function (err) {
            Utils.OpenNotify("Lỗi !.", err.responseText, "error");
        },
        complete: function () {
            Utils.LoadOut();
            EventRoleDesktop();
            if (Module.length !== "") {
                FillRolePartnerDesktop(PartnerNumber, Module);
            }
            $("#txt_PartnerNumber").val(PartnerNumber);
        }
    });
}
//2 tạo sư kiện thay đổi cho select
function EventRoleDesktop() {
    $(".select2").select2({
        width: "100%",
        placeholder: "--Chọn--",
    });
    $("#cbo_ModuleDesktop").change(function () {
        var val = $(this).val();

        $.confirm({
            type: "red",
            typeAnimated: true,
            title: "Cảnh báo !.",
            content: "Bạn có chắc đổi bộ menu thông tin Partner này ?.",
            buttons: {
                confirm: {
                    text: "Đồng ý",
                    btnClass: "btn-red",
                    action: function () {
                        LoadModuleDesktop(val);
                    }
                },
                cancel: {
                    text: "Hủy",
                }
            }
        });
    });
}
//3 khởi tạo bộ role desktop mới khi thay đổi select
function LoadModuleDesktop(Module) {
    try {
        $("#treeCheckboxDesktop").jstree("destroy");
    } catch (e) {
        console.log(e);
    }

    $("#treeCheckboxDesktop").on("changed.jstree",
        function (e, data) {
            var i, j, r = [];
            for (i = 0, j = data.selected.length; i < j; i++) {
                r.push(data.instance.get_node(data.selected[i]).id);
            }
            $("#txt_RolePartnerDesktop").val(r.join(", "));
        }).jstree({
            "core": {
                "data": {
                    type: "GET",
                    url: URL_GetRoleModule,
                    data: { "Module": Module },
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        data.d;
                        $(data).each(function () {
                            return { "id": this.id };
                        });
                    }
                },
                "checkbox": {
                    "two_state": true,
                },
                "themes": {
                    "responsive": false
                }
            },
            "plugins": ["types", "checkbox"]
        }).bind("ready.jstree", function (e, data) {
            $(this).jstree("open_all");
        });
}
//4 load bộ role desktop của khách hàng nếu đã có role
function FillRolePartnerDesktop(PartnerNumber, Module) {
    $("#cbo_ModuleDesktop").val(Module).trigger('change.select2');
    $("#treeCheckboxDesktop").on("changed.jstree",
        function (e, data) {
            var i, j, r = [];
            for (i = 0, j = data.selected.length; i < j; i++) {
                r.push(data.instance.get_node(data.selected[i]).id);
            }
            $("#txt_RolePartnerDesktop").val(r.join(", "));
        }).jstree({
            "core": {
                "data": {
                    type: "GET",
                    url: URL_GetRolePartner,
                    data: {
                        "PartnerNumber": PartnerNumber,
                        "Module": Module
                    },
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        data.d;
                        $(data).each(function () {
                            return { "id": this.id };
                        });
                    }
                },
                "checkbox": {
                    "two_state": true,
                },
                "themes": {
                    "responsive": false
                }
            },
            "plugins": ["types", "checkbox"]
        }).bind("ready.jstree", function (e, data) {
            $(this).jstree("open_all");
        });

}

//1 load html role mobile
function LoadViewRoleMobile(PartnerNumber, Module) {
    $.ajax({
        url: URL_View,
        type: "GET",
        data: {
            "Type": "MOBILE",
            "Module": Module,
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#modalRoleMobile").empty();
        },
        success: function (r) {
            Utils.OpenMagnific("#modalRoleMobile");
            $("#modalRoleMobile").append(r);
        },
        error: function (err) {
            Utils.OpenNotify("Lỗi !.", err.responseText, "error");
        },
        complete: function () {
            Utils.LoadOut();
            EventRoleMobile();
            if (Module.length !== "") {
                FillRolePartnerMobile(PartnerNumber, Module);
            }
            $("#txt_PartnerNumber").val(PartnerNumber);
        }
    });
}
//2 tạo sư kiện thay đổi cho select
function EventRoleMobile() {
    $(".select2").select2({
        width: "100%",
        placeholder: "--Chọn--",
    });
    $("#cbo_ModuleMobile").change(function () {
        var val = $(this).val();

        $.confirm({
            type: "red",
            typeAnimated: true,
            title: "Cảnh báo !.",
            content: "Bạn có chắc đổi bộ menu thông tin Partner này ?.",
            buttons: {
                confirm: {
                    text: "Đồng ý",
                    btnClass: "btn-red",
                    action: function () {
                        LoadModuleMobile(val);
                    }
                },
                cancel: {
                    text: "Hủy",
                }
            }
        });
    });
}
//3 khởi tạo bộ role desktop mới khi thay đổi select
function LoadModuleMobile(Module) {
    try {
        $("#treeCheckboxMobile").jstree("destroy");
    } catch (e) {
        console.log(e);
    }
    $("#treeCheckboxMobile").on("changed.jstree",
        function (e, data) {
            var i, j, r = [];
            for (i = 0, j = data.selected.length; i < j; i++) {
                r.push(data.instance.get_node(data.selected[i]).id);
            }
            $("#txt_RolePartnerMobile").val(r.join(", "));
        }).jstree({
            "core": {
                "data": {
                    type: "GET",
                    url: URL_GetRoleModule,
                    data: { "Module": Module },
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        data.d;
                        $(data).each(function () {
                            return { "id": this.id };
                        });
                    }
                },
                "checkbox": {
                    "two_state": true,
                },
                "themes": {
                    "responsive": false
                }
            },
            "plugins": ["types", "checkbox"]
        }).bind("ready.jstree", function (e, data) {
            $(this).jstree("open_all");
        });
}
//4 load bộ role desktop của khách hàng nếu đã có role
function FillRolePartnerMobile(PartnerNumber, Module) {
    $("#cbo_ModuleMobile").val(Module).trigger('change.select2');
    $("#treeCheckboxMobile").on("changed.jstree",
        function (e, data) {
            var i, j, r = [];
            for (i = 0, j = data.selected.length; i < j; i++) {
                r.push(data.instance.get_node(data.selected[i]).id);
            }
            $("#txt_RolePartnerMobile").val(r.join(", "));
        }).jstree({
            "core": {
                "data": {
                    type: "GET",
                    url: URL_GetRolePartner,
                    data: {
                        "PartnerNumber": PartnerNumber,
                        "Module": Module
                    },
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        data.d;
                        $(data).each(function () {
                            return { "id": this.id };
                        });
                    }
                },
                "checkbox": {
                    "two_state": true,
                },
                "themes": {
                    "responsive": false
                }
            },
            "plugins": ["types", "checkbox"]
        }).bind("ready.jstree", function (e, data) {
            $(this).jstree("open_all");
        });
}

function Edit(id) {
    $.ajax({
        url: URL_Edit,
        type: "GET",
        data: {
            "PartnerNumber": id
        },
        beforeSend: function () {
            Utils.LoadIn();
            $("#modalEdit").empty();
        },
        success: function (r) {
            Utils.OpenMagnific("#modalEdit");
            $("#modalEdit").append(r);
        },
        error: function (err) {
            Utils.OpenNotify("Lỗi !.", err.responseText, "error");
        },
        complete: function () {
            Utils.LoadOut();

            $(".select2").select2({
                width: "100%",
                placeholder: "--Chọn--",
            });
            $("#fileListPhoto").change(function () {
                Utils.PreviewImg(this, "#imgpreview");
                var str = $("#imgpreview").attr("src");
                $("#txt_Base64String").val(str);
            });

            if (id !== '') {
                $("#txt_UserName").prop("readonly", true);
                $("#txt_Password").prop("readonly", true);
            }
            else {
                $("#txt_UserName").prop("readonly", false);
                $("#txt_Password").prop("readonly", false);
            }
        }
    });
}
function Delete(id) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Cảnh báo !.",
        content: "Bạn có chắc xóa thông tin này ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: "POST",
                        data: {
                            "PartnerNumber": id
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("table tr[ partnernumber=\"" + id + "\"]").remove();
                            }
                            else {
                                Utils.OpenNotify("Lỗi !.", r.Message, "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify("Lỗi !.", err.responseText, "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}

//
function AutoPassword() {
    var input = $("#txt_Phone").val();
    $("#txt_UserName").val(input);
    $("#txt_Password").val(input);
}