﻿$(document).ready(function () {

});
function Save() {
    var PaymentKey = $("#txt_PaymentKey").val();
    var PaymentID = $("#txt_PaymentID").val();
    var PaymentDate = Utils.ConvertToYYYYMMDD($("#txt_PaymentDate").val());
    var CategoryKey = $("#cbo_Category").val();
    var CategoryName = $("#cbo_Category option:selected").text();
    var PaymentDescription = $("#txt_PaymentDescription").val();
    var CustomerName = $("#txt_CustomerName").val();
    var Address = $("#txt_Address").val();
    var AmountCurrencyMain = $("#txt_AmountCurrencyMain").val();
    var IsFeeInside = "True";

    $.ajax({
        url: URL_Save,
        type: "POST",
        data: {
            "PaymentKey": PaymentKey,
            "PaymentID": PaymentID,
            "PaymentDate": PaymentDate,
            "CategoryKey": CategoryKey,
            "CategoryName": CategoryName,
            "PaymentDescription": PaymentDescription,
            "CustomerName": CustomerName,
            "Address": Address,
            "AmountCurrencyMain": AmountCurrencyMain,
            "IsFeeInside": IsFeeInside,

        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}
function Detail(PaymentKey) {
    Utils.OpenMagnific("#modalEdit");
    if (PaymentKey.length >= 36) {
        $.ajax({
            url: URL_Detail,
            type: "GET",
            data: {
                "PaymentKey": PaymentKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);

                    var PaymentDate = Utils.ConvertToDDMMYYYY(obj.PaymentDate);

                    $("#txt_PaymentKey").val(obj.PaymentKey);
                    $("#txt_PaymentID").val(obj.PaymentID);
                    $("#txt_PaymentDate").val(PaymentDate);
                    $("#cbo_Category").val(obj.CategoryKey).trigger("change");
                    $("#txt_PaymentDescription").val(obj.PaymentDescription);
                    $("#txt_CustomerName").val(obj.CustomerName);
                    $("#txt_Address").val(obj.Address);
                    $("#txt_AmountCurrencyMain").val(obj.AmountCurrencyMain);
                    //$("#chb_IsFeeInside").val(obj.IsFeeInside);
                }
                else {
                    Utils.OpenNotify(r.Message, "Thông báo", "error");
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {

            }
        });
    }
    else {
        Utils.ClearUI("#modalEdit");
        $("#rdo1").val(1);
        $("#rdo0").val(0);

        $.ajax({
            url: URL_AutoID,
            type: "GET",
            data: {

            },
            beforeSend: function () {

            },
            success: function (r) {
                $("#txt_PaymentID").val(r);
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {
            }
        });
    }
}
function Delete(PaymentKey) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Cảnh báo !.",
        content: "Bạn có chắc xóa thông tin này ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: "POST",
                        data: {
                            "PaymentKey": PaymentKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, "Thông báo", "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}