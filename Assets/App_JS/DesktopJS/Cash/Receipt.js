﻿$(document).ready(function () {

});
function Save() {
    var ReceiptKey = $("#txt_ReceiptKey").val();
    var ReceiptID = $("#txt_ReceiptID").val();
    var ReceiptDate = Utils.ConvertToYYYYMMDD($("#txt_ReceiptDate").val());
    var CategoryKey = $("#cbo_Category").val();
    var CategoryName = $("#cbo_Category option:selected").text();
    var ReceiptDescription = $("#txt_ReceiptDescription").val();
    var CustomerName = $("#txt_CustomerName").val();
    var Address = $("#txt_Address").val();
    var AmountCurrencyMain = $("#txt_AmountCurrencyMain").val();
    var IsFeeInside = "true";
    $.ajax({
        url: URL_Save,
        type: "POST",
        data: {
            "ReceiptKey": ReceiptKey,
            "ReceiptID": ReceiptID,
            "ReceiptDate": ReceiptDate,
            "CategoryKey": CategoryKey,
            "CategoryName": CategoryName,
            "ReceiptDescription": ReceiptDescription,
            "CustomerName": CustomerName,
            "Address": Address,
            "AmountCurrencyMain": AmountCurrencyMain,
            "IsFeeInside": IsFeeInside,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {

        }
    });
}
function Detail(ReceiptKey) {
    Utils.OpenMagnific("#modalEdit");
    if (ReceiptKey.length >= 36) {
        $.ajax({
            url: URL_Detail,
            type: "GET",
            data: {
                "ReceiptKey": ReceiptKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);

                    var ReceiptDate = Utils.ConvertToDDMMYYYY(obj.ReceiptDate);

                    $("#txt_ReceiptKey").val(obj.ReceiptKey);
                    $("#txt_ReceiptID").val(obj.ReceiptID);
                    $("#txt_ReceiptDate").val(ReceiptDate);
                    $("#cbo_Category").val(obj.CategoryKey).trigger("change");
                    $("#txt_ReceiptDescription").val(obj.ReceiptDescription);
                    $("#txt_CustomerName").val(obj.CustomerName);
                    $("#txt_Address").val(obj.Address);
                    $("#txt_AmountCurrencyMain").val(obj.AmountCurrencyMain);
                    //$("#cbx_IsFeeInside").val(obj.IsFeeInside);
                }
                else {
                    Utils.OpenNotify(r.Message, "Thông báo", "error");
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {

            }
        });
    }
    else {
        Utils.ClearUI("#modalEdit");
        $("#rdo1").val(1);
        $("#rdo0").val(0);

        $.ajax({
            url: URL_AutoID,
            type: "GET",
            data: {

            },
            beforeSend: function () {

            },
            success: function (r) {
                $("#txt_ReceiptID").val(r);
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, "Thông báo", "error");
            },
            complete: function () {
            }
        });

    }
}
function Delete(ReceiptKey) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Cảnh báo !.",
        content: "Bạn có chắc xóa thông tin này ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: "POST",
                        data: {
                            "ReceiptKey": ReceiptKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("tr[key=\"" + ReceiptKey + "\"]").remove();
                                //location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, "Thông báo", "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}