﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TNStores
{
    public class Authen
    {
        public static List<string> GetModule(out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT DISTINCT Module FROM SYS_Role WHERE RecordStatus != 99 ORDER BY Module";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<string> zList = new List<string>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(r["Module"].ToString());
            }
            return zList;
        }
        public static List<Role_Model> Recursive_Role(string Module)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"; 
WITH R AS (
	-- ANCHOR PART
      SELECT 
		  RoleKey, 
		  RoleName AS NAME, RouteName, ActionName, ControllerName, Icon, RoleID, RoleURL, Description,
		  PARENT,
		  DEPTH = 0 , 
		  SORT = CAST(RoleKey AS VARCHAR(MAX))
      FROM SYS_Role
      WHERE 
	  RecordStatus <> 99
      AND Parent = '0'
	  AND module = @module
      UNION ALL
	-- RECURSIVE PART
      SELECT 		
		Sub.RoleKey, 
		Sub.RoleName AS NAME, Sub.RouteName, Sub.ActionName, Sub.ControllerName, Sub.Icon,  Sub.RoleID, Sub.RoleURL, Sub.Description,
		Sub.PARENT,
		DEPTH = R.DEPTH + 1, 
		SORT = R.SORT + '-' + CAST(Sub.RoleKey AS VARCHAR(MAX))
      FROM R
      INNER JOIN SYS_Role Sub ON CAST(R.RoleKey AS VARCHAR(MAX)) = Sub.Parent
	  WHERE 
	  RecordStatus <> 99
	  AND module = @module
) 
SELECT TREE = REPLICATE('---',R.DEPTH*1)+R.[NAME], R.RoleKey, R.RouteName, R.ActionName, R.ControllerName, R.Icon, R.RoleID, R.RoleURL, R.Description
FROM R
ORDER BY SORT";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Role_Model> zList = new List<Role_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Model()
                {
                    RoleName = r["TREE"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    RoleID = r["RoleID"].ToString(),
                    RoleURL = r["RoleURL"].ToString(),
                    ControllerName = r["ControllerName"].ToString(),
                    ActionName = r["ActionName"].ToString(),
                    RouteName = r["RouteName"].ToString(),
                    Icon = r["Icon"].ToString(),
                    Description = r["Description"].ToString(),
                });
            }

            return zList;
        }

        /// <summary>
        /// Lay bộ quyền của Partner
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Module"></param>
        /// <returns></returns>
        public static DataTable GetPartnerRole(string PartnerNumber, string Module, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
  SELECT N'' PartnerNumber, B.RoleID, B.RoleKey, B.RoleName, B.RoleURL, B.Module, 'False' AS Access, 
  B.[Level], ISNULL(B.Parent,0) AS Parent, B.[Description], B.RouteName
  FROM SYS_Role B
  WHERE B.Module = @Module
  AND B.RoleKey 
  NOT IN (
	            SELECT RoleKey FROM SYS_Role_Partner 
                WHERE CONVERT(NVARCHAR(50), PartnerNumber) = @PartnerNumber
              )
  UNION ALL
  SELECT CONVERT(NVARCHAR(50), A.PartnerNumber), B.RoleID, B.RoleKey, B.RoleName, B.RoleURL, B.Module, 'True' AS Access, 
  B.[Level], ISNULL(B.Parent,0) AS Parent, B.[Description], B.RouteName
  FROM SYS_Role_Partner A 
  LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
  WHERE RecordStatus != 99 
  AND B.Module = @Module
  AND CONVERT(NVARCHAR(50), A.PartnerNumber)  = @PartnerNumber
) 
SELECT * FROM X ORDER BY Module, RoleID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return zTable;
        }
        /// <summary>
        /// Lay bộ quyền của Module
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Module"></param>
        /// <returns></returns>
        public static DataTable GetModuleRole(string Module, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
  SELECT N'' PartnerNumber, B.RoleID, B.RoleKey, B.RoleName, B.RoleURL, B.Module, 'False' AS Access, 
  B.[Level], ISNULL(B.Parent,0) AS Parent, B.[Description], B.RouteName
  FROM SYS_Role B
  WHERE B.Module = @Module AND B.RecordStatus != 99 ORDER BY RoleID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return zTable;
        }

        public static string ApplySQL(string SQL, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

    }
}