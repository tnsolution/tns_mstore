﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
public class Business_Model
{
#region [ Field Name ]
private int _BusinessKey = 0;
private string _BusinessName = "";
private string _Description = "";
private string _URL = "";
private int _Rank = 0;
private string _RoleDesktop = "";
private string _RoleMobile = "";
private int _Slug = 0;
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
#endregion
 
#region [ Properties ]
public int BusinessKey
{
get { return _BusinessKey; }
set { _BusinessKey = value; }
}
public string BusinessName
{
get { return _BusinessName; }
set { _BusinessName = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public string URL
{
get { return _URL; }
set { _URL = value; }
}
public int Rank
{
get { return _Rank; }
set { _Rank = value; }
}
public string RoleDesktop
{
get { return _RoleDesktop; }
set { _RoleDesktop = value; }
}
public string RoleMobile
{
get { return _RoleMobile; }
set { _RoleMobile = value; }
}
public int Slug
{
get { return _Slug; }
set { _Slug = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
#endregion
}
}
