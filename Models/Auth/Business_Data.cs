﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Business_Data
    {
        public static List<Business_Model> List(out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Business WHERE RecordStatus != 99";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Business_Model> zList = new List<Business_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Business_Model()
                {
                    BusinessKey = r["BusinessKey"].ToInt(),
                    BusinessName = r["BusinessName"].ToString(),
                    Description = r["Description"].ToString(),
                    URL = r["URL"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    RoleDesktop = r["RoleDesktop"].ToString(),
                    RoleMobile = r["RoleMobile"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
