﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
public class Business_Info
{
 
public  Business_Model Business = new Business_Model();
private string _Message = "";
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
 
#region [ Constructor Get Information ]
public Business_Info()
{
}
public Business_Info(int BusinessKey)
{
string zSQL = "SELECT * FROM SYS_Business WHERE BusinessKey = @BusinessKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = BusinessKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["BusinessKey"]!= DBNull.Value)
Business.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
Business.BusinessName = zReader["BusinessName"].ToString();
Business.Description = zReader["Description"].ToString();
Business.URL = zReader["URL"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Business.Rank = int.Parse(zReader["Rank"].ToString());
Business.RoleDesktop = zReader["RoleDesktop"].ToString();
Business.RoleMobile = zReader["RoleMobile"].ToString();
if (zReader["Slug"]!= DBNull.Value)
Business.Slug = int.Parse(zReader["Slug"].ToString());
if (zReader["RecordStatus"]!= DBNull.Value)
Business.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Business.CreatedOn = (DateTime)zReader["CreatedOn"];
Business.CreatedBy = zReader["CreatedBy"].ToString();
Business.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Business.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Business.ModifiedBy = zReader["ModifiedBy"].ToString();
Business.ModifiedName = zReader["ModifiedName"].ToString();
_Message = "200 OK";
}
else
{
_Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO SYS_Business (" 
 + " BusinessName , Description , URL , Rank , RoleDesktop , RoleMobile , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @BusinessName , @Description , @URL , @Rank , @RoleDesktop , @RoleMobile , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = Business.BusinessName;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Business.Description;
zCommand.Parameters.Add("@URL", SqlDbType.NVarChar).Value = Business.URL;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Business.Rank;
zCommand.Parameters.Add("@RoleDesktop", SqlDbType.NVarChar).Value = Business.RoleDesktop;
zCommand.Parameters.Add("@RoleMobile", SqlDbType.NVarChar).Value = Business.RoleMobile;
zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Business.Slug;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Business.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Business.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Business.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Business.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Business.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "201 Created";
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO SYS_Business(" 
 + " BusinessKey , BusinessName , Description , URL , Rank , RoleDesktop , RoleMobile , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @BusinessKey , @BusinessName , @Description , @URL , @Rank , @RoleDesktop , @RoleMobile , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Business.BusinessKey;
zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = Business.BusinessName;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Business.Description;
zCommand.Parameters.Add("@URL", SqlDbType.NVarChar).Value = Business.URL;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Business.Rank;
zCommand.Parameters.Add("@RoleDesktop", SqlDbType.NVarChar).Value = Business.RoleDesktop;
zCommand.Parameters.Add("@RoleMobile", SqlDbType.NVarChar).Value = Business.RoleMobile;
zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Business.Slug;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Business.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Business.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Business.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Business.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Business.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "201 Created";
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE SYS_Business SET " 
            + " BusinessName = @BusinessName,"
            + " Description = @Description,"
            + " URL = @URL,"
            + " Rank = @Rank,"
            + " RoleDesktop = @RoleDesktop,"
            + " RoleMobile = @RoleMobile,"
            + " Slug = @Slug,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE BusinessKey = @BusinessKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Business.BusinessKey;
zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = Business.BusinessName;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Business.Description;
zCommand.Parameters.Add("@URL", SqlDbType.NVarChar).Value = Business.URL;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Business.Rank;
zCommand.Parameters.Add("@RoleDesktop", SqlDbType.NVarChar).Value = Business.RoleDesktop;
zCommand.Parameters.Add("@RoleMobile", SqlDbType.NVarChar).Value = Business.RoleMobile;
zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Business.Slug;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Business.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Business.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Business.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "200 OK";
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE SYS_Business SET RecordStatus = 99 WHERE BusinessKey = @BusinessKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Business.BusinessKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "200 OK";
}
catch (Exception Err)
{
_Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM SYS_Business WHERE BusinessKey = @BusinessKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Business.BusinessKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "200 OK";
}
catch (Exception Err)
{
_Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
