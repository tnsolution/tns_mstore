﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Order_Info
    {
        public Order_Model Order = new Order_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Order_Info()
        {
            Order.OrderKey = Guid.NewGuid().ToString();
        }
        public Order_Info(string OrderKey)
        {
            string zSQL = "SELECT * FROM FNC_Order WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Order.OrderKey = zReader["OrderKey"].ToString();
                    Order.OrderID = zReader["OrderID"].ToString();
                    if (zReader["OrderDate"] != DBNull.Value)
                    {
                        Order.OrderDate = (DateTime)zReader["OrderDate"];
                    }

                    if (zReader["StatusOrder"] != DBNull.Value)
                    {
                        Order.StatusOrder = int.Parse(zReader["StatusOrder"].ToString());
                    }

                    Order.StatusName = zReader["StatusName"].ToString();
                    Order.Description = zReader["Description"].ToString();
                    Order.ObjectKey = zReader["ObjectKey"].ToString();
                    Order.ObjectName = zReader["ObjectName"].ToString();
                    Order.SellerKey = zReader["SellerKey"].ToString();
                    Order.SellerName = zReader["SellerName"].ToString();
                    Order.BuyerKey = zReader["BuyerKey"].ToString();
                    Order.BuyerName = zReader["BuyerName"].ToString();
                    Order.BuyerPhone = zReader["BuyerPhone"].ToString();
                    Order.BuyerAddress = zReader["BuyerAddress"].ToString();
                    Order.BuyerEmail = zReader["BuyerEmail"].ToString();
                    Order.ShippingAddress = zReader["ShippingAddress"].ToString();
                    Order.ShippingCity = zReader["ShippingCity"].ToString();
                    Order.WarehouseKey = zReader["WarehouseKey"].ToString();
                    Order.WarehouseName = zReader["WarehouseName"].ToString();
                    if (zReader["TotalQuantity"] != DBNull.Value)
                    {
                        Order.TotalQuantity = float.Parse(zReader["TotalQuantity"].ToString());
                    }

                    if (zReader["AmountOrder"] != DBNull.Value)
                    {
                        Order.AmountOrder = double.Parse(zReader["AmountOrder"].ToString());
                    }

                    if (zReader["AmountDiscount"] != DBNull.Value)
                    {
                        Order.AmountDiscount = double.Parse(zReader["AmountDiscount"].ToString());
                    }

                    if (zReader["PercentDiscount"] != DBNull.Value)
                    {
                        Order.PercentDiscount = float.Parse(zReader["PercentDiscount"].ToString());
                    }

                    if (zReader["AmountOrderNoVAT"] != DBNull.Value)
                    {
                        Order.AmountOrderNoVAT = double.Parse(zReader["AmountOrderNoVAT"].ToString());
                    }

                    if (zReader["VAT"] != DBNull.Value)
                    {
                        Order.VAT = float.Parse(zReader["VAT"].ToString());
                    }

                    if (zReader["AmountVAT"] != DBNull.Value)
                    {
                        Order.AmountVAT = double.Parse(zReader["AmountVAT"].ToString());
                    }

                    if (zReader["AmountOrderIncVAT"] != DBNull.Value)
                    {
                        Order.AmountOrderIncVAT = double.Parse(zReader["AmountOrderIncVAT"].ToString());
                    }

                    if (zReader["AmountReceived"] != DBNull.Value)
                    {
                        Order.AmountReceived = double.Parse(zReader["AmountReceived"].ToString());
                    }

                    if (zReader["AmountReturned"] != DBNull.Value)
                    {
                        Order.AmountReturned = double.Parse(zReader["AmountReturned"].ToString());
                    }

                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Order.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Order.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Order.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    Order.CreditAccount = zReader["CreditAccount"].ToString();
                    Order.DebitAccount = zReader["DebitAccount"].ToString();
                    Order.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Order.Organization = zReader["Organization"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Order.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Order.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Order.CreatedBy = zReader["CreatedBy"].ToString();
                    Order.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Order.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Order.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Order.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Order ("
         + " OrderID , OrderDate , StatusOrder , StatusName , Description , ObjectKey , ObjectName , SellerKey , SellerName , BuyerKey , BuyerName , BuyerPhone , BuyerAddress , BuyerEmail , ShippingAddress , ShippingCity , WarehouseKey , WarehouseName , TotalQuantity , AmountOrder , AmountDiscount , PercentDiscount , AmountOrderNoVAT , VAT , AmountVAT , AmountOrderIncVAT , AmountReceived , AmountReturned , CategoryKey , CategoryName , Slug , CreditAccount , DebitAccount , PartnerNumber , Organization , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderID , @OrderDate , @StatusOrder , @StatusName , @Description , @ObjectKey , @ObjectName , @SellerKey , @SellerName , @BuyerKey , @BuyerName , @BuyerPhone , @BuyerAddress , @BuyerEmail , @ShippingAddress , @ShippingCity , @WarehouseKey , @WarehouseName , @TotalQuantity , @AmountOrder , @AmountDiscount , @PercentDiscount , @AmountOrderNoVAT , @VAT , @AmountVAT , @AmountOrderIncVAT , @AmountReceived , @AmountReturned , @CategoryKey , @CategoryName , @Slug , @CreditAccount , @DebitAccount , @PartnerNumber , @Organization , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Order.OrderID;
                if (Order.OrderDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Order.OrderDate;
                }

                zCommand.Parameters.Add("@StatusOrder", SqlDbType.Int).Value = Order.StatusOrder;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Order.StatusName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order.Description;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.NVarChar).Value = Order.ObjectKey;
                zCommand.Parameters.Add("@ObjectName", SqlDbType.NVarChar).Value = Order.ObjectName;
                if (Order.SellerKey != "" && Order.SellerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.SellerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = Order.SellerName;
                if (Order.BuyerKey != "" && Order.BuyerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.BuyerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Order.BuyerName;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Order.BuyerPhone;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Order.BuyerAddress;
                zCommand.Parameters.Add("@BuyerEmail", SqlDbType.NVarChar).Value = Order.BuyerEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Order.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Order.ShippingCity;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = Order.WarehouseKey;
                zCommand.Parameters.Add("@WarehouseName", SqlDbType.NVarChar).Value = Order.WarehouseName;
                zCommand.Parameters.Add("@TotalQuantity", SqlDbType.Float).Value = Order.TotalQuantity;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = Order.AmountOrder;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Order.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Order.PercentDiscount;
                zCommand.Parameters.Add("@AmountOrderNoVAT", SqlDbType.Money).Value = Order.AmountOrderNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Order.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Order.AmountVAT;
                zCommand.Parameters.Add("@AmountOrderIncVAT", SqlDbType.Money).Value = Order.AmountOrderIncVAT;
                zCommand.Parameters.Add("@AmountReceived", SqlDbType.Money).Value = Order.AmountReceived;
                zCommand.Parameters.Add("@AmountReturned", SqlDbType.Money).Value = Order.AmountReturned;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Order.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Order.CategoryName;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Order.DebitAccount;
                if (Order.PartnerNumber != "" && Order.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order.Organization;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Order("
         + " OrderKey , OrderID , OrderDate , StatusOrder , StatusName , Description , ObjectKey , ObjectName , SellerKey , SellerName , BuyerKey , BuyerName , BuyerPhone , BuyerAddress , BuyerEmail , ShippingAddress , ShippingCity , WarehouseKey , WarehouseName , TotalQuantity , AmountOrder , AmountDiscount , PercentDiscount , AmountOrderNoVAT , VAT , AmountVAT , AmountOrderIncVAT , AmountReceived , AmountReturned , CategoryKey , CategoryName , Slug , CreditAccount , DebitAccount , PartnerNumber , Organization , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @OrderID , @OrderDate , @StatusOrder , @StatusName , @Description , @ObjectKey , @ObjectName , @SellerKey , @SellerName , @BuyerKey , @BuyerName , @BuyerPhone , @BuyerAddress , @BuyerEmail , @ShippingAddress , @ShippingCity , @WarehouseKey , @WarehouseName , @TotalQuantity , @AmountOrder , @AmountDiscount , @PercentDiscount , @AmountOrderNoVAT , @VAT , @AmountVAT , @AmountOrderIncVAT , @AmountReceived , @AmountReturned , @CategoryKey , @CategoryName , @Slug , @CreditAccount , @DebitAccount , @PartnerNumber , @Organization , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Order.OrderKey != "" && Order.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                }
                else
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Order.OrderID;
                if (Order.OrderDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Order.OrderDate;
                }

                zCommand.Parameters.Add("@StatusOrder", SqlDbType.Int).Value = Order.StatusOrder;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Order.StatusName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order.Description;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.NVarChar).Value = Order.ObjectKey;
                zCommand.Parameters.Add("@ObjectName", SqlDbType.NVarChar).Value = Order.ObjectName;
                if (Order.SellerKey != "" && Order.SellerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.SellerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = Order.SellerName;
                if (Order.BuyerKey != "" && Order.BuyerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.BuyerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Order.BuyerName;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Order.BuyerPhone;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Order.BuyerAddress;
                zCommand.Parameters.Add("@BuyerEmail", SqlDbType.NVarChar).Value = Order.BuyerEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Order.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Order.ShippingCity;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = Order.WarehouseKey;
                zCommand.Parameters.Add("@WarehouseName", SqlDbType.NVarChar).Value = Order.WarehouseName;
                zCommand.Parameters.Add("@TotalQuantity", SqlDbType.Float).Value = Order.TotalQuantity;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = Order.AmountOrder;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Order.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Order.PercentDiscount;
                zCommand.Parameters.Add("@AmountOrderNoVAT", SqlDbType.Money).Value = Order.AmountOrderNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Order.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Order.AmountVAT;
                zCommand.Parameters.Add("@AmountOrderIncVAT", SqlDbType.Money).Value = Order.AmountOrderIncVAT;
                zCommand.Parameters.Add("@AmountReceived", SqlDbType.Money).Value = Order.AmountReceived;
                zCommand.Parameters.Add("@AmountReturned", SqlDbType.Money).Value = Order.AmountReturned;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Order.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Order.CategoryName;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Order.DebitAccount;
                if (Order.PartnerNumber != "" && Order.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order.Organization;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Order SET "
                        + " OrderID = @OrderID,"
                        + " OrderDate = @OrderDate,"
                        + " StatusOrder = @StatusOrder,"
                        + " StatusName = @StatusName,"
                        + " Description = @Description,"
                        + " ObjectKey = @ObjectKey,"
                        + " ObjectName = @ObjectName,"
                        + " SellerKey = @SellerKey,"
                        + " SellerName = @SellerName,"
                        + " BuyerKey = @BuyerKey,"
                        + " BuyerName = @BuyerName,"
                        + " BuyerPhone = @BuyerPhone,"
                        + " BuyerAddress = @BuyerAddress,"
                        + " BuyerEmail = @BuyerEmail,"
                        + " ShippingAddress = @ShippingAddress,"
                        + " ShippingCity = @ShippingCity,"
                        + " WarehouseKey = @WarehouseKey,"
                        + " WarehouseName = @WarehouseName,"
                        + " TotalQuantity = @TotalQuantity,"
                        + " AmountOrder = @AmountOrder,"
                        + " AmountDiscount = @AmountDiscount,"
                        + " PercentDiscount = @PercentDiscount,"
                        + " AmountOrderNoVAT = @AmountOrderNoVAT,"
                        + " VAT = @VAT,"
                        + " AmountVAT = @AmountVAT,"
                        + " AmountOrderIncVAT = @AmountOrderIncVAT,"
                        + " AmountReceived = @AmountReceived,"
                        + " AmountReturned = @AmountReturned,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " Slug = @Slug,"
                        + " CreditAccount = @CreditAccount,"
                        + " DebitAccount = @DebitAccount,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Organization = @Organization,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE OrderKey = @OrderKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Order.OrderKey != "" && Order.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                }
                else
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Order.OrderID;
                if (Order.OrderDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Order.OrderDate;
                }

                zCommand.Parameters.Add("@StatusOrder", SqlDbType.Int).Value = Order.StatusOrder;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Order.StatusName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order.Description;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.NVarChar).Value = Order.ObjectKey;
                zCommand.Parameters.Add("@ObjectName", SqlDbType.NVarChar).Value = Order.ObjectName;
                if (Order.SellerKey != "" && Order.SellerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.SellerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = Order.SellerName;
                if (Order.BuyerKey != "" && Order.BuyerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.BuyerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Order.BuyerName;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Order.BuyerPhone;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Order.BuyerAddress;
                zCommand.Parameters.Add("@BuyerEmail", SqlDbType.NVarChar).Value = Order.BuyerEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Order.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Order.ShippingCity;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = Order.WarehouseKey;
                zCommand.Parameters.Add("@WarehouseName", SqlDbType.NVarChar).Value = Order.WarehouseName;
                zCommand.Parameters.Add("@TotalQuantity", SqlDbType.Float).Value = Order.TotalQuantity;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = Order.AmountOrder;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Order.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Order.PercentDiscount;
                zCommand.Parameters.Add("@AmountOrderNoVAT", SqlDbType.Money).Value = Order.AmountOrderNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Order.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Order.AmountVAT;
                zCommand.Parameters.Add("@AmountOrderIncVAT", SqlDbType.Money).Value = Order.AmountOrderIncVAT;
                zCommand.Parameters.Add("@AmountReceived", SqlDbType.Money).Value = Order.AmountReceived;
                zCommand.Parameters.Add("@AmountReturned", SqlDbType.Money).Value = Order.AmountReturned;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Order.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Order.CategoryName;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Order.DebitAccount;
                if (Order.PartnerNumber != "" && Order.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order.Organization;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Order SET RecordStatus = 99 WHERE OrderKey = @OrderKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Order WHERE OrderKey = @OrderKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(string OrderKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE FNC_Order SET 
RecordStatus = 99, 
StatusOrder = 99, 
StatusName = N'Đã hủy', 
ModifiedOn = GetDate(), 
ModifiedBy = @ModifiedBy, 
ModifiedName = @ModifiedName
WHERE OrderKey = @OrderKey
UPDATE FNC_Order_Item SET RecordStatus = 99 WHERE OrderKey = @OrderKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE FNC_Order SET 
StatusOrder = @StatusOrder, 
StatusName = @StatusName,
ModifiedOn = GetDate(), 
ModifiedBy = @ModifiedBy, 
ModifiedName = @ModifiedName
WHERE OrderKey = @OrderKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                zCommand.Parameters.Add("@StatusOrder", SqlDbType.Int).Value = Order.StatusOrder;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Order.StatusName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
