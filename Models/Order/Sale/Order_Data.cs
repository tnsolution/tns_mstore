﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Order_Data
    {
        public static double SumOrder(string PartnerNumber, string OrderID, out string Message)
        {
            double zResult = 0;
            string zSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Receipt WHERE DocumentID = @OrderID AND RecordStatus <> 99";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID", SqlDbType.UniqueIdentifier).Value = OrderID;
                zResult = zCommand.ExecuteNonQuery().ToDouble();
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static List<Order_Model> List(string PartnerNumber, out string Message, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND OrderDate BETWEEN @FromDate AND @ToDate ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    StatusOrder = r["StatusOrder"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectName = r["ObjectName"].ToString(),
                    SellerKey = r["SellerKey"].ToString(),
                    SellerName = r["SellerName"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    WarehouseKey = r["WarehouseKey"].ToString(),
                    WarehouseName = r["WarehouseName"].ToString(),
                    TotalQuantity = r["TotalQuantity"].ToFloat(),
                    AmountOrder = r["AmountOrder"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountOrderNoVAT = r["AmountOrderNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountOrderIncVAT = r["AmountOrderIncVAT"].ToDouble(),
                    AmountReceived = r["AmountReceived"].ToDouble(),
                    AmountReturned = r["AmountReturned"].ToDouble(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Order_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    StatusOrder = r["StatusOrder"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectName = r["ObjectName"].ToString(),
                    SellerKey = r["SellerKey"].ToString(),
                    SellerName = r["SellerName"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    WarehouseKey = r["WarehouseKey"].ToString(),
                    WarehouseName = r["WarehouseName"].ToString(),
                    TotalQuantity = r["TotalQuantity"].ToFloat(),
                    AmountOrder = r["AmountOrder"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountOrderNoVAT = r["AmountOrderNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountOrderIncVAT = r["AmountOrderIncVAT"].ToDouble(),
                    AmountReceived = r["AmountReceived"].ToDouble(),
                    AmountReturned = r["AmountReturned"].ToDouble(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Order_Item_Model> ListItem(string OrderKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Order_Item WHERE RecordStatus != 99 AND OrderKey = @OrderKey ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Item_Model> zList = new List<Order_Item_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Item_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    OrderKey = r["OrderKey"].ToString(),
                    ObjectTable = r["ObjectTable"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectName = r["ObjectName"].ToString(),
                    ObjectPrice = r["ObjectPrice"].ToDouble(),
                    Quantity = r["Quantity"].ToFloat(),
                    UnitKey = r["UnitKey"].ToInt(),
                    UnitName = r["UnitName"].ToString(),
                    Amount = r["Amount"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountNoVAT = r["AmountNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountIncVAT = r["AmountIncVAT"].ToDouble(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
