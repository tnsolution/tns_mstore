﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Input_Product_Info
    {

        public Input_Product_Model Input_Product = new Input_Product_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Input_Product_Info()
        {
        }
        public Input_Product_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM IVT_Input_Product WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Input_Product.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Input_Product.OrderKey = zReader["OrderKey"].ToString();
                    Input_Product.ProductKey = zReader["ProductKey"].ToString();
                    Input_Product.ProductID = zReader["ProductID"].ToString();
                    Input_Product.ProductName = zReader["ProductName"].ToString();
                    if (zReader["PurchasePrice"] != DBNull.Value)
                    {
                        Input_Product.PurchasePrice = double.Parse(zReader["PurchasePrice"].ToString());
                    }

                    if (zReader["SalePrice"] != DBNull.Value)
                    {
                        Input_Product.SalePrice = double.Parse(zReader["SalePrice"].ToString());
                    }

                    if (zReader["QuantityDoc"] != DBNull.Value)
                    {
                        Input_Product.QuantityDoc = float.Parse(zReader["QuantityDoc"].ToString());
                    }

                    if (zReader["QuantityFact"] != DBNull.Value)
                    {
                        Input_Product.QuantityFact = float.Parse(zReader["QuantityFact"].ToString());
                    }

                    if (zReader["UnitKey"] != DBNull.Value)
                    {
                        Input_Product.UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    }

                    Input_Product.UnitName = zReader["UnitName"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                    {
                        Input_Product.Amount = double.Parse(zReader["Amount"].ToString());
                    }

                    if (zReader["AmountDiscount"] != DBNull.Value)
                    {
                        Input_Product.AmountDiscount = double.Parse(zReader["AmountDiscount"].ToString());
                    }

                    if (zReader["PercentDiscount"] != DBNull.Value)
                    {
                        Input_Product.PercentDiscount = float.Parse(zReader["PercentDiscount"].ToString());
                    }

                    if (zReader["AmountNoVAT"] != DBNull.Value)
                    {
                        Input_Product.AmountNoVAT = double.Parse(zReader["AmountNoVAT"].ToString());
                    }

                    if (zReader["VAT"] != DBNull.Value)
                    {
                        Input_Product.VAT = float.Parse(zReader["VAT"].ToString());
                    }

                    if (zReader["AmountVAT"] != DBNull.Value)
                    {
                        Input_Product.AmountVAT = double.Parse(zReader["AmountVAT"].ToString());
                    }

                    if (zReader["AmountIncVAT"] != DBNull.Value)
                    {
                        Input_Product.AmountIncVAT = double.Parse(zReader["AmountIncVAT"].ToString());
                    }

                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Input_Product.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Input_Product.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Input_Product.CreditAccount = zReader["CreditAccount"].ToString();
                    Input_Product.DebitAccount = zReader["DebitAccount"].ToString();
                    Input_Product.Description = zReader["Description"].ToString();
                    Input_Product.Organization = zReader["Organization"].ToString();
                    Input_Product.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Input_Product.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Input_Product.CreatedBy = zReader["CreatedBy"].ToString();
                    Input_Product.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Input_Product.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Input_Product.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Input_Product.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_Input_Product ("
         + " OrderKey , ProductKey , ProductID , ProductName , PurchasePrice , SalePrice , QuantityDoc , QuantityFact , UnitKey , UnitName , Amount , AmountDiscount , PercentDiscount , AmountNoVAT , VAT , AmountVAT , AmountIncVAT , Slug , RecordStatus , CreditAccount , DebitAccount , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @ProductKey , @ProductID , @ProductName , @PurchasePrice , @SalePrice , @QuantityDoc , @QuantityFact , @UnitKey , @UnitName , @Amount , @AmountDiscount , @PercentDiscount , @AmountNoVAT , @VAT , @AmountVAT , @AmountIncVAT , @Slug , @RecordStatus , @CreditAccount , @DebitAccount , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Input_Product.OrderKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Input_Product.ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Input_Product.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Input_Product.ProductName;
                zCommand.Parameters.Add("@PurchasePrice", SqlDbType.Money).Value = Input_Product.PurchasePrice;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = Input_Product.SalePrice;
                zCommand.Parameters.Add("@QuantityDoc", SqlDbType.Float).Value = Input_Product.QuantityDoc;
                zCommand.Parameters.Add("@QuantityFact", SqlDbType.Float).Value = Input_Product.QuantityFact;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Input_Product.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Input_Product.UnitName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = Input_Product.Amount;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Input_Product.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Input_Product.PercentDiscount;
                zCommand.Parameters.Add("@AmountNoVAT", SqlDbType.Money).Value = Input_Product.AmountNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Input_Product.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Input_Product.AmountVAT;
                zCommand.Parameters.Add("@AmountIncVAT", SqlDbType.Money).Value = Input_Product.AmountIncVAT;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Input_Product.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Input_Product.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Input_Product.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Input_Product.DebitAccount;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Input_Product.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Input_Product.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Input_Product.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Input_Product.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Input_Product.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Input_Product.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Input_Product.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_Input_Product("
         + " AutoKey , OrderKey , ProductKey , ProductID , ProductName , PurchasePrice , SalePrice , QuantityDoc , QuantityFact , UnitKey , UnitName , Amount , AmountDiscount , PercentDiscount , AmountNoVAT , VAT , AmountVAT , AmountIncVAT , Slug , RecordStatus , CreditAccount , DebitAccount , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @OrderKey , @ProductKey , @ProductID , @ProductName , @PurchasePrice , @SalePrice , @QuantityDoc , @QuantityFact , @UnitKey , @UnitName , @Amount , @AmountDiscount , @PercentDiscount , @AmountNoVAT , @VAT , @AmountVAT , @AmountIncVAT , @Slug , @RecordStatus , @CreditAccount , @DebitAccount , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Input_Product.AutoKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Input_Product.OrderKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Input_Product.ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Input_Product.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Input_Product.ProductName;
                zCommand.Parameters.Add("@PurchasePrice", SqlDbType.Money).Value = Input_Product.PurchasePrice;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = Input_Product.SalePrice;
                zCommand.Parameters.Add("@QuantityDoc", SqlDbType.Float).Value = Input_Product.QuantityDoc;
                zCommand.Parameters.Add("@QuantityFact", SqlDbType.Float).Value = Input_Product.QuantityFact;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Input_Product.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Input_Product.UnitName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = Input_Product.Amount;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Input_Product.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Input_Product.PercentDiscount;
                zCommand.Parameters.Add("@AmountNoVAT", SqlDbType.Money).Value = Input_Product.AmountNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Input_Product.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Input_Product.AmountVAT;
                zCommand.Parameters.Add("@AmountIncVAT", SqlDbType.Money).Value = Input_Product.AmountIncVAT;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Input_Product.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Input_Product.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Input_Product.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Input_Product.DebitAccount;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Input_Product.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Input_Product.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Input_Product.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Input_Product.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Input_Product.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Input_Product.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Input_Product.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE IVT_Input_Product SET "
                        + " OrderKey = @OrderKey,"
                        + " ProductKey = @ProductKey,"
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName,"
                        + " PurchasePrice = @PurchasePrice,"
                        + " SalePrice = @SalePrice,"
                        + " QuantityDoc = @QuantityDoc,"
                        + " QuantityFact = @QuantityFact,"
                        + " UnitKey = @UnitKey,"
                        + " UnitName = @UnitName,"
                        + " Amount = @Amount,"
                        + " AmountDiscount = @AmountDiscount,"
                        + " PercentDiscount = @PercentDiscount,"
                        + " AmountNoVAT = @AmountNoVAT,"
                        + " VAT = @VAT,"
                        + " AmountVAT = @AmountVAT,"
                        + " AmountIncVAT = @AmountIncVAT,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " CreditAccount = @CreditAccount,"
                        + " DebitAccount = @DebitAccount,"
                        + " Description = @Description,"
                        + " Organization = @Organization,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Input_Product.AutoKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Input_Product.OrderKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Input_Product.ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Input_Product.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Input_Product.ProductName;
                zCommand.Parameters.Add("@PurchasePrice", SqlDbType.Money).Value = Input_Product.PurchasePrice;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = Input_Product.SalePrice;
                zCommand.Parameters.Add("@QuantityDoc", SqlDbType.Float).Value = Input_Product.QuantityDoc;
                zCommand.Parameters.Add("@QuantityFact", SqlDbType.Float).Value = Input_Product.QuantityFact;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Input_Product.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Input_Product.UnitName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = Input_Product.Amount;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Input_Product.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Input_Product.PercentDiscount;
                zCommand.Parameters.Add("@AmountNoVAT", SqlDbType.Money).Value = Input_Product.AmountNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Input_Product.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Input_Product.AmountVAT;
                zCommand.Parameters.Add("@AmountIncVAT", SqlDbType.Money).Value = Input_Product.AmountIncVAT;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Input_Product.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Input_Product.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Input_Product.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Input_Product.DebitAccount;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Input_Product.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Input_Product.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Input_Product.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Input_Product.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Input_Product.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Input_Product SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Input_Product.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM IVT_Input_Product WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Input_Product.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
