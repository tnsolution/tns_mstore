﻿using System;
namespace TNStores
{
    public class Input_Product_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _OrderKey = "";
        private string _ProductKey = "";
        private string _ProductID = "";
        private string _ProductName = "";
        private double _PurchasePrice = 0;
        private double _SalePrice = 0;
        private float _QuantityDoc = 0;
        private float _QuantityFact = 0;
        private int _UnitKey = 0;
        private string _UnitName = "";
        private double _Amount = 0;
        private double _AmountDiscount = 0;
        private float _PercentDiscount = 0;
        private double _AmountNoVAT = 0;
        private float _VAT = 0;
        private double _AmountVAT = 0;
        private double _AmountIncVAT = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private string _Description = "";
        private string _Organization = "";
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public double PurchasePrice
        {
            get { return _PurchasePrice; }
            set { _PurchasePrice = value; }
        }
        public double SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }
        public float QuantityDoc
        {
            get { return _QuantityDoc; }
            set { _QuantityDoc = value; }
        }
        public float QuantityFact
        {
            get { return _QuantityFact; }
            set { _QuantityFact = value; }
        }
        public int UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public double AmountDiscount
        {
            get { return _AmountDiscount; }
            set { _AmountDiscount = value; }
        }
        public float PercentDiscount
        {
            get { return _PercentDiscount; }
            set { _PercentDiscount = value; }
        }
        public double AmountNoVAT
        {
            get { return _AmountNoVAT; }
            set { _AmountNoVAT = value; }
        }
        public float VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public double AmountVAT
        {
            get { return _AmountVAT; }
            set { _AmountVAT = value; }
        }
        public double AmountIncVAT
        {
            get { return _AmountIncVAT; }
            set { _AmountIncVAT = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
