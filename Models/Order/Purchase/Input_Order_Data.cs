﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Input_Order_Data
    {
        public static List<Input_Order_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Input_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Input_Order_Model> zList = new List<Input_Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Input_Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    StatusOrder = r["StatusOrder"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    Deliverer = r["Deliverer"].ToString(),
                    DocumentAttached = r["DocumentAttached"].ToString(),
                    DocumentDate = (r["DocumentDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DocumentDate"]),
                    DocumentOrigin = r["DocumentOrigin"].ToString(),
                    AmountOrder = r["AmountOrder"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountOrderNoVAT = r["AmountOrderNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountOrderIncVAT = r["AmountOrderIncVAT"].ToDouble(),
                    VendorKey = r["VendorKey"].ToString(),
                    VendorTax = r["VendorTax"].ToString(),
                    VendorName = r["VendorName"].ToString(),
                    VendorPhone = r["VendorPhone"].ToString(),
                    VendorAddress = r["VendorAddress"].ToString(),
                    VandorBank = r["VandorBank"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    WarehouseKey = r["WarehouseKey"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Slug = r["Slug"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    Organization = r["Organization"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Input_Order_Model> List(string PartnerNumber, out string Message, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Input_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND OrderDate BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Input_Order_Model> zList = new List<Input_Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Input_Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    StatusOrder = r["StatusOrder"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    Deliverer = r["Deliverer"].ToString(),
                    DocumentAttached = r["DocumentAttached"].ToString(),
                    DocumentDate = (r["DocumentDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DocumentDate"]),
                    DocumentOrigin = r["DocumentOrigin"].ToString(),
                    AmountOrder = r["AmountOrder"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountOrderNoVAT = r["AmountOrderNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountOrderIncVAT = r["AmountOrderIncVAT"].ToDouble(),
                    VendorKey = r["VendorKey"].ToString(),
                    VendorTax = r["VendorTax"].ToString(),
                    VendorName = r["VendorName"].ToString(),
                    VendorPhone = r["VendorPhone"].ToString(),
                    VendorAddress = r["VendorAddress"].ToString(),
                    VandorBank = r["VandorBank"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    WarehouseKey = r["WarehouseKey"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Slug = r["Slug"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    Organization = r["Organization"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
