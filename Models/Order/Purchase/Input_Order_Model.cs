﻿using System;
using System.Collections.Generic;

namespace TNStores
{
    public class Input_Order_Model
    {
        #region [ Field Name ]
        private float _TotalQuantity = 0;
        private string _OrderKey = "";
        private string _OrderID = "";
        private DateTime _OrderDate = DateTime.MinValue;
        private int _StatusOrder = 0;
        private string _StatusName = "";
        private string _Description = "";
        private string _Deliverer = "";
        private string _DocumentAttached = "";
        private DateTime _DocumentDate = DateTime.MinValue;
        private string _DocumentOrigin = "";
        private double _AmountOrder = 0;
        private double _AmountDiscount = 0;
        private float _PercentDiscount = 0;
        private double _AmountOrderNoVAT = 0;
        private float _VAT = 0;
        private double _AmountVAT = 0;
        private double _AmountOrderIncVAT = 0;
        private string _VendorKey = "";
        private string _VendorTax = "";
        private string _VendorName = "";
        private string _VendorPhone = "";
        private string _VendorAddress = "";
        private string _VandorBank = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private string _WarehouseKey = "";
        private int _RecordStatus = 0;
        private int _Slug = 0;
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private string _Organization = "";
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public int StatusOrder
        {
            get { return _StatusOrder; }
            set { _StatusOrder = value; }
        }
        public string StatusName
        {
            get { return _StatusName; }
            set { _StatusName = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Deliverer
        {
            get { return _Deliverer; }
            set { _Deliverer = value; }
        }
        public string DocumentAttached
        {
            get { return _DocumentAttached; }
            set { _DocumentAttached = value; }
        }
        public DateTime DocumentDate
        {
            get { return _DocumentDate; }
            set { _DocumentDate = value; }
        }
        public string DocumentOrigin
        {
            get { return _DocumentOrigin; }
            set { _DocumentOrigin = value; }
        }
        public double AmountOrder
        {
            get { return _AmountOrder; }
            set { _AmountOrder = value; }
        }
        public double AmountDiscount
        {
            get { return _AmountDiscount; }
            set { _AmountDiscount = value; }
        }
        public float PercentDiscount
        {
            get { return _PercentDiscount; }
            set { _PercentDiscount = value; }
        }
        public double AmountOrderNoVAT
        {
            get { return _AmountOrderNoVAT; }
            set { _AmountOrderNoVAT = value; }
        }
        public float VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public double AmountVAT
        {
            get { return _AmountVAT; }
            set { _AmountVAT = value; }
        }
        public double AmountOrderIncVAT
        {
            get { return _AmountOrderIncVAT; }
            set { _AmountOrderIncVAT = value; }
        }
        public string VendorKey
        {
            get { return _VendorKey; }
            set { _VendorKey = value; }
        }
        public string VendorTax
        {
            get { return _VendorTax; }
            set { _VendorTax = value; }
        }
        public string VendorName
        {
            get { return _VendorName; }
            set { _VendorName = value; }
        }
        public string VendorPhone
        {
            get { return _VendorPhone; }
            set { _VendorPhone = value; }
        }
        public string VendorAddress
        {
            get { return _VendorAddress; }
            set { _VendorAddress = value; }
        }
        public string VandorBank
        {
            get { return _VandorBank; }
            set { _VandorBank = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public string WarehouseKey
        {
            get { return _WarehouseKey; }
            set { _WarehouseKey = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public float TotalQuantity
        {
            get
            {
                return _TotalQuantity;
            }

            set
            {
                _TotalQuantity = value;
            }
        }
        #endregion

        public List<Input_Product_Model> ListItem = new List<Input_Product_Model>();
    }
}
