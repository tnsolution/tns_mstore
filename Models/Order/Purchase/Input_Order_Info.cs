﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Input_Order_Info
    {

        public Input_Order_Model Input_Order = new Input_Order_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Input_Order_Info()
        {
            Input_Order.OrderKey = Guid.NewGuid().ToString();
        }
        public Input_Order_Info(string OrderKey)
        {
            string zSQL = "SELECT * FROM IVT_Input_Order WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Input_Order.OrderKey = zReader["OrderKey"].ToString();
                    Input_Order.OrderID = zReader["OrderID"].ToString();
                    if (zReader["OrderDate"] != DBNull.Value)
                    {
                        Input_Order.OrderDate = (DateTime)zReader["OrderDate"];
                    }

                    if (zReader["StatusOrder"] != DBNull.Value)
                    {
                        Input_Order.StatusOrder = int.Parse(zReader["StatusOrder"].ToString());
                    }

                    Input_Order.StatusName = zReader["StatusName"].ToString();
                    Input_Order.Description = zReader["Description"].ToString();
                    Input_Order.Deliverer = zReader["Deliverer"].ToString();
                    Input_Order.DocumentAttached = zReader["DocumentAttached"].ToString();
                    if (zReader["DocumentDate"] != DBNull.Value)
                    {
                        Input_Order.DocumentDate = (DateTime)zReader["DocumentDate"];
                    }

                    Input_Order.DocumentOrigin = zReader["DocumentOrigin"].ToString();
                    if (zReader["AmountOrder"] != DBNull.Value)
                    {
                        Input_Order.AmountOrder = double.Parse(zReader["AmountOrder"].ToString());
                    }

                    if (zReader["AmountDiscount"] != DBNull.Value)
                    {
                        Input_Order.AmountDiscount = double.Parse(zReader["AmountDiscount"].ToString());
                    }

                    if (zReader["PercentDiscount"] != DBNull.Value)
                    {
                        Input_Order.PercentDiscount = float.Parse(zReader["PercentDiscount"].ToString());
                    }

                    if (zReader["AmountOrderNoVAT"] != DBNull.Value)
                    {
                        Input_Order.AmountOrderNoVAT = double.Parse(zReader["AmountOrderNoVAT"].ToString());
                    }

                    if (zReader["VAT"] != DBNull.Value)
                    {
                        Input_Order.VAT = float.Parse(zReader["VAT"].ToString());
                    }

                    if (zReader["AmountVAT"] != DBNull.Value)
                    {
                        Input_Order.AmountVAT = double.Parse(zReader["AmountVAT"].ToString());
                    }

                    if (zReader["AmountOrderIncVAT"] != DBNull.Value)
                    {
                        Input_Order.AmountOrderIncVAT = double.Parse(zReader["AmountOrderIncVAT"].ToString());
                    }

                    Input_Order.VendorKey = zReader["VendorKey"].ToString();
                    Input_Order.VendorTax = zReader["VendorTax"].ToString();
                    Input_Order.VendorName = zReader["VendorName"].ToString();
                    Input_Order.VendorPhone = zReader["VendorPhone"].ToString();
                    Input_Order.VendorAddress = zReader["VendorAddress"].ToString();
                    Input_Order.VandorBank = zReader["VandorBank"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Input_Order.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Input_Order.CategoryName = zReader["CategoryName"].ToString();
                    Input_Order.WarehouseKey = zReader["WarehouseKey"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Input_Order.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Input_Order.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    Input_Order.CreditAccount = zReader["CreditAccount"].ToString();
                    Input_Order.DebitAccount = zReader["DebitAccount"].ToString();
                    Input_Order.Organization = zReader["Organization"].ToString();
                    Input_Order.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Input_Order.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Input_Order.CreatedBy = zReader["CreatedBy"].ToString();
                    Input_Order.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Input_Order.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Input_Order.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Input_Order.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_Input_Order ("
         + " OrderID , OrderDate , StatusOrder , StatusName , Description , Deliverer , DocumentAttached , DocumentDate , DocumentOrigin , AmountOrder , AmountDiscount , PercentDiscount , AmountOrderNoVAT , VAT , AmountVAT , AmountOrderIncVAT , VendorKey , VendorTax , VendorName , VendorPhone , VendorAddress , VandorBank , CategoryKey , CategoryName , WarehouseKey , RecordStatus , Slug , CreditAccount , DebitAccount , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderID , @OrderDate , @StatusOrder , @StatusName , @Description , @Deliverer , @DocumentAttached , @DocumentDate , @DocumentOrigin , @AmountOrder , @AmountDiscount , @PercentDiscount , @AmountOrderNoVAT , @VAT , @AmountVAT , @AmountOrderIncVAT , @VendorKey , @VendorTax , @VendorName , @VendorPhone , @VendorAddress , @VandorBank , @CategoryKey , @CategoryName , @WarehouseKey , @RecordStatus , @Slug , @CreditAccount , @DebitAccount , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Input_Order.OrderID;
                if (Input_Order.OrderDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Input_Order.OrderDate;
                }

                zCommand.Parameters.Add("@StatusOrder", SqlDbType.Int).Value = Input_Order.StatusOrder;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Input_Order.StatusName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Input_Order.Description;
                zCommand.Parameters.Add("@Deliverer", SqlDbType.NVarChar).Value = Input_Order.Deliverer;
                zCommand.Parameters.Add("@DocumentAttached", SqlDbType.NVarChar).Value = Input_Order.DocumentAttached;
                if (Input_Order.DocumentDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = Input_Order.DocumentDate;
                }

                zCommand.Parameters.Add("@DocumentOrigin", SqlDbType.NVarChar).Value = Input_Order.DocumentOrigin;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = Input_Order.AmountOrder;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Input_Order.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Input_Order.PercentDiscount;
                zCommand.Parameters.Add("@AmountOrderNoVAT", SqlDbType.Money).Value = Input_Order.AmountOrderNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Input_Order.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Input_Order.AmountVAT;
                zCommand.Parameters.Add("@AmountOrderIncVAT", SqlDbType.Money).Value = Input_Order.AmountOrderIncVAT;
                zCommand.Parameters.Add("@VendorKey", SqlDbType.NVarChar).Value = Input_Order.VendorKey;
                zCommand.Parameters.Add("@VendorTax", SqlDbType.NVarChar).Value = Input_Order.VendorTax;
                zCommand.Parameters.Add("@VendorName", SqlDbType.NVarChar).Value = Input_Order.VendorName;
                zCommand.Parameters.Add("@VendorPhone", SqlDbType.NVarChar).Value = Input_Order.VendorPhone;
                zCommand.Parameters.Add("@VendorAddress", SqlDbType.NVarChar).Value = Input_Order.VendorAddress;
                zCommand.Parameters.Add("@VandorBank", SqlDbType.NVarChar).Value = Input_Order.VandorBank;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Input_Order.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Input_Order.CategoryName;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = Input_Order.WarehouseKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Input_Order.RecordStatus;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Input_Order.Slug;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Input_Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Input_Order.DebitAccount;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Input_Order.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Input_Order.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Input_Order.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Input_Order.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Input_Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Input_Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_Input_Order("
         + " OrderKey , OrderID , OrderDate , StatusOrder , StatusName , Description , Deliverer , DocumentAttached , DocumentDate , DocumentOrigin , AmountOrder , AmountDiscount , PercentDiscount , AmountOrderNoVAT , VAT , AmountVAT , AmountOrderIncVAT , VendorKey , VendorTax , VendorName , VendorPhone , VendorAddress , VandorBank , CategoryKey , CategoryName , WarehouseKey , RecordStatus , Slug , CreditAccount , DebitAccount , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @OrderID , @OrderDate , @StatusOrder , @StatusName , @Description , @Deliverer , @DocumentAttached , @DocumentDate , @DocumentOrigin , @AmountOrder , @AmountDiscount , @PercentDiscount , @AmountOrderNoVAT , @VAT , @AmountVAT , @AmountOrderIncVAT , @VendorKey , @VendorTax , @VendorName , @VendorPhone , @VendorAddress , @VandorBank , @CategoryKey , @CategoryName , @WarehouseKey , @RecordStatus , @Slug , @CreditAccount , @DebitAccount , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Input_Order.OrderKey != "" && Input_Order.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Input_Order.OrderKey);
                }
                else
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Input_Order.OrderID;
                if (Input_Order.OrderDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Input_Order.OrderDate;
                }

                zCommand.Parameters.Add("@StatusOrder", SqlDbType.Int).Value = Input_Order.StatusOrder;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Input_Order.StatusName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Input_Order.Description;
                zCommand.Parameters.Add("@Deliverer", SqlDbType.NVarChar).Value = Input_Order.Deliverer;
                zCommand.Parameters.Add("@DocumentAttached", SqlDbType.NVarChar).Value = Input_Order.DocumentAttached;
                if (Input_Order.DocumentDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = Input_Order.DocumentDate;
                }

                zCommand.Parameters.Add("@DocumentOrigin", SqlDbType.NVarChar).Value = Input_Order.DocumentOrigin;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = Input_Order.AmountOrder;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Input_Order.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Input_Order.PercentDiscount;
                zCommand.Parameters.Add("@AmountOrderNoVAT", SqlDbType.Money).Value = Input_Order.AmountOrderNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Input_Order.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Input_Order.AmountVAT;
                zCommand.Parameters.Add("@AmountOrderIncVAT", SqlDbType.Money).Value = Input_Order.AmountOrderIncVAT;
                zCommand.Parameters.Add("@VendorKey", SqlDbType.NVarChar).Value = Input_Order.VendorKey;
                zCommand.Parameters.Add("@VendorTax", SqlDbType.NVarChar).Value = Input_Order.VendorTax;
                zCommand.Parameters.Add("@VendorName", SqlDbType.NVarChar).Value = Input_Order.VendorName;
                zCommand.Parameters.Add("@VendorPhone", SqlDbType.NVarChar).Value = Input_Order.VendorPhone;
                zCommand.Parameters.Add("@VendorAddress", SqlDbType.NVarChar).Value = Input_Order.VendorAddress;
                zCommand.Parameters.Add("@VandorBank", SqlDbType.NVarChar).Value = Input_Order.VandorBank;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Input_Order.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Input_Order.CategoryName;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = Input_Order.WarehouseKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Input_Order.RecordStatus;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Input_Order.Slug;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Input_Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Input_Order.DebitAccount;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Input_Order.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Input_Order.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Input_Order.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Input_Order.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Input_Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Input_Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE IVT_Input_Order SET "
                        + " OrderID = @OrderID,"
                        + " OrderDate = @OrderDate,"
                        + " StatusOrder = @StatusOrder,"
                        + " StatusName = @StatusName,"
                        + " Description = @Description,"
                        + " Deliverer = @Deliverer,"
                        + " DocumentAttached = @DocumentAttached,"
                        + " DocumentDate = @DocumentDate,"
                        + " DocumentOrigin = @DocumentOrigin,"
                        + " AmountOrder = @AmountOrder,"
                        + " AmountDiscount = @AmountDiscount,"
                        + " PercentDiscount = @PercentDiscount,"
                        + " AmountOrderNoVAT = @AmountOrderNoVAT,"
                        + " VAT = @VAT,"
                        + " AmountVAT = @AmountVAT,"
                        + " AmountOrderIncVAT = @AmountOrderIncVAT,"
                        + " VendorKey = @VendorKey,"
                        + " VendorTax = @VendorTax,"
                        + " VendorName = @VendorName,"
                        + " VendorPhone = @VendorPhone,"
                        + " VendorAddress = @VendorAddress,"
                        + " VandorBank = @VandorBank,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " WarehouseKey = @WarehouseKey,"
                        + " RecordStatus = @RecordStatus,"
                        + " Slug = @Slug,"
                        + " CreditAccount = @CreditAccount,"
                        + " DebitAccount = @DebitAccount,"
                        + " Organization = @Organization,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE OrderKey = @OrderKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Input_Order.OrderKey != "" && Input_Order.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Input_Order.OrderKey);
                }
                else
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Input_Order.OrderID;
                if (Input_Order.OrderDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Input_Order.OrderDate;
                }

                zCommand.Parameters.Add("@StatusOrder", SqlDbType.Int).Value = Input_Order.StatusOrder;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Input_Order.StatusName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Input_Order.Description;
                zCommand.Parameters.Add("@Deliverer", SqlDbType.NVarChar).Value = Input_Order.Deliverer;
                zCommand.Parameters.Add("@DocumentAttached", SqlDbType.NVarChar).Value = Input_Order.DocumentAttached;
                if (Input_Order.DocumentDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = Input_Order.DocumentDate;
                }

                zCommand.Parameters.Add("@DocumentOrigin", SqlDbType.NVarChar).Value = Input_Order.DocumentOrigin;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = Input_Order.AmountOrder;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Input_Order.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Input_Order.PercentDiscount;
                zCommand.Parameters.Add("@AmountOrderNoVAT", SqlDbType.Money).Value = Input_Order.AmountOrderNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Input_Order.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Input_Order.AmountVAT;
                zCommand.Parameters.Add("@AmountOrderIncVAT", SqlDbType.Money).Value = Input_Order.AmountOrderIncVAT;
                zCommand.Parameters.Add("@VendorKey", SqlDbType.NVarChar).Value = Input_Order.VendorKey;
                zCommand.Parameters.Add("@VendorTax", SqlDbType.NVarChar).Value = Input_Order.VendorTax;
                zCommand.Parameters.Add("@VendorName", SqlDbType.NVarChar).Value = Input_Order.VendorName;
                zCommand.Parameters.Add("@VendorPhone", SqlDbType.NVarChar).Value = Input_Order.VendorPhone;
                zCommand.Parameters.Add("@VendorAddress", SqlDbType.NVarChar).Value = Input_Order.VendorAddress;
                zCommand.Parameters.Add("@VandorBank", SqlDbType.NVarChar).Value = Input_Order.VandorBank;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Input_Order.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Input_Order.CategoryName;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = Input_Order.WarehouseKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Input_Order.RecordStatus;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Input_Order.Slug;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Input_Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Input_Order.DebitAccount;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Input_Order.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Input_Order.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Input_Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Input_Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Input_Order SET RecordStatus = 99 WHERE OrderKey = @OrderKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Input_Order.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM IVT_Input_Order WHERE OrderKey = @OrderKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Input_Order.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(string OrderKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE IVT_Input_Order SET RecordStatus = 99 WHERE OrderKey = @OrderKey
UPDATE IVT_Input_Product SET RecordStatus = 99 WHERE OrderKey = @OrderKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
