﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Input_Product_Data
    {
        public static List<Input_Product_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Input_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Input_Product_Model> zList = new List<Input_Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Input_Product_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    OrderKey = r["OrderKey"].ToString(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    PurchasePrice = r["PurchasePrice"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    QuantityDoc = r["QuantityDoc"].ToFloat(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    UnitKey = r["UnitKey"].ToInt(),
                    UnitName = r["UnitName"].ToString(),
                    Amount = r["Amount"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountNoVAT = r["AmountNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountIncVAT = r["AmountIncVAT"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    Description = r["Description"].ToString(),
                    Organization = r["Organization"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
