﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
public class Task_Data
{
public static List<Task_Model> List(string PartnerNumber,  out string Message)
{
DataTable zTable = new DataTable();
string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
string zConnectionString =ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close(); Message = string.Empty;
}
catch (Exception ex)
{
Message = ex.ToString();
}
List<Task_Model> zList = new List<Task_Model>();
foreach (DataRow r in zTable.Rows){zList.Add(new Task_Model() {TaskKey= r["TaskKey"].ToString(),
TaskCode= r["TaskCode"].ToString(),
TaskCodeName= r["TaskCodeName"].ToString(),
TaskFile= r["TaskFile"].ToString(),
Subject= r["Subject"].ToString(),
TaskContent= r["TaskContent"].ToString(),
TaskFor= r["TaskFor"].ToString(),
StartDate=(r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
DueDate=(r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
Duration= r["Duration"].ToFloat(),
StatusKey= r["StatusKey"].ToInt(),
StatusName= r["StatusName"].ToString(),
PriorityKey= r["PriorityKey"].ToInt(),
PriorityName= r["PriorityName"].ToString(),
Complete= r["Complete"].ToInt(),
CompleteDate=(r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
CategoryKey= r["CategoryKey"].ToInt(),
CategoryName= r["CategoryName"].ToString(),
GroupKey= r["GroupKey"].ToInt(),
GroupName= r["GroupName"].ToString(),
ParentKey= r["ParentKey"].ToString(),
CustomerKey= r["CustomerKey"].ToString(),
CustomerID= r["CustomerID"].ToString(),
CustomerName= r["CustomerName"].ToString(),
ContractKey= r["ContractKey"].ToString(),
ContractName= r["ContractName"].ToString(),
Reminder=(r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
ApproveBy= r["ApproveBy"].ToString(),
ApproveName= r["ApproveName"].ToString(),
OwnerBy= r["OwnerBy"].ToString(),
OwnerName= r["OwnerName"].ToString(),
Style= r["Style"].ToString(),
Class= r["Class"].ToString(),
CodeLine= r["CodeLine"].ToString(),
Slug= r["Slug"].ToInt(),
RecordStatus= r["RecordStatus"].ToInt(),
Publish= r["Publish"].ToBool(),
PartnerNumber= r["PartnerNumber"].ToString(),
CreatedOn=(r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
CreatedBy= r["CreatedBy"].ToString(),
CreatedName= r["CreatedName"].ToString(),
ModifiedOn=(r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
ModifiedBy= r["ModifiedBy"].ToString(),
ModifiedName= r["ModifiedName"].ToString(),
});}
return zList;
}
}
}
