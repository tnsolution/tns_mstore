﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class BalanceSheet_Month_Internal_Data
    {
        public static List<BalanceSheet_Month_Internal_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_BalanceSheet_Month_Internal WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<BalanceSheet_Month_Internal_Model> zList = new List<BalanceSheet_Month_Internal_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new BalanceSheet_Month_Internal_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    BalanceDate = (r["BalanceDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["BalanceDate"]),
                    Amount = r["Amount"].ToDouble(),
                    BranchKey = r["BranchKey"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                });
            }
            return zList;
        }

        public static int ListSearch(string PartnerNumber, DateTime FromDate,DateTime ToDate,out string Message)
        {
            DataTable zTable = new DataTable();
            int zCount = 0;
            string zSQL = "SELECT * FROM FNC_BalanceSheet_Month_Internal WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND BalanceDate BETWEEN @FromDate AND @ToDate ";           
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;              
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            if (zTable.Rows.Count > 0)
            {
               zCount = zTable.Rows.Count;
            }
           
            return zCount;
        }
    }
}
