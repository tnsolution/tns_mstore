﻿using System;
namespace TNStores
{
    public class CashPayment_Category_Model
    {
        #region [ Field Name ]
        private int _CategoryKey = 0;
        private string _PartnerNumber = "";
        private string _CategoryNameVN = "";
        private string _CategoryNameEN = "";
        private string _CategoryNameCN = "";
        private int _Rank = 0;
        private bool _IsFeeInside;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string CategoryNameVN
        {
            get { return _CategoryNameVN; }
            set { _CategoryNameVN = value; }
        }
        public string CategoryNameEN
        {
            get { return _CategoryNameEN; }
            set { _CategoryNameEN = value; }
        }
        public string CategoryNameCN
        {
            get { return _CategoryNameCN; }
            set { _CategoryNameCN = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public bool IsFeeInside
        {
            get { return _IsFeeInside; }
            set { _IsFeeInside = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
