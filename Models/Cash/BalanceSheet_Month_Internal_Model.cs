﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
public class BalanceSheet_Month_Internal_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private DateTime _BalanceDate = DateTime.MinValue;
private double _Amount = 0;
private int _BranchKey = 0;
private string _BranchName = "";
private int _DepartmentKey = 0;
private string _DepartmentName = "";
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private string _ModifiedBy = "";
private string _ModifiedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public DateTime BalanceDate
{
get { return _BalanceDate; }
set { _BalanceDate = value; }
}
public double Amount
{
get { return _Amount; }
set { _Amount = value; }
}
public int BranchKey
{
get { return _BranchKey; }
set { _BranchKey = value; }
}
public string BranchName
{
get { return _BranchName; }
set { _BranchName = value; }
}
public int DepartmentKey
{
get { return _DepartmentKey; }
set { _DepartmentKey = value; }
}
public string DepartmentName
{
get { return _DepartmentName; }
set { _DepartmentName = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
#endregion
}
}
