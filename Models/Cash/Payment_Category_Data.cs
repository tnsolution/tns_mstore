﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class CashPayment_Category_Data
    {
        public static List<CashPayment_Category_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_CashPayment_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<CashPayment_Category_Model> zList = new List<CashPayment_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new CashPayment_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CategoryNameVN = r["CategoryNameVN"].ToString(),
                    CategoryNameEN = r["CategoryNameEN"].ToString(),
                    CategoryNameCN = r["CategoryNameCN"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    IsFeeInside = r["IsFeeInside"].ToBool(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
