﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class CashPayment_Category_Info
    {

        public CashPayment_Category_Model CashPayment_Category = new CashPayment_Category_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public CashPayment_Category_Info()
        {
        }
        public CashPayment_Category_Info(int CategoryKey)
        {
            string zSQL = "SELECT * FROM FNC_CashPayment_Category WHERE CategoryKey = @CategoryKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        CashPayment_Category.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    CashPayment_Category.PartnerNumber = zReader["PartnerNumber"].ToString();
                    CashPayment_Category.CategoryNameVN = zReader["CategoryNameVN"].ToString();
                    CashPayment_Category.CategoryNameEN = zReader["CategoryNameEN"].ToString();
                    CashPayment_Category.CategoryNameCN = zReader["CategoryNameCN"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                    {
                        CashPayment_Category.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    if (zReader["IsFeeInside"] != DBNull.Value)
                    {
                        CashPayment_Category.IsFeeInside = (bool)zReader["IsFeeInside"];
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        CashPayment_Category.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        CashPayment_Category.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    CashPayment_Category.CreatedBy = zReader["CreatedBy"].ToString();
                    CashPayment_Category.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        CashPayment_Category.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    CashPayment_Category.ModifiedBy = zReader["ModifiedBy"].ToString();
                    CashPayment_Category.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_CashPayment_Category ("
         + " PartnerNumber , CategoryNameVN , CategoryNameEN , CategoryNameCN , Rank , IsFeeInside , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PartnerNumber , @CategoryNameVN , @CategoryNameEN , @CategoryNameCN , @Rank , @IsFeeInside , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (CashPayment_Category.PartnerNumber != "" && CashPayment_Category.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(CashPayment_Category.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CategoryNameVN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameVN;
                zCommand.Parameters.Add("@CategoryNameEN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameEN;
                zCommand.Parameters.Add("@CategoryNameCN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameCN;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = CashPayment_Category.Rank;
                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = CashPayment_Category.IsFeeInside;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = CashPayment_Category.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CashPayment_Category.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CashPayment_Category.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CashPayment_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CashPayment_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_CashPayment_Category("
         + " CategoryKey , PartnerNumber , CategoryNameVN , CategoryNameEN , CategoryNameCN , Rank , IsFeeInside , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CategoryKey , @PartnerNumber , @CategoryNameVN , @CategoryNameEN , @CategoryNameCN , @Rank , @IsFeeInside , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CashPayment_Category.CategoryKey;
                if (CashPayment_Category.PartnerNumber != "" && CashPayment_Category.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(CashPayment_Category.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CategoryNameVN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameVN;
                zCommand.Parameters.Add("@CategoryNameEN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameEN;
                zCommand.Parameters.Add("@CategoryNameCN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameCN;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = CashPayment_Category.Rank;
                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = CashPayment_Category.IsFeeInside;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = CashPayment_Category.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CashPayment_Category.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CashPayment_Category.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CashPayment_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CashPayment_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE FNC_CashPayment_Category SET "
                        + " PartnerNumber = @PartnerNumber,"
                        + " CategoryNameVN = @CategoryNameVN,"
                        + " CategoryNameEN = @CategoryNameEN,"
                        + " CategoryNameCN = @CategoryNameCN,"
                        + " Rank = @Rank,"
                        + " IsFeeInside = @IsFeeInside,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CategoryKey = @CategoryKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CashPayment_Category.CategoryKey;
                if (CashPayment_Category.PartnerNumber != "" && CashPayment_Category.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(CashPayment_Category.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CategoryNameVN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameVN;
                zCommand.Parameters.Add("@CategoryNameEN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameEN;
                zCommand.Parameters.Add("@CategoryNameCN", SqlDbType.NVarChar).Value = CashPayment_Category.CategoryNameCN;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = CashPayment_Category.Rank;
                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = CashPayment_Category.IsFeeInside;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = CashPayment_Category.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CashPayment_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CashPayment_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_CashPayment_Category SET RecordStatus = 99 WHERE CategoryKey = @CategoryKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CashPayment_Category.CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_CashPayment_Category WHERE CategoryKey = @CategoryKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CashPayment_Category.CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
