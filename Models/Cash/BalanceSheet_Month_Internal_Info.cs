﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class BalanceSheet_Month_Internal_Info
    {

        public BalanceSheet_Month_Internal_Model BalanceSheet_Month_Internal = new BalanceSheet_Month_Internal_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public BalanceSheet_Month_Internal_Info()
        {
        }
        public BalanceSheet_Month_Internal_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_BalanceSheet_Month_Internal WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    if (zReader["BalanceDate"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.BalanceDate = (DateTime)zReader["BalanceDate"];
                    }

                    if (zReader["Amount"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.Amount = double.Parse(zReader["Amount"].ToString());
                    }

                    if (zReader["BranchKey"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    }

                    BalanceSheet_Month_Internal.BranchName = zReader["BranchName"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    }

                    BalanceSheet_Month_Internal.DepartmentName = zReader["DepartmentName"].ToString();
                    BalanceSheet_Month_Internal.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    BalanceSheet_Month_Internal.CreatedBy = zReader["CreatedBy"].ToString();
                    BalanceSheet_Month_Internal.CreatedName = zReader["CreatedName"].ToString();
                    BalanceSheet_Month_Internal.ModifiedBy = zReader["ModifiedBy"].ToString();
                    BalanceSheet_Month_Internal.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        BalanceSheet_Month_Internal.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_BalanceSheet_Month_Internal ("
         + " BalanceDate , Amount , BranchKey , BranchName , DepartmentKey , DepartmentName , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @BalanceDate , @Amount , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (BalanceSheet_Month_Internal.BalanceDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = BalanceSheet_Month_Internal.BalanceDate;
                }

                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = BalanceSheet_Month_Internal.Amount;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.DepartmentName;
                if (BalanceSheet_Month_Internal.PartnerNumber != "" && BalanceSheet_Month_Internal.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(BalanceSheet_Month_Internal.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = BalanceSheet_Month_Internal.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_BalanceSheet_Month_Internal("
         + " AutoKey , BalanceDate , Amount , BranchKey , BranchName , DepartmentKey , DepartmentName , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @BalanceDate , @Amount , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.AutoKey;
                if (BalanceSheet_Month_Internal.BalanceDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = BalanceSheet_Month_Internal.BalanceDate;
                }

                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = BalanceSheet_Month_Internal.Amount;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.DepartmentName;
                if (BalanceSheet_Month_Internal.PartnerNumber != "" && BalanceSheet_Month_Internal.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(BalanceSheet_Month_Internal.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = BalanceSheet_Month_Internal.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_BalanceSheet_Month_Internal SET "
                        + " BalanceDate = @BalanceDate,"
                        + " Amount = @Amount,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.AutoKey;
                if (BalanceSheet_Month_Internal.BalanceDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = BalanceSheet_Month_Internal.BalanceDate;
                }

                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = BalanceSheet_Month_Internal.Amount;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.DepartmentName;
                if (BalanceSheet_Month_Internal.PartnerNumber != "" && BalanceSheet_Month_Internal.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(BalanceSheet_Month_Internal.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = BalanceSheet_Month_Internal.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_BalanceSheet_Month_Internal SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_BalanceSheet_Month_Internal WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update(int Month, int Year)
        {
            string zSQL = "UPDATE FNC_BalanceSheet_Month_Internal SET "
                        + " BalanceDate = @BalanceDate,"
                        + " Amount = @Amount,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE MONTH(BalanceDate) = @Month AND YEAR(BalanceDate) = @Year";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;

                if (BalanceSheet_Month_Internal.BalanceDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@BalanceDate", SqlDbType.DateTime).Value = BalanceSheet_Month_Internal.BalanceDate;
                }

                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = BalanceSheet_Month_Internal.Amount;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = BalanceSheet_Month_Internal.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.DepartmentName;
                if (BalanceSheet_Month_Internal.PartnerNumber != "" && BalanceSheet_Month_Internal.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(BalanceSheet_Month_Internal.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = BalanceSheet_Month_Internal.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = BalanceSheet_Month_Internal.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
