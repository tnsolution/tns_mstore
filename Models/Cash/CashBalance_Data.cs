﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TNStores
{
    public class CashBalance_Data
    {
        public static double Amount_Month(DateTime Time, string PartnerNumber)
        {
            
            DateTime FromDate = new DateTime(Time.Year, Time.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            double zResult = 0;
            string zSQL = @"
SELECT Amount 
FROM FNC_BalanceSheet_Month_Internal 
WHERE 
BalanceDate BETWEEN @FromDate AND @ToDate 
AND RecordStatus != 99 
AND PartnerNumber = @PartnerNumber  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
           
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zResult = double.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static List<CashBalance_Model> Inside(string PartnerNumber, DateTime FromDate, DateTime ToDate, bool IsFeeInside)
        {            
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT 'P' AS TYPE, A.PaymentKey AS ObjectKey, A.PaymentID AS ObjectID, A.PaymentDate AS ObjectDate, A.DocumentID, A.PaymentDescription AS ObjectDescription, 
    B.*, 
    A.AmountCurrencyMain AS PaymentAmountCurrencyMain, 
    '' AS ReceiptAmountCurrencyMain
	FROM FNC_Payment A 
	LEFT JOIN FNC_CashPayment_Category B ON A.CategoryKey = B.CategoryKey
	WHERE A.RecordStatus <> 99 AND A.PartnerNumber = @PartnerNumber AND A.PaymentDate BETWEEN @FromDate and @ToDate
	UNION ALL
	SELECT 'R' AS TYPE,  A.ReceiptKey AS ObjectKey, A.ReceiptID AS ObjectID, A.ReceiptDate AS ObjectDate, A.DocumentID, A.ReceiptDescription AS ObjectDescription, 
    B.*, 
    '' AS PaymentAmountCurrencyMain, 
    A.AmountCurrencyMain AS ReceiptAmountCurrencyMain
	FROM FNC_Receipt A 
	LEFT JOIN FNC_CashReceipt_Category B ON A.CategoryKey = B.CategoryKey
	WHERE A.RecordStatus <> 99 AND A.PartnerNumber = @PartnerNumber AND A.ReceiptDate BETWEEN @FromDate and @ToDate
)
SELECT * FROM X ORDER BY ObjectDate DESC

";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                //zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = IsFeeInside;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<CashBalance_Model> zList = new List<CashBalance_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new CashBalance_Model()
                {
                    TYPE = r["TYPE"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectID = r["ObjectID"].ToString(),                  
                    ObjectDate = (r["ObjectDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ObjectDate"]),
                    DocumentID = r["DocumentID"].ToString(),
                    ObjectDescription = r["ObjectDescription"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),                   
                    CategoryNameVN = r["CategoryNameVN"].ToString(),
                    CategoryNameEN = r["CategoryNameEN"].ToString(),
                    CategoryNameCN = r["CategoryNameCN"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    IsFeeInside = r["IsFeeInside"].ToBool(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                    PaymentAmountCurrencyMain = r["PaymentAmountCurrencyMain"].ToDouble(),
                    ReceiptAmountCurrencyMain = r["ReceiptAmountCurrencyMain"].ToDouble(),
                });
            }
            return zList;
        }
    }
}