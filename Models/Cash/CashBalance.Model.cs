﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TNStores
{
    public class CashBalance_Model
    {
        private string _TYPE = "";
        private string objectKey = "";
        private string objectID = "";
        private DateTime objectDate = DateTime.MinValue;
        private string documentID = "";
        private string objectDescription = "";
        private int categoryKey = 0;
        private string partnerNumber = "";
        private string categoryNameVN = "";
        private string categoryNameEN = "";
        private string categoryNameCN = "";
        private int rank = 0;
        private bool isFeeInside = true;
        private int recordStatus = 0;
        private DateTime createdOn = DateTime.MinValue;
        private string createdBy = "";
        private string createdName = "";
        private DateTime modifiedOn = DateTime.MinValue;
        private string modifiedBy = "";
        private string modifiedName = "";
        private double paymentAmountCurrencyMain = 0;
        private double receiptAmountCurrencyMain = 0;

        #region
        public string TYPE
        {
            get
            {
                return _TYPE;
            }

            set
            {
                _TYPE = value;
            }
        }

        public string ObjectKey
        {
            get
            {
                return objectKey;
            }

            set
            {
                objectKey = value;
            }
        }

        public string ObjectID
        {
            get
            {
                return objectID;
            }

            set
            {
                objectID = value;
            }
        }

        public DateTime ObjectDate
        {
            get
            {
                return objectDate;
            }

            set
            {
                objectDate = value;
            }
        }

        public string DocumentID
        {
            get
            {
                return documentID;
            }

            set
            {
                documentID = value;
            }
        }

        public string ObjectDescription
        {
            get
            {
                return objectDescription;
            }

            set
            {
                objectDescription = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return categoryKey;
            }

            set
            {
                categoryKey = value;
            }
        }

        public string PartnerNumber
        {
            get
            {
                return partnerNumber;
            }

            set
            {
                partnerNumber = value;
            }
        }

        public string CategoryNameVN
        {
            get
            {
                return categoryNameVN;
            }

            set
            {
                categoryNameVN = value;
            }
        }

        public string CategoryNameEN
        {
            get
            {
                return categoryNameEN;
            }

            set
            {
                categoryNameEN = value;
            }
        }

        public string CategoryNameCN
        {
            get
            {
                return categoryNameCN;
            }

            set
            {
                categoryNameCN = value;
            }
        }

        public int Rank
        {
            get
            {
                return rank;
            }

            set
            {
                rank = value;
            }
        }

        public bool IsFeeInside
        {
            get
            {
                return isFeeInside;
            }

            set
            {
                isFeeInside = value;
            }
        }

        public int RecordStatus
        {
            get
            {
                return recordStatus;
            }

            set
            {
                recordStatus = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }

            set
            {
                createdOn = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return createdBy;
            }

            set
            {
                createdBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return createdName;
            }

            set
            {
                createdName = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return modifiedOn;
            }

            set
            {
                modifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return modifiedBy;
            }

            set
            {
                modifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return modifiedName;
            }

            set
            {
                modifiedName = value;
            }
        }

        public double PaymentAmountCurrencyMain
        {
            get
            {
                return paymentAmountCurrencyMain;
            }

            set
            {
                paymentAmountCurrencyMain = value;
            }
        }

        public double ReceiptAmountCurrencyMain
        {
            get
            {
                return receiptAmountCurrencyMain;
            }

            set
            {
                receiptAmountCurrencyMain = value;
            }
        }
        #endregion
    }
}