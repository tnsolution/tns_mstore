﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Position_Info
    {

        public Position_Model Position = new Position_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Position_Info()
        {
        }
        public Position_Info(int PositionKey)
        {
            string zSQL = "SELECT * FROM HRM_ListPosition WHERE PositionKey = @PositionKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = PositionKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["PositionKey"] != DBNull.Value)
                        Position.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    Position.PositionNameEN = zReader["PositionNameEN"].ToString();
                    Position.PositionNameVN = zReader["PositionNameVN"].ToString();
                    Position.PositionNameCN = zReader["PositionNameCN"].ToString();
                    Position.PositionID = zReader["PositionID"].ToString();
                    Position.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["BusinessKey"] != DBNull.Value)
                        Position.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                    if (zReader["ParentKey"] != DBNull.Value)
                        Position.ParentKey = int.Parse(zReader["ParentKey"].ToString());
                    Position.Description = zReader["Description"].ToString();
                    if (zReader["TypeKey"] != DBNull.Value)
                        Position.TypeKey = int.Parse(zReader["TypeKey"].ToString());
                    Position.TypeName = zReader["TypeName"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        Position.Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Position.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Position.CreatedBy = zReader["CreatedBy"].ToString();
                    Position.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Position.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Position.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Position.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Position.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_ListPosition ("
         + " PositionNameEN , PositionNameVN , PositionNameCN , PositionID , PartnerNumber , BusinessKey , ParentKey , Description , TypeKey , TypeName , Rank , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName )"
         + " VALUES ( "
         + " @PositionNameEN , @PositionNameVN , @PositionNameCN , @PositionID , @PartnerNumber , @BusinessKey , @ParentKey , @Description , @TypeKey , @TypeName , @Rank , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName )";
        string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PositionNameEN", SqlDbType.NVarChar).Value = Position.PositionNameEN;
                zCommand.Parameters.Add("@PositionNameVN", SqlDbType.NVarChar).Value = Position.PositionNameVN;
                zCommand.Parameters.Add("@PositionNameCN", SqlDbType.NVarChar).Value = Position.PositionNameCN;
                zCommand.Parameters.Add("@PositionID", SqlDbType.NVarChar).Value = Position.PositionID;
                if (Position.PartnerNumber != "" && Position.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Position.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Position.BusinessKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = Position.ParentKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Position.Description;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = Position.TypeKey;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Position.TypeName;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Position.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Position.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Position.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Position.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Position.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Position.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_ListPosition("
         + " PositionKey , PositionNameEN , PositionNameVN , PositionNameCN , PositionID , PartnerNumber , BusinessKey , ParentKey , Description , TypeKey , TypeName , Rank , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName )"
         + " VALUES ( "
         + " @PositionKey , @PositionNameEN , @PositionNameVN , @PositionNameCN , @PositionID , @PartnerNumber , @BusinessKey , @ParentKey , @Description , @TypeKey , @TypeName , @Rank , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName )";
        string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Position.PositionKey;
                zCommand.Parameters.Add("@PositionNameEN", SqlDbType.NVarChar).Value = Position.PositionNameEN;
                zCommand.Parameters.Add("@PositionNameVN", SqlDbType.NVarChar).Value = Position.PositionNameVN;
                zCommand.Parameters.Add("@PositionNameCN", SqlDbType.NVarChar).Value = Position.PositionNameCN;
                zCommand.Parameters.Add("@PositionID", SqlDbType.NVarChar).Value = Position.PositionID;
                if (Position.PartnerNumber != "" && Position.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Position.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Position.BusinessKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = Position.ParentKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Position.Description;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = Position.TypeKey;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Position.TypeName;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Position.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Position.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Position.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Position.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Position.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Position.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_ListPosition SET "
                        + " PositionNameEN = @PositionNameEN,"
                        + " PositionNameVN = @PositionNameVN,"
                        + " PositionNameCN = @PositionNameCN,"
                        + " PositionID = @PositionID,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " BusinessKey = @BusinessKey,"
                        + " ParentKey = @ParentKey,"
                        + " Description = @Description,"
                        + " TypeKey = @TypeKey,"
                        + " TypeName = @TypeName,"
                        + " Rank = @Rank,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE PositionKey = @PositionKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Position.PositionKey;
                zCommand.Parameters.Add("@PositionNameEN", SqlDbType.NVarChar).Value = Position.PositionNameEN;
                zCommand.Parameters.Add("@PositionNameVN", SqlDbType.NVarChar).Value = Position.PositionNameVN;
                zCommand.Parameters.Add("@PositionNameCN", SqlDbType.NVarChar).Value = Position.PositionNameCN;
                zCommand.Parameters.Add("@PositionID", SqlDbType.NVarChar).Value = Position.PositionID;
                if (Position.PartnerNumber != "" && Position.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Position.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Position.BusinessKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = Position.ParentKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Position.Description;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = Position.TypeKey;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Position.TypeName;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Position.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Position.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Position.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Position.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_ListPosition SET RecordStatus = 99 WHERE PositionKey = @PositionKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Position.PositionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_ListPosition WHERE PositionKey = @PositionKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Position.PositionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
