﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Payroll_Person_Data
    {
        public static List<Payroll_Person_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Payroll_Person WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Payroll_Person_Model> zList = new List<Payroll_Person_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Person_Model()
                {
                    PayrollKey = r["PayrollKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    PositionName = r["PositionName"].ToString(),
                    Month = r["Month"].ToInt(),
                    Year = r["Year"].ToInt(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    TotalWorkTime = r["TotalWorkTime"].ToInt(),
                    TotalDateWorking = r["TotalDateWorking"].ToFloat(),
                    TotalDateLeaving = r["TotalDateLeaving"].ToFloat(),
                    TotalDatePaidLeave = r["TotalDatePaidLeave"].ToFloat(),
                    TotalDateUnpaidLeave = r["TotalDateUnpaidLeave"].ToFloat(),
                    TotalDateAnnualLeave = r["TotalDateAnnualLeave"].ToFloat(),
                    TotalDateDayOff = r["TotalDateDayOff"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    FromDate = (r["FromDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["FromDate"]),
                    ToDate = (r["ToDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ToDate"]),
                    Activated = r["Activated"].ToBool(),
                    ActivatedDate = (r["ActivatedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ActivatedDate"]),
                    Reference = r["Reference"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Payroll_Person_Model> List(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Payroll_Person WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Person_Model> zList = new List<Payroll_Person_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Person_Model()
                {
                    PayrollKey = r["PayrollKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    PositionName = r["PositionName"].ToString(),
                    Month = r["Month"].ToInt(),
                    Year = r["Year"].ToInt(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    TotalWorkTime = r["TotalWorkTime"].ToInt(),
                    TotalDateWorking = r["TotalDateWorking"].ToFloat(),
                    TotalDateLeaving = r["TotalDateLeaving"].ToFloat(),
                    TotalDatePaidLeave = r["TotalDatePaidLeave"].ToFloat(),
                    TotalDateUnpaidLeave = r["TotalDateUnpaidLeave"].ToFloat(),
                    TotalDateAnnualLeave = r["TotalDateAnnualLeave"].ToFloat(),
                    TotalDateDayOff = r["TotalDateDayOff"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    FromDate = (r["FromDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["FromDate"]),
                    ToDate = (r["ToDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ToDate"]),
                    Activated = r["Activated"].ToBool(),
                    ActivatedDate = (r["ActivatedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ActivatedDate"]),
                    Reference = r["Reference"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Payroll_Person_Model> ListDetail(string PayrollKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Payroll_Person WHERE RecordStatus != 99  AND PayrollKey = @PayrollKey";
          
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PayrollKey", SqlDbType.NVarChar).Value = PayrollKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Person_Model> zList = new List<Payroll_Person_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Person_Model()
                {
                    PayrollKey = r["PayrollKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    PositionName = r["PositionName"].ToString(),
                    Month = r["Month"].ToInt(),
                    Year = r["Year"].ToInt(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    TotalWorkTime = r["TotalWorkTime"].ToInt(),
                    TotalDateWorking = r["TotalDateWorking"].ToFloat(),
                    TotalDateLeaving = r["TotalDateLeaving"].ToFloat(),
                    TotalDatePaidLeave = r["TotalDatePaidLeave"].ToFloat(),
                    TotalDateUnpaidLeave = r["TotalDateUnpaidLeave"].ToFloat(),
                    TotalDateAnnualLeave = r["TotalDateAnnualLeave"].ToFloat(),
                    TotalDateDayOff = r["TotalDateDayOff"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    FromDate = (r["FromDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["FromDate"]),
                    ToDate = (r["ToDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ToDate"]),
                    Activated = r["Activated"].ToBool(),
                    ActivatedDate = (r["ActivatedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ActivatedDate"]),
                    Reference = r["Reference"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
