﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Branch_Data
    {
        public static List<Branch_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Branch WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Branch_Model> zList = new List<Branch_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Branch_Model()
                {
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    BranchID = r["BranchID"].ToString(),
                    Address = r["Address"].ToString(),
                    Ward = r["Ward"].ToString(),
                    District = r["District"].ToString(),
                    Province = r["Province"].ToString(),
                    Country = r["Country"].ToString(),
                    Description = r["Description"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
