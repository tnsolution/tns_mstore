﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Employee_Data
    {
        public static List<Employee_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }

        public static List<Employee_Model> ListReportTo(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT EmployeeKey, LastName, FirstName 
FROM HRM_Employee A
LEFT JOIN HRM_ListPosition B ON B.PositionKey=A.PositionKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber 
AND B.[TypeKey] = 1  
ORDER BY B.Rank ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> Search(string PartnerNumber, string SearchName, string Department, DateTime FromDate, DateTime ToDate, int FromAge, int ToAge, int Gender, string Edu, string Position)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND A.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.StartingDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND (LastName + ' ' + FirstName) LIKE @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, A.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender >= 0)
            {
                zSQL += " AND A.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND A.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND A.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND A.PositionKey = @Position";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Employee_Model> zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    PhotoPath = r["PhotoPath"].ToString(),
                });
            }
            return zList;
        }

        public static string AutoEmployeeID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_EmployeeID(@Prefix, @PartnerNumber)";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }

        public static List<Employee_Model> List(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }
    }
}
