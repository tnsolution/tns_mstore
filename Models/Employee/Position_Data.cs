﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Position_Data
    {
        public static List<Position_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_ListPosition WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY Rank ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Position_Model> zList = new List<Position_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Position_Model()
                {
                    PositionKey = r["PositionKey"].ToInt(),
                    PositionNameEN = r["PositionNameEN"].ToString(),
                    PositionNameVN = r["PositionNameVN"].ToString(),
                    PositionNameCN = r["PositionNameCN"].ToString(),
                    PositionID = r["PositionID"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    BusinessKey = r["BusinessKey"].ToInt(),
                    ParentKey = r["ParentKey"].ToInt(),
                    Description = r["Description"].ToString(),
                    TypeKey = r["TypeKey"].ToInt(),
                    TypeName = r["TypeName"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                });
            }
            return zList;
        }
    }
}
