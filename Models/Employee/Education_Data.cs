﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Education_Data
    {
        public static List<Education_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Education WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Education_Model> zList = new List<Education_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Education_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    DegreeName = r["DegreeName"].ToString(),
                    DegreeBy = r["DegreeBy"].ToString(),
                    DegreePlace = r["DegreePlace"].ToString(),
                    FromDate = (r["FromDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["FromDate"]),
                    ToDate = (r["ToDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ToDate"]),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    ClassifiedKey = r["ClassifiedKey"].ToInt(),
                    ClassifiedName = r["ClassifiedName"].ToString(),
                    TypeName = r["TypeName"].ToString(),
                    ExpireDate = (r["ExpireDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ExpireDate"]),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Education_Model> ListEdu(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Education WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Employee.Length >= 36)
            {
                zSQL += " AND EmployeeKey = @Employee";
            }
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Education_Model> zList = new List<Education_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Education_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    DegreeName = r["DegreeName"].ToString(),
                    DegreeBy = r["DegreeBy"].ToString(),
                    DegreePlace = r["DegreePlace"].ToString(),
                    FromDate = (r["FromDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["FromDate"]),
                    ToDate = (r["ToDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ToDate"]),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    ClassifiedKey = r["ClassifiedKey"].ToInt(),
                    ClassifiedName = r["ClassifiedName"].ToString(),
                    TypeName = r["TypeName"].ToString(),
                    ExpireDate = (r["ExpireDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ExpireDate"]),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
