﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
public class HistoryInsure_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private string _EmployeeKey = "";
private DateTime _DateWrite = DateTime.MinValue;
private string _Description = "";
private string _StatusName = "";
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public string EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public DateTime DateWrite
{
get { return _DateWrite; }
set { _DateWrite = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public string StatusName
{
get { return _StatusName; }
set { _StatusName = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
#endregion
}
}
