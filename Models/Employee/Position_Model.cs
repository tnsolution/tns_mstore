﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Position_Model
    {
        #region [ Field Name ]
        private int _PositionKey = 0;
        private string _PositionNameEN = "";
        private string _PositionNameVN = "";
        private string _PositionNameCN = "";
        private string _PositionID = "";
        private string _PartnerNumber = "";
        private int _BusinessKey = 0;
        private int _ParentKey = 0;
        private string _Description = "";
        private int _TypeKey = 0;
        private string _TypeName = "";
        private int _Rank = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        #endregion

        #region [ Properties ]
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string PositionNameEN
        {
            get { return _PositionNameEN; }
            set { _PositionNameEN = value; }
        }
        public string PositionNameVN
        {
            get { return _PositionNameVN; }
            set { _PositionNameVN = value; }
        }
        public string PositionNameCN
        {
            get { return _PositionNameCN; }
            set { _PositionNameCN = value; }
        }
        public string PositionID
        {
            get { return _PositionID; }
            set { _PositionID = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int BusinessKey
        {
            get { return _BusinessKey; }
            set { _BusinessKey = value; }
        }
        public int ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int TypeKey
        {
            get { return _TypeKey; }
            set { _TypeKey = value; }
        }
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        #endregion
    }
}
