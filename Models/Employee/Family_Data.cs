﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Family_Data
    {
        public static List<Family_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Family WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Family_Model> zList = new List<Family_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Family_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    FatherName = r["FatherName"].ToString(),
                    FatherBirthday = (r["FatherBirthday"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["FatherBirthday"]),
                    FatherWork = r["FatherWork"].ToString(),
                    FatherAddress = r["FatherAddress"].ToString(),
                    MotherName = r["MotherName"].ToString(),
                    MotherBirthday = (r["MotherBirthday"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["MotherBirthday"]),
                    MotherWork = r["MotherWork"].ToString(),
                    MotherAddress = r["MotherAddress"].ToString(),
                    PartnersName = r["PartnersName"].ToString(),
                    PartnersBirthday = (r["PartnersBirthday"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["PartnersBirthday"]),
                    PartnersWork = r["PartnersWork"].ToString(),
                    PartnersAddress = r["PartnersAddress"].ToString(),
                    ChildExtend = r["ChildExtend"].ToString(),
                    OrtherExtend = r["OrtherExtend"].ToString(),
                    Description = r["Description"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
