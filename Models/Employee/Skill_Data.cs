﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Skill_Data
    {
        public static List<Skill_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Skill WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Skill_Model> zList = new List<Skill_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Skill_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    SkillName = r["SkillName"].ToString(),
                    Levels = r["Levels"].ToInt(),
                    Maxlevels = r["Maxlevels"].ToInt(),
                    Description = r["Description"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Skill_Model> ListSkill(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Skill WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Employee.Length >= 36)
            {
                zSQL += " AND EmployeeKey = @Employee";
            }
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Skill_Model> zList = new List<Skill_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Skill_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    SkillName = r["SkillName"].ToString(),
                    Levels = r["Levels"].ToInt(),
                    Maxlevels = r["Maxlevels"].ToInt(),
                    Description = r["Description"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
