﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Leave_Close_Data
    {
        public static List<Leave_Close_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Leave_Close WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Leave_Close_Model> zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    CloseKey = r["CloseKey"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    Incremental = r["Incremental"].ToFloat(),
                    Description = r["Description"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    PositionName = r["PositionName"].ToString(),
                    OrganizationPath = r["OrganizationPath"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    Reference = r["Reference"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
