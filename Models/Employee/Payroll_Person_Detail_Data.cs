﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
public class Payroll_Person_Detail_Data
{
public static List<Payroll_Person_Detail_Model> List(string PartnerNumber,  out string Message)
{
DataTable zTable = new DataTable();
string zSQL = "SELECT * FROM HRM_Payroll_Person_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
string zConnectionString =ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close(); Message = string.Empty;
}
catch (Exception ex)
{
Message = ex.ToString();
}
List<Payroll_Person_Detail_Model> zList = new List<Payroll_Person_Detail_Model>();
foreach (DataRow r in zTable.Rows){zList.Add(new Payroll_Person_Detail_Model() {AutoKey= r["AutoKey"].ToInt(),
PayrollKey= r["PayrollKey"].ToString(),
ItemKey= r["ItemKey"].ToInt(),
ItemID= r["ItemID"].ToString(),
ItemName= r["ItemName"].ToString(),
Quantity= r["Quantity"].ToFloat(),
UnitName= r["UnitName"].ToString(),
Total= r["Total"].ToDouble(),
ItemType= r["ItemType"].ToInt(),
MoneyPay= r["MoneyPay"].ToDouble(),
Description= r["Description"].ToString(),
PartnerNumber= r["PartnerNumber"].ToString(),
RecordStatus= r["RecordStatus"].ToInt(),
CreatedOn=(r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
CreatedBy= r["CreatedBy"].ToString(),
CreatedName= r["CreatedName"].ToString(),
ModifiedOn=(r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
ModifiedBy= r["ModifiedBy"].ToString(),
ModifiedName= r["ModifiedName"].ToString(),
});}
return zList;
}
}
}
