﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Unit_Converted_Info
    {

        public Unit_Converted_Model Unit_Converted = new Unit_Converted_Model();

        #region [ Constructor Get Information ]
        public Unit_Converted_Info()
        {
        }
        public Unit_Converted_Info(int? AutoKey)
        {
            string zSQL = "SELECT * FROM PDT_Unit_Converted WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Unit_Converted.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Unit_Converted.ProductKey = zReader["ProductKey"].ToString();
                    Unit_Converted.ProductName = zReader["ProductName"].ToString();
                    if (zReader["StandardUnitKey"] != DBNull.Value)
                    {
                        Unit_Converted.StandardUnitKey = int.Parse(zReader["StandardUnitKey"].ToString());
                    }

                    Unit_Converted.StandardUnitName = zReader["StandardUnitName"].ToString();
                    if (zReader["Rate"] != DBNull.Value)
                    {
                        Unit_Converted.Rate = float.Parse(zReader["Rate"].ToString());
                    }

                    if (zReader["UnitKey"] != DBNull.Value)
                    {
                        Unit_Converted.UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    }

                    Unit_Converted.UnitName = zReader["UnitName"].ToString();
                    Unit_Converted.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Unit_Converted.Message = "200 OK";
                }
                else
                {
                    Unit_Converted.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Unit_Converted.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Unit_Converted ("
         + " ProductKey , ProductName , StandardUnitKey , StandardUnitName , Rate , UnitKey , UnitName , PartnerNumber ) "
         + " VALUES ( "
         + " @ProductKey , @ProductName , @StandardUnitKey , @StandardUnitName , @Rate , @UnitKey , @UnitName , @PartnerNumber ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Unit_Converted.ProductKey;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Unit_Converted.ProductName;
                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Unit_Converted.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Unit_Converted.StandardUnitName;
                zCommand.Parameters.Add("@Rate", SqlDbType.Float).Value = Unit_Converted.Rate;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Unit_Converted.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Unit_Converted.UnitName;
                if (Unit_Converted.PartnerNumber != "" && Unit_Converted.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Unit_Converted.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Unit_Converted.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Unit_Converted.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Unit_Converted("
         + " AutoKey , ProductKey , ProductName , StandardUnitKey , StandardUnitName , Rate , UnitKey , UnitName , PartnerNumber ) "
         + " VALUES ( "
         + " @AutoKey , @ProductKey , @ProductName , @StandardUnitKey , @StandardUnitName , @Rate , @UnitKey , @UnitName , @PartnerNumber ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Unit_Converted.AutoKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Unit_Converted.ProductKey;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Unit_Converted.ProductName;
                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Unit_Converted.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Unit_Converted.StandardUnitName;
                zCommand.Parameters.Add("@Rate", SqlDbType.Float).Value = Unit_Converted.Rate;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Unit_Converted.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Unit_Converted.UnitName;
                if (Unit_Converted.PartnerNumber != "" && Unit_Converted.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Unit_Converted.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Unit_Converted.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Unit_Converted.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE PDT_Unit_Converted SET "
                        + " ProductKey = @ProductKey,"
                        + " ProductName = @ProductName,"
                        + " StandardUnitKey = @StandardUnitKey,"
                        + " StandardUnitName = @StandardUnitName,"
                        + " Rate = @Rate,"
                        + " UnitKey = @UnitKey,"
                        + " UnitName = @UnitName,"
                        + " PartnerNumber = @PartnerNumber"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Unit_Converted.AutoKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Unit_Converted.ProductKey;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Unit_Converted.ProductName;
                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Unit_Converted.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Unit_Converted.StandardUnitName;
                zCommand.Parameters.Add("@Rate", SqlDbType.Float).Value = Unit_Converted.Rate;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Unit_Converted.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Unit_Converted.UnitName;
                if (Unit_Converted.PartnerNumber != "" && Unit_Converted.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Unit_Converted.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Unit_Converted.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Unit_Converted.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Unit_Converted SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Unit_Converted.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Unit_Converted.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Unit_Converted.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Unit_Converted WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Unit_Converted.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Unit_Converted.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Unit_Converted.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
