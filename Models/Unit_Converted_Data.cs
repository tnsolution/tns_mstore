﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Unit_Converted_Data
    {
        public static List<Unit_Converted_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM PDT_Unit_Converted WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Unit_Converted_Model> zList = new List<Unit_Converted_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Unit_Converted_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    Rate = r["Rate"].ToFloat(),
                    UnitKey = r["UnitKey"].ToInt(),
                    UnitName = r["UnitName"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),                   
                });
            }

            return zList;
        }
    }
}
