﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace TNStores
{
    public static class Helper
    {
        public static string AutoID_Product(string PartnerNumber, string Prefix)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.Auto_ProductID(@PartnerNumber, @Prefix)";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string AutoID_PurchaseOrder(string PartnerNumber, string Prefix)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.Auto_InputID(@Prefix, @PartnerNumber)";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string AutoID_SaleOrder(string PartnerNumber, string Prefix)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_OrderID(@Prefix, @PartnerNumber)";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static string ViewScreen = "";
        public static string PartnerNumber = "";
        public static string UploadPath = "/FileUpload";

        public static string NoImage = "/assets/themes/img/unnamed.png";
        public static string DefaultImage = "/assets/themes/img/avatar.jpg";
        public static string DefaultFileThumbnail = "/assets/themes/img/unknow.png";
        public static string DefaultFileDoc = "/assets/themes/img/DOC.png";
        public static string DefaultFilePDF = "/assets/themes/img/PDF.png";

        public static string ToQuoteComma(this string data)
        {
            string[] s = data.Split(',');
            return "'" + string.Join("','", s) + "'";
        }
        /// <summary>
        /// Convert List to string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ToStringComma(this IList<string> data)
        {
            return "'" + string.Join("', '", data) + "'";
        }
        /// <summary>
        /// Chuyển List<string>
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<string> ToListString(this string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<List<string>>(data);
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public static double GetDouble(string s)
        {
            double result = Convert.ToDouble(s.Replace(",", ";").Replace(".", ",").Replace(";", "."));

            return result;
        }
        public static DataTable XMLToTable(string XMLData)
        {
            StringReader theReader = new StringReader(XMLData);
            DataSet theDataSet = new DataSet();
            theDataSet.ReadXml(theReader);
            return theDataSet.Tables[0];
        }
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (!seenKeys.Contains(keySelector(element)))
                {
                    seenKeys.Add(keySelector(element));
                    yield return element;
                }
            }
        }

        //public static string AutoEmployeeID(string Prefix, string PartnerNumber)
        //{
        //    string zResult = "";
        //    string zSQL = @"SELECT dbo.Auto_EmployeeID(@Prefix, @PartnerNumber)";

        //    string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
        //        zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
        //        zResult = zCommand.ExecuteScalar().ToString();
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }

        //    return zResult;
        //}

        public static void RunSQL(string SQL, out string Message)
        {
            string zResult = "";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Message = "";
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static string GetPartnerNumber(string PartID, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT PartnerNumber FROM SYS_Partner WHERE PartnerID = @PartnerID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = PartID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static List<TNItem> ListQuarter()
        {
            List<TNItem> zList = new List<TNItem>();
            for (int i = 1; i <= 4; i++)
            {
                zList.Add(new TNItem()
                {
                    Text = "Quý " + i.ToString(),
                    Value = i.ToString(),
                });
            }
            return zList;
        }


        public static bool DeleteSingleFile(string FilePath, out string Message)
        {
            try
            {
                string fullPath = HttpContext.Current.Server.MapPath(FilePath);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                Message = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="HttpFiles"></param>
        /// <param name="FilePath"></param>
        /// <param name="FileSave"></param>
        /// <returns></returns>
        public static bool UploadSingleFile(HttpPostedFileBase file, string FilePath, out string FileAttach)
        {
            try
            {
                
                string fileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                string fileSave = HttpContext.Current.Server.MapPath(FilePath + fileName);
                DirectoryInfo zDir = new DirectoryInfo(HttpContext.Current.Server.MapPath(FilePath));
                if (!zDir.Exists)
                {
                    zDir.Create();
                }
                else
                {
                    if (System.IO.File.Exists(fileSave))
                    {
                        System.IO.File.Delete(fileSave);
                    }
                }

                file.SaveAs(fileSave);

                FileAttach = FilePath + fileName;
                return true;
            }

            catch (Exception)
            {
                FileAttach = string.Empty;
                return false;
            }
        }

        //        /// <summary>
        //        /// 
        //        /// </summary>
        //        /// <param name="PartnerNumber"></param>
        //        /// <param name="Option">1 Danh số, 2 Chi phí, 3 Giảm giá, 4 Thực thu</param>
        //        /// <param name="FromDate"></param>
        //        /// <param name="ToDate"></param>
        //        /// <returns></returns>
        //        public static DataTable DB_Chart(string PartnerNumber, string Option, DateTime FromDate, DateTime ToDate)
        //        {
        //            DataTable zTable = new DataTable();
        //            if (Option.Length > 0)
        //            {
        //                string PivotDate = "";
        //                for (int i = FromDate.Day; i <= ToDate.Day; i++)
        //                {
        //                    PivotDate += "[" + i + "],";
        //                }
        //                if (PivotDate.Contains(","))
        //                {
        //                    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
        //                }

        //                string zSQL = @"
        //CREATE TABLE #temp
        //(
        //	Category NVARCHAR(50) ,
        //	DateView INT,
        //	InCome MONEY,
        //	[Rank] INT
        //)";
        //                string[] temp = Option.Split(',');
        //                foreach (string s in temp)
        //                {
        //                    switch (int.Parse(s))
        //                    {
        //                        case 1:
        //                            #region --Danh số--
        //                            zSQL += @"
        //INSERT INTO #temp (Category, DateView, InCome, [Rank]) VALUES (N'Doanh số', 1, 0, 0)
        //INSERT INTO #temp (Category, DateView, InCome, [Rank])
        //SELECT 
        //N'Doanh số',
        //DAY(OrderDate),
        //SUM(AmountCurrencyMain), 
        //0
        //FROM FNC_Order
        //WHERE RecordStatus <> 99
        //AND PartnerNumber = @PartnerNumber 
        //AND (OrderDate BETWEEN @FromDate AND @ToDate)
        //GROUP BY DAY(OrderDate)";
        //                            #endregion
        //                            break;

        //                        case 2:
        //                            #region --Chi phí--
        //                            zSQL += @"
        //INSERT INTO #temp (Category, DateView, InCome, [Rank]) VALUES (N'Chi phí', 1, 0, 1)
        //INSERT INTO #temp (Category,DateView, InCome, [Rank])
        //SELECT 
        //N'Chi phí', 
        //DAY(PaymentDate), 
        //SUM(AmountCurrencyMain),
        //1
        //FROM FNC_Payment
        //WHERE RecordStatus <> 99 
        //AND PartnerNumber = @PartnerNumber 
        //AND (PaymentDate BETWEEN @FromDate AND @ToDate)
        //GROUP BY DAY(PaymentDate)";
        //                            #endregion
        //                            break;

        //                        case 3:
        //                            #region --Giảm giá--
        //                            zSQL += @"
        //INSERT INTO #temp (Category, DateView, InCome, [Rank]) VALUES (N'Giảm giá', 1, 0, 2)
        //INSERT INTO #temp (Category,DateView, InCome, [Rank])
        //SELECT 
        //N'Giảm giá',
        //DAY(OrderDate),
        //SUM(AmountDiscount),
        //2
        //FROM FNC_Order
        //WHERE RecordStatus <> 99 
        //AND PartnerNumber = @PartnerNumber 
        //AND (OrderDate BETWEEN @FromDate AND @ToDate)
        //GROUP BY DAY(OrderDate)";
        //                            #endregion
        //                            break;

        //                        case 4:
        //                            #region --Thực thu--
        //                            zSQL += @"
        //INSERT INTO #temp (Category, DateView, InCome, [Rank]) VALUES (N'Thực thu', 1, 0, 3)
        //INSERT INTO #temp (Category,DateView, InCome, [Rank])
        //SELECT 
        //N'Thực thu',
        //DateView,
        //(Orders-Payment),
        //3
        // FROM
        // (
        //	SELECT DAY(OrderDate) AS DateView , 
        //	0 AS Payment,
        //	SUM(AmountCurrencyMain) AS Orders 
        //	FROM FNC_Order
        //	WHERE PartnerNumber = @PartnerNumber 
        //	AND (OrderDate BETWEEN @FromDate AND @ToDate)
        //	GROUP BY DAY(OrderDate)
        //	UNION
        //	SELECT 
        //	DAY(PaymentDate) AS DateView,
        //	SUM(AmountCurrencyMain) AS Payment ,
        //	0 AS Orders 
        //	FROM FNC_Payment
        //	WHERE PartnerNumber = @PartnerNumber 
        //	AND (PaymentDate BETWEEN @FromDate AND @ToDate)
        //	GROUP BY DAY(PaymentDate)
        //) H";
        //                            #endregion
        //                            break;

        //                        default:
        //                            #region Tất cả tùy chọn

        //                            #endregion
        //                            zSQL += "";
        //                            break;
        //                    }
        //                }

        //                zSQL += @"
        //SELECT * FROM 
        //(
        //	SELECT Category, DateView AS [Time], InCome AS Total 
        //	FROM #temp
        //) X
        //PIVOT 
        //( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY Category DROP TABLE #temp";



        //                string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        //                try
        //                {
        //                    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                    zConnect.Open();
        //                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                    zCommand.CommandType = CommandType.Text;
        //                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
        //                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                    zAdapter.Fill(zTable);
        //                    zCommand.Dispose();
        //                    zConnect.Close();
        //                }
        //                catch (Exception ex)
        //                {
        //                    string zstrMessage = ex.ToString();
        //                }

        //            }

        //            return zTable;
        //        }

        public static DataTable DT_ChartSaleAmount(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }
            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(50) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT
)";
            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank]) VALUES (N'Doanh số', 1, 0, 0)
INSERT INTO #temp (Category, DateView, [Value], [Rank])
SELECT 
N'Doanh số',
DAY(OrderDate),
SUM(AmountCurrencyMain), 
0
FROM FNC_Order
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (OrderDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(OrderDate)";
            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total 
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY Category DROP TABLE #temp";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
        public static DataTable DT_ChartSaleInCome(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }
            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(50) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT
)";
            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank]) VALUES (N'Thực thu', 1, 0, 0)
INSERT INTO #temp (Category, DateView, [Value], [Rank])
SELECT 
N'Thực thu',
DAY(OrderDate),
SUM(AmountCurrencyMain), 
0
FROM FNC_Order
WHERE RecordStatus <> 99
AND StatusOrder = 9
AND PartnerNumber = @PartnerNumber 
AND (OrderDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(OrderDate)";
            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total 
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY Category DROP TABLE #temp";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
        public static DataTable DT_ChartOrderTotal(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }
            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(50) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT
)";
            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank]) VALUES (N'Số đơn hàng', 1, 0, 0)
INSERT INTO #temp (Category, DateView, [Value], [Rank])
SELECT 
N'Số đơn hàng',
DAY(OrderDate),
COUNT(OrderKey), 
0
FROM FNC_Order
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (OrderDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(OrderDate)";
            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total 
	FROM #temp
) X
PIVOT 
( MAX(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY Category DROP TABLE #temp";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
        public static DataTable DB_ChartOrderDebt(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }
            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(50) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT
)";
            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank]) VALUES (N'Thực thu', 1, 0, 0)
INSERT INTO #temp (Category, DateView, [Value], [Rank])
SELECT 
N'Thực thu',
DAY(OrderDate),
SUM(AmountCurrencyMain), 
0
FROM FNC_Order
WHERE RecordStatus <> 99
AND StatusOrder = 8
AND PartnerNumber = @PartnerNumber 
AND (OrderDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(OrderDate)";
            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total 
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY Category DROP TABLE #temp";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }

        public static double rpt_SaleAmount(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            double zResult = 0;
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string zSQL = @"
SELECT SUM(A.AmountOrder)
FROM FNC_Order A
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber
AND (OrderDate BETWEEN @FromDate AND @ToDate)";
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToDouble();
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return zResult;
        }
        public static double rpt_OrderTotal(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            double zResult = 0;
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string zSQL = @"
SELECT COUNT(A.OrderKey)
FROM FNC_Order A
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber
AND (OrderDate BETWEEN @FromDate AND @ToDate)";
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToDouble();
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return zResult;
        }
        public static double rpt_InCome(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            double zResult = 0;
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            //            string zSQL = @"
            //SELECT SUM(A.AmountOrder)
            //FROM FNC_Order A
            //WHERE RecordStatus <> 99
            //AND StatusOrder = 9
            //AND PartnerNumber = @PartnerNumber
            //AND (OrderDate BETWEEN @FromDate AND @ToDate)";

            string zSQL = @"
            SELECT SUM(A.AmountCurrencyMain)
            FROM FNC_Receipt A
            WHERE RecordStatus <> 99
            AND PartnerNumber = @PartnerNumber
            AND (ReceiptDate BETWEEN @FromDate AND @ToDate)";

            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToDouble();
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return zResult;
        }
        public static double rpt_OrderDebt(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            double zResult = 0;
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string zSQL = @"
DECLARE @MUSTPAY MONEY = (SELECT SUM(A.AmountOrder)
FROM FNC_Order A
WHERE RecordStatus <> 99
AND StatusOrder = 8
AND PartnerNumber = @PartnerNumber
AND (OrderDate BETWEEN @FromDate AND @ToDate))

DECLARE @PAID MONEY = (SELECT ISNULL(SUM(AmountCurrencyMain),0) 
FROM FNC_Receipt 
WHERE RecordStatus <> 99 
AND Slug = 9 
AND DocumentID IN 
(
    SELECT OrderID FROM FNC_Order A WHERE RecordStatus <> 99
    AND StatusOrder = 8
    AND PartnerNumber = @PartnerNumber
    AND (OrderDate BETWEEN @FromDate AND @ToDate))
)

SELECT @MUSTPAY - @PAID";
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToDouble();
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return zResult;
        }
        public static double rpt_Payment(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            double zResult = 0;
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string zSQL = @"
SELECT SUM(A.AmountCurrencyMain)
FROM FNC_Payment A
WHERE RecordStatus<> 99
AND PartnerNumber = @PartnerNumber
AND(PaymentDate BETWEEN @FromDate AND @ToDate)";
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToDouble();
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return zResult;
        }

        public static DataTable DB_ChartAIO(string PartnerNumber, string Option, string Color, DateTime FromDate, DateTime ToDate, out string Message)
        {
            Message = string.Empty;
            DataTable zTable = new DataTable();
            if (Option.Length > 0)
            {
                string PivotDate = "";
                for (var i = FromDate.Date; i.Date <= ToDate.Date; i = i.AddDays(1))
                {
                    PivotDate += "[" + i.Day + "],";
                }
                if (PivotDate.Contains(","))
                {
                    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
                }

                string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(500) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT,
    Color NVARCHAR(50)
)";
                string[] temp = Option.Split(',');
                string[] color = Color.Split(',');
                foreach (string s in temp)
                {
                    switch (s)
                    {
                        case "DONHANG":
                            #region --Đơn hàng--
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Số đơn hàng', 1, 0, 1, '" + color[0] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Số đơn hàng',
DAY(OrderDate),
COUNT(OrderKey), 
1, '" + color[0] + @"'
FROM FNC_Order
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (OrderDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(OrderDate)";
                            #endregion
                            break;

                        case "DOANHSO":
                            #region --Danh số--
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Doanh số', 1, 0, 2, '" + color[1] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Doanh số',
DAY(OrderDate),
SUM(AmountOrder), 
2, '" + color[1] + @"'
FROM FNC_Order
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (OrderDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(OrderDate)";
                            #endregion
                            break;

                        case "THUBANHANG":
                            #region ----
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Thu bán hàng', 1, 0, 3, '" + color[2] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Thu bán hàng',
DAY(OrderDate),
SUM(AmountOrder), 
3, '" + color[2] + @"'
FROM FNC_Order
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (OrderDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(OrderDate)";
                            #endregion
                            break;


                        case "THUKHAC":
                            #region ----
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Thu khác', 1, 0, 4, '" + color[3] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Thu khác',
DAY(ReceiptDate),
SUM(AmountCurrencyMain), 
4, '" + color[3] + @"'
FROM FNC_Receipt
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(ReceiptDate)";
                            #endregion
                            break;
                            
                        case "CHIPHI":
                            #region --Phiếu chi--
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Chi phí', 1, 0, 5, '" + color[4] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT
N'Chi phí', 
DAY(PaymentDate), 
SUM(AmountCurrencyMain),
5, '" + color[4] + @"'
FROM FNC_Payment
WHERE RecordStatus<> 99
AND PartnerNumber = @PartnerNumber
AND(PaymentDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(PaymentDate)";
                            #endregion
                            break;
                    }
                }

                zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total, Color, [Rank]
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY [Rank] DROP TABLE #temp";
                                
                string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();
                    
                }
                catch (Exception ex)
                {
                    Message = ex.ToString();
                }

            }
            return zTable;
        }

        public static DataTable DB_Inventory(string PartnerNumber, string CloseMonth, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"IVT_FromDateToDate_List";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CloseMonth", SqlDbType.NVarChar).Value = CloseMonth;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = "";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
    public class PivotTable
    {
        #region Variables

        private DataTable _DataTable;

        #endregion Variables

        #region Constructors

        public PivotTable(DataTable dataTable)
        {
            _DataTable = dataTable;
        }

        #endregion Constructors

        #region Properties

        public DataTable ResultTable
        {
            get { return _DataTable; }
        }
        #endregion Properties

        #region Private Methods

        private string[] FindValues(string xAxisField, string xAxisValue, string yAxisField, string yAxisValue, string[] zAxisFields)
        {
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis++;
            }

            string[] zAxisValues = new string[zAxis];
            //set default values
            for (int i = 0; i <= zAxisValues.GetUpperBound(0); i++)
            {
                zAxisValues[i] = "0";
            }

            foreach (DataRow row in _DataTable.Rows)
            {
                if (Convert.ToString(row[xAxisField]) == xAxisValue && Convert.ToString(row[yAxisField]) == yAxisValue)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        zAxisValues[z] = Convert.ToString(Convert.ToDouble(row[zAxisFields[z]]) + Convert.ToDouble(zAxisValues[z]));
                    }
                    //   break; // If you are sure that you don't have duplicated row of data, uncomment to gain performance
                }
            }

            return zAxisValues;
        }

        private string FindValue(string xAxisField, string xAxisValue, string yAxisField, string yAxisValue, string zAxisField)
        {
            string zAxisValue = "";

            foreach (DataRow row in _DataTable.Rows)
            {
                if (Convert.ToString(row[xAxisField]) == xAxisValue && Convert.ToString(row[yAxisField]) == yAxisValue)
                {
                    zAxisValue = Convert.ToString(row[zAxisField]);
                    break;
                }
            }

            return zAxisValue;
        }
        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Creates an advanced 3D Pivot table.
        /// </summary>
        /// <param name="xAxisField">The main heading at the top of the report.</param>
        /// <param name="yAxisField">The heading on the left of the report.</param>
        /// <param name="zAxisFields">The sub heading at the top of the report.</param>
        /// <param name="mainColumnName">Name of the column in xAxis.</param>
        /// <param name="columnTotalName">Name of the column with the totals.</param>
        /// <param name="rowTotalName">Name of the row with the totals.</param>
        /// <param name="zAxisFieldsNames">Name of the columns in the zAxis.</param>
        /// <returns>HtmlTable Control.</returns>
        public DataTable Generate(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }

        /// <summary>
        /// Creates an advanced 3D Pivot table.
        /// </summary>
        /// <param name="xAxisField">The main heading at the top of the report.</param>
        /// <param name="yAxisField">The heading on the left of the report.</param>
        /// <param name="zAxisFields">The sub heading at the top of the report.</param>
        /// <param name="mainColumnName">Name of the column in xAxis.</param>
        /// <param name="columnTotalName">Name of the column with the totals.</param>
        /// <param name="rowTotalName">Name of the row with the totals.</param>
        /// <param name="zAxisFieldsNames">Name of the columns in the zAxis.</param>
        /// <param name="Sort">Name of the columns sort before add totalrow</param>
        /// <returns>HtmlTable Control.</returns>
        public DataTable Generate_NoSort(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }


            //DataView dv = table.AsDataView();
            //dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            //table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }
        public DataTable Generate(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames, int SortIndex)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }


            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }
        public DataTable Generate_ASC(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames, int SortIndex)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    double temp = 0;
                    if (double.TryParse(matrix[col, row], out temp))
                    {

                    }
                    yTotals[col] += temp;
                    //yTotals[col] +=  Convert.ToDouble("0" + );
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    double temp = 0;
                    if (double.TryParse(matrix[z, y], out temp))
                    {

                    }
                    xTotals[zCount, y] += temp;
                    //xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            //for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            //{
            //    DataRow dataRow = table.NewRow();
            //    for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
            //    {
            //        if (z == 0)
            //        {
            //            dataRow[z] = Convert.ToString(yAxis[y]);
            //        }
            //        else
            //        {
            //            dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
            //        }
            //    }

            //    //append x-axis grand totals
            //    for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
            //    {
            //        dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

            //    }
            //    table.Rows.Add(dataRow);
            //}
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        double tem = 0;
                        if (double.TryParse((matrix[(z - 1), y]).ToString(), out tem))
                        {

                        }
                        dataRow[z] = tem;// Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    double tem = 0;
                    if (double.TryParse((xTotals[z - (zAxis * xAxis.Count), y]).ToString(), out tem))
                    {

                    }
                    dataRow[z + 1] = tem; //Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }

            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " ASC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    double tem = 0;
                    if (double.TryParse((yTotals[x - 1]).ToString(), out tem))
                    {

                    }
                    dataRowTotals[x] = tem;//Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                double tem = 0;
                if (double.TryParse((xTotals[z, xTotals.GetUpperBound(1)]).ToString(), out tem))
                {

                }
                dataRowTotals[table.Columns.Count - zAxis + z] = tem;//Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }

        public DataTable Generate(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames, int SortIndex, bool isTotal)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }


            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            if (isTotal)
            {
                //append x-axis/y-axis totals
                for (int z = 0; z < zAxis; z++)
                {
                    dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
                }
                table.Rows.Add(dataRowTotals);
            }
            return table;
        }

        /// <summary>
        /// Creates a simple 3D Pivot Table.
        /// </summary>
        /// <param name="xAxisField">The heading at the top of the table.</param>
        /// <param name="yAxisField">The heading to the left of the table.</param>
        /// <param name="zAxisField">The item value field.</param>
        /// <param name="mainColumnName">Title of the main column</param>
        /// <param name="columnTotalName">Title of the total column</param>
        /// <param name="rowTotalName">Title of the row column</param>
        /// <returns></returns>
        public DataTable Generate(string xAxisField, string yAxisField, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName)
        {
            return Generate(xAxisField, yAxisField, new string[0], new string[0], zAxisField, mainColumnName, columnTotalName, rowTotalName);
        }

        public DataTable Generate(string xAxisField, string yAxisField, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName, int SortIndex)
        {
            return Generate(xAxisField, yAxisField, new string[0], new string[0], zAxisField, mainColumnName, columnTotalName, rowTotalName, SortIndex);
        }

        /// <summary>
        /// Creates a simple 3D Pivot Table.
        /// </summary>
        /// <param name="xAxisField">The heading at the top of the table.</param>
        /// <param name="yAxisField">The heading to the left of the table.</param>
        /// <param name="yAxisInfoFields">Other columns that we want to show on the y axis.</param>
        /// <param name="yAxisInfoFieldsNames">Title of the additionnal columns on y axis.</param>
        /// <param name="zAxisField">The item value field.</param>
        /// <param name="mainColumnName">Title of the main column</param>
        /// <param name="columnTotalName">Title of the total column</param>
        /// <param name="rowTotalName">Title of the row column</param>
        /// <returns></returns>
        public DataTable Generate(string xAxisField, string yAxisField, string[] yAxisInfoFields, string[] yAxisInfoFieldsNames, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName)
        {
            //style table
            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the x-axis/y-axis fields
            string[,] matrix = new string[xAxis.Count, yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string zAxisValue = FindValue(xAxisField, Convert.ToString(xAxis[x])
                                                  , yAxisField, Convert.ToString(yAxis[y]), zAxisField);



                    matrix[x, y] = zAxisValue;
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[xAxis.Count];
            for (int col = 0; col < xAxis.Count; col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    if (matrix[col, row] != "")
                    {
                        yTotals[col] += Convert.ToDouble(matrix[col, row]);
                    }
                    //yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[] xTotals = new double[(yAxis.Count + 1)];
            for (int row = 0; row < yAxis.Count; row++)
            {
                xTotals[row] = 0;
                for (int col = 0; col < xAxis.Count; col++)
                {
                    if (matrix[col, row] != "")
                    {
                        xTotals[row] += Convert.ToDouble(matrix[col, row]);
                    }
                    //xTotals[row] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }
            xTotals[xTotals.GetUpperBound(0)] = 0; //Grand Total
            for (int i = 0; i < xTotals.GetUpperBound(0); i++)
            {
                xTotals[xTotals.GetUpperBound(0)] += xTotals[i];
            }

            //Build HTML Table

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);

            foreach (string yAxisInfoFieldsName in yAxisInfoFieldsNames)
            {
                table.Columns.Add(yAxisInfoFieldsName);
            }

            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    DataColumn column = new DataColumn();
                    column.ColumnName = Convert.ToString(xAxis[x]);
                    table.Columns.Add(column);
                }
                else
                {
                    DataColumn column = new DataColumn(columnTotalName);
                    table.Columns.Add(column);
                }
            }

            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= xAxis.Count + yAxisInfoFieldsNames.Length; z++) //loop thru z-axis + 1
                {
                    if (z < yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(_DataTable.Rows[y][yAxisInfoFields[z]]);
                    }
                    if (z == yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    if (z > yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1 - yAxisInfoFieldsNames.Length), y]);
                    }
                }


                dataRow[xAxis.Count + yAxisInfoFieldsNames.Length + 1] = Convert.ToString(xTotals[y]);

                table.Rows.Add(dataRow);
            }

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (xAxis.Count + 1) + yAxisInfoFieldsNames.Length; x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                if (x > yAxisInfoFieldsNames.Length)
                {
                    if (x <= xAxis.Count + yAxisInfoFieldsNames.Length)
                    {
                        dataRowTotals[x] = Convert.ToString(yTotals[(x - 1 - yAxisInfoFieldsNames.Length)]);
                    }
                    else
                    {
                        dataRowTotals[x] = Convert.ToString(xTotals[xTotals.GetUpperBound(0)]);
                    }
                }
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }

        public DataTable Generate(string xAxisField, string yAxisField, string[] yAxisInfoFields, string[] yAxisInfoFieldsNames, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName, int SortIndex)
        {
            //style table
            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the x-axis/y-axis fields
            string[,] matrix = new string[xAxis.Count, yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string zAxisValue = FindValue(xAxisField, Convert.ToString(xAxis[x])
                                                  , yAxisField, Convert.ToString(yAxis[y]), zAxisField);



                    matrix[x, y] = zAxisValue;
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[xAxis.Count];
            for (int col = 0; col < xAxis.Count; col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    if (matrix[col, row] != "")
                    {
                        yTotals[col] += Convert.ToDouble(matrix[col, row]);
                    }
                    //yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[] xTotals = new double[(yAxis.Count + 1)];
            for (int row = 0; row < yAxis.Count; row++)
            {
                xTotals[row] = 0;
                for (int col = 0; col < xAxis.Count; col++)
                {
                    if (matrix[col, row] != "")
                    {
                        xTotals[row] += Convert.ToDouble(matrix[col, row]);
                    }
                    //xTotals[row] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }
            xTotals[xTotals.GetUpperBound(0)] = 0; //Grand Total
            for (int i = 0; i < xTotals.GetUpperBound(0); i++)
            {
                xTotals[xTotals.GetUpperBound(0)] += xTotals[i];
            }

            //Build HTML Table

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);

            foreach (string yAxisInfoFieldsName in yAxisInfoFieldsNames)
            {
                table.Columns.Add(yAxisInfoFieldsName);
            }

            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    DataColumn column = new DataColumn();
                    column.ColumnName = Convert.ToString(xAxis[x]);
                    table.Columns.Add(column);
                }
                else
                {
                    DataColumn column = new DataColumn(columnTotalName);
                    table.Columns.Add(column);
                }
            }

            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= xAxis.Count + yAxisInfoFieldsNames.Length; z++) //loop thru z-axis + 1
                {
                    if (z < yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(_DataTable.Rows[y][yAxisInfoFields[z]]);
                    }
                    if (z == yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    if (z > yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1 - yAxisInfoFieldsNames.Length), y]);
                    }
                }


                dataRow[xAxis.Count + yAxisInfoFieldsNames.Length + 1] = Convert.ToString(xTotals[y]);

                table.Rows.Add(dataRow);
            }

            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (xAxis.Count + 1) + yAxisInfoFieldsNames.Length; x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                if (x > yAxisInfoFieldsNames.Length)
                {
                    if (x <= xAxis.Count + yAxisInfoFieldsNames.Length)
                    {
                        dataRowTotals[x] = Convert.ToString(yTotals[(x - 1 - yAxisInfoFieldsNames.Length)]);
                    }
                    else
                    {
                        dataRowTotals[x] = Convert.ToString(xTotals[xTotals.GetUpperBound(0)]);
                    }
                }
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }
        #endregion Public Methods
    }
    public class JsTree
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public JsState state { get; set; }
        public string li_attr { get; set; }
        public string a_attr { get; set; }
    }
    public class JsState
    {
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
    }
    public class TNItem
    {
        public string Value { get; set; } = "";
        public string Text { get; set; } = "";
    }
    public class Pager
    {
        public Pager(
            int totalItems,
            int currentPage = 1,
            int pageSize = 10,
            int maxPages = 10)
        {
            // calculate total pages
            var totalPages = (int)Math.Ceiling(totalItems / (decimal)pageSize);

            // ensure current page isn't out of range
            if (currentPage < 1)
            {
                currentPage = 1;
            }
            else if (currentPage > totalPages)
            {
                currentPage = totalPages;
            }

            int startPage, endPage;
            if (totalPages <= maxPages)
            {
                // total pages less than max so show all pages
                startPage = 1;
                endPage = totalPages;
            }
            else
            {
                // total pages more than max so calculate start and end pages
                var maxPagesBeforeCurrentPage = (int)Math.Floor(maxPages / (decimal)2);
                var maxPagesAfterCurrentPage = (int)Math.Ceiling(maxPages / (decimal)2) - 1;
                if (currentPage <= maxPagesBeforeCurrentPage)
                {
                    // current page near the start
                    startPage = 1;
                    endPage = maxPages;
                }
                else if (currentPage + maxPagesAfterCurrentPage >= totalPages)
                {
                    // current page near the end
                    startPage = totalPages - maxPages + 1;
                    endPage = totalPages;
                }
                else
                {
                    // current page somewhere in the middle
                    startPage = currentPage - maxPagesBeforeCurrentPage;
                    endPage = currentPage + maxPagesAfterCurrentPage;
                }
            }

            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.Min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages that can be looped over
            var pages = Enumerable.Range(startPage, (endPage + 1) - startPage);

            // update object instance with all pager properties required by the view
            TotalItems = totalItems;
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
            StartIndex = startIndex;
            EndIndex = endIndex;
            Pages = pages;
        }

        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
        public int StartIndex { get; private set; }
        public int EndIndex { get; private set; }
        public IEnumerable<int> Pages { get; private set; }
    }
}