﻿using System;
namespace TNStores
{
    public class Invoice_Close_Model
    {
        #region [ Field Name ]
        private string _CloseKey = "";
        private string _CloseDate = "";
        private string _CloseType = "";
        private double _CloseAmount = 0;
        private string _Description = "";
        private string _JsonData = "";
        private int _RecordStatus = 0;
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string CloseKey
        {
            get { return _CloseKey; }
            set { _CloseKey = value; }
        }
        public string CloseDate
        {
            get { return _CloseDate; }
            set { _CloseDate = value; }
        }
        public string CloseType
        {
            get { return _CloseType; }
            set { _CloseType = value; }
        }
        public double CloseAmount
        {
            get { return _CloseAmount; }
            set { _CloseAmount = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string JsonData
        {
            get { return _JsonData; }
            set { _JsonData = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
