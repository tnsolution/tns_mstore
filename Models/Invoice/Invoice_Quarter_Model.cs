﻿namespace TNStores
{
    public class Invoice_Quarter_Model
    {
        public int Previous { get; set; } = 0;  //Số quý trước
        public int Current { get; set; } = 0;      //Số quý hiện tại
        public double AmountPrevious { get; set; } = 0; //Số tiền quý trước
        public double IncomeWithV { get; set; } = 0;  //Doanh số bán ra chịu thuế
        public double IncomeNoV { get; set; } = 0; //Doanh số bán ra không chịu thuế
        public double InComeTotal { get; set; } = 0;  //Tổng doanh số bán ra
        public double AmountVatInTotal { get; set; } = 0;    //Thuế GTGT đầu vào Q4
        public double AmountVatInDirect { get; set; } = 0;    //   -  Số V đầu vào trực tiếp
        public double AmountVatInShare { get; set; } = 0;     //   -  Số V đầu vào dùng chung
        public double AmountVatInShareToMinus { get; set; } = 0;     //Số V dc khấu trừ của hóa đơn dùng chung
        public double AmountVatAllowMinus { get; set; } = 0;   //Tổng số V được trừ trong quý
        public double AmountVatOutTotal { get; set; } = 0; //Thuế GTGT đầu ra Q4
        public double AmountPayCurrent { get; set; } = 0; //Số tiền nộp thuế của quý hiện tại
        public double AmountPayInclude { get; set; } = 0; //Số tiền nộp thuế khi cộng quý trước
        public double AmountNext { get; set; } = 0;   //Số tiền chuyển tiếp qua quý kế tiếp
    }
}