﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Invoice_Close_Info
    {

        public Invoice_Close_Model Invoice_Close = new Invoice_Close_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Invoice_Close_Info()
        {
            Invoice_Close.CloseKey = Guid.NewGuid().ToString();
        }
        public Invoice_Close_Info(string CloseDate)
        {
            string zSQL = "SELECT * FROM FNC_Invoice_Close WHERE CloseDate = @CloseDate AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = CloseDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Invoice_Close.CloseKey = zReader["CloseKey"].ToString();
                    Invoice_Close.CloseDate = zReader["CloseDate"].ToString();
                    Invoice_Close.CloseType = zReader["CloseType"].ToString();
                    if (zReader["CloseAmount"] != DBNull.Value)
                    {
                        Invoice_Close.CloseAmount = double.Parse(zReader["CloseAmount"].ToString());
                    }

                    Invoice_Close.Description = zReader["Description"].ToString();
                    Invoice_Close.JsonData = zReader["JsonData"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Invoice_Close.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Invoice_Close.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Invoice_Close.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Invoice_Close.CreatedBy = zReader["CreatedBy"].ToString();
                    Invoice_Close.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Invoice_Close.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Invoice_Close.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Invoice_Close.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Invoice_Close ("
         + " CloseDate , CloseType , CloseAmount , Description , JsonData , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CloseDate , @CloseType , @CloseAmount , @Description , @JsonData , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = Invoice_Close.CloseDate;
                zCommand.Parameters.Add("@CloseType", SqlDbType.NVarChar).Value = Invoice_Close.CloseType;
                zCommand.Parameters.Add("@CloseAmount", SqlDbType.Money).Value = Invoice_Close.CloseAmount;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Invoice_Close.Description;
                zCommand.Parameters.Add("@JsonData", SqlDbType.NVarChar).Value = Invoice_Close.JsonData;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Invoice_Close.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Invoice_Close.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Invoice_Close.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Invoice_Close.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Invoice_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Invoice_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Invoice_Close("
         + " CloseKey , CloseDate , CloseType , CloseAmount , Description , JsonData , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CloseKey , @CloseDate , @CloseType , @CloseAmount , @Description , @JsonData , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Invoice_Close.CloseKey != "" && Invoice_Close.CloseKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice_Close.CloseKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = Invoice_Close.CloseDate;
                zCommand.Parameters.Add("@CloseType", SqlDbType.NVarChar).Value = Invoice_Close.CloseType;
                zCommand.Parameters.Add("@CloseAmount", SqlDbType.Money).Value = Invoice_Close.CloseAmount;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Invoice_Close.Description;
                zCommand.Parameters.Add("@JsonData", SqlDbType.NVarChar).Value = Invoice_Close.JsonData;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Invoice_Close.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Invoice_Close.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Invoice_Close.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Invoice_Close.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Invoice_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Invoice_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE FNC_Invoice_Close SET "
                        + " CloseDate = @CloseDate,"
                        + " CloseType = @CloseType,"
                        + " CloseAmount = @CloseAmount,"
                        + " Description = @Description,"
                        + " JsonData = @JsonData,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CloseKey = @CloseKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Invoice_Close.CloseKey != "" && Invoice_Close.CloseKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice_Close.CloseKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = Invoice_Close.CloseDate;
                zCommand.Parameters.Add("@CloseType", SqlDbType.NVarChar).Value = Invoice_Close.CloseType;
                zCommand.Parameters.Add("@CloseAmount", SqlDbType.Money).Value = Invoice_Close.CloseAmount;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Invoice_Close.Description;
                zCommand.Parameters.Add("@JsonData", SqlDbType.NVarChar).Value = Invoice_Close.JsonData;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Invoice_Close.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Invoice_Close.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Invoice_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Invoice_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Invoice_Close SET RecordStatus = 99 WHERE CloseKey = @CloseKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice_Close.CloseKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Invoice_Close WHERE CloseKey = @CloseKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice_Close.CloseKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
