﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Invoice_Model
    {
        #region [ Field Name ]
        private string _ReplaceID = "";
        private string _ReplaceDescription = "";
        private int _ReturnStatus = 0;
        private string _ReplaceFile = "";

        private string _InvoiceKey = "";
        private string _InvoiceID = "";
        private string _InvoiceType = "";
        private DateTime _InvoiceDate = DateTime.MinValue;
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private string _PartnerKey = "";
        private string _PartnerName = "";
        private string _PartnerID = "";
        private string _PartnerAddress = "";
        private string _PartnerTax = "";
        private string _Contact = "";
        private string _Tel = "";
        private string _Fax = "";
        private string _Mail = "";
        private string _Description = "";
        private double _AmountNotV = 0;
        private double _AmountVat = 0;
        private double _AmountTotal = 0;
        private string _FileAttach = "";
        private string _ReferenceInvoice = "";
        private string _ReferenceDocument = "";
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private int _RecordStatus = 0;
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string InvoiceKey
        {
            get { return _InvoiceKey; }
            set { _InvoiceKey = value; }
        }
        public string InvoiceID
        {
            get { return _InvoiceID; }
            set { _InvoiceID = value; }
        }
        public string InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }
        public DateTime InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public string PartnerKey
        {
            get { return _PartnerKey; }
            set { _PartnerKey = value; }
        }
        public string PartnerName
        {
            get { return _PartnerName; }
            set { _PartnerName = value; }
        }
        public string PartnerID
        {
            get { return _PartnerID; }
            set { _PartnerID = value; }
        }
        public string PartnerAddress
        {
            get { return _PartnerAddress; }
            set { _PartnerAddress = value; }
        }
        public string PartnerTax
        {
            get { return _PartnerTax; }
            set { _PartnerTax = value; }
        }
        public string Contact
        {
            get { return _Contact; }
            set { _Contact = value; }
        }
        public string Tel
        {
            get { return _Tel; }
            set { _Tel = value; }
        }
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }
        public string Mail
        {
            get { return _Mail; }
            set { _Mail = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public double AmountNotV
        {
            get { return _AmountNotV; }
            set { _AmountNotV = value; }
        }
        public double AmountVat
        {
            get { return _AmountVat; }
            set { _AmountVat = value; }
        }
        public double AmountTotal
        {
            get { return _AmountTotal; }
            set { _AmountTotal = value; }
        }
        public string FileAttach
        {
            get { return _FileAttach; }
            set { _FileAttach = value; }
        }
        public string ReferenceInvoice
        {
            get { return _ReferenceInvoice; }
            set { _ReferenceInvoice = value; }
        }
        public string ReferenceDocument
        {
            get { return _ReferenceDocument; }
            set { _ReferenceDocument = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public string ReplaceFile
        {
            get
            {
                return _ReplaceFile;
            }

            set
            {
                _ReplaceFile = value;
            }
        }

        public int ReturnStatus
        {
            get
            {
                return _ReturnStatus;
            }

            set
            {
                _ReturnStatus = value;
            }
        }

        public string ReplaceDescription
        {
            get
            {
                return _ReplaceDescription;
            }

            set
            {
                _ReplaceDescription = value;
            }
        }

        public string ReplaceID
        {
            get
            {
                return _ReplaceID;
            }

            set
            {
                _ReplaceID = value;
            }
        }
        #endregion
    }
}
