﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Invoice_Info
    {

        public Invoice_Model Invoice = new Invoice_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Invoice_Info()
        {
            Invoice.InvoiceKey = Guid.NewGuid().ToString();
        }
        public Invoice_Info(string InvoiceKey)
        {
            string zSQL = "SELECT * FROM FNC_Invoice WHERE InvoiceKey = @InvoiceKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(InvoiceKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();

                    Invoice.ReplaceID = zReader["ReplaceID"].ToString();
                    Invoice.ReplaceDescription = zReader["ReplaceDescription"].ToString();
                    Invoice.ReplaceFile = zReader["ReplaceFile"].ToString();

                    if (zReader["ReturnStatus"] != DBNull.Value)
                    {
                        Invoice.ReturnStatus = int.Parse(zReader["ReturnStatus"].ToString());
                    }

                    Invoice.InvoiceKey = zReader["InvoiceKey"].ToString();
                    Invoice.InvoiceID = zReader["InvoiceID"].ToString();
                    Invoice.InvoiceType = zReader["InvoiceType"].ToString();
                    if (zReader["InvoiceDate"] != DBNull.Value)
                    {
                        Invoice.InvoiceDate = (DateTime)zReader["InvoiceDate"];
                    }

                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Invoice.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Invoice.CategoryName = zReader["CategoryName"].ToString();
                    Invoice.PartnerKey = zReader["PartnerKey"].ToString();
                    Invoice.PartnerName = zReader["PartnerName"].ToString();
                    Invoice.PartnerID = zReader["PartnerID"].ToString();
                    Invoice.PartnerAddress = zReader["PartnerAddress"].ToString();
                    Invoice.PartnerTax = zReader["PartnerTax"].ToString();
                    Invoice.Contact = zReader["Contact"].ToString();
                    Invoice.Tel = zReader["Tel"].ToString();
                    Invoice.Fax = zReader["Fax"].ToString();
                    Invoice.Mail = zReader["Mail"].ToString();
                    Invoice.Description = zReader["Description"].ToString();
                    if (zReader["AmountNotV"] != DBNull.Value)
                    {
                        Invoice.AmountNotV = double.Parse(zReader["AmountNotV"].ToString());
                    }

                    if (zReader["AmountVat"] != DBNull.Value)
                    {
                        Invoice.AmountVat = double.Parse(zReader["AmountVat"].ToString());
                    }

                    if (zReader["AmountTotal"] != DBNull.Value)
                    {
                        Invoice.AmountTotal = double.Parse(zReader["AmountTotal"].ToString());
                    }

                    Invoice.FileAttach = zReader["FileAttach"].ToString();
                    Invoice.ReferenceInvoice = zReader["ReferenceInvoice"].ToString();
                    Invoice.ReferenceDocument = zReader["ReferenceDocument"].ToString();
                    Invoice.CreditAccount = zReader["CreditAccount"].ToString();
                    Invoice.DebitAccount = zReader["DebitAccount"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Invoice.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Invoice.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Invoice.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Invoice.CreatedBy = zReader["CreatedBy"].ToString();
                    Invoice.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Invoice.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Invoice.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Invoice.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Invoice ("
         + " InvoiceID , InvoiceType , InvoiceDate,CategoryKey , CategoryName , PartnerKey , PartnerName , PartnerID , PartnerAddress , PartnerTax , Contact , Tel , Fax , Mail , Description , AmountNotV , AmountVat , AmountTotal , FileAttach , ReferenceInvoice , ReferenceDocument , CreditAccount , DebitAccount , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @InvoiceID , @InvoiceType ,@InvoiceDate, @CategoryKey , @CategoryName , @PartnerKey , @PartnerName , @PartnerID , @PartnerAddress , @PartnerTax , @Contact , @Tel , @Fax , @Mail , @Description , @AmountNotV , @AmountVat , @AmountTotal , @FileAttach , @ReferenceInvoice , @ReferenceDocument , @CreditAccount , @DebitAccount , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = Invoice.InvoiceID;
                zCommand.Parameters.Add("@InvoiceType", SqlDbType.NVarChar).Value = Invoice.InvoiceType;
                if (Invoice.InvoiceDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = Invoice.InvoiceDate;
                }

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Invoice.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Invoice.CategoryName;
                zCommand.Parameters.Add("@PartnerKey", SqlDbType.NVarChar).Value = Invoice.PartnerKey;
                zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = Invoice.PartnerName;
                zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = Invoice.PartnerID;
                zCommand.Parameters.Add("@PartnerAddress", SqlDbType.NVarChar).Value = Invoice.PartnerAddress;
                zCommand.Parameters.Add("@PartnerTax", SqlDbType.NVarChar).Value = Invoice.PartnerTax;
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Invoice.Contact;
                zCommand.Parameters.Add("@Tel", SqlDbType.NVarChar).Value = Invoice.Tel;
                zCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = Invoice.Fax;
                zCommand.Parameters.Add("@Mail", SqlDbType.NVarChar).Value = Invoice.Mail;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Invoice.Description;
                zCommand.Parameters.Add("@AmountNotV", SqlDbType.Money).Value = Invoice.AmountNotV;
                zCommand.Parameters.Add("@AmountVat", SqlDbType.Money).Value = Invoice.AmountVat;
                zCommand.Parameters.Add("@AmountTotal", SqlDbType.Money).Value = Invoice.AmountTotal;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.NVarChar).Value = Invoice.FileAttach;
                zCommand.Parameters.Add("@ReferenceInvoice", SqlDbType.NVarChar).Value = Invoice.ReferenceInvoice;
                zCommand.Parameters.Add("@ReferenceDocument", SqlDbType.NVarChar).Value = Invoice.ReferenceDocument;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Invoice.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Invoice.DebitAccount;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Invoice.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Invoice.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Invoice.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Invoice.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Invoice.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Invoice.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Invoice("
         + " InvoiceKey , InvoiceID , InvoiceType ,InvoiceDate, CategoryKey , CategoryName , PartnerKey , PartnerName , PartnerID , PartnerAddress , PartnerTax , Contact , Tel , Fax , Mail , Description , AmountNotV , AmountVat , AmountTotal , FileAttach , ReferenceInvoice , ReferenceDocument , CreditAccount , DebitAccount , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @InvoiceKey , @InvoiceID , @InvoiceType ,@InvoiceDate, @CategoryKey , @CategoryName , @PartnerKey , @PartnerName , @PartnerID , @PartnerAddress , @PartnerTax , @Contact , @Tel , @Fax , @Mail , @Description , @AmountNotV , @AmountVat , @AmountTotal , @FileAttach , @ReferenceInvoice , @ReferenceDocument , @CreditAccount , @DebitAccount , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Invoice.InvoiceKey != "" && Invoice.InvoiceKey.Length == 36)
                {
                    zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice.InvoiceKey);
                }
                else
                {
                    zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = Invoice.InvoiceID;
                zCommand.Parameters.Add("@InvoiceType", SqlDbType.NVarChar).Value = Invoice.InvoiceType;
                if (Invoice.InvoiceDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = Invoice.InvoiceDate;
                }

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Invoice.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Invoice.CategoryName;
                zCommand.Parameters.Add("@PartnerKey", SqlDbType.NVarChar).Value = Invoice.PartnerKey;
                zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = Invoice.PartnerName;
                zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = Invoice.PartnerID;
                zCommand.Parameters.Add("@PartnerAddress", SqlDbType.NVarChar).Value = Invoice.PartnerAddress;
                zCommand.Parameters.Add("@PartnerTax", SqlDbType.NVarChar).Value = Invoice.PartnerTax;
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Invoice.Contact;
                zCommand.Parameters.Add("@Tel", SqlDbType.NVarChar).Value = Invoice.Tel;
                zCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = Invoice.Fax;
                zCommand.Parameters.Add("@Mail", SqlDbType.NVarChar).Value = Invoice.Mail;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Invoice.Description;
                zCommand.Parameters.Add("@AmountNotV", SqlDbType.Money).Value = Invoice.AmountNotV;
                zCommand.Parameters.Add("@AmountVat", SqlDbType.Money).Value = Invoice.AmountVat;
                zCommand.Parameters.Add("@AmountTotal", SqlDbType.Money).Value = Invoice.AmountTotal;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.NVarChar).Value = Invoice.FileAttach;
                zCommand.Parameters.Add("@ReferenceInvoice", SqlDbType.NVarChar).Value = Invoice.ReferenceInvoice;
                zCommand.Parameters.Add("@ReferenceDocument", SqlDbType.NVarChar).Value = Invoice.ReferenceDocument;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Invoice.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Invoice.DebitAccount;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Invoice.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Invoice.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Invoice.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Invoice.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Invoice.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Invoice.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Invoice SET "
                        + " InvoiceID = @InvoiceID,"
                        + " InvoiceType = @InvoiceType,"
                        + " InvoiceDate = @InvoiceDate,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " PartnerKey = @PartnerKey,"
                        + " PartnerName = @PartnerName,"
                        + " PartnerID = @PartnerID,"
                        + " PartnerAddress = @PartnerAddress,"
                        + " PartnerTax = @PartnerTax,"
                        + " Contact = @Contact,"
                        + " Tel = @Tel,"
                        + " Fax = @Fax,"
                        + " Mail = @Mail,"
                        + " Description = @Description,"
                        + " AmountNotV = @AmountNotV,"
                        + " AmountVat = @AmountVat,"
                        + " AmountTotal = @AmountTotal,"
                        + " FileAttach = @FileAttach,"
                        + " ReferenceInvoice = @ReferenceInvoice,"
                        + " ReferenceDocument = @ReferenceDocument,"
                        + " CreditAccount = @CreditAccount,"
                        + " DebitAccount = @DebitAccount,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE InvoiceKey = @InvoiceKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Invoice.InvoiceKey != "" && Invoice.InvoiceKey.Length == 36)
                {
                    zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice.InvoiceKey);
                }
                else
                {
                    zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = Invoice.InvoiceID;
                zCommand.Parameters.Add("@InvoiceType", SqlDbType.NVarChar).Value = Invoice.InvoiceType;
                if (Invoice.InvoiceDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = Invoice.InvoiceDate;
                }

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Invoice.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Invoice.CategoryName;
                zCommand.Parameters.Add("@PartnerKey", SqlDbType.NVarChar).Value = Invoice.PartnerKey;
                zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = Invoice.PartnerName;
                zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = Invoice.PartnerID;
                zCommand.Parameters.Add("@PartnerAddress", SqlDbType.NVarChar).Value = Invoice.PartnerAddress;
                zCommand.Parameters.Add("@PartnerTax", SqlDbType.NVarChar).Value = Invoice.PartnerTax;
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Invoice.Contact;
                zCommand.Parameters.Add("@Tel", SqlDbType.NVarChar).Value = Invoice.Tel;
                zCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = Invoice.Fax;
                zCommand.Parameters.Add("@Mail", SqlDbType.NVarChar).Value = Invoice.Mail;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Invoice.Description;
                zCommand.Parameters.Add("@AmountNotV", SqlDbType.Money).Value = Invoice.AmountNotV;
                zCommand.Parameters.Add("@AmountVat", SqlDbType.Money).Value = Invoice.AmountVat;
                zCommand.Parameters.Add("@AmountTotal", SqlDbType.Money).Value = Invoice.AmountTotal;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.NVarChar).Value = Invoice.FileAttach;
                zCommand.Parameters.Add("@ReferenceInvoice", SqlDbType.NVarChar).Value = Invoice.ReferenceInvoice;
                zCommand.Parameters.Add("@ReferenceDocument", SqlDbType.NVarChar).Value = Invoice.ReferenceDocument;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Invoice.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Invoice.DebitAccount;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Invoice.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Invoice.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Invoice.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Invoice.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Invoice SET RecordStatus = 99 WHERE InvoiceKey = @InvoiceKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice.InvoiceKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Invoice WHERE InvoiceKey = @InvoiceKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice.InvoiceKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string ReturnInvoice()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Invoice SET ReplaceID = @ReplaceID, ReplaceDescription = @ReplaceDescription, ReplaceFile = @ReplaceFile, ReturnStatus = 1 WHERE InvoiceKey = @InvoiceKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Invoice.InvoiceKey);
                zCommand.Parameters.Add("@ReplaceID", SqlDbType.NVarChar).Value = Invoice.ReplaceID;
                zCommand.Parameters.Add("@ReplaceDescription", SqlDbType.NVarChar).Value = Invoice.ReplaceDescription;
                zCommand.Parameters.Add("@ReplaceFile", SqlDbType.NVarChar).Value = Invoice.ReplaceFile;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
