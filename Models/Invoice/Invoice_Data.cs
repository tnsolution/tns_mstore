﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Invoice_Data
    {
        /// <summary>
        /// Lấy danh sách hóa đơn ra từ ngày đến ngày
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Message"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="SearchID"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public static List<Invoice_Model> ListSearch(string PartnerNumber, out string Message, DateTime FromDate, DateTime ToDate, string SearchID, string Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Invoice WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND InvoiceDate BETWEEN @FromDate AND @ToDate AND InvoiceType = @InvoiceType";
            if (SearchID != "")
            {
                zSQL += " AND InvoiceID LIKE @SearchID";
            }
            zSQL += " ORDER BY InvoiceDate DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@SearchID", SqlDbType.NVarChar).Value = SearchID;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@InvoiceType", SqlDbType.NChar).Value = Type;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Invoice_Model> zList = new List<Invoice_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Invoice_Model()
                {
                    InvoiceKey = r["InvoiceKey"].ToString(),
                    InvoiceID = r["InvoiceID"].ToString(),
                    InvoiceType = r["InvoiceType"].ToString(),
                    InvoiceDate = (r["InvoiceDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["InvoiceDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    PartnerKey = r["PartnerKey"].ToString(),
                    PartnerName = r["PartnerName"].ToString(),
                    PartnerID = r["PartnerID"].ToString(),
                    PartnerAddress = r["PartnerAddress"].ToString(),
                    PartnerTax = r["PartnerTax"].ToString(),
                    Contact = r["Contact"].ToString(),
                    Tel = r["Tel"].ToString(),
                    Fax = r["Fax"].ToString(),
                    Mail = r["Mail"].ToString(),
                    Description = r["Description"].ToString(),
                    AmountNotV = r["AmountNotV"].ToDouble(),
                    AmountVat = r["AmountVat"].ToDouble(),
                    AmountTotal = r["AmountTotal"].ToDouble(),
                    FileAttach = r["FileAttach"].ToString(),
                    ReferenceInvoice = r["ReferenceInvoice"].ToString(),
                    ReferenceDocument = r["ReferenceDocument"].ToString(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                    ReplaceID = r["ReplaceID"].ToString(),
                    ReplaceDescription = r["ReplaceDescription"].ToString(),
                    ReplaceFile = r["ReplaceFile"].ToString(),
                    ReturnStatus = r["ReturnStatus"].ToInt(),
                });
            }
            return zList;
        }
        /// <summary>
        /// Lấy danh sách là hóa đơn ra và có VAT đầu vào từ ngày đến ngày
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Message"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static List<Invoice_Model> ListVatOUTRef(string PartnerNumber, out string Message, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Invoice WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            zSQL += " AND InvoiceDate BETWEEN @FromDate AND @ToDate AND InvoiceType='OUT' AND LEN(ReferenceInvoice)>=36";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Invoice_Model> zList = new List<Invoice_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Invoice_Model()
                {
                    InvoiceKey = r["InvoiceKey"].ToString(),
                    InvoiceID = r["InvoiceID"].ToString(),
                    InvoiceType = r["InvoiceType"].ToString(),
                    InvoiceDate = (r["InvoiceDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["InvoiceDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    PartnerKey = r["PartnerKey"].ToString(),
                    PartnerName = r["PartnerName"].ToString(),
                    PartnerID = r["PartnerID"].ToString(),
                    PartnerAddress = r["PartnerAddress"].ToString(),
                    PartnerTax = r["PartnerTax"].ToString(),
                    Contact = r["Contact"].ToString(),
                    Tel = r["Tel"].ToString(),
                    Fax = r["Fax"].ToString(),
                    Mail = r["Mail"].ToString(),
                    Description = r["Description"].ToString(),
                    AmountNotV = r["AmountNotV"].ToDouble(),
                    AmountVat = r["AmountVat"].ToDouble(),
                    AmountTotal = r["AmountTotal"].ToDouble(),
                    FileAttach = r["FileAttach"].ToString(),
                    ReferenceInvoice = r["ReferenceInvoice"].ToString(),
                    ReferenceDocument = r["ReferenceDocument"].ToString(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static List<Invoice_Close_Model> ListInvoiceYear(string PartnerNumber, int Year ,out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Invoice_Close WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND RIGHT(CloseDate, 4) = @Year";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Year", SqlDbType.NVarChar).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Invoice_Close_Model> zList = new List<Invoice_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Invoice_Close_Model()
                {
                    CloseKey = r["CloseKey"].ToString(),
                    CloseDate = r["CloseDate"].ToString(),
                    CloseType = r["CloseType"].ToString(),
                    CloseAmount = r["CloseAmount"].ToDouble(),
                    Description = r["Description"].ToString(),
                    JsonData = r["JsonData"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
