﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Document_Data
    {
        //TableKey KEY LIÊN KẾT Ở BẢNG KHÁC
        public static List<Document_Model> List(string PartnerNumber, string TableKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM GOB_Document WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND TableKey = @TableKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@TableKey", SqlDbType.NVarChar).Value = TableKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Document_Model> zList = new List<Document_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Document_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    FilePath = r["FilePath"].ToString(),
                    FileName = r["FileName"].ToString(),
                    FileExt = r["FileExt"].ToString(),
                    Description = r["Description"].ToString(),
                    Title = r["Title"].ToString(),
                });
            }

            return zList;
        }

        public static List<Document_Model> List(string PartnerNumber, int Category, string CustomerKey, string FileName, int Slug)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM GOB_Document WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";
            if (CustomerKey != string.Empty)
            {
                zSQL += " AND CustomerKey = @CustomerKey";
            }
            if (FileName != string.Empty)
            {
                zSQL += " AND FileName = @FileName";
            }
            if (Category != 0)
            {
                zSQL += " AND Category = @Category";
            }
            if (Slug != 0)
            {
                zSQL += " AND Slug = @Slug";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = FileName;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Document_Model> zList = new List<Document_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Document_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    FilePath = r["FilePath"].ToString(),
                    FileName = r["FileName"].ToString(),
                    FileExt = r["FileExt"].ToString(),
                    Base64 = r["Base64"].ToString(),
                    Description = r["Description"].ToString(),
                    Title = r["Title"].ToString(),
                    Type = r["Type"].ToInt(),
                    Category = r["Category"].ToInt(),
                    Parent = r["Parent"].ToInt(),
                    Slug = r["Slug"].ToInt(),
                    TableJoin = r["TableJoin"].ToString(),
                    TableKey = r["TableKey"].ToString(),
                    FromDate = (r["FromDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["FromDate"]),
                    ToDate = (r["ToDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ToDate"]),
                    SignDate = (r["SignDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["SignDate"]),
                    CustomerKey = r["CustomerKey"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            
            return zList;
        }

        public static List<TNItem> ListCate(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM GOB_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<TNItem> zList = new List<TNItem>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new TNItem()
                {
                    Value = r["CategoryKey"].ToString(),
                    Text = r["CategoryName"].ToString(),
                });
            }

            return zList;
        }
    }
}
