﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace TNStores
{
    public class Product_Data
    {
        public static List<Product_Model> Search(string PartnerNumber, out string Message, string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND (ProductID LIKE @Name OR ProductName LIKE @Name) ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Product_Model> ListNew(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Product_Model> Top25(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP 25 * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Product_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Product_Category_Model> ListCategory(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "WITH R AS ( SELECT CategoryKey,CategoryNameVN AS NAME,PARENT, DEPTH = 0,SORT = CAST(CategoryKey AS VARCHAR(MAX)) ";
            zSQL += " FROM PDT_Product_Category WHERE RecordStatus <> 99 AND Parent = 0 AND PartnerNumber = @PartnerNumber ";
            zSQL += " UNION ALL ";
            zSQL += " SELECT Sub.CategoryKey,Sub.CategoryNameVN AS NAME,Sub.PARENT,DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.CategoryKey AS VARCHAR(MAX)) ";
            zSQL += " FROM R INNER JOIN PDT_Product_Category Sub ON R.CategoryKey = Sub.Parent WHERE RecordStatus <> 99 AND PartnerNumber = @PartnerNumber) ";
            zSQL += " SELECT CategoryName = REPLICATE('---', R.DEPTH * 1) + R.[NAME], R.CategoryKey, R.SORT, R.DEPTH, R.Parent ";
            zSQL += " FROM R ORDER BY SORT  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Category_Model> zList = new List<Product_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryNameVN = r["CategoryName"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Level = r["SORT"].ToInt(),
                });
            }
            return zList;
        }

        public static List<Product_Model> SearchProduct(string PartnerNumber, out string Message, int CategoryKey)
        {
            DataTable zTable = new DataTable();

            string zSQL = "SELECT * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey=@CategoryKey";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
