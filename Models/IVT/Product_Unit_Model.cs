﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
public class Product_Unit_Model
{
#region [ Field Name ]
private int _UnitKey = 0;
private string _UnitID = "";
private string _UnitnameVN = "";
private string _UnitnameEN = "";
private string _UnitnameCN = "";
private string _Description = "";
private int _Rank = 0;
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
#endregion
 
#region [ Properties ]
public int UnitKey
{
get { return _UnitKey; }
set { _UnitKey = value; }
}
public string UnitID
{
get { return _UnitID; }
set { _UnitID = value; }
}
public string UnitnameVN
{
get { return _UnitnameVN; }
set { _UnitnameVN = value; }
}
public string UnitnameEN
{
get { return _UnitnameEN; }
set { _UnitnameEN = value; }
}
public string UnitnameCN
{
get { return _UnitnameCN; }
set { _UnitnameCN = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public int Rank
{
get { return _Rank; }
set { _Rank = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
#endregion
}
}
