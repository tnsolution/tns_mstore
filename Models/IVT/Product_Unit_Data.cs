﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Product_Unit_Data
    {
        public static List<Product_Unit_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product_Unit WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Unit_Model> zList = new List<Product_Unit_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Unit_Model()
                {
                    UnitKey = r["UnitKey"].ToInt(),
                    UnitID = r["UnitID"].ToString(),
                    UnitnameVN = r["UnitnameVN"].ToString(),
                    UnitnameEN = r["UnitnameEN"].ToString(),
                    UnitnameCN = r["UnitnameCN"].ToString(),
                    Description = r["Description"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
