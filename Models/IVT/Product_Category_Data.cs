﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TNStores
{
    public class Product_Category_Data
    {
        public static List<Product_Category_Model> List(string PartnerNumber, out string Message, int Level, bool zKT)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (zKT == false)
            {
                zSQL += " AND Level=@Level ";
            }
            else
            {
                zSQL += " AND Parent=@Level AND Level=2";
            }

            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Level;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Category_Model> zList = new List<Product_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryNameVN = r["CategoryNameVN"].ToString(),
                    CategoryNameEN = r["CategoryNameEN"].ToString(),
                    CategoryNameCN = r["CategoryNameCN"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Level = r["Level"].ToInt(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
