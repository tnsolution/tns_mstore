﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace TNStores
{
    public class Product_Unit_Info
    {

        public Product_Unit_Model Product_Unit = new Product_Unit_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Product_Unit_Info()
        {
        }
        public Product_Unit_Info(int UnitKey)
        {
            string zSQL = "SELECT * FROM PDT_Product_Unit WHERE UnitKey = @UnitKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = UnitKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["UnitKey"] != DBNull.Value)
                        Product_Unit.UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    Product_Unit.UnitID = zReader["UnitID"].ToString();
                    Product_Unit.UnitnameVN = zReader["UnitnameVN"].ToString();
                    Product_Unit.UnitnameEN = zReader["UnitnameEN"].ToString();
                    Product_Unit.UnitnameCN = zReader["UnitnameCN"].ToString();
                    Product_Unit.Description = zReader["Description"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        Product_Unit.Rank = int.Parse(zReader["Rank"].ToString());
                    Product_Unit.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Product_Unit.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Product_Unit.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Product_Unit.CreatedBy = zReader["CreatedBy"].ToString();
                    Product_Unit.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Product_Unit.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Product_Unit.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Product_Unit.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Unit ("
         + " UnitID , UnitnameVN , UnitnameEN , UnitnameCN , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @UnitID , @UnitnameVN , @UnitnameEN , @UnitnameCN , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UnitID", SqlDbType.NVarChar).Value = Product_Unit.UnitID;
                zCommand.Parameters.Add("@UnitnameVN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameVN;
                zCommand.Parameters.Add("@UnitnameEN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameEN;
                zCommand.Parameters.Add("@UnitnameCN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameCN;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Unit.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Unit.Rank;
                if (Product_Unit.PartnerNumber != "" && Product_Unit.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Unit.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Unit.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Unit.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Unit.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Unit.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Unit.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Unit("
         + " UnitKey , UnitID , UnitnameVN , UnitnameEN , UnitnameCN , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @UnitKey , @UnitID , @UnitnameVN , @UnitnameEN , @UnitnameCN , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Product_Unit.UnitKey;
                zCommand.Parameters.Add("@UnitID", SqlDbType.NVarChar).Value = Product_Unit.UnitID;
                zCommand.Parameters.Add("@UnitnameVN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameVN;
                zCommand.Parameters.Add("@UnitnameEN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameEN;
                zCommand.Parameters.Add("@UnitnameCN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameCN;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Unit.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Unit.Rank;
                if (Product_Unit.PartnerNumber != "" && Product_Unit.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Unit.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Unit.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Unit.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Unit.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Unit.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Unit.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE PDT_Product_Unit SET "
                        + " UnitID = @UnitID,"
                        + " UnitnameVN = @UnitnameVN,"
                        + " UnitnameEN = @UnitnameEN,"
                        + " UnitnameCN = @UnitnameCN,"
                        + " Description = @Description,"
                        + " Rank = @Rank,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE UnitKey = @UnitKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Product_Unit.UnitKey;
                zCommand.Parameters.Add("@UnitID", SqlDbType.NVarChar).Value = Product_Unit.UnitID;
                zCommand.Parameters.Add("@UnitnameVN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameVN;
                zCommand.Parameters.Add("@UnitnameEN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameEN;
                zCommand.Parameters.Add("@UnitnameCN", SqlDbType.NVarChar).Value = Product_Unit.UnitnameCN;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Unit.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Unit.Rank;
                if (Product_Unit.PartnerNumber != "" && Product_Unit.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Unit.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Unit.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Unit.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Unit.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Product_Unit SET RecordStatus = 99 WHERE UnitKey = @UnitKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Product_Unit.UnitKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Product_Unit WHERE UnitKey = @UnitKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Product_Unit.UnitKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
