﻿namespace TNStores
{
    public class Unit_Converted_Model
    {
        #region [ Field Name ]
        private int? _AutoKey = 0;
        private string _ProductKey = "";
        private string _ProductName = "";
        private int? _StandardUnitKey = 0;
        private string _StandardUnitName = "";
        private float? _Rate = 0;
        private int? _UnitKey = 0;
        private string _UnitName = "";
        private string _PartnerNumber = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int? AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public int? StandardUnitKey
        {
            get { return _StandardUnitKey; }
            set { _StandardUnitKey = value; }
        }
        public string StandardUnitName
        {
            get { return _StandardUnitName; }
            set { _StandardUnitName = value; }
        }
        public float? Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }
        public int? UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
