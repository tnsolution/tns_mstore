﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace TNStores.Controllers
{
    public class DesktopController : BaseController
    {
        // GET: Desktop
        public ActionResult Index()
        {
            return View();
        }

        #region [CUSTOMER]
        public ActionResult ListCustomer()
        {
            ViewBag.ListData = Customer_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Desktop/Customer/List.cshtml");
        }

        [HttpGet]
        public JsonResult DetailCustomer(string CustomerKey)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            Customer_Model zModel = zInfo.Customer;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateCustomer(string CustomerKey = "", string TaxNumber = "", string Phone = "", string FullName = "", string Address = "", string Description = "")
        {
            Customer_Model zModel = new Customer_Model();
            zModel.CustomerKey = CustomerKey;
            zModel.TaxNumber = TaxNumber;
            zModel.Phone = Phone;
            zModel.Address = Address;
            zModel.FullName = FullName;
            zModel.Note = Description;
            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.Customer = zModel.TrimStringProperties();
            if (CustomerKey.Length >= 36)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ServerKey();
            }

            ServerResult zResult = new ServerResult();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteCustomer(string CustomerKey)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info();
            zInfo.Customer.CustomerKey = CustomerKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [PurchaseOrder]
        public ActionResult PurchaseOrderEdit(string Key)
        {
            return View("~/Views/Desktop/PO/PurchaseEdit.cshtml");
        }

        private string Customer_PurchaseOrder(string Key, string Phone, string Name, string Address, string Tax, int isVendor, out string Message)
        {
            string CustomerKey = Guid.NewGuid().ToString();
            Customer_Info zInfo = new Customer_Info();
            if (Key.Length == 0 &&
                Phone.Length > 0 &&
                Name.Length > 0)
            {
                zInfo.Customer.FullName = Name;
                zInfo.Customer.Phone = Phone;
                zInfo.Customer.Address = Address;
                zInfo.Customer.CustomerKey = CustomerKey;
                zInfo.Customer.PartnerNumber = UserLog.PartnerNumber;
                zInfo.Customer.CreatedBy = UserLog.UserKey;
                zInfo.Customer.CreatedName = UserLog.EmployeeName;
                zInfo.Customer.ModifiedBy = UserLog.UserKey;
                zInfo.Customer.ModifiedName = UserLog.EmployeeName;
                zInfo.Create_ClientKey();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Message = string.Empty;
            }
            else
            {
                CustomerKey = Key;
                Message = zInfo.Message;
            }
            return CustomerKey;
        }
        private void Payment_PurchaseOrder(string OrderID, double Amount, string CustomerKey, string Name, string Phone, out string Message)
        {
            Payment_Model zPayment = new Payment_Model();
            zPayment.PartnerNumber = UserLog.PartnerNumber;
            zPayment.CreatedBy = UserLog.UserKey;
            zPayment.CreatedName = UserLog.EmployeeName;
            zPayment.ModifiedBy = UserLog.UserKey;
            zPayment.ModifiedName = UserLog.EmployeeName;
            zPayment.DocumentID = OrderID;
            zPayment.PaymentID = Payment_Data.Auto_Payment_ID(UserLog.PartnerNumber);
            zPayment.PaymentDate = DateTime.Now;
            zPayment.PaymentDescription = "Chi đơn nhâp, mua hàng [" + OrderID + "] tự động !.";
            zPayment.AmountCurrencyMain = Amount;
            zPayment.Slug = 9;
            zPayment.CustomerKey = CustomerKey;
            zPayment.CustomerName = Name;
            zPayment.CustomerPhone = Phone;
            zPayment.CategoryKey = 12;
            zPayment.CategoryName = "Chi mua nhập hàng";

            Payment_Info zInfo = new Payment_Info();
            zInfo.Payment = zPayment;
            zInfo.Create_ServerKey();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Message = string.Empty;
            }
            else
            {
                Message = zInfo.Message;
            }
        }
        private void Save_PurchaseOrderItem(List<Input_Product_Model> ListItem, string OrderKey, out string Message)
        {
            Message = string.Empty;
            for (int i = 0; i < ListItem.Count; i++)
            {
                Input_Product_Info zItemInfo = new Input_Product_Info();
                zItemInfo.Input_Product = ListItem[i];
                zItemInfo.Input_Product.OrderKey = OrderKey;
                zItemInfo.Input_Product.PartnerNumber = UserLog.PartnerNumber;
                zItemInfo.Input_Product.CreatedBy = UserLog.UserKey;
                zItemInfo.Input_Product.CreatedName = UserLog.EmployeeName;
                zItemInfo.Input_Product.ModifiedBy = UserLog.UserKey;
                zItemInfo.Input_Product.ModifiedName = UserLog.EmployeeName;
                zItemInfo.Create_ServerKey();

                if (zItemInfo.Code != "200" &&
                    zItemInfo.Code != "201")
                {
                    Message = zItemInfo.Message;
                    return;
                }
            }
        }
        private void Update_PurchaseStockItem(List<Input_Product_Model> ListItem, string Orderkey, string OrderID, out string Message)
        {
            string SQL = "";
            for (int i = 0; i < ListItem.Count; i++)
            {
                string ProductKey = ListItem[i].ProductKey;
                float QuantityFact = ListItem[i].QuantityFact;
                string QuantityLog = DateTime.Now + ";IN;" + Orderkey + ";" + OrderID + ";" + QuantityFact + Environment.NewLine;

                SQL += "UPDATE PDT_Product SET QuantityFact = QuantityFact + " + QuantityFact + ", QuantityLog = ISNULL(QuantityLog, '') + N'" + QuantityLog + "' WHERE RecordStatus <> 99 AND ProductKey = '" + ProductKey + "'";
            }

            Helper.RunSQL(SQL, out Message);
        }

        [HttpPost]
        public JsonResult Save_PurchaseOrder(string Info, string Method = "Pay")
        {
            ServerResult zResult = new ServerResult();
            Input_Order_Model zOrderClient = JsonConvert.DeserializeObject<Input_Order_Model>(Info);
            double RealAmount = 0;
            string OrderKey = Guid.NewGuid().ToString();
            string OrderID = Helper.AutoID_PurchaseOrder(UserLog.PartnerNumber, "PO");
            string Message = "";
            try
            {
                Input_Order_Model zOrderModel = (Input_Order_Model)Session["PurchaseOrder"];

                if (zOrderModel != null && zOrderModel.ListItem.Count > 0)
                {
                    #region [Khách hàng]
                    string CustomerKey = Customer_PurchaseOrder(zOrderClient.VendorKey, zOrderClient.VendorPhone, zOrderClient.VendorName, zOrderClient.VendorAddress, string.Empty, 0, out Message);
                    if (Message.Length > 0)
                    {
                        zResult.Success = false;
                        zResult.Message = Message;
                        return Json(zResult, JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    #region [Đơn hàng]                   
                    RealAmount = zOrderClient.AmountOrder - zOrderClient.AmountDiscount;

                    zOrderModel.DocumentOrigin = zOrderClient.DocumentOrigin;
                    zOrderModel.OrderID = OrderID;
                    zOrderModel.AmountOrder = zOrderClient.AmountOrder;
                    zOrderModel.PercentDiscount = zOrderClient.PercentDiscount;
                    zOrderModel.AmountDiscount = zOrderClient.AmountDiscount;
                    zOrderModel.AmountOrderIncVAT = RealAmount;

                    zOrderModel.VendorKey = CustomerKey;
                    zOrderModel.VendorName = zOrderClient.VendorName;
                    zOrderModel.VendorPhone = zOrderClient.VendorPhone;
                    zOrderModel.VendorAddress = zOrderClient.VendorAddress;
                    zOrderModel.OrderDate = DateTime.Now;

                    zOrderModel.PartnerNumber = UserLog.PartnerNumber;
                    zOrderModel.CreatedBy = UserLog.UserKey;
                    zOrderModel.CreatedName = UserLog.EmployeeName;
                    zOrderModel.ModifiedBy = UserLog.UserKey;
                    zOrderModel.ModifiedName = UserLog.EmployeeName;

                    Input_Order_Info zInfo = new Input_Order_Info();
                    zInfo.Input_Order = zOrderModel;
                    zInfo.Input_Order.OrderKey = OrderKey;
                    zInfo.Create_ClientKey();

                    if (zInfo.Code == "200" ||
                        zInfo.Code == "201")
                    {
                        Save_PurchaseOrderItem(zOrderModel.ListItem, OrderKey, out Message);
                        if (Message != string.Empty)
                        {
                            zResult.Success = false;
                            zResult.Message = Message;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        zResult.Success = false;
                        zResult.Message = zInfo.Message;
                        return Json(zResult, JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    #region [Cập nhật tồn sản phẩm]
                    Update_PurchaseStockItem(zOrderModel.ListItem, OrderKey, OrderID, out Message);
                    if (Message != string.Empty)
                    {
                        zResult.Success = false;
                        zResult.Message = Message;
                        return Json(zResult, JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    if (Method == "Pay")
                    {
                        #region [Phiếu chi]
                        Payment_PurchaseOrder(OrderID, RealAmount, CustomerKey, zOrderClient.VendorName, zOrderClient.VendorPhone, out Message);
                        if (Message != string.Empty)
                        {
                            zResult.Success = false;
                            zResult.Message = Message;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                        #endregion
                    }
                    Session["PurchaseOrder"] = null;

                    if (Message == string.Empty)
                    {
                        zResult.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        //xem lại đơn, thông tin thanh toán
        public ActionResult Preview_PurchaseOrder()
        {
            Input_Order_Model zOrder = new Input_Order_Model();
            if (Session["PurchaseOrder"] != null)
            {
                zOrder = Session["PurchaseOrder"] as Input_Order_Model;
            }

            return PartialView("~/Views/Shared/_Preview_PurchaseOrder.cshtml", zOrder);
        }
        [HttpGet]
        public JsonResult Check_PurchaseOrder()
        {
            ServerResult zResult = new ServerResult();
            if (Session["PurchaseOrder"] == null)
            {
                zResult.Success = false;
                zResult.Message = "Bạn chưa chọn mặt hàng !.";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = true;
                zResult.Data = Url.Action("Preview_PurchaseOrder", "Desktop");
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        //danh sách đơn hàng nhập
        public ActionResult List_PurchaseOrder(string btnAction = "", string FromDate = "", string ToDate = "")
        {
            #region [--DateTime--]
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate == string.Empty &&
                ToDate == string.Empty)
            {
                zFromDate = TN_Utils.FirstDayOfWeek(DateTime.Now);
                zToDate = TN_Utils.LastDayOfWeek(DateTime.Now);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                switch (btnAction)
                {
                    case "btn_Prev":
                        zFromDate = zFromDate.AddDays(-6);
                        zToDate = zToDate.AddDays(-6);
                        break;
                    case "btn_Next":
                        zFromDate = zFromDate.AddDays(+6);
                        zToDate = zToDate.AddDays(+6);
                        break;
                }
            }
            #endregion

            var List = Input_Order_Data.List(UserLog.PartnerNumber, out string Message, zFromDate, zToDate);
            ViewBag.List = List;
            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");

            return View("~/Views/Desktop/PO/PurchaseList.cshtml");
        }
        [HttpGet]
        public JsonResult Sum_PurchaseOrder()
        {
            ServerResult zResult = new ServerResult();
            Input_Order_Model zOrder = new Input_Order_Model();
            if (Session["PurchaseOrder"] != null)
            {
                zOrder = Session["PurchaseOrder"] as Input_Order_Model;
                if (zOrder.ListItem.Count > 0)
                {
                    zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);

                    zResult.Success = true;
                    zResult.Data = JsonConvert.SerializeObject(zOrder);
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }

            zResult.Success = false;
            zResult.Message = "Chưa có đơn hàng";
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        //hủy đơn hàng nhập mua
        [HttpPost]
        public JsonResult Cancel_PurchaseOrder(string OrderKey)
        {
            ServerResult zServerResult = new ServerResult();

            Session["PurchaseOrder"] = null;
            Input_Order_Info zInfo = new Input_Order_Info();
            zInfo.Delete(OrderKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zServerResult.Data = OrderKey;
                zServerResult.Success = true;
            }
            else
            {
                zServerResult.Success = false;
                zServerResult.Message = zInfo.Message;
            }
            return Json(zServerResult, JsonRequestBehavior.AllowGet);
        }

        //xóa item đơn hàng nhập mua
        public ActionResult DelItem_PurchaseOrder(string ObjectKey)
        {
            string Message = "";
            Input_Order_Model zOrder = (Input_Order_Model)Session["PurchaseOrder"];
            var Item = zOrder.ListItem.FirstOrDefault(s => s.ProductKey == ObjectKey);
            if (Item != null)
            {
                zOrder.ListItem.Remove(Item);
                zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.PurchasePrice);
            }

            if (zOrder.ListItem.Count == 0)
            {
                Session["PurchaseOrder"] = null;
            }
            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out Message);
            return View("~/Views/Desktop/PO/_PurchaseItem.cshtml", zOrder.ListItem);
        }

        //edit item đơn hàng nhập mua
        public ActionResult EditItem_PurchaseOrder(string Item)
        {
            Input_Product_Model zClientItem = JsonConvert.DeserializeObject<Input_Product_Model>(Item);

            Input_Order_Model zOrder = (Input_Order_Model)Session["PurchaseOrder"];
            var zEditItem = zOrder.ListItem.FirstOrDefault(s => s.ProductKey == zClientItem.ProductKey);
            if (zClientItem != null)
            {
                zEditItem.UnitKey = zClientItem.UnitKey;
                zEditItem.UnitName = zClientItem.UnitName;
                zEditItem.QuantityFact = zClientItem.QuantityFact;
                zEditItem.SalePrice = zClientItem.SalePrice;
                zEditItem.PurchasePrice = zClientItem.PurchasePrice;
                zEditItem.Amount = (zClientItem.PurchasePrice * zClientItem.QuantityFact);
                zEditItem.Description = zClientItem.Description.Trim();
            }
            zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);
            zOrder.TotalQuantity = zOrder.ListItem.Sum(s => s.QuantityFact);
            Session["PurchaseOrder"] = zOrder;

            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Desktop/PO/_PurchaseItem.cshtml", zOrder.ListItem);
        }

        //thêm item đơn hàng nhập mua
        public ActionResult AddItem_PurchaseOrder(string ProductKey = "")
        {
            string Message = "";
            Input_Order_Model zOrder;
            if (Session["PurchaseOrder"] == null)
            {
                zOrder = new Input_Order_Model();
                Product_Model zProduct = new Product_Info(ProductKey).Product;
                zOrder.ListItem.Add(new Input_Product_Model
                {
                    QuantityFact = 1,
                    ProductKey = zProduct.ProductKey,
                    ProductName = zProduct.ProductName,
                    PurchasePrice = zProduct.StandardCost,
                    Amount = zProduct.StandardCost,
                    SalePrice = zProduct.SalePrice,
                    UnitKey = zProduct.StandardUnitKey,
                    UnitName = zProduct.StandardUnitName
                });
            }
            else
            {
                zOrder = (Input_Order_Model)Session["PurchaseOrder"];
                var Item = zOrder.ListItem.FirstOrDefault(s => s.ProductKey == ProductKey);
                if (Item != null)
                {
                    Item.QuantityFact = Item.QuantityFact + 1;
                    Item.Amount = Item.PurchasePrice * Item.QuantityFact;
                }
                else
                {
                    Product_Model zProduct = new Product_Info(ProductKey).Product;
                    zOrder.ListItem.Add(new Input_Product_Model
                    {
                        QuantityFact = 1,
                        ProductKey = zProduct.ProductKey,
                        ProductName = zProduct.ProductName,
                        PurchasePrice = zProduct.StandardCost,
                        SalePrice = zProduct.SalePrice,
                        Amount = zProduct.StandardCost,
                        UnitKey = zProduct.StandardUnitKey,
                        UnitName = zProduct.StandardUnitName
                    });
                }
            }

            zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.PurchasePrice);
            Session["PurchaseOrder"] = zOrder;

            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out Message);
            return View("~/Views/Desktop/PO/_PurchaseItem.cshtml", zOrder.ListItem);
        }

        /// <summary>
        /// Tạo đơn hàng nhập mới
        /// </summary>
        /// <param name="OrderKey"></param>
        /// <returns></returns>
        public ActionResult PurchaseOrderNew()
        {
            string Message = "";
            Input_Order_Model zModel = new Input_Order_Model();
            zModel.OrderID = "Tự động phát sinh"; //Helper.AutoID_InputOrder(UserLog.PartnerNumber, "PO");
            zModel.OrderDate = DateTime.Now;
            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;
            Session["PurchaseOrder"] = zModel;
            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out Message);
            return View("~/Views/Desktop/PO/PurchaseOrder.cshtml", zModel);
        }
        #endregion

        #region [SaleOrder]
        [HttpGet]
        public ActionResult Receipt_SaleOrder(string OrderKey)
        {
            ServerResult zResult = new ServerResult();
            Order_Info zInfo = new Order_Info(OrderKey);
            Order_Model zModel = zInfo.Order;
            return PartialView("~/Views/Shared/_CollectMoney_SaleOrder.cshtml", zModel);
        }

        [HttpPost]
        public JsonResult Collect_SaleOrder(string Amount, string OrderID, string Name, string Phone, string Description, string CustomerKey, string OrderKey)
        {
            ServerResult zResult = new ServerResult();
            string Message = "";

            Receipt_SaleOrder(OrderID, Amount.ToDouble(), CustomerKey, Name, Phone, out Message);
            if (Message != string.Empty)
            {
                zResult.Success = false;
                zResult.Message = Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {

                double temp = Order_Data.SumOrder(UserLog.PartnerID, OrderID, out Message);
                temp = temp + Amount.ToDouble();

                Order_Info zInfo = new Order_Info(OrderKey);
                if (zInfo.Order.AmountOrder <= temp)
                {
                    zInfo.Order.StatusOrder = 9;
                    zInfo.Order.StatusName = "Đã thanh toán";
                    zInfo.Order.ModifiedBy = UserLog.UserKey;
                    zInfo.Order.ModifiedName = UserLog.EmployeeName;
                    zInfo.UpdateStatus();
                }


                zResult.Success = true;
                zResult.Message = string.Empty;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        private string Customer_SaleOrder(string Key, string Phone, string Name, string Address, string Tax, int isVendor, out string Message)
        {
            string CustomerKey = Guid.NewGuid().ToString();
            Customer_Info zInfo = new Customer_Info();
            if (Key.Length == 0 &&
                Phone.Length > 0 &&
                Name.Length > 0)
            {
                zInfo.Customer.FullName = Name;
                zInfo.Customer.Phone = Phone;
                zInfo.Customer.Address = Address;
                zInfo.Customer.CustomerKey = CustomerKey;
                zInfo.Customer.PartnerNumber = UserLog.PartnerNumber;
                zInfo.Customer.CreatedBy = UserLog.UserKey;
                zInfo.Customer.CreatedName = UserLog.EmployeeName;
                zInfo.Customer.ModifiedBy = UserLog.UserKey;
                zInfo.Customer.ModifiedName = UserLog.EmployeeName;
                zInfo.Create_ClientKey();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Message = string.Empty;
            }
            else
            {
                CustomerKey = Key;
                Message = zInfo.Message;
            }
            return CustomerKey;
        }
        private void Receipt_SaleOrder(string OrderID, double Amount, string CustomerKey, string Name, string Phone, out string Message)
        {
            Receipt_Model zModel = new Receipt_Model();
            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;
            zModel.DocumentID = OrderID;
            zModel.ReceiptID = Receipt_Data.Auto_Receipt_ID(UserLog.PartnerNumber);
            zModel.ReceiptDate = DateTime.Now;
            zModel.ReceiptDescription = "Thu bán đơn hàng [" + OrderID + "] tự động !.";
            zModel.AmountCurrencyMain = Amount;
            zModel.Slug = 9;
            zModel.CustomerKey = CustomerKey;
            zModel.CustomerName = Name;
            zModel.CustomerPhone = Phone;
            zModel.CategoryKey = 12;
            zModel.CategoryName = "Thu bán hàng";

            Receipt_Info zInfo = new Receipt_Info();
            zInfo.Receipt = zModel;
            zInfo.Create_ServerKey();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Message = string.Empty;
            }
            else
            {
                Message = zInfo.Message;
            }
        }
        private void Update_SaleStockItem(List<Order_Item_Model> ListItem, string OrderKey, string OrderID, out string Message)
        {
            string SQL = "";
            for (int i = 0; i < ListItem.Count; i++)
            {
                string ProductKey = ListItem[i].ObjectKey;
                float QuantityFact = ListItem[i].Quantity;
                string QuantityLog = DateTime.Now + ";OUT;NEW;" + OrderKey + ";" + OrderID + ";" + QuantityFact;

                SQL += "UPDATE PDT_Product SET QuantityFact = QuantityFact - " + QuantityFact + ", QuantityLog = ISNULL(QuantityLog, '') + N'" + QuantityLog + "' WHERE RecordStatus <> 99 AND ProductKey = '" + ProductKey + "'";
            }

            Helper.RunSQL(SQL, out Message);
        }
        private void Return_SaleOrderItem(List<Order_Item_Model> ListItem, string OrderKey, string OrderID, out string Message)
        {
            string SQL = "";
            for (int i = 0; i < ListItem.Count; i++)
            {
                string ProductKey = ListItem[i].ObjectKey;
                float QuantityFact = ListItem[i].Quantity;
                string QuantityLog = DateTime.Now + ";OUT;RETURN;" + OrderKey + ";" + OrderID + ";" + QuantityFact + Environment.NewLine;

                SQL += "UPDATE PDT_Product SET QuantityFact = QuantityFact + " + QuantityFact + ", QuantityLog = ISNULL(QuantityLog, '') + N'" + QuantityLog + "' WHERE RecordStatus <> 99 AND ProductKey = '" + ProductKey + "'";
            }

            Helper.RunSQL(SQL, out Message);
        }
        private void Return_SaleOrderMoney(string OrderID, out string Message)
        {
            Receipt_Info zInfo = new Receipt_Info(OrderID, true);
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Message = string.Empty;
            }
            else
            {
                Message = zInfo.Message;
            }

            Message = string.Empty;
        }
        private void Save_SaleOrderItem(List<Order_Item_Model> ListItem, string OrderKey, out string Message)
        {
            Message = string.Empty;
            for (int i = 0; i < ListItem.Count; i++)
            {
                Order_Item_Info zInfo = new Order_Item_Info();
                zInfo.Order_Item = ListItem[i];
                zInfo.Order_Item.OrderKey = OrderKey;
                zInfo.Order_Item.PartnerNumber = UserLog.PartnerNumber;
                zInfo.Order_Item.CreatedBy = UserLog.UserKey;
                zInfo.Order_Item.CreatedName = UserLog.EmployeeName;
                zInfo.Order_Item.ModifiedBy = UserLog.UserKey;
                zInfo.Order_Item.ModifiedName = UserLog.EmployeeName;
                zInfo.Create_ServerKey();

                if (zInfo.Code != "200" &&
                    zInfo.Code != "201")
                {
                    Message = zInfo.Message;
                }
            }
        }
        //in đơn hàng 
        public ActionResult Print_SaleOrder(string OrderKey)
        {
            Order_Info zInfo = new Order_Info(OrderKey);
            zInfo.Order.ListItem = Order_Data.ListItem(OrderKey, out string Message);

            Partner_Model zPart = new Partner_Info(UserLog.PartnerNumber).Partner;
            ViewBag.StoreName = zPart.StoreName;
            ViewBag.StorePhone = zPart.PartnerPhone;
            ViewBag.StoreAddress = zPart.PartnerAddress;
            ViewBag.StoreLogo = zPart.Logo_Large;
            ViewBag.BillConfig = zPart.BillConfig;

            string b64String = TN_Utils.QRWrite(zInfo.Order.OrderID, out Message);
            ViewBag.QRCode = b64String;

            return PartialView("~/Views/Shared/_Print.cshtml", zInfo.Order);
        }
        //
        [HttpPost]
        public JsonResult Save_SaleOrder(string Info, string Method)
        {
            string OrderID = Helper.AutoID_SaleOrder(UserLog.PartnerNumber, "SO");
            string OrderKey = Guid.NewGuid().ToString();

            ServerResult zResult = new ServerResult();
            try
            {
                Order_Model zOrderModel = (Order_Model)Session["SaleOrder"];
                Order_Model zOrderClient = JsonConvert.DeserializeObject<Order_Model>(Info);
                if (zOrderModel != null &&
                    zOrderModel.ListItem.Count > 0)
                {
                    #region [Khách hàng]
                    string CustomerKey = Customer_PurchaseOrder(zOrderClient.BuyerKey, zOrderClient.BuyerPhone, zOrderClient.BuyerName, string.Empty, string.Empty, 0, out string Message);
                    if (Message.Length > 0)
                    {
                        zResult.Success = false;
                        zResult.Message = Message;
                        return Json(zResult, JsonRequestBehavior.AllowGet);
                    }
                    #endregion                    

                    #region [Đơn hàng]
                    zOrderModel.OrderID = OrderID;
                    if (zOrderClient.PercentDiscount > 0 && zOrderClient.AmountDiscount > 0)
                    {
                        zOrderModel.AmountOrder = zOrderClient.AmountOrder - zOrderClient.AmountDiscount;
                        zOrderModel.PercentDiscount = zOrderClient.PercentDiscount;
                        zOrderModel.AmountDiscount = zOrderClient.AmountDiscount;
                    }
                    else
                    {
                        zOrderModel.AmountOrder = zOrderClient.AmountOrder;
                        zOrderModel.PercentDiscount = zOrderClient.PercentDiscount;
                        zOrderModel.AmountDiscount = zOrderClient.AmountDiscount;
                    }

                    zOrderModel.AmountReceived = zOrderClient.AmountReceived;
                    zOrderModel.AmountReturned = zOrderClient.AmountReturned;

                    zOrderModel.BuyerKey = CustomerKey;
                    zOrderModel.BuyerName = zOrderClient.BuyerName;
                    zOrderModel.BuyerPhone = zOrderClient.BuyerPhone;
                    zOrderModel.OrderDate = DateTime.Now;

                    //thực thu, nếu khách trả nhiều hơn tiền hàng thì thực thuc = tiền hàng (hoàn tiền lại), còn ít hơn thì thực thu = tiền khách trả (cọc).
                    double RealAmount = 0;
                    if (zOrderClient.AmountReceived >=
                        zOrderClient.AmountOrder)
                    {
                        RealAmount = zOrderClient.AmountOrder;
                        zOrderModel.StatusOrder = 9;
                        zOrderModel.StatusName = "Đã thanh toán";
                    }
                    else
                    {
                        RealAmount = zOrderClient.AmountReceived;
                        zOrderModel.StatusOrder = 8;
                        zOrderModel.StatusName = "Đã cọc";
                    }

                    zOrderModel.PartnerNumber = UserLog.PartnerNumber;
                    zOrderModel.CreatedBy = UserLog.UserKey;
                    zOrderModel.CreatedName = UserLog.EmployeeName;
                    zOrderModel.ModifiedBy = UserLog.UserKey;
                    zOrderModel.ModifiedName = UserLog.EmployeeName;

                    Order_Info zInfo = new Order_Info();
                    zInfo.Order = zOrderModel;
                    zInfo.Order.OrderKey = OrderKey;
                    zInfo.Create_ClientKey();

                    if (zInfo.Code == "200" ||
                        zInfo.Code == "201")
                    {
                        Save_SaleOrderItem(zOrderModel.ListItem, OrderKey, out Message);
                        if (Message != string.Empty)
                        {
                            zResult.Success = false;
                            zResult.Message = Message;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        zResult.Success = false;
                        zResult.Message = zInfo.Message;
                        return Json(zResult, JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    #region [Cap nhật tồn sản phẩm]
                    Update_SaleStockItem(zOrderModel.ListItem, OrderKey, OrderID, out Message);
                    if (Message != string.Empty)
                    {
                        zResult.Success = false;
                        zResult.Message = Message;
                        return Json(zResult, JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    if (Method == "Pay")
                    {
                        #region [Phiếu thu]
                        Receipt_SaleOrder(OrderID, RealAmount, CustomerKey, zOrderClient.BuyerName, zOrderClient.BuyerPhone, out Message);
                        if (Message != string.Empty)
                        {
                            zResult.Success = false;
                            zResult.Message = Message;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                        #endregion
                    }
                    Session["SaleOrder"] = null;
                    if (Message == string.Empty)
                    {
                        zResult.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            if (zResult.Success)
            {
                zResult.Data = Url.Action("Print_SaleOrder", "Desktop", new { OrderKey });
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        //xem trước hóa đơn, bản in             
        public ActionResult Preview_SaleOrder()
        {
            Order_Model zOrder = new Order_Model();
            if (Session["SaleOrder"] != null)
            {
                zOrder = Session["SaleOrder"] as Order_Model;
            }

            return PartialView("~/Views/Shared/_Preview_SaleOrder.cshtml", zOrder);
        }
        //kiem tra đơn hàng
        [HttpGet]
        public JsonResult Check_SaleOrder()
        {
            ServerResult zResult = new ServerResult();
            if (Session["SaleOrder"] == null)
            {
                zResult.Success = false;
                zResult.Message = "Bạn chưa chọn mặt hàng !.";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Order_Model zOrder = Session["SaleOrder"] as Order_Model;
                if (zOrder.ListItem.Count <= 0)
                {
                    zResult.Success = false;
                    zResult.Message = "Bạn chưa chọn mặt hàng !.";
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }

                zResult.Success = true;
                zResult.Data = Url.Action("Preview_SaleOrder", "Desktop");
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        //DANH SÁCH ĐƠN HÀNG
        public ActionResult List_SaleOrder(string btnAction = "", string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate == string.Empty &&
                ToDate == string.Empty)
            {
                zFromDate = TN_Utils.FirstDayOfWeek(DateTime.Now);
                zToDate = TN_Utils.LastDayOfWeek(DateTime.Now);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {

                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                switch (btnAction)
                {
                    case "btn_Prev":
                        zFromDate = zFromDate.AddDays(-6);
                        zToDate = zToDate.AddDays(-6);
                        break;
                    case "btn_Next":
                        zFromDate = zFromDate.AddDays(+6);
                        zToDate = zToDate.AddDays(+6);
                        break;
                }
            }

            var ListOrder = Order_Data.List(UserLog.PartnerNumber, out string Message, zFromDate, zToDate);
            ViewBag.ListOrder = ListOrder;
            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");
            return View("~/Views/Desktop/SO/SaleList.cshtml");
        }
        [HttpGet]
        public JsonResult Sum_SaleOrder()
        {
            ServerResult zResult = new ServerResult();
            Order_Model zOrder = new Order_Model();
            if (Session["SaleOrder"] != null)
            {
                zOrder = Session["SaleOrder"] as Order_Model;

                if (zOrder.ListItem.Count > 0)
                {
                    //tổng tiền giảm giá
                    zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);
                    //tổng tiền đơn hàng
                    zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);

                    zResult.Success = true;
                    zResult.Data = JsonConvert.SerializeObject(zOrder);
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }

            zResult.Success = false;
            zResult.Message = "Chưa có đơn hàng";
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        //xóa đơn hàng
        [HttpPost]
        public JsonResult Cancel_SaleOrder(string OrderKey)
        {
            ServerResult zServerResult = new ServerResult();
            Session["SaleOrder"] = null;
            Order_Info zInfo = new Order_Info(OrderKey);
            zInfo.Order.ListItem = Order_Data.ListItem(OrderKey, out string Message);

            zInfo.Delete(OrderKey);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zServerResult.Data = OrderKey;
                zServerResult.Success = true;

                Return_SaleOrderItem(zInfo.Order.ListItem, OrderKey, zInfo.Order.OrderID, out Message);
                Return_SaleOrderMoney(zInfo.Order.OrderID, out Message);
            }
            else
            {
                zServerResult.Success = false;
                zServerResult.Message = zInfo.Message;
            }
            return Json(zServerResult, JsonRequestBehavior.AllowGet);
        }
        //xóa item đơn hàng
        public ActionResult DelItem_SaleOrder(string ObjectKey)
        {
            Order_Model zOrder = (Order_Model)Session["SaleOrder"];
            var Item = zOrder.ListItem.FirstOrDefault(s => s.ObjectKey == ObjectKey);
            if (Item != null)
            {
                zOrder.ListItem.Remove(Item);
            }

            //tổng tiền giảm giá
            zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);
            //tổng tiền đơn hàng
            zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);

            if (zOrder.ListItem.Count == 0)
            {
                Session["Order"] = null;
            }

            return View("~/Views/Desktop/SO/_SaleItem.cshtml", zOrder);
        }
        //edit item đơn hàng
        public ActionResult EditItem_SaleOrder(string Item)
        {
            Order_Item_Model zClientItem = JsonConvert.DeserializeObject<Order_Item_Model>(Item);

            Order_Model zOrder = (Order_Model)Session["SaleOrder"];
            var zEditItem = zOrder.ListItem.FirstOrDefault(s => s.ObjectKey == zClientItem.ObjectKey);
            if (zClientItem != null)
            {
                zEditItem.ObjectPrice = zClientItem.ObjectPrice;
                zEditItem.Quantity = zClientItem.Quantity;
                zEditItem.Amount = (zClientItem.ObjectPrice * zClientItem.Quantity);
                zEditItem.Description = zClientItem.Description.Trim();
            }

            //tổng tiền giảm giá
            zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);
            //tổng tiền đơn hàng
            zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);
            Session["SaleOrder"] = zOrder;

            return View("~/Views/Desktop/SO/_SaleItem.cshtml", zOrder);
        }
        //add item vào đơn hàng
        public ActionResult AddItem_SaleOrder(string ProductKey)
        {
            Order_Model zOrder;
            Product_Info zInfo = new Product_Info(ProductKey);
            Product_Model zModel = zInfo.Product;

            if (Session["SaleOrder"] == null)
            {
                zOrder = new Order_Model();

                zOrder.ListItem.Add(new Order_Item_Model
                {
                    ObjectTable = "PDT_Product",
                    ObjectKey = zModel.ProductKey,
                    ObjectName = zModel.ProductName,
                    ObjectPrice = zModel.SalePrice,
                    Quantity = 1,
                    Amount = zModel.SalePrice,
                    UnitKey = zModel.StandardUnitKey,
                    UnitName = zModel.StandardUnitName
                });
            }
            else
            {
                zOrder = (Order_Model)Session["SaleOrder"];
                var Item = zOrder.ListItem.FirstOrDefault(s => s.ObjectKey == ProductKey);
                if (Item != null)
                {
                    //tắt tính năng kiểm tra số lượng tồn
                    //if (Item.Quantity >= zModel.SafetyInStock)
                    //{
                    //    ViewBag.ErrorQuantity = "true";
                    //}
                    //else
                    //{
                    //    ViewBag.ErrorQuantity = null;
                    //    Item.Quantity = Item.Quantity + 1;
                    //    Item.Amount = Item.ObjectPrice * Item.Quantity;
                    //}


                    Item.Quantity = Item.Quantity + 1;
                    Item.Amount = Item.ObjectPrice * Item.Quantity;
                }
                else
                {
                    Product_Model zProduct = new Product_Info(ProductKey).Product;
                    zOrder.ListItem.Add(new Order_Item_Model
                    {
                        ObjectTable = "PDT_Product",
                        ObjectKey = zProduct.ProductKey,
                        ObjectName = zProduct.ProductName,
                        ObjectPrice = zProduct.SalePrice,
                        Quantity = 1,
                        Amount = zProduct.SalePrice,
                        UnitKey = zProduct.StandardUnitKey,
                        UnitName = zProduct.StandardUnitName
                    });
                }
            }

            //tổng tiền giảm giá
            zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);
            //tổng tiền đơn hàng
            zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.ObjectPrice);
            Session["SaleOrder"] = zOrder;
            return View("~/Views/Desktop/SO/_SaleItem.cshtml", zOrder);
        }

        public ActionResult SaleOrderNew()
        {
            ViewBag.ListCategory = Product_Data.ListCategory(UserLog.PartnerNumber, out string Message);

            Order_Model zModel = new Order_Model();
            zModel.OrderID = "Tự động phát sinh";
            zModel.OrderDate = DateTime.Now;
            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;
            Session["SaleOrder"] = zModel;
            return View("~/Views/Desktop/SO/SaleOrder.cshtml", zModel);
        }


        // ---- chuyển đổi đơn vị bán hàng
        public ActionResult AddItem_SaleOrder_Unit(string ProductKey)
        {
            Order_Model zOrder;
            Product_Info zInfo = new Product_Info(ProductKey);
            Product_Model zModel = zInfo.Product;

            if (Session["SaleOrder"] == null)
            {
                zOrder = new Order_Model();

                zOrder.ListItem.Add(new Order_Item_Model
                {
                    ObjectTable = "PDT_Product",
                    ObjectKey = zModel.ProductKey,
                    ObjectName = zModel.ProductName,
                    ObjectPrice = zModel.SalePrice,
                    Quantity = 1,
                    Amount = zModel.SalePrice,
                    UnitKey = zModel.StandardUnitKey,
                    UnitName = zModel.StandardUnitName
                });
            }
            else
            {
                zOrder = (Order_Model)Session["SaleOrder"];
                var Item = zOrder.ListItem.FirstOrDefault(s => s.ObjectKey == ProductKey);
                if (Item != null)
                {
                    //tắt tính năng kiểm tra số lượng tồn
                    //if (Item.Quantity >= zModel.SafetyInStock)
                    //{
                    //    ViewBag.ErrorQuantity = "true";
                    //}
                    //else
                    //{
                    //    ViewBag.ErrorQuantity = null;
                    //    Item.Quantity = Item.Quantity + 1;
                    //    Item.Amount = Item.ObjectPrice * Item.Quantity;
                    //}


                    Item.Quantity = Item.Quantity + 1;
                    Item.Amount = Item.ObjectPrice * Item.Quantity;
                }
                else
                {
                    Product_Model zProduct = new Product_Info(ProductKey).Product;
                    zOrder.ListItem.Add(new Order_Item_Model
                    {
                        ObjectTable = "PDT_Product",
                        ObjectKey = zProduct.ProductKey,
                        ObjectName = zProduct.ProductName,
                        ObjectPrice = zProduct.SalePrice,
                        Quantity = 1,
                        Amount = zProduct.SalePrice,
                        UnitKey = zProduct.StandardUnitKey,
                        UnitName = zProduct.StandardUnitName
                    });
                }
            }

            //tổng tiền giảm giá
            zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);
            //tổng tiền đơn hàng
            zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.ObjectPrice);
            Session["SaleOrder"] = zOrder;

            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Desktop/SO/_SaleItem_Unit.cshtml", zOrder);
        }
        public ActionResult SaleOrderNew_Unit()
        {
            ViewBag.ListCategory = Product_Data.ListCategory(UserLog.PartnerNumber, out string Message);

            Order_Model zModel = new Order_Model();
            zModel.OrderID = "Tự động phát sinh";
            zModel.OrderDate = DateTime.Now;
            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;
            Session["SaleOrder"] = zModel;
            return View("~/Views/Desktop/SO/SaleOrder_Unit.cshtml", zModel);
        }


        #endregion

        #region [EMPLOYEE]
        /// <summary>
        /// upload file nhiều cung 1 lúc = ajax
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UploadFile(string id)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string fileName = "";
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        string strGui = Guid.NewGuid().ToString();
                        fileName = strGui + Path.GetExtension(fileContent.FileName);

                        #region[Upload]   
                        string zFilePath = "/FileUpload/" + Helper.PartnerNumber + "/Employee/" + id + "/";
                        string ServerPath = Server.MapPath(zFilePath);
                        string zFileSave = Path.Combine(ServerPath, fileName);

                        // Check Foder
                        DirectoryInfo zDir = new DirectoryInfo(ServerPath);
                        if (!zDir.Exists)
                        {
                            zDir.Create();
                        }
                        else
                        {
                            if (System.IO.File.Exists(zFileSave))
                            {
                                System.IO.File.Delete(zFileSave);
                            }
                        }

                        fileContent.SaveAs(zFileSave);
                        #endregion

                        Document_Info zInfo = new Document_Info();
                        zInfo.Document.TableJoin = "HRM_Employee";
                        zInfo.Document.TableKey = id;
                        zInfo.Document.PartnerNumber = Helper.PartnerNumber;
                        zInfo.Document.FileExt = Path.GetExtension(fileContent.FileName);
                        zInfo.Document.FileName = fileContent.FileName;
                        zInfo.Document.FilePath = Path.Combine(zFilePath, fileName); ;
                        zInfo.Create_ServerKey();
                    }
                }

                zResult.Success = true;

            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Search(
            string SearchName = "", string Department = "", string FromDate = "", string ToDate = "",
            string Age = "", string Edu = "", int Gender = 0, string Position = "", string ViewPage = "")
        {
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zFromDate);
            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zToDate);

            int FromAge = 0, ToAge = 0;
            if (Age.Contains('-'))
            {
                FromAge = Age.Split('-')[0].ToInt();
                ToAge = Age.Split('-')[1].ToInt();
            }

            ViewBag.SelectPosition = Position_Data.List(UserLog.PartnerNumber, out string Message);
            ViewBag.SelectReportTo = Employee_Data.ListReportTo(UserLog.PartnerNumber);
            ViewBag.SelectDepartment = Department_Data.List(UserLog.PartnerNumber, out string Message2);
            ViewBag.SelectBranch = Branch_Data.List(UserLog.PartnerNumber, out string Message3);

            switch (ViewPage)
            {
                default:
                    ViewBag.ListData = Employee_Data.Search(UserLog.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Desktop/ListEmployee.cshtml");

                case "LIST":
                    ViewBag.ListData = Employee_Data.Search(UserLog.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Desktop/ListEmployee.cshtml");
            }
        }

        public ActionResult ListEmployee()
        {
            string Message = "";
            ViewBag.ListData = Employee_Data.List(UserLog.PartnerNumber);
            ViewBag.SelectReportTo = Employee_Data.ListReportTo(UserLog.PartnerNumber);
            ViewBag.SelectPosition = Position_Data.List(UserLog.PartnerNumber, out Message);
            ViewBag.SelectDepartment = Department_Data.List(UserLog.PartnerNumber, out Message);
            ViewBag.SelectBranch = Branch_Data.List(UserLog.PartnerNumber, out Message);
            return View("~/Views/Desktop/Employee/List.cshtml");
        }

        public ActionResult EditEmployee(string EmployeeKey)
        {
            string Message = "";
            ViewBag.ListSelectPosition = Position_Data.List(UserLog.PartnerNumber, out Message);
            ViewBag.ListSelectReportTo = Employee_Data.ListReportTo(UserLog.PartnerNumber);
            ViewBag.ListSelectDepartment = Department_Data.List(UserLog.PartnerNumber, out Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber, out Message);
            ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, EmployeeKey);

            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Family_Info zInfo1 = new Family_Info(EmployeeKey);

            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo1.Family;

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(UserLog.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(UserLog.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(UserLog.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(UserLog.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(UserLog.PartnerNumber, EmployeeKey);
                ViewBag.ListPayroll = Payroll_Person_Data.List(UserLog.PartnerNumber, EmployeeKey);
            }
            return View("~/Views/Desktop/Employee/Edit.cshtml", zObj);
        }

        [HttpGet]
        public JsonResult GetID()
        {
            string ID = Employee_Data.AutoEmployeeID("TNS", UserLog.PartnerNumber);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }

        #region [Init Info]
        [HttpPost]
        public JsonResult SaveEmployee(
                 string EmployeeKey, string EmployeeID, string LastName, string FirstName,
                 int PositionKey, string PositionName, int Gender, string BirthDay,
                 string Passport, string Address, string Email, string MobiPhone,
                 string ReportToKey, string ReportToName,
                 string BranchKey, string BranchName,
                 string DepartmentKey, string DepartmentName, string StartDate)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info();
            Employee_Model zModel = new Employee_Model();

            zModel.EmployeeID = EmployeeID.Trim();
            zModel.LastName = LastName.Trim();
            zModel.FirstName = FirstName.Trim();
            zModel.PositionKey = PositionKey;
            zModel.PositionName = PositionName.Trim();
            zModel.Gender = Gender;
            zModel.BranchKey = BranchKey;
            zModel.BranchName = BranchName;
            zModel.DepartmentKey = DepartmentKey;
            zModel.DepartmentName = DepartmentName;

            DateTime zBirthDay = DateTime.MinValue;
            DateTime.TryParseExact(BirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zBirthDay);
            zModel.Birthday = zBirthDay;

            DateTime zStartDate = DateTime.MinValue;
            DateTime.TryParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zStartDate);
            zModel.StartingDate = zStartDate;

            zModel.PassportNumber = Passport.Trim();
            zModel.AddressRegister = Address.Trim();
            zModel.Email = Email.Trim();
            zModel.MobiPhone = MobiPhone.Trim();
            zModel.ReportToKey = ReportToKey.Trim();
            zModel.ReportToName = ReportToName.Trim();

            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (EmployeeKey == "")
            {
                zInfo.Employee = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.EmployeeKey = EmployeeKey;
                zInfo.Employee = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult DetailEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info();
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Delete();
            Employee_Model zModel = zInfo.Employee;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Edit Info]      

        #region[Info]
        [HttpPost]
        public ActionResult SaveInfo(string EmployeeKey, string LastName, string FirstName, int rdoGender, string BirthDay,
           string Passport, string Address, string Email, string MobiPhone, HttpPostedFileBase[] files)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);

            zInfo.Employee.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Employee.LastName = LastName.Trim();
            zInfo.Employee.FirstName = FirstName.Trim();
            zInfo.Employee.Gender = rdoGender;

            DateTime zBirthDay = DateTime.MinValue;
            DateTime.TryParseExact(BirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zBirthDay);

            zInfo.Employee.Birthday = zBirthDay;
            zInfo.Employee.PassportNumber = Passport.Trim();
            zInfo.Employee.AddressRegister = Address.Trim();
            zInfo.Employee.Email = Email.Trim();
            zInfo.Employee.MobiPhone = MobiPhone.Trim();

            zInfo.Employee.CreatedBy = UserLog.UserKey;
            zInfo.Employee.CreatedName = UserLog.EmployeeName;
            zInfo.Employee.ModifiedBy = UserLog.UserKey;
            zInfo.Employee.ModifiedName = UserLog.EmployeeName;

            if (EmployeeKey == "")
            {
                EmployeeKey = Guid.NewGuid().ToString();
                zInfo.Employee.PhotoPath = StoreFilePost(files, EmployeeKey);
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Employee.PhotoPath = StoreFilePost(files, EmployeeKey);
                zInfo.Update();
            }

            ViewBag.Message = zInfo.Message;
            return RedirectToAction("EditEmployee", new { EmployeeKey });
        }
        [HttpGet]
        public JsonResult DetailInfo(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Family]
        [HttpPost]
        public JsonResult SaveFamily(string EmployeeKey, string FatherName, string FatherBirthday, string FatherWork,
            string FatherAddress, string MotherName, string MotherBirthday, string MotherWork, string MotherAddress,
            string PartnersName, string PartnersBirthday, string PartnersWork, string PartnersAddress,
            string ChildExtend, string OrtherExtend)
        {
            ServerResult zResult = new ServerResult();
            Family_Info zInfo = new Family_Info(EmployeeKey);
            Family_Model zModel = new Family_Model();

            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.EmployeeKey = EmployeeKey.Trim();
            zModel.FatherName = FatherName.Trim();

            DateTime zFatherBirthday = DateTime.MinValue;
            DateTime.TryParseExact(FatherBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFatherBirthday);

            zModel.FatherBirthday = zFatherBirthday;
            zModel.FatherWork = FatherWork.Trim();
            zModel.FatherAddress = FatherAddress.Trim();
            zModel.MotherName = MotherName.Trim();

            DateTime zMotherBirthday = DateTime.MinValue;
            DateTime.TryParseExact(MotherBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zMotherBirthday);

            zModel.MotherBirthday = zMotherBirthday;
            zModel.MotherWork = MotherWork.Trim();
            zModel.MotherAddress = MotherAddress.Trim();
            zModel.PartnersName = PartnersName.Trim();

            DateTime zPartnersBirthday = DateTime.MinValue;
            DateTime.TryParseExact(PartnersBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zPartnersBirthday);

            zModel.PartnersBirthday = zPartnersBirthday;
            zModel.PartnersWork = PartnersWork.Trim();
            zModel.PartnersAddress = PartnersAddress.Trim();
            zModel.ChildExtend = ChildExtend.Trim();
            zModel.OrtherExtend = OrtherExtend.Trim();

            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Family.AutoKey == 0)
            {
                zInfo.Family = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.AutoKey = zInfo.Family.AutoKey;
                zInfo.Family = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailFamily(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Family_Info zInfo = new Family_Info(EmployeeKey);
            Family_Model zModel = zInfo.Family;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Job]
        [HttpPost]
        public JsonResult SaveJob(string EmployeeKey, string StartingDate, string ReportToKey, string ReportToName,
           string DepartmentKey, string DepartmentName, string BranchKey, string BranchName, int PositionKey, string PositionName, string Note, int StatusKey, string StatusName)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);

            zInfo.Employee.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Employee.ReportToKey = ReportToKey.Trim();
            zInfo.Employee.ReportToName = ReportToName.Trim();

            DateTime zStartingDate = DateTime.MinValue;
            DateTime.TryParseExact(StartingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zStartingDate);

            zInfo.Employee.WorkingStatusKey = StatusKey;
            zInfo.Employee.WorkingStatusName = StatusName;
            zInfo.Employee.StartingDate = zStartingDate;
            zInfo.Employee.DepartmentKey = DepartmentKey.Trim();
            zInfo.Employee.DepartmentName = DepartmentName.Trim();

            zInfo.Employee.BranchKey = BranchKey.Trim();
            zInfo.Employee.BranchName = BranchName.Trim();

            zInfo.Employee.PositionKey = PositionKey;
            zInfo.Employee.PositionName = PositionName.Trim();
            zInfo.Employee.Note = Note.Trim();

            zInfo.Employee.CreatedBy = UserLog.UserKey;
            zInfo.Employee.CreatedName = UserLog.EmployeeName;
            zInfo.Employee.ModifiedBy = UserLog.UserKey;
            zInfo.Employee.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Employee.EmployeeKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailJob(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region[Education]
        [HttpPost]
        public JsonResult SaveEdu(string EmployeeKey, string FromDate, string ToDate, string DegreeName,
           string DegreeBy, int StatusKey, string StatusName, string Description, int ClassifiedKey, string ClassifiedName, string TypeName, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info(AutoKey);

            zInfo.Education.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Education.EmployeeKey = EmployeeKey;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Education.FromDate = zFromDate;

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            zInfo.Education.ToDate = zToDate;
            zInfo.Education.DegreeName = DegreeName.Trim();
            zInfo.Education.DegreeBy = DegreeBy.Trim();

            zInfo.Education.StatusKey = StatusKey;
            zInfo.Education.StatusName = StatusName.Trim();
            zInfo.Education.Description = Description.Trim();
            zInfo.Education.ClassifiedKey = ClassifiedKey.ToInt();
            zInfo.Education.ClassifiedName = ClassifiedName.Trim();
            zInfo.Education.TypeName = TypeName.Trim();

            zInfo.Education.CreatedBy = UserLog.UserKey;
            zInfo.Education.CreatedName = UserLog.EmployeeName;
            zInfo.Education.ModifiedBy = UserLog.UserKey;
            zInfo.Education.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Education.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailEdu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info(AutoKey);
            Education_Model zModel = zInfo.Education;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteEdu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info();
            zInfo.Education.AutoKey = AutoKey;
            zInfo.Delete();
            Education_Model zModel = zInfo.Education;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region[Exprience]
        [HttpPost]
        public JsonResult SaveExp(string EmployeeKey, string FromDate, string ToDate, string UnitWork,
           string UnitPosition, string Description, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info(AutoKey);

            zInfo.Experience.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Experience.EmployeeKey = EmployeeKey;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            zInfo.Experience.FromDate = zFromDate;

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            zInfo.Experience.ToDate = zToDate;

            zInfo.Experience.UnitWork = UnitWork.Trim();
            zInfo.Experience.UnitPosition = UnitPosition.Trim();
            zInfo.Experience.Description = Description.Trim();
            zInfo.Experience.CreatedBy = UserLog.UserKey;
            zInfo.Experience.CreatedName = UserLog.EmployeeName;
            zInfo.Experience.ModifiedBy = UserLog.UserKey;
            zInfo.Experience.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Experience.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailExp(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info(AutoKey);
            Experience_Model zModel = zInfo.Experience;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteExp(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info();
            zInfo.Experience.AutoKey = AutoKey;
            zInfo.Delete();
            Experience_Model zModel = zInfo.Experience;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Skills]
        [HttpPost]
        public JsonResult SaveSkill(string EmployeeKey, string SkillName, int Levels, int Maxlevels = 10, string Description = "", int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info(AutoKey);

            zInfo.Skill.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Skill.EmployeeKey = EmployeeKey;

            zInfo.Skill.SkillName = SkillName.Trim();
            zInfo.Skill.Levels = Levels.ToInt();
            zInfo.Skill.Maxlevels = Maxlevels.ToInt();
            zInfo.Skill.Description = Description.Trim();

            zInfo.Skill.CreatedBy = UserLog.UserKey;
            zInfo.Skill.CreatedName = UserLog.EmployeeName;
            zInfo.Skill.ModifiedBy = UserLog.UserKey;
            zInfo.Skill.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Skill.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailSkill(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info(AutoKey);
            Skill_Model zModel = zInfo.Skill;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteSkill(int Autokey)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info();
            zInfo.Skill.AutoKey = Autokey;
            zInfo.Delete();
            Skill_Model zModel = zInfo.Skill;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[History Insure]
        [HttpPost]
        public JsonResult SaveIn(string EmployeeKey, string DateWrite, string StatusName, string Description, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info(AutoKey);

            zInfo.HistoryInsure.PartnerNumber = UserLog.PartnerNumber;
            zInfo.HistoryInsure.EmployeeKey = EmployeeKey;

            DateTime zDateWrite = DateTime.MinValue;
            DateTime.TryParseExact(DateWrite, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateWrite);
            zInfo.HistoryInsure.DateWrite = zDateWrite;

            zInfo.HistoryInsure.StatusName = StatusName.Trim();
            zInfo.HistoryInsure.Description = Description.Trim();
            zInfo.HistoryInsure.CreatedBy = UserLog.UserKey;
            zInfo.HistoryInsure.CreatedName = UserLog.EmployeeName;
            zInfo.HistoryInsure.ModifiedBy = UserLog.UserKey;
            zInfo.HistoryInsure.ModifiedName = UserLog.EmployeeName;

            if (zInfo.HistoryInsure.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailIn(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info(AutoKey);
            HistoryInsure_Model zModel = zInfo.HistoryInsure;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteIn(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info();
            zInfo.HistoryInsure.AutoKey = AutoKey;
            zInfo.Delete();
            HistoryInsure_Model zModel = zInfo.HistoryInsure;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion
        private string StoreFilePost(HttpPostedFileBase[] files, string Key)
        {
            string zFilePath = "";
            string fileName = "";
            try
            {
                if (files.Length > 0)
                {
                    foreach (HttpPostedFileBase file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            string strGui = Guid.NewGuid().ToString();
                            fileName = strGui + System.IO.Path.GetExtension(file.FileName);

                            #region[Upload]
                            zFilePath = "/FileUpload/" + Helper.PartnerNumber + "/Employee/" + Key + "/";
                            string zFileSave = Path.Combine(Server.MapPath(zFilePath), fileName);
                            DirectoryInfo zDir = new DirectoryInfo(Server.MapPath(zFilePath));
                            if (!zDir.Exists)
                            {
                                zDir.Create();
                            }
                            else
                            {
                                if (System.IO.File.Exists(zFileSave))
                                {
                                    System.IO.File.Delete(zFileSave);
                                }
                            }

                            file.SaveAs(zFileSave);
                            #endregion
                        }
                    }
                }

                return zFilePath + fileName;
            }
            catch (Exception)
            {
                return "0";
            }
        }
        #endregion

        #region [TASK]
        public ActionResult ListTask()
        {
            ViewBag.ListTask = Task_Data.List(UserLog.PartnerNumber, out string Message);
            ViewBag.ListCustomer = Customer_Data.List(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListEmployee = Employee_Data.List(UserLog.PartnerNumber);
            ViewBag.ListUser = User_Data.List(UserLog.PartnerNumber);
            ViewBag.UserKey = UserLog.UserKey;
            ViewBag.EmployeeName = UserLog.EmployeeName;

            return View("~/Views/Desktop/Task/ListTask.cshtml");
        }

        [HttpPost]
        public JsonResult SaveTask()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zTaskObj = Request.Form["TaskObj"];
                string zTaskKey = Request.Form["TaskKey"];
                Task_Model zModel = JsonConvert.DeserializeObject<Task_Model>(zTaskObj);
                //zModel.PartnerKey = CreateCustomer(zModel.PartnerTax, zModel.PartnerName);

                Task_Info zInfo = new Task_Info(zTaskKey);
                string zTaskFile = zInfo.Task.TaskFile;
                zInfo.Task = zModel;
                if (zModel.TaskFile.Length > 0)
                {
                    zInfo.Task.TaskFile = zModel.TaskFile;
                }
                else
                {
                    zInfo.Task.TaskFile = zTaskFile;
                }
                //zInfo.Invoice.PartnerTax = zModel.PartnerTax.Trim();
                //zInfo.Invoice.PartnerName = zModel.PartnerName.Trim();

                zInfo.Task.PartnerNumber = UserLog.PartnerNumber;
                zInfo.Task.CreatedBy = UserLog.UserKey;
                zInfo.Task.CreatedName = UserLog.EmployeeName;
                zInfo.Task.ModifiedBy = UserLog.ModifiedBy;
                zInfo.Task.ModifiedName = UserLog.EmployeeName;
               
                if (Request.Files.Count > 0)
                {
                    string zFilePath = Helper.UploadPath + "/" + UserLog.PartnerNumber + "/Invoice/";
                    HttpPostedFileBase files = Request.Files[0];
                    bool zUploadResult = Helper.UploadSingleFile(files, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        //xóa file cũ khi upload thanh công
                        if (zInfo.Task.TaskFile.Length > 0)
                        {
                            string Message = "";
                            Helper.DeleteSingleFile(zInfo.Task.TaskFile, out Message);
                        }
                        //gán đường dẫn mới
                        zInfo.Task.TaskFile = zFilePath;
                    }
                }

                if (zInfo.Task.TaskKey == "")
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();

                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult DetailTask(string TaskKey)
        {
            ServerResult zResult = new ServerResult();
            Task_Info zInfo = new Task_Info(TaskKey);
            Task_Model zModel = zInfo.Task;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteTask(string TaskKey)
        {
            ServerResult zResult = new ServerResult();
            Task_Info zInfo = new Task_Info();
            zInfo.Task.TaskKey = TaskKey;
            zInfo.Delete();
            //Product_Category_Model zModel = zInfo.Product_Category;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        [HttpGet]
        public JsonResult GetCustomerByPhone(string Phone)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(Phone, UserLog.PartnerNumber, true);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult AutoNumber(string term)
        {
            if (term.Length <= 4)
            {
                List<TNItem> number = new List<TNItem>();

                for (int n = 1; n <= 4; n++)
                {
                    string temp = "0";
                    for (int k = 0; k < n; k++)
                    {
                        temp = temp + "0";
                    }

                    double no = (term.ToInt() * ("1" + temp).ToInt() * 1000);

                    if (term.Length == 2)
                    {
                        no = no / 10;
                    }
                    if (term.Length == 3)
                    {
                        no = no / 100;
                    }
                    if (term.Length == 4)
                    {
                        no = no / 1000;
                    }

                    if (no > 0)
                    {
                        number.Add(new TNItem()
                        {
                            Value = no.ToString(),
                            Text = no.ToString("n0") + " VNĐ",
                        });
                    }
                }

                string data = JsonConvert.SerializeObject(number);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        private string ChartJS_XLabelName(DataTable Table)
        {
            string zLabel = "";
            for (int i = 3; i < Table.Columns.Count; i++)
            {
                DataColumn c = Table.Columns[i];
                zLabel += "'" + c.ColumnName + "',";
            }
            zLabel = zLabel.Remove(zLabel.LastIndexOf(','), 1);
            return zLabel;
        }
        private string ChartJS_Dataset(DataTable Table)
        {
            string zDataset = "";
            foreach (DataRow r in Table.Rows)
            {
                int Num = TN_Utils.RandomNumber(0, 299);

                zDataset += "{ label: '" + r[0].ToString() + "',";
                //zDataset += " backgroundColor: 'rgba(" + Num + ", 99, " + Num + ", 0.2)',";
                zDataset += " backgroundColor: '" + r[1].ToString() + "',";
                zDataset += " borderColor: 'rgba(" + Num + ", 99, " + Num + ", 1)',";
                zDataset += " borderWidth: 1,";
                zDataset += " data: [";
                for (int i = 3; i < Table.Columns.Count; i++)
                {
                    zDataset += r[i].ToDouble() + ",";
                }
                zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
                zDataset += " ]},";
            }

            zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
            return zDataset;
        }

        //tìm kiếm sản phẩm theo loại, đẻ chọn ra đơn hàng
        public ActionResult Product_Select(int CategoryKey = 0, string Option = "", string Name = "", string For = "SO")
        {
            var zListProduct = new List<Product_Model>();
            if (Name != string.Empty)
            {
                zListProduct = Product_Data.Search(UserLog.PartnerNumber, out string Message1, Name);
            }
            else
            {
                if (Option.Length > 0)
                {
                    if (Option == "ORDER")
                    {
                        zListProduct = Product_Data.ListNew(UserLog.PartnerNumber, out string Message1);
                    }

                    if (Option == "TOP")
                    {
                        zListProduct = Product_Data.Top25(UserLog.PartnerNumber, out string Message1);
                    }
                }
                else
                {
                    zListProduct = Product_Data.SearchProduct(UserLog.PartnerNumber, out string Message1, CategoryKey);
                }
            }
            ViewBag.ListProduct = zListProduct;
            ViewBag.For = For;
            return PartialView("~/Views/Shared/_Product_Select.cshtml");
        }

        [HttpGet]
        public JsonResult OpenDocument(int Key)
        {
            ServerResult zResult = new ServerResult();
            Document_Info zInfo = new Document_Info(Key);
            zResult.Data = JsonConvert.SerializeObject(zInfo.Document);
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Document()
        {
            ViewBag.ListCustomer = Customer_Data.List(UserLog.PartnerNumber, out string Message);
            ViewBag.ListCategory = Document_Data.ListCate(UserLog.PartnerNumber);
            

            //ViewBag.Customer = Customer;
            //ViewBag.FileName = FileName;
            //ViewBag.Category = Category;

            return View("~/Views/Desktop/Document.cshtml");
        }

        public ActionResult ListViewDocument(int Category = 0, string Customer = "", string FileName = "", int Slug = 9)
        {
            ViewBag.ListData = Document_Data.List(UserLog.PartnerNumber, Category, Customer, FileName, Slug);
            return View("~/Views/Shared/_Document_View.cshtml");
        }

        public ActionResult UploadDocument(string Category = "")
        {
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    string zFilePath = Helper.UploadPath + "/" + UserLog.PartnerNumber + "/Document/" + file.FileName;
                    bool zUploadResult = Helper.UploadSingleFile(file, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        Document_Info zInfo = new Document_Info();
                        zInfo.Document.FilePath = zFilePath;
                        zInfo.Document.FileName = file.FileName;
                        zInfo.Document.Slug = 9;

                        zInfo.Document.PartnerNumber = UserLog.PartnerNumber;
                        zInfo.Document.CreatedBy = UserLog.UserKey;
                        zInfo.Document.CreatedName = UserLog.EmployeeName;
                        zInfo.Document.ModifiedBy = UserLog.UserKey;
                        zInfo.Document.ModifiedName = UserLog.EmployeeName;

                        zInfo.Create_ServerKey();
                    }
                }
            }

            return RedirectToAction("Document", new { Category });
        }

        [HttpPost]
        public JsonResult UpdateDocument(int AutoKey, int CategoryKey, string Title, string FromDate, string ToDate, string SignDate, string Customer)
        {
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zFromDate);
            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zToDate);
            DateTime zSignDate = DateTime.MinValue;
            DateTime.TryParseExact(SignDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zSignDate);

            ServerResult zResult = new ServerResult();
            Document_Info zInfo = new Document_Info(AutoKey);

            zInfo.Document.Title = Title;
            zInfo.Document.FromDate = zFromDate;
            zInfo.Document.ToDate = zToDate;
            zInfo.Document.SignDate = zSignDate;
            zInfo.Document.Category = CategoryKey;
            zInfo.Document.Slug = 1;
            zInfo.Document.CustomerKey = Customer;

            zInfo.Document.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Document.CreatedBy = UserLog.UserKey;
            zInfo.Document.CreatedName = UserLog.EmployeeName;
            zInfo.Document.ModifiedBy = UserLog.UserKey;
            zInfo.Document.ModifiedName = UserLog.EmployeeName;
            zInfo.Update();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;                
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;                
            }

            DocumentHub.BroadcastData();

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteDocument(int Key)
        {
            ServerResult zResult = new ServerResult();
            Document_Info zInfo = new Document_Info(Key);
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                string Message = "";
                Helper.DeleteSingleFile(zInfo.Document.FilePath, out Message);
                zResult.Success = true;
                zResult.Message = Message;               
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        //----------------------------Tồn Kho
        public ActionResult StockList(string DateView = "")
        {
            DateTime zToDate = DateTime.Now;
            DateTime zFromDate = DateTime.Now;

            if (DateView == string.Empty)
            {
                zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                zToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }

            //tồn kho
            string CloseMonth = "01/" + DateTime.Now.Year.ToString();
            DataTable zTable = Helper.DB_Inventory(UserLog.PartnerNumber, CloseMonth, zFromDate.AddDays(-1), zToDate);
            ViewBag.TableIVT = zTable;

            return View("~/Views/Desktop/StockList.cshtml");
        }
    }
}