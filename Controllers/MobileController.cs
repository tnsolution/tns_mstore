﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace TNStores.Controllers
{
    public class MobileController : BaseController
    {
        public ActionResult mDashboard(string btnAction = "", string FromDate = "", string ToDate = "",
        string Option = "DONHANG,DOANHSO,THUCTHU,PHAITHU,CHIPHI,TON",
        string Color = "#383f4882,#0088cc82,#e361598f,#2baab194,#8000807a,#ffa50080")
        {
            string Message = "";
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate == string.Empty &&
                ToDate == string.Empty)
            {
                zFromDate = FirstDayOfWeek(DateTime.Now);
                zToDate = LastDayOfWeek(DateTime.Now);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                switch (btnAction)
                {
                    case "btn_Prev":
                        zFromDate = zFromDate.AddDays(-6);
                        zToDate = zToDate.AddDays(-6);
                        break;
                    case "btn_Next":
                        zFromDate = zFromDate.AddDays(+6);
                        zToDate = zToDate.AddDays(+6);
                        break;
                }
            }

            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");

            ViewBag.Option = Option;
            ViewBag.SaleAmount = Helper.rpt_SaleAmount(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.OrderTotal = Helper.rpt_OrderTotal(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.InCome = Helper.rpt_InCome(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.OrderDebt = Helper.rpt_OrderDebt(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.Payment = Helper.rpt_Payment(UserLog.PartnerNumber, zFromDate, zToDate, out Message);

            DataTable zTable = Helper.DB_ChartAIO(UserLog.PartnerNumber, Option, Color, zFromDate, zToDate, out Message);
            if (zTable.Rows.Count > 0)
            {
                ViewBag.Label = ChartJS_XLabelName(zTable);
                ViewBag.Dataset = ChartJS_Dataset(zTable);
            }

            //ViewBag.ListOrder = Order_Data.List(UserLog.PartnerNumber, out Message, zFromDate, zToDate);
            //ViewBag.ListPayment = Payment_Data.ListSearch(UserLog.PartnerNumber, out Message, zFromDate, zToDate, "");

            return View("~/Views/Mobile/Sale/Week.cshtml");
        }

        //trang chuyển tiếp đầu tiên trên mobile
        public ActionResult Index()
        {
            return View();
        }

        #region Setting - config
        public ActionResult Setting()
        {
            return View("~/Views/Mobile/Config/Index.cshtml");
        }

        #region [Product Category]
        [HttpPost]
        public JsonResult SaveCategoryProduct(int CategoryKey = 0, string CategoryName = "", int Level = 1, int Parent = 0, int Rank = 0)
        {
            ServerResult zResult = new ServerResult();
            Product_Category_Info zInfo = new Product_Category_Info(CategoryKey);

            zInfo.Product_Category.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Product_Category.CategoryKey = CategoryKey;
            zInfo.Product_Category.CategoryNameVN = CategoryName.Trim();
            zInfo.Product_Category.Parent = Parent.ToInt();
            zInfo.Product_Category.Rank = Rank.ToInt();
            zInfo.Product_Category.Level = Level.ToInt();

            if (zInfo.Product_Category.CategoryKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteProductCategory(int CategoryKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Category_Info zInfo = new Product_Category_Info();
            zInfo.Product_Category.CategoryKey = CategoryKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CategoryProduct()
        {
            ViewBag.ListData = Product_Category_Data.List(UserLog.PartnerNumber, out string Message, 1, false);
            return View("~/Views/Mobile/Config/CategoryProduct.cshtml");
        }
        #endregion

        #region Product
        [HttpGet]
        public JsonResult CheckIDProduct(string ProductID)
        {
            ServerResult zResult = new ServerResult();
            Product_Info zInfo = new Product_Info(ProductID, true);
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Data = TN_Utils.QRWrite(ProductID, out string Message);
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetIDProduct()
        {
            string ID = Helper.AutoID_Product(UserLog.PartnerNumber, "SKU");
            return Json(ID, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListProduct()
        {
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Mobile/Config/Product.cshtml");
        }

        public ActionResult ProductEdit(string ProductKey = "")
        {
            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message);
            ViewBag.ListCategory = Product_Data.ListCategory(UserLog.PartnerNumber, out Message);
            Product_Info zInfo = new Product_Info(ProductKey);
            if (ProductKey.Length <= 0)
            {
                zInfo.Product.ProductID = Helper.AutoID_Product(UserLog.PartnerNumber, "SKU");
            }

            return View("~/Views/Mobile/Config/ProductEdit.cshtml", zInfo.Product);
        }

        [HttpPost]
        public JsonResult SaveProduct()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zProductObj = Request.Form["ProductObj"];
                string zProductKey = Request.Form["ProductKey"];

                #region [Apply data to model]
                Product_Model zModel = JsonConvert.DeserializeObject<Product_Model>(zProductObj);
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;

                if (zModel.PhotoPath.Length > 0)
                {
                    string Message = "";
                    Helper.DeleteSingleFile(zModel.PhotoPath, out Message);
                }

                if (Request.Files.Count > 0)
                {
                    string zFilePath = Helper.UploadPath + "/Product/";
                    HttpPostedFileBase files = Request.Files[0];
                    bool zUploadResult = Helper.UploadSingleFile(files, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zModel.PhotoPath = zFilePath;
                    }
                }
                #endregion

                #region [Save data to db]
                Product_Info zInfo = new Product_Info(zProductKey);
                zInfo.Product = zModel;

                if (zInfo.Product.ProductKey == "")
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }
                #endregion

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteProduct(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Info zInfo = new Product_Info();
            zInfo.Product.ProductKey = ProductKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public ActionResult StoreInfo(HttpPostedFileBase[] files, string PartnerName = "", string PartnerPhone = "", string PartnerAddress = "", string StoreName = "")
        {
            User_Model zUser = Session["User_Model"] as User_Model;
            string PartnerNumber = zUser.PartnerNumber;
            Partner_Info zInfo = new Partner_Info(PartnerNumber);

            if (PartnerName != string.Empty &&
               PartnerPhone != string.Empty &&
               PartnerAddress != string.Empty &&
               StoreName != string.Empty)
            {
                Partner_Model zModel = new Partner_Model();
                zModel.PartnerName = PartnerName;
                zModel.PartnerPhone = PartnerPhone;
                zModel.PartnerAddress = PartnerAddress;
                zModel.PartnerNumber = PartnerNumber;
                zModel.StoreName = StoreName;

                if (files!=null && files.Length > 0)
                {
                    Stream fs = files[0].InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    zModel.Logo_Large = "data:image/png;base64," + base64String;
                }
                else
                {
                    zModel.Logo_Large = zInfo.Partner.Logo_Large;
                }

                zInfo.Partner = zModel;
                zInfo.UpdateManual();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    ViewBag.Message = "Cập nhật thành công !.";
                }
                else
                {
                    ViewBag.Message = "Lỗi " + zInfo.Message;
                }
            }

            return View("~/Views/Mobile/Config/StoreInfo.cshtml", zInfo.Partner);
        }

        public ActionResult ChangePass(string OldPass = "", string NewPass = "", string RePass = "")
        {
            if (Session["User_Model"] != null
                && OldPass != string.Empty
                && NewPass != string.Empty
                && RePass != string.Empty)
            {
                User_Model zUser = Session["User_Model"] as User_Model;
                string UserKey = zUser.UserKey;

                User_Info zInfo = new User_Info(UserKey);
                if (zInfo.User.Password == TN_Utils.HashPass(OldPass))
                {
                    if (NewPass == RePass)
                    {
                        zInfo.ResetPass(UserKey, TN_Utils.HashPass(RePass));
                        ViewBag.Message = "Đã đổi mật khẩu thành công !.";
                    }
                    else
                    {
                        ViewBag.Message = "Mật khẩu mới chưa giống nhau vui lòng nhập lại !.";
                    }
                }
                else
                {
                    ViewBag.Message = "Mật khẩu cũ không đúng vui lòng nhập lại !.";
                }
            }

            return View("~/Views/Mobile/Config/ChangePass.cshtml");
        }

        #region [Unit]
        public ActionResult Unit()
        {
            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Mobile/Config/Unit.cshtml");
        }
        [HttpPost]
        public JsonResult DeleteUnit(int UnitKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Unit_Info zInfo = new Product_Unit_Info();
            zInfo.Product_Unit.UnitKey = UnitKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SaveUnit(int UnitKey, string Unitname)
        {
            ServerResult zResult = new ServerResult();
            Product_Unit_Info zInfo = new Product_Unit_Info(UnitKey);
            zInfo.Product_Unit.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Product_Unit.UnitKey = UnitKey.ToInt();
            zInfo.Product_Unit.UnitnameVN = Unitname.Trim();
            if (zInfo.Product_Unit.UnitKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Thu chi
        public ActionResult CategoryReciept()
        {
            ViewBag.List = CashReceipt_Category_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Mobile/Config/CategoryReceipt.cshtml");
        }

        public ActionResult CategoryPayment()
        {
            ViewBag.List = CashPayment_Category_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Mobile/Config/CategoryPayment.cshtml");
        }

        [HttpPost]
        public JsonResult SaveCategory(int CategoryKey, string CategoryName, string Type)
        {
            ServerResult zResult = new ServerResult();
            if (Type == "Payment")
            {
                CashPayment_Category_Info zInfo = new CashPayment_Category_Info();
                CashPayment_Category_Model zModel = new CashPayment_Category_Model();
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CategoryNameVN = CategoryName;
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;
                zInfo.CashPayment_Category = zModel;
                if (CategoryKey > 0)
                {
                    zInfo.CashPayment_Category.CategoryKey = CategoryKey;
                    zInfo.CashPayment_Category.RecordStatus = 1;
                    zInfo.Update();
                }
                else
                {
                    zInfo.CashPayment_Category.RecordStatus = -1;
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            if (Type == "Receipt")
            {
                CashReceipt_Category_Info zInfo = new CashReceipt_Category_Info();
                CashReceipt_Category_Model zModel = new CashReceipt_Category_Model();
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CategoryNameVN = CategoryName;
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;
                zInfo.CashReceipt_Category = zModel;
                if (CategoryKey > 0)
                {
                    zInfo.CashReceipt_Category.CategoryKey = CategoryKey;
                    zInfo.CashReceipt_Category.RecordStatus = -1;
                    zInfo.Update();
                }
                else
                {
                    zInfo.CashReceipt_Category.RecordStatus = -1;
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteCategory(int CategoryKey, string Type)
        {
            ServerResult zResult = new ServerResult();
            if (Type == "Payment")
            {
                CashPayment_Category_Info zInfo = new CashPayment_Category_Info(CategoryKey);
                zInfo.CashPayment_Category.PartnerNumber = UserLog.PartnerNumber;
                zInfo.CashPayment_Category.CreatedBy = UserLog.UserKey;
                zInfo.CashPayment_Category.CreatedName = UserLog.EmployeeName;
                zInfo.CashPayment_Category.ModifiedBy = UserLog.ModifiedBy;
                zInfo.CashPayment_Category.ModifiedName = UserLog.EmployeeName;
                zInfo.Delete();
                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            if (Type == "Receipt")
            {
                CashReceipt_Category_Info zInfo = new CashReceipt_Category_Info(CategoryKey);
                zInfo.CashReceipt_Category.PartnerNumber = UserLog.PartnerNumber;
                zInfo.CashReceipt_Category.CreatedBy = UserLog.UserKey;
                zInfo.CashReceipt_Category.CreatedName = UserLog.EmployeeName;
                zInfo.CashReceipt_Category.ModifiedBy = UserLog.ModifiedBy;
                zInfo.CashReceipt_Category.ModifiedName = UserLog.EmployeeName;
                zInfo.Delete();
                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult BillConfig(HttpPostedFileBase[] files, string Title = "", string Description = "", string End = "")
        {
            User_Model zUser = Session["User_Model"] as User_Model;
            string PartnerNumber = zUser.PartnerNumber;
            Partner_Info zInfo = new Partner_Info(PartnerNumber);
            string Data = zInfo.Partner.BillConfig;
            string base64String = "";
            if (Title != string.Empty ||
                Description != string.Empty ||
                End != string.Empty)
            {                
                if (files.Length > 0)
                {
                    if (files[0] != null)
                    {
                        Stream fs = files[0].InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        base64String = "data:image/png;base64," + base64String;
                    }
                }

                if (base64String == string.Empty &&
                    zInfo.Partner.Logo_Large.Length > 0)
                {
                    base64String = zInfo.Partner.Logo_Large;
                }

                Data = Title.Trim() + "|" + Description.Trim() + "|" + End.Trim() + "|" + base64String;
                zInfo.Partner.PartnerNumber = PartnerNumber;
                zInfo.UpdateBill(Data);

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    ViewBag.Message = "Cập nhật thành công !.";
                }
                else
                {
                    ViewBag.Message = "Lỗi " + zInfo.Message;
                }
            }
            ViewBag.Data = Data;
            return View("~/Views/Mobile/Config/BillInfo.cshtml");
        }

        #endregion

        #region [INVOICE]
        [HttpPost]
        public JsonResult ReturnInvoice()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zReplaceID = Request.Form["ID"];
                string zReplaceDescription = Request.Form["Description"];
                string zInvoiceKey = Request.Form["InvoiceKey"];

                Invoice_Info zInfo = new Invoice_Info(zInvoiceKey);

                //đã có file thì xóa
                if (zInfo.Invoice.ReplaceFile.Length > 0)
                {
                    string Message = "";
                    Helper.DeleteSingleFile(zInfo.Invoice.ReplaceFile, out Message);
                }

                //Check có file upload
                if (Request.Files.Count > 0)
                {
                    string zFilePath = Helper.UploadPath + "/Invoice/";
                    HttpPostedFileBase files = Request.Files[0];
                    bool zUploadResult = Helper.UploadSingleFile(files, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zInfo.Invoice.ReplaceID = zReplaceID;
                        zInfo.Invoice.ReplaceDescription = zReplaceDescription;
                        zInfo.Invoice.ReplaceFile = zFilePath;
                        zInfo.ReturnInvoice();
                        if (zInfo.Code == "200" ||
                            zInfo.Code == "201")
                        {
                            zResult.Success = true;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            zResult.Success = false;
                            zResult.Message = zInfo.Message;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvoiceIndex()
        {
            return View();
        }

        public ActionResult SearchInvoiceIN_Button(string btnAction = "", string DateView = "")
        {
            #region [Xử lý ngày tháng]
            int Quarter = (DateTime.Now.Month + 2) / 3;
            int Year = DateTime.Now.Year;

            if (DateView != string.Empty)
            {
                Quarter = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Quarter = (DateTime.Now.Month + 2) / 3;
                    Year = DateTime.Now.Year;
                }
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Quarter = Quarter - 1;
                    break;

                case "btn_Next":
                    Quarter = Quarter + 1;
                    break;
            }

            if (Quarter <= 0)
            {
                Quarter = 4;
                Year = Year - 1;
            }
            if (Quarter >= 5)
            {
                Quarter = 1;
                Year = Year + 1;
            }

            DateTime zFromDate = DateTime.Now;
            DateTime zToDate = DateTime.Now;

            switch (Quarter)
            {
                case 1:
                    zFromDate = new DateTime(Year, 01, 01, 00, 00, 00);
                    zToDate = new DateTime(Year, 03, 31, 23, 59, 59);
                    break;

                case 2:
                    zFromDate = new DateTime(Year, 04, 01, 00, 00, 00);
                    zToDate = new DateTime(Year, 06, 30, 23, 59, 59);
                    break;

                case 3:
                    zFromDate = new DateTime(Year, 07, 01, 00, 00, 00);
                    zToDate = new DateTime(Year, 09, 30, 23, 59, 59);
                    break;

                case 4:
                    zFromDate = new DateTime(Year, 10, 01, 00, 00, 00);
                    zToDate = new DateTime(Year, 12, 31, 23, 59, 59);
                    break;
            }
            #endregion
            #region View data
            var InvoiceIn = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "", "IN");
            var InvoiceOut = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, zFromDate, zToDate, "", "OUT");

            ///tổng kết quý
            Invoice_Quarter_Model zSummary = new Invoice_Quarter_Model();

            zSummary.Previous = Quarter.ToInt() - 1;
            zSummary.Current = Quarter.ToInt();

            double DoanhSoChiuThue = zSummary.IncomeWithV = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double DoanhSoKhongThue = zSummary.IncomeNoV = InvoiceOut.Where(s => s.AmountVat == 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double TongDoanhSo = zSummary.InComeTotal = DoanhSoChiuThue + DoanhSoKhongThue;

            double VatVaoTrucTiep = zSummary.AmountVatInDirect = InvoiceIn.Where(s => s.CategoryKey == 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungTamTinh = zSummary.AmountVatInShare = InvoiceIn.Where(s => s.CategoryKey > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungDuocTru = zSummary.AmountVatInShareToMinus = Math.Round((DoanhSoChiuThue / TongDoanhSo) * VatVaoDungChungTamTinh);
            double VatVaoTongCong = zSummary.AmountVatInTotal = VatVaoTrucTiep + VatVaoDungChungTamTinh;

            #endregion

            ViewBag.ListInvoiceOUT = InvoiceOut;
            ViewBag.ListInvoiceIN = InvoiceIn;
            ViewBag.DateView = Quarter + "/" + Year;
            ViewBag.Summary = zSummary;

            return View("~/Views/Mobile/Invoice/In.cshtml");
        }
        public ActionResult SearchInvoiceIN_Date(string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            #region View data
            var InvoiceIn = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "", "IN");
            var InvoiceOut = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, zFromDate, zToDate, "", "OUT");

            ///tổng kết quý
            Invoice_Quarter_Model zSummary = new Invoice_Quarter_Model();

            double DoanhSoChiuThue = zSummary.IncomeWithV = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double DoanhSoKhongThue = zSummary.IncomeNoV = InvoiceOut.Where(s => s.AmountVat == 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double TongDoanhSo = zSummary.InComeTotal = DoanhSoChiuThue + DoanhSoKhongThue;

            double VatVaoTrucTiep = zSummary.AmountVatInDirect = InvoiceIn.Where(s => s.CategoryKey == 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungTamTinh = zSummary.AmountVatInShare = InvoiceIn.Where(s => s.CategoryKey > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungDuocTru = zSummary.AmountVatInShareToMinus = Math.Round((DoanhSoChiuThue / TongDoanhSo) * VatVaoDungChungTamTinh);
            double VatVaoTongCong = zSummary.AmountVatInTotal = VatVaoTrucTiep + VatVaoDungChungTamTinh;

            #endregion

            ViewBag.Summary = zSummary;
            ViewBag.ListInvoiceIN = InvoiceIn;
            ViewBag.ListInvoiceOUT = InvoiceOut;
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");
            return View("~/Views/Mobile/Invoice/In.cshtml");
        }

        public ActionResult SearchInvoiceOUT_Button(string btnAction = "", string DateView = "")
        {
            #region Xử lý ngày tháng sang QUÝ
            int Quarter = (DateTime.Now.Month + 2) / 3;
            int Year = DateTime.Now.Year;

            if (DateView != string.Empty)
            {
                Quarter = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Quarter = (DateTime.Now.Month + 2) / 3;
                    Year = DateTime.Now.Year;
                }
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Quarter = Quarter - 1;
                    break;

                case "btn_Next":
                    Quarter = Quarter + 1;
                    break;
            }

            if (Quarter <= 0)
            {
                Quarter = 4;
                Year = Year - 1;
            }
            if (Quarter >= 5)
            {
                Quarter = 1;
                Year = Year + 1;
            }

            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;

            switch (Quarter)
            {
                case 1:
                    FromDate = new DateTime(Year, 01, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 03, 31, 23, 59, 59);
                    break;

                case 2:
                    FromDate = new DateTime(Year, 04, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 06, 30, 23, 59, 59);
                    break;

                case 3:
                    FromDate = new DateTime(Year, 07, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 09, 30, 23, 59, 59);
                    break;

                case 4:
                    FromDate = new DateTime(Year, 10, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 12, 31, 23, 59, 59);
                    break;
            }
            #endregion
            #region View data
            var InvoiceIn = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, FromDate, ToDate, "", "IN");
            var InvoiceOut = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, FromDate, ToDate, "", "OUT");

            Invoice_Quarter_Model zSummary = new Invoice_Quarter_Model();

            zSummary.Previous = Quarter.ToInt() - 1;
            zSummary.Current = Quarter.ToInt();

            double DoanhSoChiuThue = zSummary.IncomeWithV = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double DoanhSoKhongThue = zSummary.IncomeNoV = InvoiceOut.Where(s => s.AmountVat == 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double TongDoanhSo = zSummary.InComeTotal = DoanhSoChiuThue + DoanhSoKhongThue;

            double VatVaoTrucTiep = zSummary.AmountVatInDirect = InvoiceIn.Where(s => s.CategoryKey == 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungTamTinh = zSummary.AmountVatInShare = InvoiceIn.Where(s => s.CategoryKey > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungDuocTru = zSummary.AmountVatInShareToMinus = Math.Round((DoanhSoChiuThue / TongDoanhSo) * VatVaoDungChungTamTinh);
            double VatVaoTongCong = zSummary.AmountVatInTotal = VatVaoTrucTiep + VatVaoDungChungDuocTru;

            double SoVatDuocTru = zSummary.AmountVatAllowMinus = VatVaoTrucTiep + VatVaoDungChungDuocTru;
            double SoVatDauTra = zSummary.AmountVatOutTotal = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double SoVatQuy = zSummary.AmountPayCurrent = VatVaoTongCong - SoVatDauTra;
            #endregion

            ViewBag.ListInvoiceOUT = InvoiceOut;
            ViewBag.ListInvoiceIN = InvoiceIn;
            ViewBag.Summary = zSummary;
            ViewBag.DateView = Quarter + "/" + Year;

            return View("~/Views/Mobile/Invoice/Out.cshtml");
        }
        public ActionResult SearchInvoiceOUT_Date(string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            var InvoiceIn = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "", "IN");
            var InvoiceOut = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, zFromDate, zToDate, "", "OUT");

            Invoice_Quarter_Model zSummary = new Invoice_Quarter_Model();

            double DoanhSoChiuThue = zSummary.IncomeWithV = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double DoanhSoKhongThue = zSummary.IncomeNoV = InvoiceOut.Where(s => s.AmountVat == 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double TongDoanhSo = zSummary.InComeTotal = DoanhSoChiuThue + DoanhSoKhongThue;

            double VatVaoTrucTiep = zSummary.AmountVatInDirect = InvoiceIn.Where(s => s.CategoryKey == 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungTamTinh = zSummary.AmountVatInShare = InvoiceIn.Where(s => s.CategoryKey > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungDuocTru = zSummary.AmountVatInShareToMinus = Math.Round((DoanhSoChiuThue / TongDoanhSo) * VatVaoDungChungTamTinh);
            double VatVaoTongCong = zSummary.AmountVatInTotal = VatVaoTrucTiep + VatVaoDungChungDuocTru;

            double SoVatDuocTru = zSummary.AmountVatAllowMinus = VatVaoTrucTiep + VatVaoDungChungDuocTru;
            double SoVatDauTra = zSummary.AmountVatOutTotal = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double SoVatQuy = zSummary.AmountPayCurrent = VatVaoTongCong - SoVatDauTra;

            ViewBag.ListInvoiceIN = InvoiceIn;
            ViewBag.ListInvoiceOUT = InvoiceOut;
            ViewBag.Summary = zSummary;
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");

            return View("~/Views/Mobile/Invoice/Out.cshtml");
        }

        public ActionResult InvoiceBalanceQuarter(string btnAction = "", string DateView = "")
        {
            #region Quarter Datetime
            int Quarter = (DateTime.Now.Month + 2) / 3;
            int Year = DateTime.Now.Year;

            if (DateView != string.Empty)
            {
                Quarter = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Quarter = Quarter - 1;
                    break;

                case "btn_Next":
                    Quarter = Quarter + 1;
                    break;
            }

            if (Quarter <= 0)
            {
                Quarter = 4;
                Year = Year - 1;
            }
            if (Quarter >= 5)
            {
                Quarter = 1;
                Year = Year + 1;
            }

            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;

            switch (Quarter)
            {
                case 1:
                    FromDate = new DateTime(Year, 01, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 03, 31, 23, 59, 59);
                    break;

                case 2:
                    FromDate = new DateTime(Year, 04, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 06, 30, 23, 59, 59);
                    break;

                case 3:
                    FromDate = new DateTime(Year, 07, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 09, 30, 23, 59, 59);
                    break;

                case 4:
                    FromDate = new DateTime(Year, 10, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 12, 31, 23, 59, 59);
                    break;
            }
            ViewBag.DateView = Quarter + "/" + Year;
            #endregion

            var InvoiceIn = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, FromDate, ToDate, "", "IN");
            ViewBag.ListInvoiceIN = InvoiceIn;
            var InvoiceOut = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, FromDate, ToDate, "", "OUT");
            ViewBag.ListInvoiceOUT = InvoiceOut;

            Invoice_Quarter_Model zSummary = new Invoice_Quarter_Model();
            double SoVatTruoc = zSummary.AmountPrevious = new Invoice_Close_Info((Quarter - 1) + "/" + Year).Invoice_Close.CloseAmount;

            double DoanhSoChiuThue = zSummary.IncomeWithV = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double DoanhSoKhongThue = zSummary.IncomeNoV = InvoiceOut.Where(s => s.AmountVat == 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double TongDoanhSo = zSummary.InComeTotal = DoanhSoChiuThue + DoanhSoKhongThue;

            double VatVaoTrucTiep = zSummary.AmountVatInDirect = InvoiceIn.Where(s => s.CategoryKey == 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungTamTinh = zSummary.AmountVatInShare = InvoiceIn.Where(s => s.CategoryKey == 1 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungDuocTru = zSummary.AmountVatInShareToMinus = Math.Round((DoanhSoChiuThue / TongDoanhSo) * VatVaoDungChungTamTinh);
            double VatVaoTongCong = zSummary.AmountVatInTotal = VatVaoTrucTiep + VatVaoDungChungTamTinh;

            double SoVatDuocTru = zSummary.AmountVatAllowMinus = VatVaoTrucTiep + VatVaoDungChungDuocTru;
            double SoVatDauTra = zSummary.AmountVatOutTotal = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double SoVatQuy = zSummary.AmountPayCurrent = SoVatDuocTru - SoVatDauTra;
            double SoVatGop = zSummary.AmountPayInclude = SoVatQuy + SoVatTruoc;

            if (SoVatGop > 0)
            {
                zSummary.AmountNext = SoVatGop;
            }

            TempData["ReportInvoice"] = zSummary;
            ViewBag.Summary = zSummary;
            return View("~/Views/Mobile/Invoice/Quarter.cshtml");
        }
        public ActionResult InvoiceBalanceYear(string btnAction = "", string DateView = "")
        {
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Year = DateView.ToInt();
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Year = Year - 1;
                    break;

                case "btn_Next":
                    Year = Year + 1;
                    break;
            }

            List<Invoice_Close_Model> zList = Invoice_Data.ListInvoiceYear(UserLog.PartnerNumber, Year, out string Mess);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-borderd bg-white Custom table-sm table-striped table-responsive-lg'>");
            zSb.AppendLine(HtmlHeader(zList.Count));
            zSb.AppendLine(HtmlRow("V được trừ chuyển qua", "AmountPrevious", zList));
            zSb.AppendLine(HtmlRow("V đầu vào", "AmountVatInTotal", zList));
            zSb.AppendLine(HtmlRow("  -  V trực tiếp", "AmountVatInDirect", zList));
            zSb.AppendLine(HtmlRow("  -  V dùng chung", "AmountVatInShare", zList));
            zSb.AppendLine(HtmlRow("V được trừ dùng chung", "AmountVatInShareToMinus", zList));
            zSb.AppendLine(HtmlRow("Tổng V được trừ trong quý", "AmountVatAllowMinus", zList));
            zSb.AppendLine(HtmlRow("Doanh số bán ra có V", "IncomeWithV", zList));
            zSb.AppendLine(HtmlRow("Doanh số bán ra không V", "IncomeNoV", zList));
            zSb.AppendLine(HtmlRow("Tổng doanh số bán ra", "InComeTotal", zList));
            zSb.AppendLine(HtmlRow("V đầu ra", "AmountVatOutTotal", zList));
            zSb.AppendLine(HtmlRow("Số tiền nộp thuế", "AmountPayCurrent", zList));
            zSb.AppendLine(HtmlRow("Số tiền nộp thuế khi gộp quý trước", "AmountPayInclude", zList));
            zSb.AppendLine("</table>");

            ViewBag.ScriptChart = ChartGenInvoice(zList);
            ViewBag.HtmlData = zSb.ToString();
            ViewBag.DateView = Year;
            return View("~/Views/Mobile/Invoice/Year.cshtml");
        }

        private string HtmlHeader(int Col)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<tr>");
            zSb.AppendLine("    <td>#</td>");
            zSb.AppendLine("    <td>Nội dung</td>");
            for (int i = 1; i <= Col; i++)
            {
                zSb.AppendLine("    <td class='text-center'>Q" + i + "</td>");
            }
            zSb.AppendLine("    <td class='text-center'>Tổng</td>");
            zSb.AppendLine("</tr>");
            return zSb.ToString();
        }
        private string HtmlRow(string text, string field, List<Invoice_Close_Model> listData)
        {
            double TotalRow = 0;
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<tr>");
            zSb.AppendLine("<td><i class='fas fa-circle-notch'></i></td>");
            zSb.AppendLine("<td class='font-weight-semibold text-dark' style='white-space: nowrap;'>" + text + "</td>");

            for (int i = 1; i <= listData.Count; i++)
            {
                try
                {
                    var item = listData[i - 1];
                    if (item.CloseDate.Split('/')[0].ToInt() == i)
                    {
                        Invoice_Quarter_Model zQuarter = JsonConvert.DeserializeObject<Invoice_Quarter_Model>(item.JsonData);
                        var propertyInfo = zQuarter.GetType().GetProperty(field);
                        var value = propertyInfo.GetValue(zQuarter, null);
                        TotalRow += value.ToDouble();
                        zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>" + value.FormatMoneyFromObject() + "</td>");
                    }
                    else
                    {
                        zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>0</td>");
                    }
                }
                catch (Exception)
                {
                    zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>0</td>");
                }
            }
            zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>" + TotalRow.FormatMoneyFromObject() + "</td>");
            zSb.AppendLine("</tr>");

            return zSb.ToString();
        }

        [HttpPost]
        public JsonResult SaveQuarterInvoice(string DateView = "")
        {
            int Quarter = (DateTime.Now.Month + 2) / 3;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Quarter = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();
            }
            string zCloseDate = Quarter + "/" + Year;
            ServerResult zResult = new ServerResult();
            if (TempData["ReportInvoice"] != null)
            {
                Invoice_Quarter_Model zReport = TempData["ReportInvoice"] as Invoice_Quarter_Model;
                #region [Apply Data]
                Invoice_Close_Model zModel = new Invoice_Close_Model();
                zModel.CloseAmount = zReport.AmountNext;
                zModel.CloseType = "Quarter";
                zModel.CloseDate = zCloseDate;
                zModel.JsonData = JsonConvert.SerializeObject(zReport);
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;
                #endregion
                #region [Save Data]

                Invoice_Close_Info zInfo = new Invoice_Close_Info(zCloseDate);
                if (zInfo.Invoice_Close.CloseKey.Length > 0)
                {
                    zInfo.Invoice_Close = zModel;
                    zInfo.Update();
                }
                else
                {
                    zInfo.Invoice_Close = zModel;
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                #endregion
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "TempData ERROR";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #region [Nhập, xóa, sửa hđ]
        [HttpPost]
        public JsonResult mSaveInvoice()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zInvoiceObj = Request.Form["InvoiceObj"];
                string zInvoiceKey = Request.Form["InvoiceKey"];
                Invoice_Model zModel = JsonConvert.DeserializeObject<Invoice_Model>(zInvoiceObj);

                #region [-- Hóa Đơn --]
                Invoice_Info zInfo = new Invoice_Info(zInvoiceKey);
                zInfo.Invoice = zModel;

                if (zModel.PartnerKey.Length <= 0)
                {
                    zModel.PartnerKey = CreateCustomer(zModel.PartnerTax, zModel.PartnerName);
                }

                zInfo.Invoice.PartnerKey = zModel.PartnerKey;
                zInfo.Invoice.PartnerTax = zModel.PartnerTax;
                zInfo.Invoice.PartnerName = zModel.PartnerName;

                zInfo.Invoice.PartnerNumber = UserLog.PartnerNumber;
                zInfo.Invoice.CreatedBy = UserLog.UserKey;
                zInfo.Invoice.CreatedName = UserLog.EmployeeName;
                zInfo.Invoice.ModifiedBy = UserLog.ModifiedBy;
                zInfo.Invoice.ModifiedName = UserLog.EmployeeName;

                if (zInfo.Invoice.FileAttach.Length > 0)
                {
                    string Message = "";
                    Helper.DeleteSingleFile(zInfo.Invoice.FileAttach, out Message);
                }

                if (Request.Files.Count > 0)
                {
                    string zFilePath = Helper.UploadPath + "/Invoice/";
                    HttpPostedFileBase files = Request.Files[0];
                    bool zUploadResult = Helper.UploadSingleFile(files, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zInfo.Invoice.FileAttach = zFilePath;
                    }
                }

                if (zInfo.Invoice.InvoiceKey == "")
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                }
                #endregion

            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult mDetailInvoice(string InvoiceKey)
        {
            ServerResult zResult = new ServerResult();
            Invoice_Info zInfo = new Invoice_Info(InvoiceKey);
            Invoice_Model zModel = zInfo.Invoice;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult mDeleteInvoice(string InvoiceKey)
        {
            ServerResult zResult = new ServerResult();
            Invoice_Info zInfo = new Invoice_Info();
            zInfo.Invoice.InvoiceKey = InvoiceKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpGet]
        public JsonResult GetCustomerByTax(string Tax)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(Tax, UserLog.PartnerNumber);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetCustomerByKey(string Key)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(Key);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (zInfo.Code == "404")
                {
                    zResult.Success = false;
                    zResult.Message = "Không tìm thấy khách hàng này trong dữ liệu, ứng dụng sẽ cập nhật thông tin khách hàng mới này !.";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                }
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        private string ChartGenInvoice(List<Invoice_Close_Model> listData)
        {
            string Script = "var Quarter = [";
            for (int i = 1; i <= listData.Count; i++)
            {
                Script += "\"Q" + i + "\",";
            }
            if (Script.Contains(','))
            {
                Script = Script.Remove(Script.LastIndexOf(','));
            }
            Script += "];";

            Script += " var color = Chart.helpers.color;" + Environment.NewLine;
            Script += " var barChartData = {" + Environment.NewLine;
            Script += "     labels : Quarter," + Environment.NewLine;
            Script += "     datasets: [{" + Environment.NewLine;
            Script += "     label: \"Số tiền nộp\"," + Environment.NewLine;
            Script += "     backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString()," + Environment.NewLine;
            Script += "     borderColor: window.chartColors.red," + Environment.NewLine;
            Script += "     borderWidth: 1," + Environment.NewLine;
            Script += "     data: [" + Environment.NewLine;
            for (int i = 0; i < listData.Count; i++)
            {
                var zQuarter = JsonConvert.DeserializeObject<Invoice_Quarter_Model>(listData[i].JsonData);
                var AmountPayInclude = zQuarter.AmountPayInclude;
                if (AmountPayInclude < 0)
                {
                    AmountPayInclude = -AmountPayInclude;
                }
                Script += AmountPayInclude + ",";
            }
            if (Script.Contains(','))
            {
                Script = Script.Remove(Script.LastIndexOf(','));
            }
            Script += "]" + Environment.NewLine;
            Script += " }]};" + Environment.NewLine;
            return Script;
        }
        #endregion

        #region [SALES]
        private void Receipt_SaleOrder(string OrderID, double Amount, string CustomerKey, string Name, string Phone, out string Message)
        {
            Receipt_Model zModel = new Receipt_Model();
            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;
            zModel.DocumentID = OrderID;
            zModel.ReceiptID = Receipt_Data.Auto_Receipt_ID(UserLog.PartnerNumber);
            zModel.ReceiptDate = DateTime.Now;
            zModel.ReceiptDescription = "Thu bán đơn hàng [" + OrderID + "] tự động !.";
            zModel.AmountCurrencyMain = Amount;
            zModel.Slug = 9;
            zModel.CustomerKey = CustomerKey;
            zModel.CustomerName = Name;
            zModel.CustomerPhone = Phone;
            zModel.CategoryKey = 12;
            zModel.CategoryName = "Thu bán hàng";

            Receipt_Info zInfo = new Receipt_Info();
            zInfo.Receipt = zModel;
            zInfo.Create_ServerKey();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Message = string.Empty;
            }
            else
            {
                Message = zInfo.Message;
            }
        }
        [HttpGet]
        public ActionResult Receipt_SaleOrder(string OrderKey)
        {
            ServerResult zResult = new ServerResult();
            Order_Info zInfo = new Order_Info(OrderKey);
            Order_Model zModel = zInfo.Order;
            return PartialView("~/Views/Shared/_CollectMoney_SaleOrder.cshtml", zModel);
        }
        [HttpPost]
        public JsonResult Collect_SaleOrder(string Amount, string OrderID, string Name, string Phone, string Description, string CustomerKey, string OrderKey)
        {
            ServerResult zResult = new ServerResult();
            string Message = "";

            Receipt_SaleOrder(OrderID, Amount.ToDouble(), CustomerKey, Name, Phone, out Message);
            if (Message != string.Empty)
            {
                zResult.Success = false;
                zResult.Message = Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {

                double temp = Order_Data.SumOrder(UserLog.PartnerID, OrderID, out Message);
                temp = temp + Amount.ToDouble();

                Order_Info zInfo = new Order_Info(OrderKey);
                if (zInfo.Order.AmountOrder <= temp)
                {
                    zInfo.Order.StatusOrder = 9;
                    zInfo.Order.StatusName = "Đã thanh toán";
                    zInfo.Order.ModifiedBy = UserLog.UserKey;
                    zInfo.Order.ModifiedName = UserLog.EmployeeName;
                    zInfo.UpdateStatus();
                }


                zResult.Success = true;
                zResult.Message = string.Empty;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult OrderList(string btnAction = "", string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate == string.Empty &&
                ToDate == string.Empty)
            {
                zFromDate = TN_Utils.FirstDayOfWeek(DateTime.Now);
                zToDate = TN_Utils.LastDayOfWeek(DateTime.Now);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {

                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                switch (btnAction)
                {
                    case "btn_Prev":
                        zFromDate = zFromDate.AddDays(-6);
                        zToDate = zToDate.AddDays(-6);
                        break;
                    case "btn_Next":
                        zFromDate = zFromDate.AddDays(+6);
                        zToDate = zToDate.AddDays(+6);
                        break;
                }
            }

            var ListOrder = Order_Data.List(UserLog.PartnerNumber, out string Message, zFromDate, zToDate);
            ViewBag.ListOrder = ListOrder;
            ViewBag.ViewDate = zFromDate.ToString("dd/MM/yyyy") + "-" + zToDate.ToString("dd/MM/yyyy");
            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");
            return View("~/Views/Mobile/Sale/Order.cshtml");
        }

        [HttpGet]
        public JsonResult CheckCart()
        {
            ServerResult zResult = new ServerResult();
            if (Session["SaleOrder"] == null)
            {
                zResult.Success = false;
                zResult.Message = "Bạn chưa chọn mặt hàng !.";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = true;
                zResult.Data = Url.Action("Preview", "Mobile");
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Preview()
        {
            Order_Model zOrder = new Order_Model();
            if (Session["SaleOrder"] != null)
            {
                zOrder = Session["SaleOrder"] as Order_Model;
            }

            return PartialView("~/Views/Shared/_Preview_SaleOrder.cshtml", zOrder);
        }

        public ActionResult SaleIndex()
        {
            return View("~/Views/Mobile/Sale/Index.cshtml");
        }

        private string ChartJS_XLabelName(DataTable Table)
        {
            string zLabel = "";
            for (int i = 3; i < Table.Columns.Count; i++)
            {
                DataColumn c = Table.Columns[i];
                zLabel += "'" + c.ColumnName + "',";
            }
            zLabel = zLabel.Remove(zLabel.LastIndexOf(','), 1);
            return zLabel;
        }
        private string ChartJS_Dataset(DataTable Table)
        {
            string zDataset = "";
            foreach (DataRow r in Table.Rows)
            {
                int Num = TN_Utils.RandomNumber(0, 299);

                zDataset += "{ label: '" + r[0].ToString() + "',";
                //zDataset += " backgroundColor: 'rgba(" + Num + ", 99, " + Num + ", 0.2)',";
                zDataset += " backgroundColor: '" + r[1].ToString() + "',";
                zDataset += " borderColor: 'rgba(" + Num + ", 99, " + Num + ", 1)',";
                zDataset += " borderWidth: 1,";
                zDataset += " data: [";
                for (int i = 3; i < Table.Columns.Count; i++)
                {
                    zDataset += r[i].ToDouble() + ",";
                }
                zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
                zDataset += " ]},";
            }

            zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
            return zDataset;
        }

        public ActionResult rptWeek(string btnAction = "", string FromDate = "", string ToDate = "",
        string Option = "DONHANG,DOANHSO,THUCTHU,PHAITHU,CHIPHI,TON",
        string Color = "#383f4882,#0088cc82,#e361598f,#2baab194,#8000807a,#ffa50080")
        {
            string Message = "";
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate == string.Empty &&
                ToDate == string.Empty)
            {
                zFromDate = FirstDayOfWeek(DateTime.Now);
                zToDate = LastDayOfWeek(DateTime.Now);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                switch (btnAction)
                {
                    case "btn_Prev":
                        zFromDate = zFromDate.AddDays(-6);
                        zToDate = zToDate.AddDays(-6);
                        break;
                    case "btn_Next":
                        zFromDate = zFromDate.AddDays(+6);
                        zToDate = zToDate.AddDays(+6);
                        break;
                }
            }

            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");

            ViewBag.Option = Option;
            ViewBag.SaleAmount = Helper.rpt_SaleAmount(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.OrderTotal = Helper.rpt_OrderTotal(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.InCome = Helper.rpt_InCome(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.OrderDebt = Helper.rpt_OrderDebt(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.Payment = Helper.rpt_Payment(UserLog.PartnerNumber, zFromDate, zToDate, out Message);

            DataTable zTable = Helper.DB_ChartAIO(UserLog.PartnerNumber, Option, Color, zFromDate, zToDate, out Message);
            if (zTable.Rows.Count > 0)
            {
                ViewBag.Label = ChartJS_XLabelName(zTable);
                ViewBag.Dataset = ChartJS_Dataset(zTable);
            }

            //ViewBag.ListOrder = Order_Data.List(UserLog.PartnerNumber, out Message, zFromDate, zToDate);
            //ViewBag.ListPayment = Payment_Data.ListSearch(UserLog.PartnerNumber, out Message, zFromDate, zToDate, "");

            return View("~/Views/Mobile/Sale/Week.cshtml");
        }

        public ActionResult Cart()
        {
            Order_Model zOrder;
            if (Session["SaleOrder"] == null)
            {
                zOrder = new Order_Model();
            }
            else
            {
                zOrder = (Order_Model)Session["SaleOrder"];
            }

            return View("~/Views/Mobile/Sale/Cart.cshtml", zOrder);
        }

        public ActionResult Product(int CategoryKey = 0)
        {
            ViewBag.ListProductCategory = Product_Data.ListCategory(UserLog.PartnerNumber, out string Message);
            ViewBag.ListProductSearch = Product_Data.SearchProduct(UserLog.PartnerNumber, out string Message1, CategoryKey);
            ViewBag.CategoryKey = CategoryKey;
            ViewBag.ListProductUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message3);
            return View("~/Views/Mobile/Sale/Product.cshtml");
        }

        public ActionResult Print(string OrderKey)
        {
            Order_Info zInfo = new Order_Info(OrderKey);
            zInfo.Order.ListItem = Order_Data.ListItem(OrderKey, out string Message);

            Partner_Model zPart = new Partner_Info(UserLog.PartnerNumber).Partner;
            ViewBag.StoreName = zPart.StoreName;
            ViewBag.StorePhone = zPart.PartnerPhone;
            ViewBag.StoreAddress = zPart.PartnerAddress;
            ViewBag.StoreLogo = zPart.Logo_Large;
            ViewBag.BillConfig = zPart.BillConfig;

            string b64String = TN_Utils.QRWrite(zInfo.Order.OrderID, out Message);
            ViewBag.QRCode = b64String;

            return View("~/Views/Shared/_Print.cshtml", zInfo.Order);
        }

        public ActionResult CancelOrder(string OrderKey)
        {
            Session["SaleOrder"] = null;
            Order_Info zInfo = new Order_Info();
            zInfo.Delete(OrderKey);
            return RedirectToAction("Product");
        }

        [HttpPost]
        public JsonResult RemoveCart(string ObjectKey)
        {
            ServerResult zServerResult = new ServerResult();
            if (Session["SaleOrder"] != null)
            {
                Order_Model zOrder = (Order_Model)Session["SaleOrder"];
                var Item = zOrder.ListItem.FirstOrDefault(s => s.ObjectKey == ObjectKey);
                if (Item != null)
                {
                    zOrder.ListItem.Remove(Item);
                }

                zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);
                zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);

                float CartQuantity = zOrder.ListItem.Sum(s => s.Quantity);
                zServerResult.Success = true;
                zServerResult.Data = CartQuantity.ToString();

                if (zOrder.ListItem.Count == 0)
                {
                    Session["SaleOrder"] = null;
                }
            }
            else
            {
                zServerResult.Success = false;
                zServerResult.Message = "Bạn chưa có sản phẩm trong giỏ hàng !.";
            }

            return Json(zServerResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddToCart(string ProductKey)
        {
            ServerResult zServerResult = new ServerResult();
            try
            {
                Order_Model zOrder;
                if (Session["SaleOrder"] == null)
                {
                    zOrder = new Order_Model();
                    Product_Model zProduct = new Product_Info(ProductKey).Product;
                    zOrder.ListItem.Add(new Order_Item_Model
                    {
                        ObjectTable = "PDT_Product",
                        ObjectKey = zProduct.ProductKey,
                        ObjectName = zProduct.ProductName,
                        ObjectPrice = zProduct.SalePrice,
                        Quantity = 1,
                        AmountVAT = 0,
                        Amount = zProduct.SalePrice,
                        UnitKey = zProduct.StandardUnitKey,
                        UnitName = zProduct.StandardUnitName
                    });
                }
                else
                {
                    zOrder = (Order_Model)Session["SaleOrder"];
                    var Item = zOrder.ListItem.FirstOrDefault(s => s.ObjectKey == ProductKey);
                    if (Item != null)
                    {
                        Item.Quantity = Item.Quantity + 1;
                        Item.AmountVAT = 0;
                        Item.Amount = Item.ObjectPrice * Item.Quantity;
                    }
                    else
                    {
                        Product_Model zProduct = new Product_Info(ProductKey).Product;
                        zOrder.ListItem.Add(new Order_Item_Model
                        {
                            ObjectTable = "PDT_Product",
                            ObjectKey = zProduct.ProductKey,
                            ObjectName = zProduct.ProductName,
                            ObjectPrice = zProduct.SalePrice,
                            Quantity = 1,
                            AmountVAT = 0,
                            Amount = zProduct.SalePrice,
                            UnitKey = zProduct.StandardUnitKey,
                            UnitName = zProduct.StandardUnitName
                        });
                    }
                }

                zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.ObjectPrice);
                zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);

                Session["SaleOrder"] = zOrder;
                float CartQuantity = zOrder.ListItem.Sum(s => s.Quantity);

                zServerResult.Success = true;
                zServerResult.Data = CartQuantity.ToString();
            }
            catch (Exception ex)
            {
                zServerResult.Success = false;
                zServerResult.Message = ex.ToString();
            }

            return Json(zServerResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult EditCart(string Item)
        {
            ServerResult zServerResult = new ServerResult();
            try
            {
                Order_Item_Model zClientItem = JsonConvert.DeserializeObject<Order_Item_Model>(Item);
                Order_Model zOrder = (Order_Model)Session["SaleOrder"];
                var zEditItem = zOrder.ListItem.FirstOrDefault(s => s.ObjectKey == zClientItem.ObjectKey);
                if (zClientItem != null)
                {
                    zEditItem.ObjectPrice = zClientItem.ObjectPrice;
                    zEditItem.Quantity = zClientItem.Quantity;
                    zEditItem.Amount = (zClientItem.ObjectPrice * zClientItem.Quantity);
                    zEditItem.Description = zClientItem.Description.Trim();
                }
                //tổng tiền giảm giá
                zOrder.AmountDiscount = zOrder.ListItem.Sum(s => s.AmountDiscount);
                //tổng tiền đơn hàng
                zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);
                Session["SaleOrder"] = zOrder;

                float CartQuantity = zOrder.ListItem.Sum(s => s.Quantity);

                zServerResult.Success = true;
                zServerResult.Data = CartQuantity.ToString();
            }
            catch (Exception ex)
            {
                zServerResult.Success = false;
                zServerResult.Message = ex.ToString();
            }

            return Json(zServerResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveCart(string Info, string Method)
        {
            ServerResult zServerResult = new ServerResult();
            try
            {
                Order_Model zOrderModel = (Order_Model)Session["SaleOrder"];
                Order_Model zOrderClient;
                if (zOrderModel != null && zOrderModel.ListItem.Count > 0)
                {
                    #region [Khách hàng]
                    zOrderClient = JsonConvert.DeserializeObject<Order_Model>(Info);
                    Customer_Info zCusInfo = new Customer_Info();
                    if (zOrderClient.BuyerKey.Length == 0)
                    {
                        if (zOrderClient.BuyerPhone.Length > 0 &&
                            zOrderClient.BuyerName.Length > 0)
                        {
                            string NewKey = Guid.NewGuid().ToString();
                            zOrderClient.BuyerKey = NewKey;

                            zCusInfo = new Customer_Info();
                            zCusInfo.Customer.FullName = zOrderClient.BuyerName;
                            zCusInfo.Customer.Phone = zOrderClient.BuyerPhone;
                            zCusInfo.Customer.CustomerKey = NewKey;
                            zCusInfo.Customer.PartnerNumber = UserLog.PartnerNumber;
                            zCusInfo.Customer.CreatedBy = UserLog.UserKey;
                            zCusInfo.Customer.CreatedName = UserLog.EmployeeName;
                            zCusInfo.Customer.ModifiedBy = UserLog.UserKey;
                            zCusInfo.Customer.ModifiedName = UserLog.EmployeeName;
                            zCusInfo.Create_ClientKey();

                            if (zCusInfo.Code == "200" ||
                                zCusInfo.Code == "201")
                            {
                                zServerResult.Success = true;
                            }
                            else
                            {
                                zServerResult.Success = false;
                                zServerResult.Message = zCusInfo.Message;
                                return Json(zServerResult, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            zOrderClient.BuyerPhone = "KHÔNG XÁC ĐỊNH";
                            zOrderClient.BuyerName = "KHÁCH VÃNG LAI";
                        }
                    }
                    #endregion

                    #region [Đơn hàng]
                    string ID = Helper.AutoID_SaleOrder(UserLog.PartnerNumber, "SO");
                    string OrderKey = Guid.NewGuid().ToString();
                    zOrderModel.OrderID = ID;

                    zOrderModel.AmountOrder = zOrderClient.AmountOrder;
                    zOrderModel.PercentDiscount = zOrderClient.PercentDiscount;
                    zOrderModel.AmountDiscount = zOrderClient.AmountDiscount;
                    zOrderModel.AmountReceived = zOrderClient.AmountReceived;
                    zOrderModel.AmountReturned = zOrderClient.AmountReturned;

                    zOrderModel.BuyerKey = zOrderClient.BuyerKey;
                    zOrderModel.BuyerName = zOrderClient.BuyerName;
                    zOrderModel.BuyerPhone = zOrderClient.BuyerPhone;
                    zOrderModel.OrderDate = DateTime.Now;

                    //thực thu, nếu khách trả nhiều hơn tiền hàng thì thực thuc = tiền hàng (hoàn tiền lại), còn ít hơn thì thực thu = tiền khách trả (cọc).
                    double RealAmount = 0;
                    if (zOrderClient.AmountReceived >
                        zOrderClient.AmountOrder)
                    {
                        RealAmount = zOrderClient.AmountOrder;
                        zOrderModel.StatusOrder = 9;
                        zOrderModel.StatusName = "Đã thanh toán";
                    }
                    else
                    {
                        RealAmount = zOrderClient.AmountReceived;
                        zOrderModel.StatusOrder = 8;
                        zOrderModel.StatusName = "Đã cọc";
                    }

                    zOrderModel.PartnerNumber = UserLog.PartnerNumber;
                    zOrderModel.CreatedBy = UserLog.UserKey;
                    zOrderModel.CreatedName = UserLog.EmployeeName;
                    zOrderModel.ModifiedBy = UserLog.UserKey;
                    zOrderModel.ModifiedName = UserLog.EmployeeName;

                    Order_Info zInfo = new Order_Info();
                    zInfo.Order = zOrderModel;
                    zInfo.Order.OrderKey = OrderKey;
                    zInfo.Create_ClientKey();

                    if (zInfo.Code == "200" ||
                        zInfo.Code == "201")
                    {
                        for (int i = 0; i < zOrderModel.ListItem.Count; i++)
                        {
                            Order_Item_Info zItemInfo = new Order_Item_Info();
                            zItemInfo.Order_Item = zOrderModel.ListItem[i];
                            zItemInfo.Order_Item.OrderKey = OrderKey;
                            zItemInfo.Order_Item.PartnerNumber = UserLog.PartnerNumber;
                            zItemInfo.Order_Item.CreatedBy = UserLog.UserKey;
                            zItemInfo.Order_Item.CreatedName = UserLog.EmployeeName;
                            zItemInfo.Order_Item.ModifiedBy = UserLog.UserKey;
                            zItemInfo.Order_Item.ModifiedName = UserLog.EmployeeName;
                            zItemInfo.Create_ServerKey();

                            if (zItemInfo.Code == "200" ||
                                zItemInfo.Code == "201")
                            {
                                zServerResult.Data = OrderKey; //Url.Action("Print", "Mobile", new { OrderKey });
                                zServerResult.Success = true;
                            }
                            else
                            {
                                zServerResult.Success = false;
                                zServerResult.Message = zItemInfo.Message;
                                return Json(zServerResult, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        zServerResult.Success = false;
                        zServerResult.Message = zInfo.Message;
                        return Json(zServerResult, JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    if (Method == "Pay")
                    {
                        #region [Phiếu thu]
                        Receipt_Model zReceipt = new Receipt_Model();
                        zReceipt.PartnerNumber = UserLog.PartnerNumber;
                        zReceipt.CreatedBy = UserLog.UserKey;
                        zReceipt.CreatedName = UserLog.EmployeeName;
                        zReceipt.ModifiedBy = UserLog.UserKey;
                        zReceipt.ModifiedName = UserLog.EmployeeName;
                        zReceipt.DocumentID = ID;
                        zReceipt.ReceiptID = Receipt_Data.Auto_Receipt_ID(UserLog.PartnerNumber);
                        zReceipt.ReceiptDate = DateTime.Now;
                        zReceipt.ReceiptDescription = "Thu bán đơn hàng [" + ID + "] tự động !.";
                        zReceipt.AmountCurrencyMain = RealAmount;
                        zReceipt.Slug = 9;
                        zReceipt.CustomerKey = zOrderClient.BuyerKey;
                        zReceipt.CustomerName = zOrderClient.BuyerName;
                        zReceipt.CustomerPhone = zOrderClient.BuyerPhone;
                        zReceipt.CategoryKey = 12;
                        zReceipt.CategoryName = "Thu bán hàng";

                        Receipt_Info zReceiptInfo = new Receipt_Info();
                        zReceiptInfo.Receipt = zReceipt;
                        zReceiptInfo.Create_ServerKey();

                        if (zReceiptInfo.Code == "200" ||
                            zReceiptInfo.Code == "201")
                        {
                            zServerResult.Success = true;
                        }
                        else
                        {
                            zServerResult.Success = false;
                            zServerResult.Message = zReceiptInfo.Message;
                            return Json(zServerResult, JsonRequestBehavior.AllowGet);
                        }
                        #endregion
                    }
                    Session["SaleOrder"] = null;
                }
            }
            catch (Exception ex)
            {
                zServerResult.Success = false;
                zServerResult.Message = ex.ToString();
            }

            return Json(zServerResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetCustomerByPhone(string Phone)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(Phone, UserLog.PartnerNumber, true);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        private string CreateCustomer(string Tax, string Name)
        {
            string Key = Guid.NewGuid().ToString();
            Customer_Info zCus;
            if (Tax.Length > 0)
            {
                zCus = new Customer_Info(Tax, UserLog.PartnerNumber);
            }
            else
            {
                zCus = new Customer_Info();
            }

            zCus.Customer.CustomerKey = Key;
            zCus.Customer.CustomerID = Tax.Trim();
            zCus.Customer.TaxNumber = Tax.Trim();
            zCus.Customer.FullName = Name.Trim();

            zCus.Customer.PartnerNumber = UserLog.PartnerNumber;
            zCus.Customer.CreatedBy = UserLog.UserKey;
            zCus.Customer.CreatedName = UserLog.EmployeeName;
            zCus.Customer.ModifiedBy = UserLog.ModifiedBy;
            zCus.Customer.ModifiedName = UserLog.EmployeeName;

            zCus.Create_ClientKey();

            return Key;
        }
        #endregion

        #region [CASH]
        public ActionResult CashIndex()
        {
            return View();
        }

        #region[Danh sách phiếu chi]
        /// <summary>
        /// Tìm theo button tới lui THÁNG
        /// </summary>
        /// <param name="btnAction"></param>
        /// <param name="DateView"></param>
        /// <returns></returns>
        public ActionResult SearchPayment_Button(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }
            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            ViewBag.ListCategoryPayment = CashPayment_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            var Payment = Payment_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "");
            ViewBag.ListPayment = Payment;
            ViewBag.PaymentAmountCurrencyMain = Payment.Sum(s => s.AmountCurrencyMain);
            ViewBag.DateView = Month + "/" + Year;
            return View("~/Views/Mobile/Cash/Payment.cshtml");
        }
        /// <summary>
        /// Tim theo tùy chọn từ ngày đến ngày
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="SearchID"></param>
        /// <returns></returns>
        public ActionResult SearchPayment_Date(string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            ViewBag.ListCategoryPayment = CashPayment_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            var Payment = Payment_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "");
            ViewBag.ListPayment = Payment;
            ViewBag.PaymentAmountCurrencyMain = Payment.Sum(s => s.AmountCurrencyMain);
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");
            return View("~/Views/Mobile/Cash/Payment.cshtml");
        }

        #endregion

        #region [Nhập, xóa, sửa, AutoID phiếu chi]
        [HttpPost]
        public JsonResult mSavePayment(string PaymentKey, string PaymentID, DateTime PaymentDate, string PaymentDescription, int CategoryKey, string CategoryName, string CustomerName, string Address, double AmountCurrencyMain, bool IsFeeInside)
        {
            ServerResult zResult = new ServerResult();
            Payment_Info zInfo = new Payment_Info(PaymentKey);

            zInfo.Payment.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Payment.PaymentKey = PaymentKey.Trim();
            zInfo.Payment.PaymentID = PaymentID.Trim();
            zInfo.Payment.PaymentDate = PaymentDate;
            zInfo.Payment.PaymentDescription = PaymentDescription.Trim();
            zInfo.Payment.CategoryKey = CategoryKey.ToInt();
            zInfo.Payment.CategoryName = CategoryName.Trim();
            //zInfo.Payment.CustomerKey = CustomerKey.Trim();
            zInfo.Payment.CustomerName = CustomerName.Trim();
            zInfo.Payment.Address = Address.Trim();
            zInfo.Payment.AmountCurrencyMain = AmountCurrencyMain.ToDouble();
            //zInfo.Payment.IsFeeInside = IsFeeInside.ToBool();

            if (zInfo.Payment.PaymentKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult mDetailPayment(string PaymentKey)
        {
            ServerResult zResult = new ServerResult();
            Payment_Info zInfo = new Payment_Info(PaymentKey);
            Payment_Model zModel = zInfo.Payment;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult mDeletePayment(string PaymentKey)
        {
            ServerResult zResult = new ServerResult();
            Payment_Info zInfo = new Payment_Info();
            zInfo.Payment.PaymentKey = PaymentKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetPaymentID()
        {
            string ID = Payment_Data.Auto_Payment_ID(UserLog.PartnerNumber);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Phiếu thu]
        /// <summary>
        /// Tìm theo button tới lui THÁNG
        /// </summary>
        /// <param name="btnAction"></param>
        /// <param name="DateView"></param>
        /// <returns></returns>
        public ActionResult SearchReceipt_Button(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }
            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            ViewBag.ListCategoryReceipt = CashReceipt_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            var Receipt = Receipt_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "");
            ViewBag.ListReceipt = Receipt;
            ViewBag.ReceiptAmountCurrencyMain = Receipt.Sum(s => s.AmountCurrencyMain);
            ViewBag.DateView = Month + "/" + Year;

            return View("~/Views/Mobile/Cash/Receipt.cshtml");
        }
        /// <summary>
        /// Tim theo tùy chọn từ ngày đến ngày
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="SearchID"></param>
        /// <returns></returns>
        public ActionResult SearchReceipt_Date(string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            ViewBag.ListCategoryReceipt = CashReceipt_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            var Receipt = Receipt_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "");
            ViewBag.ListReceipt = Receipt;
            ViewBag.ReceiptAmountCurrencyMain = Receipt.Sum(s => s.AmountCurrencyMain);
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");
            return View("~/Views/Mobile/Cash/Payment.cshtml");
        }
        #endregion

        #region [Nhập, xóa, sửa AutoID phiếu thu]
        [HttpPost]
        public JsonResult mSaveReceipt(string ReceiptKey, string ReceiptID, DateTime ReceiptDate, string ReceiptDescription, int CategoryKey, string CategoryName, string CustomerName, string Address, double AmountCurrencyMain, bool IsFeeInside)
        {
            ServerResult zResult = new ServerResult();
            Receipt_Info zInfo = new Receipt_Info(ReceiptKey);

            zInfo.Receipt.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Receipt.ReceiptKey = ReceiptKey.Trim();
            zInfo.Receipt.ReceiptID = ReceiptID.Trim();
            zInfo.Receipt.ReceiptDate = ReceiptDate;
            zInfo.Receipt.ReceiptDescription = ReceiptDescription.Trim();
            zInfo.Receipt.CategoryKey = CategoryKey.ToInt();
            zInfo.Receipt.CategoryName = CategoryName.Trim();
            //zInfo.Receipt.CustomerKey = CustomerKey.Trim();
            zInfo.Receipt.CustomerName = CustomerName.Trim();
            zInfo.Receipt.Address = Address.Trim();
            zInfo.Receipt.AmountCurrencyMain = AmountCurrencyMain.ToDouble();
            //zInfo.Receipt.IsFeeInside = IsFeeInside.ToBool();

            if (zInfo.Receipt.ReceiptKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult mDetailReceipt(string ReceiptKey)
        {
            ServerResult zResult = new ServerResult();
            Receipt_Info zInfo = new Receipt_Info(ReceiptKey);
            Receipt_Model zModel = zInfo.Receipt;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult mDeleteReceipt(string ReceiptKey)
        {
            ServerResult zResult = new ServerResult();
            Receipt_Info zInfo = new Receipt_Info();
            zInfo.Receipt.ReceiptKey = ReceiptKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetReceiptID()
        {
            string ID = Receipt_Data.Auto_Receipt_ID(UserLog.PartnerNumber);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Danh sách CashBalance]
        public ActionResult Balance(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();
            }
            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            var PreAmount = CashBalance_Data.Amount_Month(zFromDate.AddMonths(-1), UserLog.PartnerNumber);
            var Inside = CashBalance_Data.Inside(UserLog.PartnerNumber, zFromDate, zToDate, true);

            ViewBag.PreAmount = PreAmount;
            ViewBag.ListInside = Inside;
            ViewBag.PaymentAmountCurrencyMain = Inside.Sum(s => s.PaymentAmountCurrencyMain);
            ViewBag.ReceiptAmountCurrencyMain = Inside.Sum(s => s.ReceiptAmountCurrencyMain);
            ViewBag.EndAmount = (PreAmount + (Inside.Sum(s => s.ReceiptAmountCurrencyMain)) - (Inside.Sum(s => s.PaymentAmountCurrencyMain)));
            ViewBag.DateView = Month + "/" + Year;
            return View("~/Views/Mobile/Cash/Balance.cshtml");
        }
        #endregion

        #region[Lưu cuối kỳ]
        [HttpPost]
        public JsonResult Save(string Amount, string DateView)
        {
            DateTime zDateView;
            DateTime.TryParse(DateView, out zDateView);

            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            int zCount = BalanceSheet_Month_Internal_Data.ListSearch(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            ServerResult zResult = new ServerResult();
            BalanceSheet_Month_Internal_Info zInfo = new BalanceSheet_Month_Internal_Info();
            zInfo.BalanceSheet_Month_Internal.PartnerNumber = UserLog.PartnerNumber;
            zInfo.BalanceSheet_Month_Internal.Amount = Amount.ToDouble();
            zInfo.BalanceSheet_Month_Internal.BalanceDate = zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 0, 0, 0);
            if (zCount == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                int Month = zToDate.Month;
                int Year = zToDate.Year;
                zInfo.Update(Month, Year);
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        private DateTime FirstDayOfWeek(DateTime date)
        {
            DayOfWeek fdow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            int offset = fdow - date.DayOfWeek;
            DateTime fdowDate = date.AddDays(offset);
            return fdowDate;
        }

        private DateTime LastDayOfWeek(DateTime date)
        {
            DateTime ldowDate = FirstDayOfWeek(date).AddDays(6);
            return ldowDate;
        }

        [HttpGet]
        public JsonResult AutoNumber(string term)
        {
            if (term.Length <= 4)
            {
                List<TNItem> number = new List<TNItem>();

                for (int n = 1; n <= 4; n++)
                {
                    string temp = "0";
                    for (int k = 0; k < n; k++)
                    {
                        temp = temp + "0";
                    }

                    double no = (term.ToInt() * ("1" + temp).ToInt() * 1000);

                    if (term.Length == 2)
                    {
                        no = no / 10;
                    }
                    if (term.Length == 3)
                    {
                        no = no / 100;
                    }
                    if (term.Length == 4)
                    {
                        no = no / 1000;
                    }

                    if (no > 0)
                    {
                        number.Add(new TNItem()
                        {
                            Value = no.ToString(),
                            Text = no.ToString("n0") + " VNĐ",
                        });
                    }
                }

                string data = JsonConvert.SerializeObject(number);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #region [QR CODE]
        [HttpPost]
        public JsonResult Scan(HttpPostedFileBase file)
        {
            string barcode = "";
            try
            {
                string path = "";
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/App_Data"), fileName);
                    file.SaveAs(path);
                    barcode = TN_Utils.QRScan(path, out string Message);

                    ViewBag.Title = Message;
                }
            }
            catch (Exception ex)
            {
                ViewBag.Title = ex.Message;
            }
            return Json(barcode);
        }
        #endregion
    }
}