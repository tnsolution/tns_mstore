﻿using System;
using System.Data;
using System.Globalization;
using System.Web.Mvc;

namespace TNStores.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index(string btnAction = "", string FromDate = "", string ToDate = "",
            string Option = "DONHANG,DOANHSO,THUBANHANG,THUKHAC,CHIPHI,TON",
            string Color = "#383f4882,#0088cc82,#e361598f,#2baab194,#8000807a,#ffa50080")
        {
            string Message = "";

            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate == string.Empty &&
                ToDate == string.Empty)
            {
                zFromDate = TN_Utils.FirstDayOfWeek(DateTime.Now);
                zToDate = TN_Utils.LastDayOfWeek(DateTime.Now);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                switch (btnAction)
                {
                    case "btn_Prev":
                        zFromDate = zFromDate.AddDays(-6);
                        zToDate = zToDate.AddDays(-6);
                        break;
                    case "btn_Next":
                        zFromDate = zFromDate.AddDays(+6);
                        zToDate = zToDate.AddDays(+6);
                        break;
                }
            }

            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");

            ViewBag.Option = Option;
            ViewBag.SaleAmount = Helper.rpt_SaleAmount(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.OrderTotal = Helper.rpt_OrderTotal(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.InCome = Helper.rpt_InCome(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.OrderDebt = Helper.rpt_OrderDebt(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
            ViewBag.Payment = Helper.rpt_Payment(UserLog.PartnerNumber, zFromDate, zToDate, out Message);

            DataTable zTable = Helper.DB_ChartAIO(UserLog.PartnerNumber, Option, Color, zFromDate, zToDate, out Message);
            if (zTable.Rows.Count > 0)
            {
                ViewBag.Label = ChartJS_XLabelName(zTable);
                ViewBag.Dataset = ChartJS_Dataset(zTable);
            }

            //
            ViewBag.ListOrder = Order_Data.List(UserLog.PartnerNumber, out Message, zFromDate, zToDate);
            ViewBag.ListPayment = Payment_Data.ListSearch(UserLog.PartnerNumber, out Message, zFromDate, zToDate, "");
            ViewBag.Message = TN_Utils.GetFirstLine(Message);
            return View();
        }

        private string ChartJS_XLabelName(DataTable Table)
        {
            string zLabel = "[";
            for (int i = 3; i < Table.Columns.Count; i++)
            {
                DataColumn c = Table.Columns[i];
                zLabel += "'" + c.ColumnName + "',";
            }
            zLabel = zLabel.Remove(zLabel.LastIndexOf(','), 1);
            zLabel += "]";
            return zLabel;
        }
        private string ChartJS_Dataset(DataTable Table)
        {
            string zDataset = "";
            foreach (DataRow r in Table.Rows)
            {
                int Num = TN_Utils.RandomNumber(0, 299);

                zDataset += "{ label: '" + r[0].ToString() + "',";
                //zDataset += " backgroundColor: 'rgba(" + Num + ", 99, " + Num + ", 0.2)',";
                zDataset += " backgroundColor: '" + r[1].ToString() + "',";
                zDataset += " borderColor: 'rgba(" + Num + ", 99, " + Num + ", 1)',";
                zDataset += " borderWidth: 1,";
                zDataset += " data: [";
                for (int i = 3; i < Table.Columns.Count; i++)
                {
                    zDataset += r[i].ToDouble() + ",";
                }
                zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
                zDataset += " ]},";
            }

            zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
            return zDataset;
        }
    }
}