﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TNStores.Controllers
{
    public class FinancialController : BaseController
    {
        //--------------------------------------------------------------------------------------------------------------------------------------------- INVOICE

        #region [Danh sách hđ vào]
        public ActionResult ListInvoiceIN()
        {
            DateTime zFromDate = DateTime.Now;
            DateTime zToDate = DateTime.Now;
            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
            zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            var ListInvoice = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "", "IN");
            TempData["ListExcel"] = ListInvoice;
            ViewBag.ListInvoiceIN = ListInvoice;
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");
            return View("~/Views/Desktop/Financial/Invoice/IN.cshtml");
        }
        /// <summary>
        /// Tim theo tùy chọn từ ngày đến ngày
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="SearchID"></param>
        /// <returns></returns>
        public ActionResult SearchInvoiceIN_Date(string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            var ListInvoice = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "", "IN");
            TempData["ListExcel"] = ListInvoice;
            ViewBag.ListInvoiceIN = ListInvoice;
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");

            return View("~/Views/Desktop/Financial/Invoice/IN.cshtml");
        }
        /// <summary>
        /// Tìm theo button tới lui QUÝ
        /// </summary>
        /// <param name="btnAction"></param>
        /// <param name="DateView"></param>
        /// <returns></returns>
        public ActionResult SearchInvoiceIN_Button(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }
            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion
            ViewBag.DateView = Month + "/" + Year;
            var ListInvoice = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "", "IN");
            TempData["ListExcel"] = ListInvoice;
            ViewBag.ListInvoiceIN = ListInvoice;
            return View("~/Views/Desktop/Financial/Invoice/IN.cshtml");
        }
        #endregion

        #region [Danh sách hđ ra]
        public ActionResult ListInvoiceOUT()
        {
            DateTime zFromDate = DateTime.Now;
            DateTime zToDate = DateTime.Now;
            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
            zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            var ListInvoice = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, zFromDate, zToDate, "", "OUT");
            TempData["ListExcel"] = ListInvoice;
            ViewBag.ListInvoiceOUT = ListInvoice;
            ViewBag.ListInvoiceIN = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message2, zFromDate, zToDate, "", "IN");
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");
            return View("~/Views/Desktop/Financial/Invoice/OUT.cshtml");
        }
        public ActionResult SearchInvoiceOUT_Date(string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");
            var ListInvoice = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, zFromDate, zToDate, "", "OUT");
            TempData["ListExcel"] = ListInvoice;
            ViewBag.ListInvoiceOUT = ListInvoice;
            ViewBag.ListInvoiceIN = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message2, zFromDate, zToDate, "", "IN");

            return View("~/Views/Desktop/Financial/Invoice/OUT.cshtml");
        }
        /// <summary>
        /// Tìm theo button tới lui QUÝ
        /// </summary>
        /// <param name="btnAction"></param>
        /// <param name="DateView"></param>
        /// <returns></returns>
        public ActionResult SearchInvoiceOUT_Button(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }
            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion
            ViewBag.DateView = Month + "/" + Year;
            var ListInvoice = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, zFromDate, zToDate, "", "OUT");
            TempData["ListExcel"] = ListInvoice;
            ViewBag.ListInvoiceOUT = ListInvoice;
            ViewBag.ListInvoiceIN = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message2, zFromDate, zToDate, "", "IN");
            return View("~/Views/Desktop/Financial/Invoice/OUT.cshtml");
        }
        #endregion

        #region [Nhập, xóa, sửa hđ]
        [HttpPost]
        public JsonResult SaveInvoice()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zInvoiceObj = Request.Form["InvoiceObj"];
                string zInvoiceKey = Request.Form["InvoiceKey"];

                Invoice_Model zModel = JsonConvert.DeserializeObject<Invoice_Model>(zInvoiceObj);
                if (zModel.PartnerKey == "NEW")
                {
                    zModel.PartnerKey = CreateCustomer(zModel.PartnerTax, zModel.PartnerName);
                }
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                Invoice_Info zInfo = new Invoice_Info(zInvoiceKey);
                //Check có file upload
                if (Request.Files.Count > 0)
                {
                    string zFilePath = Helper.UploadPath + "/" + UserLog.PartnerNumber + "/Invoice/";
                    HttpPostedFileBase files = Request.Files[0];
                    bool zUploadResult = Helper.UploadSingleFile(files, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        //xóa file cũ khi upload thanh công
                        if (zInfo.Invoice.FileAttach.Length > 0)
                        {
                            string Message = "";
                            Helper.DeleteSingleFile(zInfo.Invoice.FileAttach, out Message);
                        }
                        //gán đường dẫn mới
                        zModel.FileAttach = zFilePath;
                    }
                }
                else
                {
                    //không có file mới thì gắn lại file cũ
                    zModel.FileAttach = zInfo.Invoice.FileAttach;
                }

                zInfo.Invoice = zModel.TrimStringProperties();
                if (zInfo.Invoice.InvoiceKey == "")
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult DetailInvoice(string InvoiceKey)
        {
            ServerResult zResult = new ServerResult();
            Invoice_Info zInfo = new Invoice_Info(InvoiceKey);
            Invoice_Model zModel = zInfo.Invoice;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteInvoice(string InvoiceKey)
        {
            ServerResult zResult = new ServerResult();
            Invoice_Info zInfo = new Invoice_Info();
            zInfo.Invoice.InvoiceKey = InvoiceKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Cân đối hóa đơn]
        public ActionResult InvoiceBalanceYear(string btnAction = "", string DateView = "")
        {
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Year = DateView.ToInt();
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Year = Year - 1;
                    break;

                case "btn_Next":
                    Year = Year + 1;
                    break;
            }

            List<Invoice_Close_Model> zList = Invoice_Data.ListInvoiceYear(UserLog.PartnerNumber, Year, out string Mess);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-borderd bg-white Custom table-sm table-striped table-responsive-lg'>");
            zSb.AppendLine(HtmlHeader(zList.Count));
            zSb.AppendLine(HtmlRow("V được trừ chuyển qua", "AmountPrevious", zList));
            zSb.AppendLine(HtmlRow("V đầu vào", "AmountVatInTotal", zList));
            zSb.AppendLine(HtmlRow("  -  V trực tiếp", "AmountVatInDirect", zList));
            zSb.AppendLine(HtmlRow("  -  V dùng chung", "AmountVatInShare", zList));
            zSb.AppendLine(HtmlRow("V được trừ dùng chung", "AmountVatInShareToMinus", zList));
            zSb.AppendLine(HtmlRow("Tổng V được trừ trong quý", "AmountVatAllowMinus", zList));
            zSb.AppendLine(HtmlRow("Doanh số bán ra có V", "IncomeWithV", zList));
            zSb.AppendLine(HtmlRow("Doanh số bán ra không V", "IncomeNoV", zList));
            zSb.AppendLine(HtmlRow("Tổng doanh số bán ra", "InComeTotal", zList));
            zSb.AppendLine(HtmlRow("V đầu ra", "AmountVatOutTotal", zList));
            zSb.AppendLine(HtmlRow("Số tiền nộp thuế", "AmountPayCurrent", zList));
            zSb.AppendLine(HtmlRow("Số tiền nộp thuế khi gộp quý trước", "AmountPayInclude", zList));
            zSb.AppendLine("</table>");
            ViewBag.HtmlData = zSb.ToString();
            ViewBag.DateView = Year;
            return View("~/Views/Desktop/Financial/Invoice/Year.cshtml");
        }

        public string HtmlHeader(int Col)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<tr>");
            zSb.AppendLine("    <td>#</td>");
            zSb.AppendLine("    <td>Nội dung</td>");
            for (int i = 1; i <= Col; i++)
            {
                zSb.AppendLine("    <td class='text-center'>Q" + i + "</td>");
            }
            zSb.AppendLine("    <td class='text-center'>Tổng</td>");
            zSb.AppendLine("</tr>");
            return zSb.ToString();
        }
        public string HtmlRow(string text, string field, List<Invoice_Close_Model> listData)
        {
            double TotalRow = 0;
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<tr>");
            zSb.AppendLine("<td><i class='fas fa-circle-notch'></i></td>");
            zSb.AppendLine("<td class='font-weight-semibold text-dark' style='white-space: nowrap;'>" + text + "</td>");

            for (int i = 1; i <= listData.Count; i++)
            {
                try
                {
                    var item = listData[i - 1];
                    if (item.CloseDate.Split('/')[0].ToInt() == i)
                    {
                        Invoice_Quarter_Model zQuarter = JsonConvert.DeserializeObject<Invoice_Quarter_Model>(item.JsonData);
                        var propertyInfo = zQuarter.GetType().GetProperty(field);
                        var value = propertyInfo.GetValue(zQuarter, null);
                        TotalRow += value.ToDouble();
                        zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>" + value.FormatMoneyFromObject() + "</td>");
                    }
                    else
                    {
                        zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>0</td>");
                    }
                }
                catch (Exception)
                {
                    zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>0</td>");
                }
            }
            zSb.AppendLine("<td class='font-weight-semibold text-danger text-right'>" + TotalRow.FormatMoneyFromObject() + "</td>");
            zSb.AppendLine("</tr>");

            return zSb.ToString();
        }

        public ActionResult InvoiceBalanceQuarter(string btnAction = "", string DateView = "")
        {
            #region Quarter Datetime
            int Quarter = (DateTime.Now.Month + 2) / 3;
            int Year = DateTime.Now.Year;

            if (DateView != string.Empty)
            {
                Quarter = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Quarter = Quarter - 1;
                    break;

                case "btn_Next":
                    Quarter = Quarter + 1;
                    break;
            }

            if (Quarter <= 0)
            {
                Quarter = 4;
                Year = Year - 1;
            }
            if (Quarter >= 5)
            {
                Quarter = 1;
                Year = Year + 1;
            }

            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;

            switch (Quarter)
            {
                case 1:
                    FromDate = new DateTime(Year, 01, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 03, 31, 23, 59, 59);
                    break;

                case 2:
                    FromDate = new DateTime(Year, 04, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 06, 30, 23, 59, 59);
                    break;

                case 3:
                    FromDate = new DateTime(Year, 07, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 09, 30, 23, 59, 59);
                    break;

                case 4:
                    FromDate = new DateTime(Year, 10, 01, 00, 00, 00);
                    ToDate = new DateTime(Year, 12, 31, 23, 59, 59);
                    break;
            }
            ViewBag.DateView = Quarter + "/" + Year;
            TempData["DateView"] = Quarter + "/" + Year;
            #endregion

            var InvoiceIn = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message, FromDate, ToDate, "", "IN");
            ViewBag.ListInvoiceIN = InvoiceIn;
            var InvoiceOut = Invoice_Data.ListSearch(UserLog.PartnerNumber, out string Message1, FromDate, ToDate, "", "OUT");
            ViewBag.ListInvoiceOUT = InvoiceOut;
            var InvoiceRef = Invoice_Data.ListVatOUTRef(UserLog.PartnerNumber, out string Message2, FromDate, ToDate);
            ViewBag.ListInvoiceRef = InvoiceRef;

            ///tổng kết quý
            Invoice_Quarter_Model zSummary = new Invoice_Quarter_Model();

            zSummary.Previous = Quarter.ToInt() - 1;
            zSummary.Current = Quarter.ToInt();

            double SoVatTruoc = zSummary.AmountPrevious = new Invoice_Close_Info((Quarter - 1) + "/" + Year).Invoice_Close.CloseAmount;

            double DoanhSoChiuThue = zSummary.IncomeWithV = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double DoanhSoKhongThue = zSummary.IncomeNoV = InvoiceOut.Where(s => s.AmountVat == 0 && s.ReturnStatus != 1).Sum(s => s.AmountNotV);
            double TongDoanhSo = zSummary.InComeTotal = DoanhSoChiuThue + DoanhSoKhongThue;

            double VatVaoTrucTiep = zSummary.AmountVatInDirect = InvoiceIn.Where(s => s.CategoryKey == 0).Sum(s => s.AmountVat);
            double VatVaoDungChungTamTinh = zSummary.AmountVatInShare = InvoiceIn.Where(s => s.CategoryKey == 1 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double VatVaoDungChungDuocTru = zSummary.AmountVatInShareToMinus = Math.Round((DoanhSoChiuThue / TongDoanhSo) * VatVaoDungChungTamTinh);
            double VatVaoTongCong = zSummary.AmountVatInTotal = VatVaoTrucTiep + VatVaoDungChungDuocTru;

            double SoVatDuocTru = zSummary.AmountVatAllowMinus = VatVaoTrucTiep + VatVaoDungChungDuocTru;
            double SoVatDauTra = zSummary.AmountVatOutTotal = InvoiceOut.Where(s => s.AmountVat > 0 && s.ReturnStatus != 1).Sum(s => s.AmountVat);
            double SoVatQuy = zSummary.AmountPayCurrent = VatVaoTongCong - SoVatDauTra;
            double SoVatGop = zSummary.AmountPayInclude = SoVatQuy + SoVatTruoc;

            if (SoVatGop > 0)
            {
                zSummary.AmountNext = SoVatGop;
            }

            TempData["ReportInvoice"] = zSummary;
            ViewBag.Summary = zSummary;
            return View("~/Views/Desktop/Financial/Invoice/Quarter.cshtml");
        }

        [HttpGet]
        public JsonResult GetAmountPrevious(string Quarter = "", string Year = "")
        {
            int zQuarter = Quarter.ToInt() - 1;
            string zCloseDate = ("Q" + zQuarter + "/" + Year);
            Invoice_Close_Info zInfo = new Invoice_Close_Info(zCloseDate);
            return Json(zInfo.Invoice_Close.CloseAmount.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveQuarterInvoice()
        {

            ServerResult zResult = new ServerResult();
            if (TempData["ReportInvoice"] != null &&
                TempData["DateView"] != null)
            {
                string zCloseDate = TempData["DateView"].ToString();
                Invoice_Quarter_Model zReport = TempData["ReportInvoice"] as Invoice_Quarter_Model;

                #region [Apply Data]
                Invoice_Close_Model zModel = new Invoice_Close_Model();
                zModel.CloseAmount = zReport.AmountNext;
                zModel.CloseType = "Quarter";
                zModel.CloseDate = zCloseDate;
                zModel.Description = "";
                zModel.JsonData = JsonConvert.SerializeObject(zReport);
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;
                #endregion

                #region [Save Data]
                Invoice_Close_Info zInfo = new Invoice_Close_Info(zCloseDate);
                if (zInfo.Invoice_Close.CloseKey.Length > 0)
                {
                    zModel.CloseKey = zInfo.Invoice_Close.CloseKey;
                    zInfo.Invoice_Close = zModel;
                    zInfo.Update();
                }
                else
                {
                    zInfo.Invoice_Close = zModel;
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                #endregion
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "TempData ERROR";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Other AJAX help xử lý phụ]
        /// <summary>
        /// lấy mã khách = mã số thuế
        /// </summary>
        /// <param name="Tax"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetCustomerByTax(string Tax)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(Tax, UserLog.PartnerNumber);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// lấy mã khách = key
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetCustomerByKey(string Key)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(Key);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (zInfo.Code == "404")
                {
                    zResult.Success = false;
                    zResult.Message = "Không tìm thấy khách hàng này trong dữ liệu, ứng dụng sẽ cập nhật thông tin khách hàng mới này !.";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                }
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// lưu nhanh thông tin khách hàng trả về key khách hàng mới vừa tạo
        /// </summary>
        /// <param name="Tax"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        private string CreateCustomer(string Tax, string Name)
        {
            string Key = Guid.NewGuid().ToString();
            Customer_Info zCus = new Customer_Info(Tax, UserLog.PartnerNumber);

            zCus.Customer.CustomerKey = Key;
            zCus.Customer.CustomerID = Tax.Trim();
            zCus.Customer.TaxNumber = Tax.Trim();
            zCus.Customer.FullName = Name.Trim();

            zCus.Customer.PartnerNumber = UserLog.PartnerNumber;
            zCus.Customer.CreatedBy = UserLog.UserKey;
            zCus.Customer.CreatedName = UserLog.EmployeeName;
            zCus.Customer.ModifiedBy = UserLog.ModifiedBy;
            zCus.Customer.ModifiedName = UserLog.EmployeeName;

            zCus.Create_ClientKey();

            return Key;
        }
        #endregion

        public ActionResult ExportToExcel()
        {
            if (TempData["ListExcel"] != null)
            {
                List<Invoice_Model> zList = TempData["ListExcel"] as List<Invoice_Model>;

                DataTable zTable = new DataTable();
                zTable.Columns.Add("STT", typeof(string));
                zTable.Columns.Add("Số hóa đơn", typeof(string));
                zTable.Columns.Add("Ngày phát hành", typeof(string));
                zTable.Columns.Add("Tên người bán", typeof(string));
                zTable.Columns.Add("Mã số thuế", typeof(string));
                zTable.Columns.Add("Giá trị chưa thuế", typeof(string));
                zTable.Columns.Add("Thuế GTGT", typeof(string));
                zTable.Columns.Add("Ghi chú", typeof(string));

                int No = 1;
                foreach (Invoice_Model zItem in zList)
                {
                    DataRow r = zTable.NewRow();

                    r[0] = No;
                    r[1] = zItem.InvoiceID;
                    r[2] = zItem.InvoiceDate.ToString("dd/MM/yyyy");
                    r[3] = zItem.PartnerName;
                    r[4] = zItem.PartnerTax;
                    r[5] = zItem.AmountNotV.ToString();
                    r[6] = zItem.AmountVat.ToString();
                    r[7] = zItem.Description;

                    zTable.Rows.Add(r);

                    No++;
                }

                var gv = new GridView();
                gv.DataSource = zTable;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=BK_Mua_Ban.xls");
                Response.ContentType = "application/ms-excel";
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                Response.Charset = "";
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                Response.Output.Write(objStringWriter.ToString());
                Response.Flush();
                Response.End();
            }

            return View("~/Views/Desktop/Financial/Invoice/IN.cshtml");
        }

        [HttpPost]
        public JsonResult ReturnInvoice()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zReplaceID = Request.Form["ID"];
                string zReplaceDescription = Request.Form["Description"];
                string zInvoiceKey = Request.Form["InvoiceKey"];

                Invoice_Info zInfo = new Invoice_Info(zInvoiceKey);

                //đã có file thì xóa
                if (zInfo.Invoice.ReplaceFile.Length > 0)
                {
                    string Message = "";
                    Helper.DeleteSingleFile(zInfo.Invoice.ReplaceFile, out Message);
                }

                //Check có file upload
                if (Request.Files.Count > 0)
                {
                    string zFilePath = Helper.UploadPath + "/" + UserLog.PartnerNumber + "/Invoice/";
                    HttpPostedFileBase files = Request.Files[0];
                    bool zUploadResult = Helper.UploadSingleFile(files, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zInfo.Invoice.ReplaceID = zReplaceID;
                        zInfo.Invoice.ReplaceDescription = zReplaceDescription;
                        zInfo.Invoice.ReplaceFile = zFilePath;
                        zInfo.ReturnInvoice();
                        if (zInfo.Code == "200" ||
                            zInfo.Code == "201")
                        {
                            zResult.Success = true;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            zResult.Success = false;
                            zResult.Message = zInfo.Message;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DownloadZip()
        {
            string Message = string.Empty;
            if (System.IO.File.Exists(Server.MapPath
                            ("~/zipfiles/invoice.zip")))
            {
                System.IO.File.Delete(Server.MapPath
                              ("~/zipfiles/invoice.zip"));
            }
            ZipArchive zip = ZipFile.Open(Server.MapPath
                     ("~/zipfiles/invoice.zip"), ZipArchiveMode.Create);

            if (TempData["ListExcel"] != null)
            {
                List<Invoice_Model> _List = TempData["ListExcel"] as List<Invoice_Model>;

                foreach (var item in _List)
                {
                    if (item.FileAttach.Trim().Length > 0)
                    {
                        string fileext = GetLast(item.FileAttach, 4);
                        string filepath = "~/" + item.FileAttach;
                        string filename = item.PartnerName + "-" + item.InvoiceID + fileext;

                        try
                        {
                            zip.CreateEntryFromFile(Server.MapPath
                            (filepath), filename);
                        }
                        catch (Exception ex)
                        {
                            Message += ex.ToString().GetFirstLine();
                        }
                    }
                }
            }
            ViewBag.Message = Message;
            zip.Dispose();
            return File(Server.MapPath("~/zipfiles/invoice.zip"),
                      "application/zip", "TNS.zip");
        }

        //--------------------------------------------------------------------------------------------------------------------------------------------- CASH

        #region [Phiếu chi]
        public ActionResult ListPayment()
        {
            DateTime zFromDate = DateTime.Now;
            DateTime zToDate = DateTime.Now;
            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
            zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            ViewBag.ListCategoryPayment = CashPayment_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListPayment = Payment_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "");
            ViewBag.DateView = DateTime.Now.Month + "/" + DateTime.Now.Year;
            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");
            return View("~/Views/Desktop/Financial/Cash/Payment.cshtml");
        }
        /// <summary>
        /// Tim theo tùy chọn từ ngày đến ngày
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="SearchID"></param>
        /// <returns></returns>
        public ActionResult SearchPayment_Date(string FromDate = "", string ToDate = "", string SearchID = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            ViewBag.ListCategoryPayment = CashPayment_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListPayment = Payment_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, SearchID);
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");

            return View("~/Views/Desktop/Financial/Cash/Payment.cshtml");
        }
        /// <summary>
        /// Tìm theo button tới lui THÁNG
        /// </summary>
        /// <param name="btnAction"></param>
        /// <param name="DateView"></param>
        /// <returns></returns>
        public ActionResult SearchPayment_Button(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }
            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            ViewBag.ListCategoryPayment = CashPayment_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListPayment = Payment_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, string.Empty);
            ViewBag.DateView = Month + "/" + Year;

            return View("~/Views/Desktop/Financial/Cash/Payment.cshtml");
        }
        #endregion

        #region [Nhập, xóa, sửa, AutoID phiếu chi]
        [HttpPost]
        public JsonResult SavePayment(string PaymentKey, string PaymentID, DateTime PaymentDate, string PaymentDescription, int CategoryKey, string CategoryName, string CustomerName, string Address, double AmountCurrencyMain, bool IsFeeInside)
        {
            ServerResult zResult = new ServerResult();
            Payment_Info zInfo = new Payment_Info(PaymentKey);

            zInfo.Payment.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Payment.PaymentKey = PaymentKey.Trim();
            zInfo.Payment.PaymentID = PaymentID.Trim();
            zInfo.Payment.PaymentDate = PaymentDate;
            zInfo.Payment.PaymentDescription = PaymentDescription.Trim();
            zInfo.Payment.CategoryKey = CategoryKey.ToInt();
            zInfo.Payment.CategoryName = CategoryName.Trim();
            //zInfo.Payment.CustomerKey = CustomerKey.Trim();
            zInfo.Payment.CustomerName = CustomerName.Trim();
            zInfo.Payment.Address = Address.Trim();
            zInfo.Payment.AmountCurrencyMain = AmountCurrencyMain.ToDouble();
            //zInfo.Payment.IsFeeInside = IsFeeInside.ToBool();

            zInfo.Payment.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Payment.CreatedBy = UserLog.UserKey;
            zInfo.Payment.CreatedName = UserLog.EmployeeName;
            zInfo.Payment.ModifiedBy = UserLog.UserKey;
            zInfo.Payment.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Payment.PaymentKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailPayment(string PaymentKey)
        {
            ServerResult zResult = new ServerResult();
            Payment_Info zInfo = new Payment_Info(PaymentKey);
            Payment_Model zModel = zInfo.Payment;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeletePayment(string PaymentKey)
        {
            ServerResult zResult = new ServerResult();
            Payment_Info zInfo = new Payment_Info();
            zInfo.Payment.PaymentKey = PaymentKey;
            zInfo.Payment.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Payment.CreatedBy = UserLog.UserKey;
            zInfo.Payment.CreatedName = UserLog.EmployeeName;
            zInfo.Payment.ModifiedBy = UserLog.UserKey;
            zInfo.Payment.ModifiedName = UserLog.EmployeeName;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetPaymentID()
        {
            string ID = Payment_Data.Auto_Payment_ID(UserLog.PartnerNumber);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Phiếu thu]
        public ActionResult ListReceipt()
        {
            DateTime zFromDate = DateTime.Now;
            DateTime zToDate = DateTime.Now;
            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
            zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            ViewBag.ListCategoryReceipt = CashReceipt_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListReceipt = Receipt_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, "");
            ViewBag.DateView = DateTime.Now.Month + "/" + DateTime.Now.Year;

            return View("~/Views/Desktop/Financial/Cash/Receipt.cshtml");
        }
        /// <summary>
        /// Tim theo tùy chọn từ ngày đến ngày
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="SearchID"></param>
        /// <returns></returns>
        public ActionResult SearchReceipt_Date(string FromDate = "", string ToDate = "", string SearchID = "")
        {
            DateTime zFromDate = DateTime.Now;
            if (FromDate.Length > 0)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            }

            DateTime zToDate = DateTime.Now;
            if (ToDate.Length > 0)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            }

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            ViewBag.ListCategoryReceipt = CashReceipt_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListReceipt = Receipt_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, SearchID);
            ViewBag.DateView = zFromDate.ToString("dd/MM") + " - " + zToDate.ToString("dd/MM") + " /" + zFromDate.ToString("yyyy");

            return View("~/Views/Desktop/Financial/Cash/Receipt.cshtml");
        }
        /// <summary>
        /// Tìm theo button tới lui THÁNG
        /// </summary>
        /// <param name="btnAction"></param>
        /// <param name="DateView"></param>
        /// <returns></returns>
        public ActionResult SearchReceipt_Button(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            ViewBag.ListCategoryReceipt = CashReceipt_Category_Data.List(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListReceipt = Receipt_Data.ListSearch(UserLog.PartnerNumber, out string Message, zFromDate, zToDate, string.Empty);
            ViewBag.DateView = Month + "/" + Year;
            return View("~/Views/Desktop/Financial/Cash/Receipt.cshtml");
        }
        #endregion

        #region [Nhập, xóa, sửa AutoID phiếu thu]
        [HttpPost]
        public JsonResult SaveReceipt(string ReceiptKey, string ReceiptID, DateTime ReceiptDate, string ReceiptDescription, int CategoryKey, string CategoryName, string CustomerName, string Address, double AmountCurrencyMain, bool IsFeeInside)
        {
            ServerResult zResult = new ServerResult();
            Receipt_Info zInfo = new Receipt_Info(ReceiptKey);

            zInfo.Receipt.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Receipt.ReceiptKey = ReceiptKey.Trim();
            zInfo.Receipt.ReceiptID = ReceiptID.Trim();
            zInfo.Receipt.ReceiptDate = ReceiptDate;
            zInfo.Receipt.ReceiptDescription = ReceiptDescription.Trim();
            zInfo.Receipt.CategoryKey = CategoryKey.ToInt();
            zInfo.Receipt.CategoryName = CategoryName.Trim();
            //zInfo.Receipt.CustomerKey = CustomerKey.Trim();
            zInfo.Receipt.CustomerName = CustomerName.Trim();
            zInfo.Receipt.Address = Address.Trim();
            zInfo.Receipt.AmountCurrencyMain = AmountCurrencyMain.ToDouble();
            //zInfo.Receipt.IsFeeInside = IsFeeInside.ToBool();

            zInfo.Receipt.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Receipt.CreatedBy = UserLog.UserKey;
            zInfo.Receipt.CreatedName = UserLog.EmployeeName;
            zInfo.Receipt.ModifiedBy = UserLog.UserKey;
            zInfo.Receipt.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Receipt.ReceiptKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailReceipt(string ReceiptKey)
        {
            ServerResult zResult = new ServerResult();
            Receipt_Info zInfo = new Receipt_Info(ReceiptKey);
            Receipt_Model zModel = zInfo.Receipt;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteReceipt(string ReceiptKey)
        {
            ServerResult zResult = new ServerResult();
            Receipt_Info zInfo = new Receipt_Info();
            zInfo.Receipt.ReceiptKey = ReceiptKey;
            zInfo.Receipt.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Receipt.CreatedBy = UserLog.UserKey;
            zInfo.Receipt.CreatedName = UserLog.EmployeeName;
            zInfo.Receipt.ModifiedBy = UserLog.UserKey;
            zInfo.Receipt.ModifiedName = UserLog.EmployeeName;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetReceiptID()
        {
            string ID = Receipt_Data.Auto_Receipt_ID(UserLog.PartnerNumber);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Danh sách CashBalance]
        public ActionResult CashBalance()
        {
            DateTime zFromDate = DateTime.Now;
            DateTime zToDate = DateTime.Now;
            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
            zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            var PreAmount = CashBalance_Data.Amount_Month(zFromDate.AddMonths(-1), UserLog.PartnerNumber);
            ViewBag.PreAmount = PreAmount;
            var Inside = CashBalance_Data.Inside(UserLog.PartnerNumber, zFromDate, zToDate, true);
            ViewBag.ListInside = Inside;
            ViewBag.DateView = DateTime.Now.Month + "/" + DateTime.Now.Year;
            return View("~/Views/Desktop/Financial/Cash/Balance.cshtml");
        }
        /// <summary>
        /// Tìm theo button tới lui THÁNG
        /// </summary>
        /// <param name="btnAction"></param>
        /// <param name="DateView"></param>
        /// <returns></returns>
        public ActionResult SearchCashBalance_Button(string btnAction = "", string DateView = "")
        {
            #region Datetime string to fromdate - todate
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();
            }
            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            var PreAmount = CashBalance_Data.Amount_Month(zFromDate.AddMonths(-1), UserLog.PartnerNumber);
            ViewBag.PreAmount = PreAmount;
            var Inside = CashBalance_Data.Inside(UserLog.PartnerNumber, zFromDate, zToDate, true);
            ViewBag.ListInside = Inside;
            ViewBag.DateView = Month + "/" + Year;
            return View("~/Views/Desktop/Financial/Cash/Balance.cshtml");
        }
        #endregion

        #region[Lưu cuối kỳ]
        [HttpPost]
        public JsonResult Save(string Amount, string DateView)
        {
            DateTime zDateView;
            DateTime.TryParse(DateView, out zDateView);

            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            int zCount = BalanceSheet_Month_Internal_Data.ListSearch(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            ServerResult zResult = new ServerResult();
            BalanceSheet_Month_Internal_Info zInfo = new BalanceSheet_Month_Internal_Info();
            zInfo.BalanceSheet_Month_Internal.PartnerNumber = UserLog.PartnerNumber;
            zInfo.BalanceSheet_Month_Internal.Amount = Amount.ToDouble();
            zInfo.BalanceSheet_Month_Internal.BalanceDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 0, 0, 0);
            if (zCount == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                int Month = zToDate.Month;
                int Year = zToDate.Year;
                zInfo.Update(Month, Year);
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        private string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
            {
                return source;
            }

            return source.Substring(source.Length - tail_length);
        }
    }
}