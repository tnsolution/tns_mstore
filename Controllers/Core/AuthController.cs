﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TNStores.Controllers
{
    public class AuthController : Controller
    {
        // GET: Login
        public ActionResult SignIn()
        {
            if (Request.Cookies[".ASPXAUTH"] != null)
            {
                string zCok = Request.Cookies[".ASPXAUTH"].Value;
                FormsAuthenticationTicket zTicket = FormsAuthentication.Decrypt(zCok);
                if (zTicket.UserData.Length > 0)
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };

                    var zUserData = JsonConvert.DeserializeObject<User_Model>(zTicket.UserData, settings);
                    ViewBag.Info = zUserData;
                }
            }

            return View("~/Views/SignIn.cshtml");
        }
        [HttpPost]
        public ActionResult SignIn(string UserName, string Password, string PartnerID, string Device, bool Remember = false)
        {
            string ViewScreen = ConfigurationManager.AppSettings["ModuleRole"] + "_" + Device;

            ServerResult zResult = new ServerResult();

            if (UserName != string.Empty &&
                Password != string.Empty)
            //&& PartnerID != string.Empty)
            {
                //string PartnerNumber = Helper.GetPartnerNumber(PartnerID, out Error);
                //if (PartnerNumber.Length <= 0)
                //{
                //    ViewBag.Message = "Mã định danh không tồn tại !.";
                //    return View("~/Views/SignIn.cshtml");
                //}

                User_Info zInfo = new User_Info(UserName, Password);
                if (zInfo.User.Code == "200")
                {
                    if (DateTime.Now > zInfo.User.ExpireDate.Value)
                    {
                        ViewBag.Message = "Tài khoản của bạn đã hết hạn sử dụng ngày " + zInfo.User.ExpireDate.Value.ToString("dd/MM/yyyy") + " !.";
                    }
                    else
                    {
                        string Message = "";
                        zInfo.User.ListRole = User_Data.ReadUserRole(zInfo.User.PartnerNumber, zInfo.User.UserKey, ViewScreen, out Message);
                        zInfo.User.VIEWTYPE = Device;
                        Session["User_Model"] = zInfo.User;
                        Session["Partner_Model"] = new Partner_Info(zInfo.User.PartnerNumber).Partner;
                        

                        var UserData = JsonConvert.SerializeObject(new User_Model { UserName = UserName, Password = Password, PartnerID = PartnerID });
                        int timeout = Remember ? 525600 : 30;
                        // Timeout in minutes, 525600 = 365 days.
                        var ticket = new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddMinutes(timeout), false, UserData);
                        string encrypted = FormsAuthentication.Encrypt(ticket);
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                        cookie.Expires = DateTime.Now.AddMinutes(timeout);
                        cookie.HttpOnly = true;
                        // make sure cookie not available in javascript.
                        Response.Cookies.Add(cookie);

                        if (Device.ToUpper() == "MOBILE")
                        {
                            return RedirectToRoute("mTrangChu");
                        }
                        else
                        {
                            return RedirectToRoute("dTrangChu");
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "Tên hoặc mật khẩu không đúng !.";
                }
            }
            else
            {
                ViewBag.Message = "Tên hoặc mật khẩu không đúng !.";
            }


            return View("~/Views/SignIn.cshtml");
        }

        public ActionResult SignUp()
        {
            return View("~/Views/SignUp.cshtml");
        }

        public ActionResult SignOut()
        {
            Session["User_Model"] = null;
            return View("~/Views/SignIn.cshtml");
        }
    }
}