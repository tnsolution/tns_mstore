﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace TNStores.Controllers
{
    public class ConfigController : BaseController
    {
        private static string VIEWTYPE = UserLog.VIEWTYPE;
        //#region DÙNG CHUNG CHO DESKTOP VÀ MOBILE
        //public ActionResult A_InfoBill()
        //{
        //    string PartnerNumber = UserLog.PartnerNumber;
        //    Partner_Info zInfo = new Partner_Info(PartnerNumber);           
        //    ViewBag.Data = zInfo.Partner.BillConfig;

        //    string zPageView = "~/Views/" + VIEWTYPE + "/Config/BillInfo.cshtml";
        //    return View(zPageView, );
        //}

        //public ActionResult A_InfoBillUpdate(HttpPostedFileBase[] files, string Title = "", string Description = "", string End = "")
        //{
        //    User_Model zUser = Session["User_Model"] as User_Model;
        //    string PartnerNumber = zUser.PartnerNumber;
        //    Partner_Info zInfo = new Partner_Info(PartnerNumber);
        //    string Data = zInfo.Partner.BillConfig;
        //    string base64String = "";
        //    if (Title != string.Empty ||
        //        Description != string.Empty ||
        //        End != string.Empty)
        //    {

        //        if (files.Length > 0)
        //        {
        //            if (files[0] != null)
        //            {
        //                Stream fs = files[0].InputStream;
        //                BinaryReader br = new BinaryReader(fs);
        //                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
        //                base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
        //                base64String = "data:image/png;base64," + base64String;
        //            }
        //        }
        //    }

        //    if (base64String == string.Empty &&
        //        zInfo.Partner.Logo_Large.Length > 0)
        //    {
        //        base64String = zInfo.Partner.Logo_Large;
        //    }

        //    Data = Title.Trim() + "|" + Description.Trim() + "|" + End.Trim() + "|" + base64String;
        //    zInfo.Partner.PartnerNumber = PartnerNumber;
        //    zInfo.UpdateBill(Data);

        //    if (zInfo.Code == "200" ||
        //        zInfo.Code == "201")
        //    {
        //        ViewBag.Message = "Cập nhật thành công !.";
        //    }
        //    else
        //    {
        //        ViewBag.Message = "Lỗi " + zInfo.Message;
        //    }

        //    ViewBag.Data = Data;
        //    return RedirectToAction("A_InfoBill");
        //}

        //public ActionResult A_InfoStore()
        //{
        //    string PartnerNumber = UserLog.PartnerNumber;
        //    Partner_Info zInfo = new Partner_Info(PartnerNumber);

        //    return View("~/Views/Desktop/Config/StoreInfo.cshtml", zInfo.Partner);
        //}
        //#endregion







        // GET: Config   

        #region[-                  -]
        [HttpGet]
        public JsonResult CheckIDProduct(string ProductID)
        {
            ServerResult zResult = new ServerResult();
            Product_Info zInfo = new Product_Info(ProductID, true);
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Data = TN_Utils.QRWrite(ProductID, out string Message);
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetIDProduct()
        {
            string ID = Helper.AutoID_Product(UserLog.PartnerNumber, "SKU");
            return Json(ID, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CategoryLevel1()
        {
            ViewBag.ListProductCategory = Product_Category_Data.List(UserLog.PartnerNumber, out string Message, 1, false);
            return View("~/Views/Desktop/Config/CategoryLevel1.cshtml");
        }
        public ActionResult CategoryLevel2()
        {
            ViewBag.ListProductCategory = Product_Category_Data.List(UserLog.PartnerNumber, out string Message, 2, false);
            ViewBag.ListLevel1 = Product_Category_Data.List(UserLog.PartnerNumber, out string Message1, 1, false);
            return View("~/Views/Desktop/Config/CategoryLevel2.cshtml");
        }
        [HttpPost]
        public JsonResult SaveProductCategory(int CategoryKey, string CategoryNameVN, string CategoryNameEN, string CategoryNameCN, string Description, int Parent, int Rank, int Level)
        {
            ServerResult zResult = new ServerResult();
            Product_Category_Info zInfo = new Product_Category_Info(CategoryKey);

            zInfo.Product_Category.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Product_Category.CategoryKey = CategoryKey;
            zInfo.Product_Category.CategoryNameVN = CategoryNameVN.Trim();
            zInfo.Product_Category.CategoryNameEN = CategoryNameEN.Trim();
            zInfo.Product_Category.CategoryNameCN = CategoryNameCN.Trim();
            zInfo.Product_Category.Description = Description.Trim();
            zInfo.Product_Category.Parent = Parent.ToInt();
            zInfo.Product_Category.Rank = Rank.ToInt();
            zInfo.Product_Category.Level = Level.ToInt();

            if (zInfo.Product_Category.CategoryKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailProductCategory(int CategoryKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Category_Info zInfo = new Product_Category_Info(CategoryKey);
            Product_Category_Model zModel = zInfo.Product_Category;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteProductCategory(int CategoryKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Category_Info zInfo = new Product_Category_Info();
            zInfo.Product_Category.CategoryKey = CategoryKey;
            zInfo.Delete();
            //Product_Category_Model zModel = zInfo.Product_Category;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ListProduct()
        {
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, out string Message);
            ViewBag.ListProductCategory = Product_Data.ListCategory(UserLog.PartnerNumber, out string Message1);
            ViewBag.ListProductUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message2);
            return View("~/Views/Desktop/Config/Product.cshtml");
        }
        [HttpPost]
        public JsonResult SaveProduct()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zProductObj = Request.Form["ProductObj"];
                string zProductKey = Request.Form["ProductKey"];

                #region [Apply data to model]
                Product_Model zModel = JsonConvert.DeserializeObject<Product_Model>(zProductObj);
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;

                if (zModel.PhotoPath.Length > 0)
                {
                    string Message = "";
                    Helper.DeleteSingleFile(zModel.PhotoPath, out Message);
                }

                if (Request.Files.Count > 0)
                {
                    string zFilePath = Helper.UploadPath + "/Product/";
                    HttpPostedFileBase files = Request.Files[0];
                    bool zUploadResult = Helper.UploadSingleFile(files, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zModel.PhotoPath = zFilePath;
                    }
                }
                #endregion

                #region [Save data to db]
                Product_Info zInfo = new Product_Info(zProductKey);
                zInfo.Product = zModel;

                if (zInfo.Product.ProductKey == "")
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }
                #endregion

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult DetailProduct(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Info zInfo = new Product_Info(ProductKey);
            Product_Model zModel = zInfo.Product;

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteProduct(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Info zInfo = new Product_Info();
            zInfo.Product.ProductKey = ProductKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ListProductUnit()
        {
            ViewBag.ListProductUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Desktop/Config/Unit.cshtml");
        }
        [HttpPost]
        public JsonResult SaveProductUnit(int UnitKey, string UnitID, string UnitnameVN, int Rank, string Description)
        {
            ServerResult zResult = new ServerResult();
            Product_Unit_Info zInfo = new Product_Unit_Info(UnitKey);

            zInfo.Product_Unit.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Product_Unit.UnitKey = UnitKey.ToInt();
            zInfo.Product_Unit.UnitID = UnitID.Trim();
            zInfo.Product_Unit.UnitnameVN = UnitnameVN.Trim();
            zInfo.Product_Unit.Description = Description.Trim();
            zInfo.Product_Unit.Rank = Rank.ToInt();
            if (zInfo.Product_Unit.UnitKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailProductUnit(int UnitKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Unit_Info zInfo = new Product_Unit_Info(UnitKey);
            Product_Unit_Model zModel = zInfo.Product_Unit;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteProductUnit(int UnitKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Unit_Info zInfo = new Product_Unit_Info();
            zInfo.Product_Unit.UnitKey = UnitKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [   Position    ]
        public ActionResult ListPosition()
        {
            ViewBag.ListPosition = Position_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Desktop/Config/ListPosition.cshtml");
        }

        [HttpPost]
        public JsonResult SavePosition(int PositionKey, string PositionID, string PositionNameVN, string PositionNameEN, string PositionNameCN, string Description, int TypeKey, string TypeName, int Rank)
        {
            ServerResult zResult = new ServerResult();
            Position_Info zInfo = new Position_Info(PositionKey);

            zInfo.Position.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Position.PositionKey = PositionKey;
            zInfo.Position.PositionID = PositionID;
            zInfo.Position.PositionNameVN = PositionNameVN.Trim();
            zInfo.Position.PositionNameEN = PositionNameEN.Trim();
            zInfo.Position.PositionNameCN = PositionNameCN.Trim();
            zInfo.Position.Description = Description.Trim();
            zInfo.Position.TypeKey = TypeKey.ToInt();
            zInfo.Position.TypeName = TypeName.Trim();
            zInfo.Position.Rank = Rank.ToInt();
            if (zInfo.Position.PositionKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailPosition(int PositionKey)
        {
            ServerResult zResult = new ServerResult();
            Position_Info zInfo = new Position_Info(PositionKey);
            Position_Model zModel = zInfo.Position;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeletePosition(int PositionKey)
        {
            ServerResult zResult = new ServerResult();
            Position_Info zInfo = new Position_Info();
            zInfo.Position.PositionKey = PositionKey;
            zInfo.Delete();
            //Product_Category_Model zModel = zInfo.Product_Category;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region [ Department ]
        public ActionResult ListDepartment()
        {
            ViewBag.ListDepartment = Department_Data.List(UserLog.PartnerNumber, out string Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber, out string Message1);
            return View("~/Views/Desktop/Config/ListDepartment.cshtml");
        }

        [HttpPost]
        public JsonResult SaveDepartment(string DepartmentKey, string DepartmentName, string DepartmentID, string Address, int Rank, string BranchKey, string BranchName, string Description)
        {
            ServerResult zResult = new ServerResult();
            Department_Info zInfo = new Department_Info(DepartmentKey);

            zInfo.Department.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Department.DepartmentKey = DepartmentKey.Trim();
            zInfo.Department.DepartmentName = DepartmentName.Trim();
            zInfo.Department.DepartmentID = DepartmentID.Trim();
            zInfo.Department.Address = Address.Trim();
            zInfo.Department.Description = Description.Trim();
            zInfo.Department.Rank = Rank.ToInt();
            zInfo.Department.BranchKey = BranchKey.Trim();
            zInfo.Department.BranchName = BranchName.Trim();
            zInfo.Department.Description = Description.Trim();
            if (zInfo.Department.DepartmentKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailDepartment(string DepartmentKey)
        {
            ServerResult zResult = new ServerResult();
            Department_Info zInfo = new Department_Info(DepartmentKey);
            Department_Model zModel = zInfo.Department;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteDepartment(string DepartmentKey)
        {
            ServerResult zResult = new ServerResult();
            Department_Info zInfo = new Department_Info();
            zInfo.Department.DepartmentKey = DepartmentKey;
            zInfo.Delete();
            //Product_Category_Model zModel = zInfo.Product_Category;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public ActionResult ChangePass(string OldPass = "", string NewPass = "", string RePass = "")
        {
            if (Session["User_Model"] != null
                && OldPass != string.Empty
                && NewPass != string.Empty
                && RePass != string.Empty)
            {
                User_Model zUser = Session["User_Model"] as User_Model;
                string UserKey = zUser.UserKey;

                User_Info zInfo = new User_Info(UserKey);
                if (zInfo.User.Password == TN_Utils.HashPass(OldPass))
                {
                    if (NewPass == RePass)
                    {
                        zInfo.ResetPass(UserKey, TN_Utils.HashPass(RePass));
                        ViewBag.Message = "Đã đổi mật khẩu thành công !.";
                    }
                    else
                    {
                        ViewBag.Message = "Mật khẩu mới chưa giống nhau vui lòng nhập lại !.";
                    }
                }
                else
                {
                    ViewBag.Message = "Mật khẩu cũ không đúng vui lòng nhập lại !.";
                }
            }

            return View("~/Views/Desktop/Config/ChangePass.cshtml");
        }

        public ActionResult InfoStore()
        {
            User_Model zUser = Session["User_Model"] as User_Model;
            string PartnerNumber = zUser.PartnerNumber;
            Partner_Info zInfo = new Partner_Info(PartnerNumber);
            
            return View("~/Views/Desktop/Config/StoreInfo.cshtml", zInfo.Partner);
        }

        public ActionResult UpdateStore(HttpPostedFileBase[] files, string PartnerName = "", string PartnerPhone = "", string PartnerAddress = "", string StoreName = "")
        {
            User_Model zUser = Session["User_Model"] as User_Model;
            string PartnerNumber = zUser.PartnerNumber;
            Partner_Info zInfo = new Partner_Info(PartnerNumber);
            string base64String = "";
            if (PartnerName != string.Empty &&
                PartnerPhone != string.Empty &&
                PartnerAddress != string.Empty &&
                StoreName != string.Empty)
            {
                Partner_Model zModel = new Partner_Model();
                zModel.PartnerName = PartnerName;
                zModel.PartnerPhone = PartnerPhone;
                zModel.PartnerAddress = PartnerAddress;
                zModel.PartnerNumber = PartnerNumber;
                zModel.StoreName = StoreName;

                if (files.Length > 0)
                {
                    if (files[0] != null)
                    {
                        Stream fs = files[0].InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        zModel.Logo_Large = "data:image/png;base64," + base64String;
                    }
                }

                if (base64String == string.Empty &&
                    zInfo.Partner.Logo_Large.Length > 0)
                {
                    base64String = zInfo.Partner.Logo_Large;
                }

                zInfo.Partner = zModel;
                zInfo.UpdateManual();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    ViewBag.Message = "Cập nhật thành công !.";
                }
                else
                {
                    ViewBag.Message = "Lỗi " + zInfo.Message;
                }
            }

            return RedirectToAction("InfoStore"); //View("~/Views/Desktop/Config/StoreInfo.cshtml", zInfo.Partner);
        }

        public ActionResult InfoBill()
        {
            User_Model zUser = Session["User_Model"] as User_Model;
            string PartnerNumber = zUser.PartnerNumber;
            Partner_Info zInfo = new Partner_Info(PartnerNumber);
            string Data = zInfo.Partner.BillConfig;
            ViewBag.Data = Data;
            return View("~/Views/Desktop/Config/BillInfo.cshtml", zInfo.Partner);
        }
        public ActionResult UpdateBill(HttpPostedFileBase[] files, string Title = "", string Description = "", string End = "")
        {
            User_Model zUser = Session["User_Model"] as User_Model;
            string PartnerNumber = zUser.PartnerNumber;
            Partner_Info zInfo = new Partner_Info(PartnerNumber);
            string Data = zInfo.Partner.BillConfig;
            string base64String = "";
            if (Title != string.Empty ||
                Description != string.Empty ||
                End != string.Empty)
            {

                if (files.Length > 0)
                {
                    if (files[0] != null)
                    {
                        Stream fs = files[0].InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        base64String = "data:image/png;base64," + base64String;
                    }
                }
            }

            if (base64String == string.Empty &&
                zInfo.Partner.Logo_Large.Length > 0)
            {
                base64String = zInfo.Partner.Logo_Large;
            }

            Data = Title.Trim() + "|" + Description.Trim() + "|" + End.Trim() + "|" + base64String;
            zInfo.Partner.PartnerNumber = PartnerNumber;
            zInfo.UpdateBill(Data);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                ViewBag.Message = "Cập nhật thành công !.";
            }
            else
            {
                ViewBag.Message = "Lỗi " + zInfo.Message;
            }

            ViewBag.Data = Data;
            return RedirectToAction("InfoBill"); //View("~/Views/Desktop/Config/BillInfo.cshtml");
        }

        [HttpPost]
        public JsonResult SaveCategory(int CategoryKey, string CategoryName, string Type)
        {
            ServerResult zResult = new ServerResult();
            if (Type == "Payment")
            {
                CashPayment_Category_Info zInfo = new CashPayment_Category_Info();
                CashPayment_Category_Model zModel = new CashPayment_Category_Model();
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CategoryNameVN = CategoryName;
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;
                zInfo.CashPayment_Category = zModel;
                if (CategoryKey > 0)
                {
                    zInfo.CashPayment_Category.CategoryKey = CategoryKey;
                    zInfo.CashPayment_Category.RecordStatus = 1;
                    zInfo.Update();
                }
                else
                {
                    zInfo.CashPayment_Category.RecordStatus = -1;
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            if (Type == "Receipt")
            {
                CashReceipt_Category_Info zInfo = new CashReceipt_Category_Info();
                CashReceipt_Category_Model zModel = new CashReceipt_Category_Model();
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CategoryNameVN = CategoryName;
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.ModifiedBy;
                zModel.ModifiedName = UserLog.EmployeeName;
                zInfo.CashReceipt_Category = zModel;
                if (CategoryKey > 0)
                {
                    zInfo.CashReceipt_Category.CategoryKey = CategoryKey;
                    zInfo.CashReceipt_Category.RecordStatus = -1;
                    zInfo.Update();
                }
                else
                {
                    zInfo.CashReceipt_Category.RecordStatus = -1;
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteCategory(int CategoryKey, string Type)
        {
            ServerResult zResult = new ServerResult();
            if (Type == "Payment")
            {
                CashPayment_Category_Info zInfo = new CashPayment_Category_Info(CategoryKey);
                zInfo.CashPayment_Category.PartnerNumber = UserLog.PartnerNumber;
                zInfo.CashPayment_Category.CreatedBy = UserLog.UserKey;
                zInfo.CashPayment_Category.CreatedName = UserLog.EmployeeName;
                zInfo.CashPayment_Category.ModifiedBy = UserLog.ModifiedBy;
                zInfo.CashPayment_Category.ModifiedName = UserLog.EmployeeName;
                zInfo.Delete();
                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            if (Type == "Receipt")
            {
                CashReceipt_Category_Info zInfo = new CashReceipt_Category_Info(CategoryKey);
                zInfo.CashReceipt_Category.PartnerNumber = UserLog.PartnerNumber;
                zInfo.CashReceipt_Category.CreatedBy = UserLog.UserKey;
                zInfo.CashReceipt_Category.CreatedName = UserLog.EmployeeName;
                zInfo.CashReceipt_Category.ModifiedBy = UserLog.ModifiedBy;
                zInfo.CashReceipt_Category.ModifiedName = UserLog.EmployeeName;
                zInfo.Delete();
                if (zInfo.Code == "200" || zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Data = string.Empty;
                    zResult.Message = string.Empty;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Data = string.Empty;
                    zResult.Message = zInfo.Message;
                }
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListCategoryReciept()
        {
            ViewBag.List = CashReceipt_Category_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Desktop/Config/CategoryReceipt.cshtml");
        }

        public ActionResult ListCategoryPayment()
        {
            ViewBag.List = CashPayment_Category_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Desktop/Config/CategoryPayment.cshtml");
        }


        #region [Quy đổi đơn vị tính]
        public ActionResult UnitConvert()
        {
            ViewBag.ListUnit = Product_Unit_Data.List(UserLog.PartnerNumber, out string Message);
            ViewBag.ListUnitConvert = Unit_Converted_Data.List(UserLog.PartnerNumber, out Message);
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, out Message);                       

            return View("~/Views/Desktop/Config/UnitConvert.cshtml");
        }

        [HttpGet]
        public JsonResult UnitDetail(int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Unit_Converted_Info zInfo = new Unit_Converted_Info(AutoKey);
            if (zInfo.Unit_Converted.Code == "200")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Unit_Converted);
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Unit_Converted.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UnitSave(int AutoKey = 0, string ProductKey = "", string ProductName = "",
            int StandardUnitKey = 0, string StandardUnitName = "",
            int UnitKey = 0, string UnitName = "", float Rate = 0)
        {
            ServerResult zResult = new ServerResult();
            Unit_Converted_Info zInfo = new Unit_Converted_Info();
            Unit_Converted_Model zModel = new Unit_Converted_Model();

            zModel.AutoKey = AutoKey;
            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.ProductKey = ProductKey;
            zModel.ProductName = ProductName;
            zModel.UnitKey = UnitKey;
            zModel.UnitName = UnitName;
            zModel.StandardUnitKey = StandardUnitKey;
            zModel.StandardUnitName = StandardUnitName;
            zModel.Rate = Rate;

            if (AutoKey > 0)
            {
                zInfo.Unit_Converted = zModel;
                zInfo.Update();
            }
            else
            {
                zInfo.Unit_Converted = zModel;
                zInfo.Create_ServerKey();
            }

            if (zInfo.Unit_Converted.Code == "200" ||
                zInfo.Unit_Converted.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Unit_Converted.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UnitDelete(int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Unit_Converted_Info zInfo = new Unit_Converted_Info();
            zInfo.Unit_Converted.AutoKey = AutoKey;
            zInfo.Delete();
            if (zInfo.Unit_Converted.Code == "200")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Unit_Converted.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}