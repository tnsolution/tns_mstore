﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.Mvc;

namespace TNStores.Controllers
{
    public class AdminController : BaseController
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        #region [Roles]
        public ActionResult ListRole(string Module = "")
        {
            string Message = "";
            ViewBag.ListModule = Authen.GetModule(out Message);
            ViewBag.ListRole = Authen.Recursive_Role(Module);
            ViewBag.SelectModule = Module;
            return View("~/Views/Desktop/Admin/Role/List.cshtml");
        }

        public ActionResult EditRole(string RoleKey = "", string Module = "")
        {
            string Message = "";
            ViewBag.ListModule = Authen.GetModule(out Message);
            ViewBag.ListRole = Authen.Recursive_Role(Module);

            Role_Info zInfo = new Role_Info(RoleKey);
            return PartialView("~/Views/Desktop/Admin/Role/Edit.cshtml", zInfo.Role);
        }

        public ActionResult UpdateRole(
            string RoleKey = "", string Module = "", string Level = "0", string RoleName = "",
            string Parent = "0", string Description = "", string RouteName = "", string Icon = "",
            string RoleID = "", string ControllerName = "", string ActionName = "", string ParamName = "", int Rank = 0)
        {
            string Message = "";

            #region [--Save Data--]
            Role_Info zInfo = new Role_Info();
            zInfo.Role.RoleKey = RoleKey;
            zInfo.Role.Level = Level.ToInt();
            zInfo.Role.RoleName = RoleName;
            zInfo.Role.RouteName = RouteName;
            zInfo.Role.Parent = Parent;
            zInfo.Role.Icon = Icon;
            zInfo.Role.RoleID = RoleID;
            zInfo.Role.Rank = Rank;
            zInfo.Role.ControllerName = ControllerName;
            zInfo.Role.ParamName = ParamName;
            zInfo.Role.ActionName = ActionName;
            zInfo.Role.Module = Module;
            zInfo.Role.Description = Description;

            if (RoleKey.Length >= 36)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ServerKey();
            }

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                Message += zInfo.Message;
            }
            #endregion

            ViewBag.ListModule = Authen.GetModule(out Message);
            ViewBag.ListRole = Authen.Recursive_Role(Module);
            return RedirectToAction("ListRole", new { Module });
        }

        public JsonResult DeleteRole(string RoleKey = "")
        {
            ServerResult zResult = new ServerResult();
            if (RoleKey.Length > 0)
            {
                Role_Info zInfo = new Role_Info(RoleKey);
                zInfo.Delete();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Message = "Đã xóa thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Message.GetFirstLine();
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không tìm thấy thông tin !.";
                zResult.Success = false;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region [Partner]
        public ActionResult PartialRoleView(string Type, string Module)
        {
            string Message = "";
            ViewBag.ListModule = Authen.GetModule(out Message);
            ViewBag.Module = Module;

            if (Type == "MOBILE")
            {
                return View("~/Views/Desktop/Admin/Partner/RoleMobile.cshtml");
            }
            else
            {
                return View("~/Views/Desktop/Admin/Partner/RoleDesktop.cshtml");
            }
        }
        public ActionResult SaveDesktop(string PartnerNumber, string PartnerRoleDesktop, string Module)
        {
            string CustomQuery =
                "UPDATE SYS_Partner SET ModuleRole = N'" + Module + "' WHERE PartnerNumber = '" + PartnerNumber + "' " + Environment.NewLine +
                "DELETE SYS_Role_Partner WHERE PartnerNumber = '" + PartnerNumber + "' AND Module = N'" + Module + "'" + Environment.NewLine;
            #region [get string role desktop]
            if (PartnerRoleDesktop.Length > 0 &&
                PartnerRoleDesktop.Contains(","))
            {
                string[] temp = PartnerRoleDesktop.Split(',');
                foreach (string s in temp)
                {
                    CustomQuery += "INSERT INTO SYS_Role_Partner (RoleKey, PartnerNumber, Module) VALUES ('" + s.Trim() + "','" + PartnerNumber + "', N'" + Module + "')" + Environment.NewLine;
                }
            }
            #endregion

            if (CustomQuery != string.Empty)
            {
                string Message = "";
                Authen.ApplySQL(CustomQuery, out Message);

                if (Message == string.Empty)
                {
                    Partner_Info zPart = new Partner_Info(PartnerNumber);
                    string[] temp = PartnerRoleDesktop.Split(',');
                    //lưu quyền cho menu
                    string UserKey = new User_Info(zPart.Partner.PartnerPhone, zPart.Partner.PartnerPhone, PartnerNumber).User.UserKey;
                    if (UserKey != string.Empty)
                    {
                        string zSQL = @" DELETE SYS_User_Role WHERE UserKey = '" + UserKey + "' AND Module = N'" + Module + "'";
                        foreach (string s in temp)
                        {
                            zSQL += @"
INSERT INTO SYS_User_Role 
(UserKey, RoleKey, RoleAll, RoleAdd, RoleRead, RoleEdit, RoleDel, Module, RecordStatus, CreatedBy, CreatedName, ModifiedBy, ModifiedName, PartnerNumber) 
VALUES 
('" + Guid.Parse(UserKey) + "', '" + Guid.Parse(s) + "', 'FALSE', 'FALSE', 'TRUE', 'FALSE', 'FALSE', N'" + Module + "', 1, '" + UserLog.UserKey + "', N'" + UserLog.EmployeeName + "', '" + UserLog.UserKey + "', N'" + UserLog.EmployeeName + "', '" + PartnerNumber + "') \r\n";
                        }

                        Authen.ApplySQL(zSQL, out Message);

                        if (Message == string.Empty)
                        {
                            return RedirectToAction("ListPartner");
                        }
                        else
                        {
                            return Content(Message);
                        }
                    }
                }
                else
                {
                    return Content(Message);
                }
            }

            return RedirectToAction("ListPartner");
        }
        public ActionResult SaveMobile(string PartnerNumber, string PartnerRoleMobile, string Module)
        {
            string CustomQuery =
            "UPDATE SYS_Partner SET ModuleRoleMobile = N'" + Module + "' WHERE PartnerNumber = '" + PartnerNumber + "' " + Environment.NewLine +
            "DELETE SYS_Role_Partner WHERE PartnerNumber = '" + PartnerNumber + "' AND Module = N'" + Module + "'" + Environment.NewLine;
            #region [get string role mobile]
            if (PartnerRoleMobile.Length > 0 &&
                PartnerRoleMobile.Contains(","))
            {
                string[] temp = PartnerRoleMobile.Split(',');
                foreach (string s in temp)
                {
                    CustomQuery += "INSERT INTO SYS_Role_Partner (RoleKey, PartnerNumber, Module) VALUES ('" + s.Trim() + "','" + PartnerNumber + "', N'" + Module + "')" + Environment.NewLine;
                }
            }
            #endregion

            if (CustomQuery != string.Empty)
            {
                string Message = "";
                Authen.ApplySQL(CustomQuery, out Message);

                if (Message == string.Empty)
                {
                    Partner_Info zPart = new Partner_Info(PartnerNumber);
                    string[] temp = PartnerRoleMobile.Split(',');
                    //lưu quyền cho menu
                    string UserKey = new User_Info(zPart.Partner.PartnerPhone, zPart.Partner.PartnerPhone, PartnerNumber).User.UserKey;
                    if (UserKey != string.Empty)
                    {
                        string zSQL = @" DELETE SYS_User_Role WHERE UserKey = '" + UserKey + "' AND Module = N'" + Module + "'";
                        foreach (string s in temp)
                        {
                            zSQL += @"
INSERT INTO SYS_User_Role 
(UserKey, RoleKey, RoleAll, RoleAdd, RoleRead, RoleEdit, RoleDel, Module, RecordStatus, CreatedBy, CreatedName, ModifiedBy, ModifiedName, PartnerNumber) 
VALUES 
('" + Guid.Parse(UserKey) + "', '" + Guid.Parse(s) + "', 'FALSE', 'FALSE', 'TRUE', 'FALSE', 'FALSE', N'" + Module + "', 1, '" + UserLog.UserKey + "', N'" + UserLog.EmployeeName + "', '" + UserLog.UserKey + "', N'" + UserLog.EmployeeName + "', '" + PartnerNumber + "') \r\n";
                        }

                        Authen.ApplySQL(zSQL, out Message);
                        if (Message == string.Empty)
                        {
                            return RedirectToAction("ListPartner");
                        }
                        else
                        {
                            return Content(Message);
                        }
                    }

                }
                else
                {
                    return Content(Message);
                }
            }

            return RedirectToAction("ListPartner");
        }

        public ActionResult ListPartner()
        {
            ViewBag.ListData = Partner_Data.List();
            return View("~/Views/Desktop/Admin/Partner/List.cshtml");
        }
        public ActionResult SearchPartner(string Name)
        {
            ViewBag.ListData = Partner_Data.Search(Name);
            return View("~/Views/Partner/List.cshtml");
        }

        public ActionResult SavePartner(
            string PartnerNumber, string PartnerName, string PartnerID,
            string PartnerPhone, string PartnerAddress,
            string Base64String = "", bool Activated = false,
            string StoreID = "", string StoreName = "",
            string UserName = "", string Password = "")
        {
            string Key = PartnerNumber;

            #region [Info]
            Partner_Model zModel = new Partner_Model();
            zModel.Logo_Large = Base64String;
            zModel.PartnerNumber = Key;
            zModel.PartnerID = PartnerID;
            zModel.PartnerName = PartnerName;
            zModel.PartnerAddress = PartnerAddress;
            zModel.PartnerPhone = PartnerPhone;

            zModel.Activated = Activated;
            if (Activated)
            {
                zModel.ActivationDate = DateTime.Now;
            }

            zModel.StoreID = StoreID;
            zModel.StoreName = StoreName;

            Partner_Info zInfo = new Partner_Info();
            if (Key.Length >= 36)
            {
                zInfo.Partner = zModel;
                zInfo.Update();
            }
            else
            {
                Key = Guid.NewGuid().ToString();
                zModel.PartnerNumber = Key;
                zInfo.Partner = zModel;
                zInfo.Create_ClientKey();
            }
            #endregion

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                //int zCount = User_Data.CheckUserName(UserName);
                //if (zCount == 0)
                //{
                //    #region [User]
                //    User_Info zUser = new User_Info();
                //    zUser.User.CreatedBy = UserLog.UserKey;
                //    zUser.User.CreatedName = UserLog.EmployeeName;
                //    zUser.User.ModifiedBy = UserLog.UserKey;
                //    zUser.User.ModifiedName = UserLog.EmployeeName;
                //    zUser.User.PartnerNumber = Key;
                //    zUser.User.Activate = true;
                //    zUser.User.ExpireDate = DateTime.Now.AddYears(1);
                //    zUser.User.UserName = UserName.Trim();
                //    zUser.User.Password = TN_Utils.HashPass(Password.Trim());
                //    zUser.Create_ServerKey();
                //    #endregion
                //}

                return RedirectToAction("ListPartner");
            }
            else
            {
                ViewBag.Message = zInfo.Message;
                return Content(zInfo.Message);
            }
        }
        public ActionResult EditPartner(string PartnerNumber = "")
        {
            #region [Get Record]
            Partner_Model zModel = new Partner_Model();
            Partner_Info zInfo = new Partner_Info();
            if (PartnerNumber.Length >= 36)
            {
                zInfo = new Partner_Info(PartnerNumber);
                zModel = zInfo.Partner;

                if (zInfo.Code != "200")
                {
                    ViewBag.Message = zInfo.Message;
                }
            }
            else
            {
                zModel.PartnerID = zInfo.AutoID();
            }
            #endregion

            string Message = "";
            ViewBag.ListModule = Authen.GetModule(out Message);
            ViewBag.Message += Message;

            User_Model zUserModel = new User_Info(zModel.PartnerPhone, true).User;
            zModel.UserName = zUserModel.UserName;
            zModel.Password = zUserModel.Password;
            return PartialView("~/Views/Desktop/Admin/Partner/Edit.cshtml", zModel);
        }
        public JsonResult DeletePartner(string PartnerNumber = "")
        {
            ServerResult zResult = new ServerResult();
            if (PartnerNumber.Length > 0)
            {
                Partner_Info zInfo = new Partner_Info(PartnerNumber);
                zInfo.Delete();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Message = "Đã xóa thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Message.GetFirstLine();
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không tìm thấy thông tin !.";
                zResult.Success = false;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Bộ quyền đã có của partner
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Module"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult RolePartner(string PartnerNumber, string Module)
        {
            IList<JsTree> nodes = new List<JsTree>();
            DataTable zTable = Authen.GetPartnerRole(PartnerNumber, Module, out string Message);
            foreach (DataRow r in zTable.Rows)
            {
                JsState jsS = new JsState();
                if (r["Access"].ToBool())
                {
                    jsS.selected = true;
                }
                else
                {
                    jsS.selected = false;
                }

                nodes.Add(new JsTree
                {
                    id = r["RoleKey"].ToString(),
                    parent = r["Parent"].ToString() == "0" ? "#" : r["Parent"].ToString(),
                    text = "<span style='font-weight:bold'>" + r["RoleName"].ToString() + "</span>: " + r["Description"].ToString(),
                    state = jsS
                });
            }

            string data = JsonConvert.SerializeObject(nodes);
            return Json(new { d = data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Bộ quyền của module
        /// </summary>
        /// <param name="Module"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult RoleModule(string Module)
        {
            IList<JsTree> nodes = new List<JsTree>();
            DataTable zTable = Authen.GetModuleRole(Module, out string Message);
            foreach (DataRow r in zTable.Rows)
            {
                JsState jsS = new JsState();
                if (r["Access"].ToBool() &&
                    r["RouteName"].ToString().Length > 0)
                {
                    jsS.selected = true;
                }
                else
                {
                    jsS.selected = false;
                }

                nodes.Add(new JsTree
                {
                    id = r["RoleKey"].ToString(),
                    parent = r["Parent"].ToString() == "0" ? "#" : r["Parent"].ToString(),
                    text = "<span style='font-weight:bold'>" + r["RoleName"].ToString() + "</span>: " + r["Description"].ToString(),
                    state = jsS
                });
            }

            string data = JsonConvert.SerializeObject(nodes);
            return Json(new { d = data }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Users]
        [HttpGet]
        public JsonResult CheckUser(string UserName)
        {
            ServerResult zResult = new ServerResult();
            int zCount = User_Data.CheckUserName(UserName);

            if (zCount > 0)
            {
                zResult.Success = false;
                zResult.Message = "Tên đăng nhập này đã được sử dụng vui lòng chọn tên khác !.";
            }
            else
            {
                zResult.Success = true;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ResetPass(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Model zUserLog = (User_Model)Session["User_Model"];
            string Random = TN_Utils.RandomPassword();
            User_Info zInfo = new User_Info();
            zInfo.User.ModifiedName = zUserLog.EmployeeName;
            zInfo.User.ModifiedBy = zUserLog.UserKey;
            zInfo.ResetPass(UserKey, TN_Utils.HashPass(Random));

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = Random;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + Random;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.User.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Activate(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Model zUserLog = (User_Model)Session["User_Model"];

            User_Info zInfo = new User_Info();
            zInfo.User.ModifiedName = zUserLog.EmployeeName;
            zInfo.User.ModifiedBy = zUserLog.UserKey;
            zInfo.SetActivate(UserKey);

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.User.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangePass(string OldPass, string NewPass)
        {
            ServerResult zResult = new ServerResult();
            User_Model zUserLog = (User_Model)Session["User_Model"];

            User_Info zInfo = new User_Info(UserLog.UserName, OldPass, Helper.PartnerNumber);
            if (zInfo.User.Code != "200")
            {
                zResult.Success = false;
                zResult.Message = "Tên mật khẩu không đúng vui lòng thử lại !.";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }

            zInfo.User.ModifiedName = zUserLog.EmployeeName;
            zInfo.User.ModifiedBy = zUserLog.UserKey;
            zInfo.ResetPass(zUserLog.UserKey, TN_Utils.HashPass(NewPass));

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = NewPass;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + NewPass;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.User.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveUser(
            string UserKey = "", string UserName = "", string Password = "", string lbl_Password = "",
            string EmployeeKey = "", string EmployeeName = "", string Description = "", string Activate = "",
            string ExpireDate = "", string PartnerNumber = "")
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info(UserKey);
            zInfo.User.PartnerNumber = PartnerNumber;
            zInfo.User.UserName = UserName.Trim();
            zInfo.User.EmployeeKey = EmployeeKey.Trim();
            zInfo.User.EmployeeName = EmployeeName.Trim();
            zInfo.User.Description = Description.Trim();

            if (lbl_Password.Trim() != Password.Trim())
            {
                zInfo.User.Password = TN_Utils.HashPass(Password.Trim());
            }

            DateTime zExpireDate = DateTime.Now.AddYears(1);
            if (ExpireDate != string.Empty)
            {
                DateTime.TryParseExact(ExpireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zExpireDate);
            }

            zInfo.User.ExpireDate = zExpireDate;
            if (Activate.ToInt() == 0)
            {
                zInfo.User.Activate = false;
            }
            else
            {
                zInfo.User.Activate = true;
            }

            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.EmployeeName;

            if (UserKey.Length < 36)
            {
                zInfo.User.UserKey = Guid.NewGuid().ToString();
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.User.Message.GetFirstLine();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailUser(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info(UserKey);

            User_Model zModel = zInfo.User;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.User.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteUser(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info();
            zInfo.Delete(UserKey);
            User_Model zModel = zInfo.User;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.User.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ListUser(string PartnerNumber = "")
        {
            ViewBag.ListPartner = Partner_Data.List();
            return View("~/Views/Desktop/Admin/User/List.cshtml");
        }
        public ActionResult SearchUser(string PartnerNumber = "", string UserName = "")
        {
            List<User_Model> zList = User_Data.Search(PartnerNumber, UserName);
            return PartialView("~/Views/Desktop/Admin/User/_tblData.cshtml", zList);
        }

        public ActionResult AuthRoleDesktop(string UserKey, string Module)
        {
            string Message = "";
            string PartnerNumber = new User_Info(UserKey).User.PartnerNumber;
            var zList = User_Data.ListUserRole(PartnerNumber, UserKey, Module, out Message);
            ViewBag.ListData = zList;
            return PartialView("~/Views/Desktop/Admin/User/RoleDesktop.cshtml");
        }
        public ActionResult AuthRoleMobile(string UserKey, string Module)
        {
            string Message = "";
            string PartnerNumber = new User_Info(UserKey).User.PartnerNumber;
            var zList = User_Data.ListUserRole(PartnerNumber, UserKey, Module, out Message);
            ViewBag.ListData = zList;
            return PartialView("~/Views/Desktop/Admin/User/RoleMobile.cshtml");
        }
        [HttpPost]
        public JsonResult AuthRoleDesktopSave(string UserKey, string Role, string Module)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info(UserKey);

            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.EmployeeName;

            if (Role.Length > 0)
            {
                List<User_Role> zRole = JsonConvert.DeserializeObject<List<User_Role>>(Role);

                zInfo.SetRoleAccess(zRole, UserKey, Module);
                if (zInfo.User.Code == "200" ||
                    zInfo.User.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.User.Message.GetFirstLine();
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AuthRoleMobileSave(string UserKey, string Role, string Module)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info(UserKey);

            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.EmployeeName;

            if (Role.Length > 0)
            {
                List<User_Role> zRole = JsonConvert.DeserializeObject<List<User_Role>>(Role);

                zInfo.SetRoleAccess(zRole, UserKey, Module);
                if (zInfo.User.Code == "200" ||
                    zInfo.User.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.User.Message.GetFirstLine();
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion                

        public ActionResult TableSample()
        {
            ViewBag.ListPartner = Partner_Data.List();
            return View("~/Views/Desktop/Admin/Data/TableSample.cshtml");
        }
        public ActionResult CopySample(string SourcePartner, string DestinationPartner)
        {
            if (SourcePartner == string.Empty &&
                DestinationPartner == string.Empty)
            {
                ViewBag.Message = "Bạn phải chọn dữ liệu nguồn và dữ liệu đích !.";
            }
            else
            {
                string SQL = @"
INSERT INTO PDT_Product_Unit 
(UnitnameVN, PartnerNumber, RecordStatus) 
SELECT UnitnameVN, '" + DestinationPartner + "', 0 FROM PDT_Product_Unit WHERE RecordStatus <> 99 AND PartnerNumber = '" + SourcePartner + "'";

                SQL += @"INSERT INTO PDT_Product_Category 
(CategoryNameVN, PartnerNumber, RecordStatus) 
SELECT CategoryNameVN, '" + DestinationPartner + "', 0 FROM PDT_Product_Category WHERE PartnerNumber = '" + SourcePartner + "'";

                SQL += @"
INSERT INTO PDT_Product 
(ProductID, ProductName, StandardCost, SalePrice, PartnerNumber, RecordStatus) 
SELECT ProductID, ProductName, StandardCost, SalePrice,'" + DestinationPartner + "', 0 FROM PDT_Product WHERE RecordStatus <> 99 AND PartnerNumber = '" + SourcePartner + "'";

                SQL += @"
INSERT INTO FNC_CashPayment_Category 
(CategoryNameVN, PartnerNumber, RecordStatus) 
SELECT CategoryNameVN, '" + DestinationPartner + "', 0 FROM FNC_CashPayment_Category WHERE RecordStatus <> 99 AND PartnerNumber = '" + SourcePartner + "'";

                SQL += @"
INSERT INTO FNC_CashReceipt_Category 
(CategoryNameVN, PartnerNumber, RecordStatus) 
SELECT CategoryNameVN, '" + DestinationPartner + "', 0 FROM FNC_CashReceipt_Category WHERE RecordStatus <> 99 AND PartnerNumber = '" + SourcePartner + "'";

                Helper.RunSQL(SQL, out string message);

                if (message != string.Empty)
                {
                    ViewBag.Message = message;
                }
                else
                {
                    ViewBag.Message = "Copy dữ liệu thành công các bảng sau [đơn vị tính, loại sản phẩm, sản phẩm, danh mục thu, chi] !.";
                }
            }
            return View("~/Views/Desktop/Admin/Data/TableSample.cshtml");
        }
    }
}