﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TNStores.Controllers
{
    public class BaseController : Controller
    {
        public static User_Model UserLog = new User_Model();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.IsChildAction)
            {
                return;
            }

            bool NotAuthen = false;
            if (Session["User_Model"] == null)
            {
                NotAuthen = true;
            }
            else
            {
                UserLog = Session["User_Model"] as User_Model;
            }

            if (NotAuthen)
            {
                string url = Url.RouteUrl("dangnhap"); //("SignIn", "Auth");
                filterContext.Result = new RedirectResult(url);
                return;
            }

            ActionDescriptor actionDescriptor = filterContext.ActionDescriptor;
            string actionName = actionDescriptor.ActionName;
            string controllerName = actionDescriptor.ControllerDescriptor.ControllerName;

            var ListRole = UserLog.ListRole;
            var AccessRole = ListRole.SingleOrDefault(s => s.ActionName == actionName && s.ControllerName ==controllerName);
            if (AccessRole != null &&
                AccessRole.RoleRead)
            {
                //cho phép thực hiện
            }
            else
            {
                //không cho phép thực hiện
            }




            ViewBag.Count1 = 1;
            ViewBag.Message1 = "Gần đến hạn !.";
        }

        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            if (Session["User_Model"] == null)
            {
                return new JsonResult { Data = "Error" };
            }
            else
            {
                return new JsonResult { Data = "Success" };
            }            
        }
    }
}